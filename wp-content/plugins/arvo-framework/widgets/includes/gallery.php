<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/29/2016
 * Time: 2:42 PM
 */
class G5Plus_Widget_Gallery extends  G5Plus_Widget {
	public function __construct() {
		$this->widget_cssclass    = 'widget-gallery';
		$this->widget_id          = 'g5plus-gallery';
		$this->widget_name        = esc_html__( 'G5Plus: Gallery', 'g5plus-arvo' );
		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => '',
				'label' => esc_html__( 'Title', 'g5plus-arvo' )
			),
			'hover_effect' => array(
				'label'   => esc_html__('Hover Effect', 'g5plus-arvo'),
				'type'    => 'select',
				'std'     => '',
				'options' => array(
					'default-effect' => esc_html__('Default', 'g5plus-arvo'),
					'suprema-effect' => esc_html__('Suprema', 'g5plus-arvo'),
					'layla-effect'   => esc_html__('Layla', 'g5plus-arvo'),
					'bubba-effect'   => esc_html__('Bubba', 'g5plus-arvo'),
					'jazz-effect'    => esc_html__('Jazz', 'g5plus-arvo'),
				)
			),
			'images' => array(
				'type'  => 'images',
				'std'   => '',
				'label' => esc_html__( 'Images', 'g5plus-arvo' )
			),
			'columns' => array(
				'type' => 'select',
				'label' => esc_html__('Columns', 'g5plus-arvo'),
				'options' => array('2' => '2' , '3' => '3','4' => '4', '5'=> '5', '6' => '6'),
				'std' => 4
			)
		);
		parent::__construct();
	}

	function widget($args, $instance) {
		extract( $args, EXTR_SKIP );
		$title = (!empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$hover_effect = (!empty($instance['hover_effect'])) ? $instance['hover_effect'] : 'default-effect';
		$images = (!empty( $instance['images'] ) ) ? $instance['images'] : '';
		$columns = (!empty( $instance['columns'] ) ) ? $instance['columns'] : '4';

		$wrapper_classes = array(
			'g5plus-gallery',
			'g5plus-widget-gallery',
			'clearfix',
			'text-center',
			'col-gap-3'
		);
		$wrapper_classes[] = 'row';
		$wrapper_classes[] = 'columns-'.$columns;
		$wrapper_classes[] = 'columns-md-'.$columns;
		$wrapper_classes[] = 'columns-sm-'.$columns;
		$wrapper_classes[] = 'columns-xs-'.$columns;
		$wrapper_classes[] = 'columns-mb-'.$columns;

		$gallery_item_class = array('gf-gallery-item');
		$gallery_item_class[] = 'gf-item-wrap';

		echo wp_kses_post($args['before_widget']);
		if ($title) {
			echo wp_kses_post($args['before_title'] . $title . $args['after_title']);
		}
		$gallery_id = rand();
		?>
		<div class="<?php echo join( ' ', $wrapper_classes ); ?>">
			<?php if ($images):
				$images = explode(',',$images);
				foreach ($images as $image) {?>
					<div class="<?php echo join(' ', $gallery_item_class); ?>">
						<div class="gf-gallery-inner <?php echo esc_attr( $hover_effect ) ?>">
							<?php
							$thumbnail = wp_get_attachment_image_src($image, 'thumbnail');
							$image_full = wp_get_attachment_image_src($image, 'full');
							if ($thumbnail && is_array($thumbnail)) { ?>
								<a href="<?php echo esc_url($image_full[0]) ?>"
								   data-thumb-src="<?php echo esc_url( $thumbnail[0] ); ?>" data-rel="lightGallery"
								   data-gallery-id="<?php echo esc_html( $gallery_id ) ?>">
									<i class="fa fa-search"></i>
								</a>
								<div class="effect-content">
									<img src="<?php echo esc_url($thumbnail[0]) ?>" alt="<?php the_title(); ?>"
									     title="<?php the_title(); ?>">
								</div>
							<?php }?>
						</div>
					</div>
				<?php }
			endif;?>
		</div>
		<?php
		echo wp_kses_post($args['after_widget']);
	}
}