<?php
class G5Plus_Widget_Banner extends  G5Plus_Widget {
    public function __construct() {
        $this->widget_cssclass    = 'widget-banner';
        $this->widget_id          = 'g5plus-banner';
        $this->widget_name        = esc_html__( 'G5Plus: Banner', 'g5plus-arvo' );
        $this->settings           = array(
            'title'  => array(
                'type'  => 'text',
                'std'   => '',
                'label' => esc_html__( 'Title', 'g5plus-arvo' )
            ),
            'image' => array(
                'type'  => 'image',
                'std'   => '',
                'label' => esc_html__( 'Image', 'g5plus-arvo' )
            ),
			'hover_effect' => array(
				'label'   => esc_html__('Hover Effect', 'g5plus-arvo'),
				'type'    => 'select',
				'std'     => '',
				'options' => array(
					''               => esc_html__('Normal', 'g5plus-arvo'),
					'suprema-effect' => esc_html__('Suprema', 'g5plus-arvo'),
					'layla-effect'   => esc_html__('Layla', 'g5plus-arvo'),
					'bubba-effect'   => esc_html__('Bubba', 'g5plus-arvo'),
					'jazz-effect'    => esc_html__('Jazz', 'g5plus-arvo'),
				)
			),
            'link' => array(
	            'type'  => 'text',
	            'std'   => '',
	            'label' => esc_html__( 'Link', 'g5plus-arvo' )
            )
        );
        parent::__construct();
    }
    function widget($args, $instance) {
        extract( $args, EXTR_SKIP );
        $title = (!empty( $instance['title'] ) ) ? $instance['title'] : '';
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        $image = (!empty( $instance['image'] ) ) ? $instance['image'] : '';
		$hover_effect = (!empty($instance['hover_effect'])) ? $instance['hover_effect'] : '';
		$link = (!empty($instance['link'])) ? $instance['link'] : '';

	    $image_src = wp_get_attachment_image_src($image, 'full');


	    echo wp_kses_post($args['before_widget']);
	    if ($title) {
		    echo wp_kses_post($args['before_title'] . $title . $args['after_title']);
	    }
	    ?>
	    <div class="g5plus-banner <?php echo esc_attr($hover_effect); ?>">
		    <?php if($image_src && is_array($image_src)): ?>
		        <?php if(!empty( $link )): ?>
				    <a class="bn-link" href="<?php echo esc_url($link); ?>"></a>
			    <?php endif; ?>
			    <div class="effect-content">
				    <img src="<?php echo esc_url($image_src[0]); ?>" alt="<?php echo esc_attr($title); ?>"/>
			    </div>
		    <?php endif; ?>
	    </div>
		<?php
	    echo wp_kses_post($args['after_widget']);
    }
}