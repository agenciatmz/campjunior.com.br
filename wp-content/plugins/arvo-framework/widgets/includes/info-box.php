<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 5/13/2016
 * Time: 8:35 AM
 */
class G5Plus_Widget_Info_Box extends G5Plus_Widget_Acf{
    public function __construct(){
        $this->widget_cssclass = 'widget-info-box';
        $this->widget_description = esc_html__("Display Info Box", 'g5plus-arvo');
        $this->widget_id = 'g5plus-info-box';
        $this->widget_name = esc_html__('G5Plus: Info Box', 'g5plus-arvo');
        $this->settings = array(
            'id' => 'g5plus_info_box',
            'type' => 'rows',
            'title' => esc_html__('Info Box', 'g5plus-arvo'),
            'subtitle' => esc_html__('Unlimited Info box with drag and drop sortings.', 'g5plus-arvo'),
            'fields' => array(
	            array(
		            'name' => 'text',
		            'title' => esc_html__('Text', 'g5plus-arvo'),
		            'type' => 'text',
		            'is_title_block' => 1
	            ),
                array(
                    'name' => 'icon',
                    'title' => esc_html__('Icon', 'g5plus-arvo'),
                    'type' => 'icon'
                ),
            ),
            'extra' => array(
                array(
                    'name' => 'title',
                    'title' => esc_html__('Title', 'g5plus-arvo'),
                    'type' => 'text',
                ),
	            array(
		            'name' => 'layout',
		            'title' => esc_html__('Layout', 'g5plus-arvo'),
		            'type' => 'select',
		            'options' => array(
			            'vertical' => esc_html__('Vertical', 'g5plus-arvo'),
			            'horizontal' => esc_html__('Horizontal', 'g5plus-arvo')
		            ),
		            'std' => 'vertical'
	            )
            )
        );
        parent::__construct();
    }
    function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );

        $extra = array_key_exists('extra', $instance) ? $instance['extra'] : array();
        $title = array_key_exists('title', $extra) ? $extra['title'] : '';
        $title = apply_filters('widget_title', $title, $instance, $this->id_base);
	    $layout = array_key_exists('layout', $extra) ? $extra['layout'] : '';
        $info_boxes = array_key_exists('fields',$instance) ? $instance['fields'] : array() ;

        echo wp_kses_post($args['before_widget']);
        if (!empty($title)) {
            echo wp_kses_post($args['before_title'] . $title . $args['after_title']);
        }
        ?>
        <ul class="if-<?php echo esc_attr($layout); ?>">
            <?php foreach ($info_boxes as $info_box) : ?>
                <li>
	                <i class="<?php echo esc_attr($info_box['icon']); ?>"></i>
	                <div class="if-content"><?php echo wp_kses_post($info_box['text']); ?></div>
                </li>
            <?php endforeach;?>
        </ul>
        <?php
        echo wp_kses_post($args['after_widget']);
    }
}