<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/18/2015
 * Time: 2:07 PM
 */
class G5Plus_Widget_Image extends  G5Plus_Widget {
    public function __construct() {
        $this->widget_cssclass    = 'widget-image';
        $this->widget_id          = 'g5plus-image';
        $this->widget_name        = esc_html__( 'G5Plus: Image', 'g5plus-arvo' );
        $this->settings           = array(
            'title'  => array(
                'type'  => 'text',
                'std'   => '',
                'label' => esc_html__( 'Title', 'g5plus-arvo' )
            ),
            'image' => array(
                'type'  => 'image',
                'std'   => '',
                'label' => esc_html__( 'Image', 'g5plus-arvo' )
            ),
            'link' => array(
	            'type'  => 'text',
	            'std'   => '',
	            'label' => esc_html__( 'Link', 'g5plus-arvo' )
            ),
            'alt' => array(
	            'type'  => 'text',
	            'std'   => '',
	            'label' => esc_html__( 'Alt', 'g5plus-arvo' )
            )
        );
        parent::__construct();
    }
    function widget($args, $instance) {
        extract( $args, EXTR_SKIP );
        $title = (!empty( $instance['title'] ) ) ? $instance['title'] : '';
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
        $image = (!empty( $instance['image'] ) ) ? $instance['image'] : '';
		$link = isset($instance['link']) ? $instance['link'] : '';
	    $alt = (!empty( $instance['alt'] ) ) ? $instance['alt'] : '';

	    $image_src = wp_get_attachment_image_src($image, 'full');

	    echo wp_kses_post($args['before_widget']);
	    if ($title) {
		    echo wp_kses_post($args['before_title'] . $title . $args['after_title']);
	    }
	    ?>
		<div class="g5plus-widget-image">
		    <?php if($image_src && is_array($image_src)): ?>
				<?php if (!empty($link)): ?>
					<a href="<?php echo esc_url($link); ?>"></a>
				<?php endif;?>
				<img src="<?php echo esc_url($image_src[0]); ?>" alt="<?php echo esc_attr($alt); ?>"/>
		    <?php endif;?>
		</div>
		<?php
	    echo wp_kses_post($args['after_widget']);
    }
}