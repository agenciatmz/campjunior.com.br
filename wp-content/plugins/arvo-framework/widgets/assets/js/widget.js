(function($){
	"use strict";
	var G5Plus_Widget = {
		init: function(){
			this.event();
			this.widget_select2();
		},
		event: function(){
			$(document).on('widget-added', G5Plus_Widget.widget_select2);
			$(document).on('widget-updated', G5Plus_Widget.widget_select2);
		},
		widget_select2: function(event, widget){
			if (typeof (widget) == "undefined") {
				$('#widgets-right select.widget-select2:not(.select2-ready)').each(function(){
					G5Plus_Widget.widget_select2_item(this);
				});
			}
			else {
				$('select.widget-select2:not(.select2-ready)', widget).each(function(){
					G5Plus_Widget.widget_select2_item(this);
				});
			}
		},
		widget_select2_item: function(target){
			$(target).addClass('select2-ready');
			$(target).select2({width : '100%'});
			var $multiple = $(target).attr('multiple');
			if (typeof($multiple) != 'undefined') {
				var data_value = $(target).attr('data-value').split(',');
				for (var i = 0; i < data_value.length; i++) {
					var $element = $(target).find('option[value="'+ data_value[i] +'"]');
					$element.detach();
					$(target).append($element);
				}
				$(target).val(data_value).trigger('change');
				$(target).on('select2:selecting',function(e){
					var ids = $('input',$(this).parent()).val();
					if (ids != "") {
						ids +=",";
					}
					ids += e.params.args.data.id;
					$('input',$(this).parent()).val(ids);
				}).on('select2:unselecting',function(e){
					var ids = $('input',$(this).parent()).val();
					var arr_ids = ids.split(",");
					var newIds = "";
					for(var i = 0 ; i < arr_ids.length; i++) {
						if (arr_ids[i] != e.params.args.data.id){
							if (newIds != "") {
								newIds +=",";
							}
							newIds += arr_ids[i];
						}
					}
					$('input',$(this).parent()).val(newIds);
				}).on('select2:select',function(e){
					var element = e.params.data.element;
					var $element = $(element);

					$element.detach();
					$(this).append($element);
					$(this).trigger("change");
				});
			}
		}
	};

	var G5Plus_Widget_Gallery = {
		_frame : null,
		init: function(){
			$(document).on('widget-added', G5Plus_Widget_Gallery.event);
			$(document).on('widget-updated', G5Plus_Widget_Gallery.event);
			this.event();
		},
		event : function() {
			$('.widget-media-field-wrap.gallery').on('click', '.button', function(e){
				e.preventDefault();
				G5Plus_Widget_Gallery.frame($(this)).open();
			});

			var $gallery = $('.widget-media-field.gallery');
			$gallery.on('update', function(){
				var ids = [];
				$(this).find('> span').each(function(){
					ids.push($(this).data('id'));
				});
				$(this).next('input[type="hidden"]').val(ids.join(','));
			});

			$gallery.sortable({
				placeholder: "widget-media-field-highlight",
				revert: 200,
				tolerance: 'pointer',
				stop: function () {
					$gallery.trigger('update');
				}
			});

			$gallery.on('click', 'span.close', function(e){
				$(this).parent().fadeOut(200, function(){
					$(this).remove();
					$gallery.trigger('update');
				});
			});
		},
		frame : function($button) {
			if (this._frame) {
				this._frame.clicked_button = $button;
				return this._frame;
			}

			this._frame = wp.media({
				title : g5plus_widget_gallery.media_title,
				library : {
					type : 'image'
				},
				button : {
					text : g5plus_widget_gallery.media_button
				},
				multiple : true
			});

			this._frame.clicked_button = $button;

			this._frame.state('library').on('select', this.select);
			return this._frame;
		},
		select : function() {
			var selection = this.get('selection'),
				$wrap = G5Plus_Widget_Gallery._frame.clicked_button.parent().parent(),
				$gallery = $('.widget-media-field',$wrap),
				$values = $('input[type="hidden"]',$wrap).val().split(',');
			selection.each(function(model) {
				var thumbnail = model.attributes.url;
				if( model.attributes.sizes !== undefined && (model.attributes.sizes.thumbnail !== undefined || model.attributes.sizes.full !== undefined)  && $values.indexOf(model.id.toString()) == -1 ) {
					if (model.attributes.sizes.thumbnail !== undefined) {
						thumbnail = model.attributes.sizes.thumbnail.url;
					} else if(model.attributes.sizes.full !== undefined) {
						thumbnail = model.attributes.sizes.full.url;
					}
					$gallery.append('<span data-id="' + model.id + '" title="' + model.attributes.title + '"><img src="' + thumbnail + '" alt="" /><span class="close">x</span></span>');
					$gallery.trigger('update');
				}
			});
		}
	};

	var G5Plus_Widget_Image =  {
		_frame : null,
		init: function(){
			$(document).on('widget-added', G5Plus_Widget_Image.event);
			$(document).on('widget-updated', G5Plus_Widget_Image.event);
			this.event();
		},
		event: function(){
			$('.widget-media-field-wrap.image').on('click', '.button', function(e){
				e.preventDefault();
				G5Plus_Widget_Image.frame($(this)).open();
			});

			var $gallery = $('.widget-media-field.image');
			$gallery.on('update', function(){
				var ids = [];
				$(this).find('> span').each(function(){
					ids.push($(this).data('id'));
				});
				$(this).next('input[type="hidden"]').val(ids.join(','));
			});

			$gallery.on('click', 'span.close', function(e){
				$(this).parent().fadeOut(200, function(){
					$(this).remove();
					$gallery.trigger('update');
				});
			});
		},
		frame : function($button) {
			if (this._frame) {
				this._frame.clicked_button = $button;
				return this._frame;
			}

			this._frame = wp.media({
				title : g5plus_widget_gallery.media_title,
				library : {
					type : 'image'
				},
				button : {
					text : g5plus_widget_gallery.media_button
				},
				multiple : false
			});

			this._frame.clicked_button = $button;

			this._frame.state('library').on('select', this.select);
			return this._frame;
		},
		select : function() {
			var selection = this.get('selection'),
				$wrap = G5Plus_Widget_Image._frame.clicked_button.parent().parent(),
				$gallery = $('.widget-media-field',$wrap),
				$values = $('input[type="hidden"]',$wrap).val();
			selection.each(function(model) {
				var thumbnail = model.attributes.url;
				if( model.attributes.sizes !== undefined && (model.attributes.sizes.thumbnail !== undefined || model.attributes.sizes.full !== undefined)  && $values !==  model.id.toString() ) {
					if (model.attributes.sizes.thumbnail !== undefined) {
						thumbnail = model.attributes.sizes.thumbnail.url;
					} else if(model.attributes.sizes.full !== undefined) {
						thumbnail = model.attributes.sizes.full.url;
					}
					$gallery.html('<span data-id="' + model.id + '" title="' + model.attributes.title + '"><img src="' + thumbnail + '" alt="" /><span class="close">x</span></span>');
					$gallery.trigger('update');
				}
			});
		}
	};

	$(document).ready(function(){
		G5Plus_Widget.init();
		G5Plus_Widget_Gallery.init();
		G5Plus_Widget_Image.init();
	});
})(jQuery);
