<?php
get_header();

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'arvo-archive-portfolio-css', plugins_url(GF_PLUGIN_NAME . '/shortcodes/portfolio/assets/css/single-portfolio.min.css'), array(), false);
}
$min_suffix_js = gf_get_option('enable_minifile_js',0) == 1 ? '.min' : '';
wp_enqueue_script(GF_PLUGIN_PREFIX . 'portfolio', plugins_url(GF_PLUGIN_NAME . '/shortcodes/portfolio/assets/js/portfolio' . $min_suffix_js . '.js'), false, true);
wp_localize_script(GF_PLUGIN_PREFIX . 'portfolio', 'g5plus_portfolio_meta', array(
	'ajax_url' => admin_url('admin-ajax.php?activate-multi=true')
));

// Start the loop
while ( have_posts() ) : the_post();
	// Include the page content template.
	gf_get_template('cpt/portfolio/templates/single/content-portfolio');
	// End of the loop.
endwhile;
get_footer();
