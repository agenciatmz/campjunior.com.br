<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 7/20/15
 * Time: 1:58 PM
 */
$column = 4;
$image_size = 'portfolio-size-sm';

$column_get = gf_get_option('portfolio_related_column');
$image_related = gf_get_option('custom_portfolio_related_image_size');
if (isset($column_get)) {
	$column = $column_get;
}
if (isset($image_related)) {
	$image_size = $image_related;
}
$post_not_in = get_the_ID();
?>
<div class="portfolio-related">
	<h4>
		<span><?php esc_html_e('RELATED PROJECTS', 'g5plus-arvo') ?></span></h4>
	<?php
	echo do_shortcode('[g5plus_portfolio layout_style="portfolio-slider" category="' . implode(',', $arrCatId) . '" items="-1" columns="' . $column . '" columns_gap="col-gap-30" image_size="' . $image_size . '" current_page="1" nav="true" nav_style="round" nav_position="top-68" dots="" post_not_in="' . $post_not_in . '"]'); ?>
</div>
