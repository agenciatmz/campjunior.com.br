<?php
/**
 * The template for displaying archive our-team pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */

get_header();

$custom_portfolio_layout_style = gf_get_option('custom_portfolio_layout_style');
if (!isset($custom_portfolio_layout_style)) $custom_portfolio_layout_style = 'portfolio-grid';

if($custom_portfolio_layout_style == 'portfolio-masonry-metro-style4') {
	$custom_portfolio_filter = $custom_portfolio_paging = $custom_portfolio_active_cate_color = '';
} else {
	$custom_portfolio_filter = gf_get_option('custom_portfolio_filter');
	$custom_portfolio_active_cate_color = gf_get_option( 'custom_portfolio_active_cate_color' );
	$custom_portfolio_paging = gf_get_option('custom_portfolio_paging');
	if (!isset($custom_portfolio_filter)) $custom_portfolio_filter = 1;
	$custom_portfolio_filter = $custom_portfolio_filter ? 'true' : 'false';
	if (!isset($custom_portfolio_active_cate_color)) $custom_portfolio_active_cate_color = 'active-cate-accent';
	if (!isset($custom_portfolio_paging)) $custom_portfolio_paging = '';
}
$custom_portfolio_hover_effect = gf_get_option('custom_portfolio_hover_effect');
$custom_portfolio_item_amount = gf_get_option( 'custom_portfolio_item_amount' );
$custom_portfolio_column = gf_get_option('custom_portfolio_column');
$custom_portfolio_image_size = gf_get_option('custom_portfolio_image_size');
$custom_portfolio_columns_gap = gf_get_option( 'custom_portfolio_columns_gap' );

if (!isset($custom_portfolio_hover_effect)) $custom_portfolio_hover_effect = 'default-effect';
if (!isset($custom_portfolio_item_amount)) $custom_portfolio_item_amount = 8;
if (!isset($custom_portfolio_column)) $custom_portfolio_column = 4;
if (!isset($custom_portfolio_image_size)) $custom_portfolio_image_size = 'portfolio-size-xxl';
if (!isset($custom_portfolio_columns_gap)) $custom_portfolio_columns_gap = 'col-gap-0';

$category = '';
$cat = get_queried_object();
if ($cat && property_exists($cat, 'term_id')) {
	$category = $cat->slug;
}

$shortCode = '[g5plus_portfolio layout_style="' . $custom_portfolio_layout_style . '" category=""
					 hover_effect="' . $custom_portfolio_hover_effect . '" is_filter="' . $custom_portfolio_filter . '"
					 active_cate_color="'.$custom_portfolio_active_cate_color.'"
					 show_paging="'.$custom_portfolio_paging.'" item_amount="'.$custom_portfolio_item_amount.'"
					 columns="' . $custom_portfolio_column . '" image_size="'.$custom_portfolio_image_size.'"
					 columns_gap="'.$custom_portfolio_columns_gap.'" current_page="1" archive_category="'.$category.'"]';
echo do_shortcode($shortCode);
get_footer();
