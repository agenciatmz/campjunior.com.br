<?php
// don't load directly
if (!defined('ABSPATH')) die('-1');

if (!defined('G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY'))
	define('G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY', 'portfolio-category');

if (!defined('G5PLUS_PORTFOLIO_POST_TYPE'))
	define('G5PLUS_PORTFOLIO_POST_TYPE', 'portfolio');

if (!defined('G5PLUS_PORTFOLIO_DIR_PATH'))
	define('G5PLUS_PORTFOLIO_DIR_PATH', plugin_dir_path(__FILE__));

if (!class_exists('GF_Portfolio')) {
	class GF_Portfolio
	{

		function __construct()
		{
			add_action('init', array($this, 'register_taxonomies'), 5);
			add_action('init', array($this, 'register_post_types'), 6);

			add_action('wp_ajax_nopriv_portfolio_load_more', array($this,'load_more_callback'));
			add_action('wp_ajax_portfolio_load_more',  array($this,'load_more_callback'));

			add_action('wp_ajax_nopriv_portfolio_filter_carousel', array($this,'filter_carousel_callback'));
			add_action('wp_ajax_portfolio_filter_carousel',  array($this,'filter_carousel_callback'));

			add_action('wp_ajax_nopriv_portfolio_load_gallery', array($this, 'load_gallery_callback'));
			add_action('wp_ajax_portfolio_load_gallery', array($this, 'load_gallery_callback'));

			add_filter('rwmb_meta_boxes', array($this, 'register_meta_boxes'));
			add_filter('g5plus_image_size',array($this,'register_image_size'));
			add_filter('g5plus_filter_layout_wrap_class',array($this,'get_layout_wrap_class'));
			add_filter('g5plus_filter_layout_inner_class',array($this,'get_layout_inner_class'));

			add_filter('single_template', array($this, 'get_single_template'));
			add_filter( 'archive_template', array($this,'get_archive_template'));
			add_filter('gf_post_type_preset', array($this,'post_type_preset_apply'));
			add_filter('gf_post_type_search',array($this,'post_type_search_apply'));
			add_filter('gf_custom_page_layout',array($this,'custom_page_layout_apply'));
			add_filter('gf_custom_page_title',array($this,'custom_page_layout_apply'));

			if (is_admin()) {
				add_filter('manage_edit-' . G5PLUS_PORTFOLIO_POST_TYPE . '_columns', array($this, 'add_columns'));
				add_action('manage_' . G5PLUS_PORTFOLIO_POST_TYPE . '_posts_custom_column', array($this, 'set_columns_value'), 10, 2);
				add_action('admin_menu', array($this, 'addMenuChangeSlug'));
			}
		}

		// Custom function
		function load_more_callback(){
			if (class_exists('WPBMap')) {
				WPBMap::addAllMappedShortcodes();
			}
			$layout_style = $_REQUEST['layout_style'];
			$data_source = $_REQUEST['data_source'];
			$names = $_REQUEST['names'];
			$is_filter = $_REQUEST['is_filter'];
			$columns_gap = $_REQUEST['columns_gap'];
			$category = $_REQUEST['category'];
			$current_page = $_REQUEST['current_page'];
			$columns = $_REQUEST['columns'];
			$item_per_page = $_REQUEST['item_per_page'];
			$image_size = $_REQUEST['image_size'];
			$hover_effect = $_REQUEST['hover_effect'];
			echo do_shortcode('[g5plus_portfolio layout_style="'.$layout_style.'" category="'.$category.'" show_category="" columns_gap="'.$columns_gap.'"
								columns="'.$columns.'" show_paging="load-more" item_amount='.$item_per_page.' is_filter="'.$is_filter.'"
								image_size="'.$image_size.'" current_page="'.$current_page.'" hover_effect="'.$hover_effect.'"
								data_source="'.$data_source.'" names="'.$names.'"]');
			die();
		}

		function filter_carousel_callback() {
			if (class_exists('WPBMap')) {
				WPBMap::addAllMappedShortcodes();
			}
			$category = str_replace('.','',$_REQUEST['category']);
			if($category =='*') {
				$category = '';
			}
			$data_source = $_REQUEST['data_source'];
			$names = $_REQUEST['names'];
			$item_amount           = $_REQUEST['item_amount'];
			$hover_effect      = $_REQUEST['hover_effect'];
			$image_size        = $_REQUEST['image_size'];
			$short_code        = '[g5plus_portfolio title="" layout_style="portfolio-slider" item_amount="'.$item_amount.'"
									category="' . $category . '" items="-1" columns="4" hover_effect="' . $hover_effect . '" 
									show_pagging="" image_size="' . $image_size . '" data_source="'.$data_source.'" names="'.$names.'"]';
			echo do_shortcode( $short_code );
			die();
		}

		function load_gallery_callback()
		{
			$post_id = $_REQUEST['post_id'];
			$meta_values = get_post_meta($post_id, 'portfolio-format-gallery', false);
			$galleries = array();
			$meta_values[] = get_post_thumbnail_id($post_id);
			if (is_array($meta_values) && count($meta_values) > 0) {
				$args_attachment = array(
					'orderby'        => 'post__in',
					'post__in'       => $meta_values,
					'post_type'      => 'attachment',
					'posts_per_page' => '-1',
					'post_status'    => 'inherit');
				$attachments = new WP_Query($args_attachment);
				global $post;
				while ($attachments->have_posts()) : $attachments->the_post();
					$attach_id = get_post_thumbnail_id();
					$image_thumb = wp_get_attachment_image_src($attach_id);
					$image_thumb_link = '';
					if (sizeof($image_thumb) > 0) {
						$image_thumb_link = $image_thumb['0'];
					}
					$galleries[] = array(
						'subHtml' => $post->post_title,
						'thumb' => $image_thumb_link,
						'src'     => $post->guid,
					);
				endwhile;
				wp_reset_postdata();
				echo json_encode($galleries);
			}
			wp_die();
		}

		public function post_type_preset_apply($post_types) {
			$post_types['portfolio'] =  array(
				'name' => esc_html__('Portfolio','g5plus-arvo'),
				'category' => G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY
			);
			return $post_types;
		}

		function post_type_search_apply($post_types){
			$post_types[G5PLUS_PORTFOLIO_POST_TYPE] = esc_html__('Portfolio','g5plus-arvo');
			return $post_types;
		}

		function custom_page_layout_apply($settings){
			$settings['portfolio'] = array(
				'title' => esc_html__('Archive Portfolio','g5plus-arvo'),
				'category' => 'portfolio-category',
				'is_archive' => true,
				'post_type' => 'portfolio'
			);


			$settings['single_portfolio'] = array(
				'title' => esc_html__('Single Portfolio','g5plus-arvo'),
				'is_single' => true,
				'post_type' => 'portfolio'
			);
			return $settings;
		}

		function register_image_size($image_sizes){
			$sizes = array(
				'portfolio-size-xs' => array(
					'width'  => 370,
					'height' => 370
				),
				'portfolio-size-sm' => array(
					'width'  => 480,
					'height' => 480
				),
				'portfolio-size-md' => array(
					'width'  => 585,
					'height' => 585
				),
				'portfolio-size-lg' => array(
					'width'  => 640,
					'height' => 427
				),
				'portfolio-size-xl' => array(
					'width'  => 640,
					'height' => 480
				),
				'portfolio-size-xxl' => array(
					'width'  => 640,
					'height' => 640
				),
				'portfolio-single-md' => array(
					'width'  => 1170,
					'height' => 570
				),
			);
			return array_merge($image_sizes,$sizes);
		}

		function get_layout_wrap_class($layout_wrap_class){
			$wrap_class = array();
			if (is_post_type_archive(G5PLUS_PORTFOLIO_POST_TYPE) || is_tax(G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY)) {
				$wrap_class[] = 'archive-portfolio-wrap';
			}
			return array_merge($layout_wrap_class,$wrap_class);
		}

		function get_layout_inner_class($layout_inner_class){
			$inner_class = array();
			if (is_post_type_archive(G5PLUS_PORTFOLIO_POST_TYPE) || is_tax(G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY)) {
				$inner_class[] = 'archive-portfolio-inner';
			}
			return array_merge($layout_inner_class,$inner_class);
		}

		function register_taxonomies()
		{
			if (taxonomy_exists(G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY)) {
				return;
			}

			$post_type = G5PLUS_PORTFOLIO_POST_TYPE;
			$taxonomy_slug = G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY;
			$taxonomy_name = 'Portfolio Categories';

			$post_type_slug = get_option('g5plus-cpt-' . $post_type . '-config');
			if (isset($post_type_slug) && is_array($post_type_slug) &&
			    array_key_exists('taxonomy_slug', $post_type_slug) && $post_type_slug['taxonomy_slug'] != ''
			) {
				$taxonomy_slug = $post_type_slug['taxonomy_slug'];
				$taxonomy_name = $post_type_slug['taxonomy_name'];
			}
			register_taxonomy(G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY, G5PLUS_PORTFOLIO_POST_TYPE,
				array('hierarchical' => true,
				      'label'        => $taxonomy_name,
				      'query_var'    => true,
				      'rewrite'      => array('slug' => $taxonomy_slug))
			);
			flush_rewrite_rules();
		}

		function get_single_template($single)
		{
			global $post;
			/* Checks for single template by post type */
			if ($post->post_type == G5PLUS_PORTFOLIO_POST_TYPE) {
				$plugin_path =  untrailingslashit( G5PLUS_PORTFOLIO_DIR_PATH );
				$single = $plugin_path . '/templates/single/single-portfolio.php';
			}
			return $single;
		}

		function get_archive_template($archive_template) {
			if (is_post_type_archive(G5PLUS_PORTFOLIO_POST_TYPE) || is_tax(G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY)) {
				$plugin_path =  untrailingslashit( G5PLUS_PORTFOLIO_DIR_PATH );
				$archive_template = $plugin_path . '/templates/archive/archive-portfolio.php';
			}
			return $archive_template;
		}

		function add_columns($columns)
		{
			unset(
				$columns['title'],
				$columns['date']
			);
			$cols = array_merge(array('cb' => ('')), $columns);
			$cols = array_merge($cols, array('title' => esc_html__('Name', 'g5plus-arvo')));
			$cols = array_merge($cols, array('thumbnail' => esc_html__('Thumbnail', 'g5plus-arvo')));
			$cols = array_merge($cols, array('date' => esc_html__('Date', 'g5plus-arvo')));
			return $cols;
		}

		function register_meta_boxes($meta_boxes)
		{
			$meta_boxes[] = array(
				'title' => esc_html__('Portfolio Extra', 'g5plus-arvo'),
				'id' => 'arvo-meta-box-portfolio-format-gallery',
				'pages' => array(G5PLUS_PORTFOLIO_POST_TYPE),
				'fields' => array(
					array(
						'name' => esc_html__('Established Date ', 'g5plus-arvo'),
						'id' => 'established-date',
						'type' => 'date'
					),
					array(
						'name' => esc_html__('Link To Detail', 'g5plus-arvo'),
						'id' => 'portfolio-link',
						'type' => 'text',
					),
					array(
						'name' => esc_html__('Open Single Page?', 'g5plus-arvo'),
						'id' => 'portfolio-open-single-page',
						'type' => 'checkbox',
						'required-field' => array('portfolio-link', '<>', ''),
						'std' => 1
					),
					array(
						'name' => esc_html__('Client', 'g5plus-arvo'),
						'id' => 'portfolio-client',
						'type' => 'text',
					),
					array(
						'name' => esc_html__('Client Site', 'g5plus-arvo'),
						'id' => 'portfolio-client-site',
						'type' => 'text',
					),
					array(
						'name' => esc_html__('Gallery', 'g5plus-arvo'),
						'id' => 'portfolio-format-gallery',
						'type' => 'image_advanced',
					),
					array(
						'name' => esc_html__('Video URL (oembed) or Embed Code', 'g5plus-arvo'),
						'id' => 'portfolio-video',
						'type' => 'textarea',
						'std'  => ''
					),
					array(
						'name' => esc_html__('View Detail Style', 'g5plus-arvo'),
						'id' => 'portfolio_detail_style',
						'type' => 'select',
						'std'      => 'none',
						'options' => array(
							'none'              => esc_html__( 'Inherit From Theme Options', 'g5plus-arvo' ),
							'single-image'      => esc_html__( 'Single Image', 'g5plus-arvo' ),
							'horizontal-slider' => esc_html__( 'Horizontal Slider', 'g5plus-arvo' ),
							'video-layout'      => esc_html__( 'Video Layout', 'g5plus-arvo' ),
							'two-columns'       => esc_html__( 'Two Columns', 'g5plus-arvo' )
						),
						'multiple' => false
					),
					array(
						'name' => esc_html__('Custom Fields', 'g5plus-arvo'),
						'id' => 'portfolio-custom-field',
						'type' => 'key-value'
					)
				)
			);
			return $meta_boxes;
		}

		function register_post_types()
		{
			$post_type = G5PLUS_PORTFOLIO_POST_TYPE;

			if (post_type_exists($post_type)) {
				return;
			}
			$post_type_slug = get_option('g5plus-cpt-' . $post_type . '-config');
			if (!isset($post_type_slug) || !is_array($post_type_slug)) {
				$slug = 'portfolio';
				$name = $singular_name = 'Portfolio';
			} else {
				$slug = $post_type_slug['slug'];
				$name = $post_type_slug['name'];
				$singular_name = $post_type_slug['singular_name'];
			}

			register_post_type($post_type,
				array(
					'label' => esc_html__('Portfolio', 'g5plus-arvo'),
					'description' => esc_html__('Portfolio Information', 'g5plus-arvo'),
					'labels'      => array(
						'name' => $name,
						'singular_name' => $singular_name,
						'menu_name' => $name,
						'parent_item_colon' => esc_html__('Parent Portfolio:', 'g5plus-arvo'),
						'all_items' => sprintf(esc_html__('All %s', 'g5plus-arvo'), $name),
						'view_item' => esc_html__('View Portfolio', 'g5plus-arvo'),
						'add_new_item' => sprintf(esc_html__('Add New  %s', 'g5plus-arvo'), $name),
						'add_new' => esc_html__('Add New', 'g5plus-arvo'),
						'edit_item' => esc_html__('Edit Portfolio', 'g5plus-arvo'),
						'update_item' => esc_html__('Update Portfolio', 'g5plus-arvo'),
						'search_items' => esc_html__('Search Portfolio', 'g5plus-arvo'),
						'not_found' => esc_html__('Not found', 'g5plus-arvo'),
						'not_found_in_trash' => esc_html__('Not found in Trash', 'g5plus-arvo'),
					),
					'supports'    => array('title', 'editor', 'thumbnail'),
					'public'      => true,
					'show_ui'     => true,
					'_builtin'    => false,
					'has_archive' => true,
					'menu_icon'   => 'dashicons-screenoptions',
					'rewrite'     => array('slug' => $slug, 'with_front' => true)
				)
			);
			flush_rewrite_rules();
		}

		function addMenuChangeSlug()
		{
			call_user_func('add' . '_submenu_page', 'edit.php?post_type=portfolio', 'Setting', 'Settings', 'edit_posts', wp_basename(__FILE__), array($this, 'initPageSettings'));
		}

		function initPageSettings()
		{
			gf_get_template('cpt/settings',  array('taxonomy_key' => G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY));
		}

		function set_columns_value($column, $post_id)
		{
			switch ($column) {
				case 'thumbnail': {
					echo get_the_post_thumbnail($post_id, 'thumbnail');
					break;
				}
			}
		}
	}
	new GF_Portfolio();
}