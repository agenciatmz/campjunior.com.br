<?php
if (!class_exists('GF_FrameWork_CPT')) {
    class GF_FrameWork_CPT{
        public function __construct(){
            $this->includes();

        }

        private function includes(){
            /**
             * Include post type
             */
            include_once plugin_dir_path(__FILE__) . '/portfolio/portfolio.php';
        }

        public static function get_cpt_template($template, $template_args = array()){
            $template = ltrim( $template, '/\\' );
            $plugins = '/theme-plugins/'.GF_PLUGIN_NAME.'/cpt/';
            $path = get_stylesheet_directory().$plugins.'/'.$template;
            if(!file_exists($path)){
                $path = G5PLUS_THEME_DIR.$template;
            }
            if(!file_exists($path)){
                $path = GF_PLUGIN_NAME.'cpt/'.$template;
            }
            if(file_exists($path)){
                include $path;
            }else{
                esc_html_e('Could not find template','g5plus-arvo');
            }
        }

        public static function get_shortcode_template($template, $template_args = array()){
            $template = ltrim( $template, '/\\' );
            $plugins = '/theme-plugins/'.GF_PLUGIN_NAME.'/shortcodes/';
            $path = get_stylesheet_directory().$plugins.'/'.$template;
            if(!file_exists($path)){
                $path = G5PLUS_THEME_DIR.$template;
            }
            if(!file_exists($path)){
                $path = GF_PLUGIN_NAME.'shortcodes/'.$template;
            }

            if(file_exists($path)){
                include $path;
            }else{
                esc_html_e('Could not find template','g5plus-arvo');
            }
        }
    }
    new GF_FrameWork_CPT();
}


