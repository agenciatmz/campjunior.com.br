<?php
/**
 * Created by PhpStorm.
 * User: Kaga
 * Date: 20/5/2016
 * Time: 3:57 PM
 */
return array(
	'name' => esc_html__('Progress', 'g5plus-arvo'),
	'base' => 'g5plus_progress',
	'class' => '',
	'icon' => 'fa fa-hourglass-start',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Title', 'g5plus-arvo' ),
			'param_name' => 'title',
			'admin_label' => true,
		),
		array(
			'type' => 'datetimepicker',
			'heading' => esc_html__('Time Start', 'g5plus-arvo'),
			'param_name' => 'time_start',
			'admin_label' => true,
		),
		array(
			'type' => 'datetimepicker',
			'heading' => esc_html__('Time End', 'g5plus-arvo'),
			'param_name' => 'time_end',
			'admin_label' => true,
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);