<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $time_start
 * @var $time_end
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Progress_Bar
 */
$title = $time_start = $time_end = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$wrapper_styles = array();

$wrapper_classes = array(
	'g5plus-progress',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);
// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}

if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', array_filter($wrapper_styles) ) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($atts['css'], ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'progress', plugins_url(GF_PLUGIN_NAME . '/shortcodes/progress/assets/css/progress.min.css'), array(), false, 'all');
}

?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<div id="progress_bar"></div>
	<span class="title"><?php echo esc_html($title) ?></span>
	<span id="progress_value"></span>
</div>
<script>
	jQuery(document).ready(function () {
		var elem = document.getElementById("progress_bar");
		var values = document.getElementById("progress_value");
		var value = 0;
		var width = 0;

		var start_time = new Date("<?php echo esc_attr($time_start) ?>");
		var end_time = new Date("<?php echo esc_attr($time_end) ?>");
		var time_now = new Date();

		// Caculator time
		var all_time = (end_time - start_time);
		var status_time =(time_now - start_time);

		// Caculator percent complete
		var percent = Math.round((status_time/all_time)*100);

		if(percent>=100){
			percent = 100;
		}
		var id = setInterval(frame, 10);
		function frame() {
			width++;
			value++;
			elem.style.width = width + '%';
			values.innerHTML = value + "%";
			if (width >= percent) {
				elem.style.width = percent + '%';
				values.innerHTML = percent + "%";
				return;
			}
		}
	});
</script>