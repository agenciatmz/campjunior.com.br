<?php
return array(
	'name' => esc_html__( 'Video', 'g5plus-arvo' ),
	'base' => 'g5plus_video',
	'icon' => 'fa fa-play-circle',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Link', 'g5plus-arvo' ),
			'param_name' => 'link',
			'value' => '',
			'description' => esc_html__( 'Enter link video', 'g5plus-arvo' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
			'param_name' => 'color_scheme',
			'value' => array(
				esc_html__('Light', 'g5plus-arvo') => 'color-light',
				esc_html__('Dark', 'g5plus-arvo') => 'color-dark'),
			'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'std' =>'color-light',
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);