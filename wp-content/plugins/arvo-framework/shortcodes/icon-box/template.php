<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $layout_style
 * @var $color_scheme
 * @var $icon_bg_style
 * @var $icon_scheme_color
 * @var $icon_bg_size
 * @var $icon_align
 * @var $is_box_shadow
 * @var $is_box_shadow_other
 * @var $title
 * @var $description
 * @var $icon_type
 * @var $icon_font
 * @var $icon_image
 * @var $link
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Icon_Box
 */

$layout_style = $color_scheme = $icon_bg_style = $icon_scheme_color = $icon_bg_size = $icon_align = $is_box_shadow
	= $is_box_shadow_other = $title = $description = $icon_type = $icon_font = $icon_image = $link = $css_animation
	= $animation_duration = $animation_delay = $el_class = $css = '';
$atts         = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$wrapper_styles     = array();

if ( $layout_style != 'ib-left' ) {
	$icon_align = '';
}
if ( $layout_style == 'ib-left-inline' || $layout_style == 'ib-right-inline' || $layout_style == 'ib-right-bg' ) {
	$icon_bg_style = 'icon-classic';
}
if ( $icon_bg_style == 'icon-classic' ) {
	$is_box_shadow = false;
}
if ( $layout_style != 'ib-right-bg' ) {
	$is_box_shadow_other = false;
}
if ( $is_box_shadow ) {
	$is_box_shadow = 'has-box-shadow';
} else {
	if ( $is_box_shadow_other ) {
		$is_box_shadow = 'bg-has-box-shadow';
	} else {
		$is_box_shadow = '';
	}
}

$wrapper_classes = array(
	'g5plus-icon-box',
	$layout_style,
	'clearfix',
	$color_scheme,
	$icon_align,
	$icon_bg_style,
	$icon_scheme_color,
	$icon_bg_size,
	$is_box_shadow,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);
$ib_class        = array(
	'ib-shape-inner'
);

//parse link
$link_attributes = $title_attributes = array();
$link            = ( '||' === $link ) ? '' : $link;
$link            = vc_build_link( $link );
$use_link        = false;
if ( strlen( $link['url'] ) > 0 ) {
	$use_link          = true;
	$link_attributes[] = 'href="' . esc_url( trim( $link['url'] ) ) . '"';
	if ( strlen( $link['target'] ) > 0 ) {
		$link_attributes[] = 'target="' . trim( $link['target'] ) . '"';
	}
	if ( strlen( $link['rel'] ) > 0 ) {
		$link_attributes[] = 'rel="' . trim( $link['rel'] ) . '"';
	}
	$title_attributes = $link_attributes;
	if ( strlen( $link['title'] ) > 0 ) {
		$link_attributes[] = 'title="' . trim( $link['title'] ) . '"';
	}
	$title_attributes[] = 'title="' . esc_attr( trim( $title ) ) . '"';
}

// icon html
$icon_html = '';
if ( $icon_type == 'image' ) {
	if ( ! empty( $icon_image ) ) {
		$icon_image_id  = preg_replace( '/[^\d]/', '', $icon_image );
		$icon_image_src = wp_get_attachment_image_src( $icon_image_id, 'full' );
		if ( ! empty( $icon_image_src[0] ) ) {
			$icon_image_src = $icon_image_src[0];
			$icon_html      = '<img alt="' . the_title_attribute( array(
					'post' => $icon_image_id,
					'echo' => false
				) ) . '" src="' . esc_url( $icon_image_src ) . '">';
		}

	}
} else {
	$icon_html = '<i class="' . esc_attr( $icon_font ) . '"></i>';
}

// animation
$animation_style = $this->getStyleAnimation( $animation_duration, $animation_delay );
if ( sizeof( $animation_style ) > 0 ) {
	$wrapper_styles = $animation_style;
}
if ( $wrapper_styles ) {
	$wrapper_attributes[] = 'style="' . implode( '; ', $wrapper_styles ) . '"';
}

$class_to_filter = implode( ' ', array_filter( $wrapper_classes ) );
$class_to_filter .= vc_shortcode_custom_css_class( $css, ' ' );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts );

if ( ! ( defined( 'G5PLUS_SCRIPT_DEBUG' ) && G5PLUS_SCRIPT_DEBUG ) ) {
	wp_enqueue_style( GF_PLUGIN_PREFIX . 'icon-box', plugins_url( GF_PLUGIN_NAME . '/shortcodes/icon-box/assets/css/icon-box.min.css' ), array(), false, 'all' );
}

?>
<div class="<?php echo esc_attr( $css_class ) ?>" <?php echo implode( ' ', $wrapper_attributes ); ?>>
	<?php if ( $layout_style == 'ib-left-inline' ): ?>
		<?php if ( ! empty( $title ) || ! empty( $description ) || ! empty( $icon_html ) ): ?>
			<div class="ib-content">
				<div class="ib-shape">
					<div class="<?php echo implode( ' ', $ib_class ); ?>">
						<?php if ( $use_link ): ?>
							<a <?php echo implode( ' ', $link_attributes ); ?>>
								<?php echo wp_kses_post( $icon_html ); ?>
							</a>
						<?php else:
							echo wp_kses_post( $icon_html );
						endif; ?>
					</div>
				</div>
				<?php if ( ! empty( $title ) ):
					if ( $use_link ): ?>
						<a <?php echo implode( ' ', $title_attributes ); ?>>
							<span class="s-font"><?php echo esc_attr( $title ) ?></span>
						</a>
					<?php else: ?>
						<span class="s-font"><?php echo esc_attr( $title ) ?></span>
					<?php endif;
				endif;
				if ( ! empty( $description ) ): ?>
					<p><?php echo wp_kses_post( $description ); ?></p>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php elseif ( $layout_style == 'ib-right-inline' ): ?>
		<?php if ( ! empty( $title ) || ! empty( $description ) || ! empty( $icon_html ) ): ?>
			<div class="ib-content">
				<?php if ( ! empty( $title ) ):
					if ( $use_link ): ?>
						<a <?php echo implode( ' ', $title_attributes ); ?>>
							<span class="s-font"><?php echo esc_attr( $title ) ?></span>
						</a>
					<?php else: ?>
						<span class="s-font"><?php echo esc_attr( $title ) ?></span>
					<?php endif;
				endif; ?>
				<div class="ib-shape">
					<div class="<?php echo implode( ' ', $ib_class ); ?>">
						<?php if ( $use_link ): ?>
							<a <?php echo implode( ' ', $link_attributes ); ?>>
								<?php echo wp_kses_post( $icon_html ); ?>
							</a>
						<?php else:
							echo wp_kses_post( $icon_html );
						endif; ?>
					</div>
				</div>
				<?php if ( ! empty( $description ) ): ?>
					<p><?php echo wp_kses_post( $description ); ?></p>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php elseif ( $layout_style != 'ib-right' && $layout_style != 'ib-right-bg' ): ?>
		<div class="ib-shape">
			<div class="<?php echo implode( ' ', $ib_class ); ?>">
				<?php if ( $use_link ): ?>
					<a <?php echo implode( ' ', $link_attributes ); ?>>
						<?php echo wp_kses_post( $icon_html ); ?>
					</a>
				<?php else:
					echo wp_kses_post( $icon_html );
				endif; ?>
			</div>
		</div>
		<?php if ( ! empty( $title ) || ! empty( $description ) ): ?>
			<div class="ib-content">
				<?php if ( ! empty( $title ) ):
					if ( $use_link ): ?>
						<a <?php echo implode( ' ', $title_attributes ); ?>>
							<span class="s-font"><?php echo esc_attr( $title ) ?></span>
						</a>
					<?php else: ?>
						<span class="s-font"><?php echo esc_attr( $title ) ?></span>
					<?php endif;
				endif;
				if ( ! empty( $description ) ): ?>
					<p><?php echo wp_kses_post( $description ); ?></p>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php else: ?>
		<?php if ( ! empty( $title ) || ! empty( $description ) ): ?>
			<div class="ib-content">
				<?php if ( ! empty( $title ) ):
					if ( $use_link ): ?>
						<a <?php echo implode( ' ', $title_attributes ); ?>>
							<span class="s-font"><?php echo esc_attr( $title ) ?></span>
						</a>
					<?php else: ?>
						<span class="s-font"><?php echo esc_attr( $title ) ?></span>
					<?php endif;
				endif;
				if ( ! empty( $description ) ): ?>
					<p><?php echo wp_kses_post( $description ); ?></p>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<div class="ib-shape">
			<div class="<?php echo implode( ' ', $ib_class ); ?>">
				<?php if ( $use_link ): ?>
					<a <?php echo implode( ' ', $link_attributes ); ?>>
						<?php echo wp_kses_post( $icon_html ); ?>
					</a>
				<?php else:
					echo wp_kses_post( $icon_html );
				endif; ?>
			</div>
		</div>
	<?php endif; ?>
</div>
