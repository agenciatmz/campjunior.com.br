<?php
return array(
	'base' => 'g5plus_icon_box',
	'name' => esc_html__('Icon Box','g5plus-arvo'),
	'icon' => 'fa fa-diamond',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo'),
			'param_name' => 'layout_style',
			'value' => array(
				esc_html__('Icon top - Alignment center','g5plus-arvo') => 'text-center',
				esc_html__('Icon top - Alignment left','g5plus-arvo') => 'text-left',
				esc_html__('Icon left - Text right','g5plus-arvo') => 'ib-left',
				esc_html__('Icon right - Text left','g5plus-arvo') => 'ib-right',
				esc_html__('Icon left - Icon inline Title','g5plus-arvo') => 'ib-left-inline',
				esc_html__('Icon right - Icon inline Title','g5plus-arvo') => 'ib-right-inline',
				esc_html__('Icon right - Text left, Background','g5plus-arvo') => 'ib-right-bg'
			),
			'std' => 'text-center',
			'admin_label' => true,
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
			'param_name' => 'color_scheme',
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'icon-box-dark',
				esc_html__('Light', 'g5plus-arvo') => 'icon-box-light'
			),
			'std' => 'icon-box-dark',
			'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Icon Background Style', 'g5plus-arvo'),
			'param_name' => 'icon_bg_style',
			'value' => array(
				esc_html__('Classic', 'g5plus-arvo') => 'icon-classic',
				esc_html__('Circle - Fill Color', 'g5plus-arvo') => 'icon-bg-circle-fill',
				esc_html__('Circle - Outline', 'g5plus-arvo') => 'icon-bg-circle-outline',
				esc_html__('Circle - Both Fill Color And Outline', 'g5plus-arvo') => 'icon-bg-circle-fill-outline',
			),
			'std' => 'icon-classic',
			'group' => esc_html__('Icon Options', 'g5plus-arvo'),
			'description' => esc_html__('Select Icon Background Style.', 'g5plus-arvo'),
			'dependency' => array('element'=>'layout_style', 'value'=>array('text-left', 'text-center', 'ib-left','ib-right'))
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Icon Scheme Color', 'g5plus-arvo'),
			'param_name' => 'icon_scheme_color',
			'value' => array(
				esc_html__('Primary', 'g5plus-arvo') => 'icon-scheme-accent',
				esc_html__('Dark', 'g5plus-arvo') => 'icon-scheme-dark',
				esc_html__('Light', 'g5plus-arvo') => 'icon-scheme-light',
				esc_html__('Gray', 'g5plus-arvo') => 'icon-scheme-gray'
			),
			'std' => 'icon-scheme-accent',
			'group' => esc_html__('Icon Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Icon & Background Size', 'g5plus-arvo'),
			'param_name' => 'icon_bg_size',
			'value' => array(
				esc_html__('Extra Large', 'g5plus-arvo') => 'ib-extra-large',
				esc_html__('Large', 'g5plus-arvo') => 'ib-large',
				esc_html__('Medium', 'g5plus-arvo') => 'ib-medium',
				esc_html__('Small', 'g5plus-arvo') => 'ib-small'
			),
			'std' => 'ib-large',
			'group' => esc_html__('Icon Options', 'g5plus-arvo'),
			'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Icon Vertical Alignment', 'g5plus-arvo'),
			'param_name' => 'icon_align',
			'value' => array(
				esc_html__('Top', 'g5plus-arvo') => 'icon-align-top',
				esc_html__('Middle', 'g5plus-arvo') => 'icon-align-middle'
			),
			'std' => 'icon-align-top',
			'group' => esc_html__('Icon Options', 'g5plus-arvo'),
			'description' => esc_html__('Select Icon Vertical Alignment.', 'g5plus-arvo'),
			'dependency' => array('element'=>'layout_style', 'value'=>array('ib-left','ib-right')),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
		),
		array(
			'type'       => 'checkbox',
			'heading'    => esc_html__('Icon Background With Box Shadow', 'g5plus-arvo'),
			'param_name' => 'is_box_shadow',
			'group' => esc_html__('Icon Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element'=>'icon_bg_style', 'value'=>array('icon-bg-circle-fill', 'icon-bg-circle-outline', 'icon-bg-circle-fill-outline'))
		),
		array(
			'type'       => 'checkbox',
			'heading'    => esc_html__('Background With Box Shadow', 'g5plus-arvo'),
			'param_name' => 'is_box_shadow_other',
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element'=>'layout_style', 'value' => array('ib-right-bg'))
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Title', 'g5plus-arvo' ),
			'param_name' => 'title',
			'value' => '',
			'admin_label' => true,
		),
		array(
			'type' => 'textarea',
			'heading' => esc_html__('Description', 'g5plus-arvo'),
			'param_name' => 'description',
			'value' => '',
			'description' => esc_html__('Provide the description for this element.', 'g5plus-arvo'),
			'dependency' => array('element'=>'layout_style', 'value'=>array('text-center', 'text-left', 'ib-left', 'ib-right', 'ib-left-inline', 'ib-right-inline'))
		),
		array_merge( gf_vc_map_add_icon_type(), array(
			'group' => esc_html__('Icon Options', 'g5plus-arvo'))
		),
		array_merge( gf_vc_map_add_icon_font(), array(
				'group' => esc_html__('Icon Options', 'g5plus-arvo'))
		),
		array_merge( gf_vc_map_add_icon_image(), array(
				'group' => esc_html__('Icon Options', 'g5plus-arvo'))
		),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__('Link (url)', 'g5plus-arvo'),
			'param_name' => 'link',
			'value' => '',
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);