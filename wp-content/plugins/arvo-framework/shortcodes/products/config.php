<?php
return array(
	'base' => 'g5plus_products',
	'name' => esc_html__('Products','g5plus-arvo'),
	'icon' => 'fa fa-product-hunt',
	'category' => GF_SHORTCODE_CATEGORY,
	'description' => esc_html__('Show multiple products','g5plus-arvo'),
	'params' =>  array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Show', 'g5plus-arvo'),
			'param_name' => 'show',
			"admin_label" => true,
			'value' => array(
				esc_html__('All', 'g5plus-arvo') => 'all',
				esc_html__('Sale Off', 'g5plus-arvo') => 'sale',
				esc_html__('New In', 'g5plus-arvo') => 'new-in',
				esc_html__('Featured', 'g5plus-arvo') => 'featured',
				esc_html__('Top rated', 'g5plus-arvo') => 'top-rated',
				esc_html__('Recent review', 'g5plus-arvo') => 'recent-review',
				esc_html__('Best Selling', 'g5plus-arvo') => 'best-selling',
				esc_html__('Narrow Products', 'g5plus-arvo') => 'products'
			)
		),
		array(
			'type' => 'autocomplete',
			'heading' => esc_html__( 'Narrow Products', 'g5plus-arvo' ),
			'param_name' => 'slugs',
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'save_always' => true,
			'description' => esc_html__( 'Enter List of Products', 'g5plus-arvo' ),
			'dependency' => array('element' => 'show','value' => 'products'),
		),

		 array_merge(gf_vc_map_add_narrow_product_category(),array(
			 'dependency' => array('element' => 'show','value_not_equal_to' => array('products'))
		 )),

		array(
			'type' => 'textfield',
			'heading' => esc_html__('Number of products', 'g5plus-arvo' ),
			'description' => esc_html__('Enter number of products to display.', 'g5plus-arvo' ),
			'param_name' => 'number',
			'value' => 8,
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'show','value_not_equal_to' => array('products'))
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns', 'g5plus-arvo'),
			'param_name' => 'columns',
			'value' => array( '1' => 1, '2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 4,
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Order by', 'g5plus-arvo'),
			'param_name' => 'orderby',
			'value' => array(
				esc_html__('Date', 'g5plus-arvo') => 'date',
				esc_html__('Price', 'g5plus-arvo') => 'price',
				esc_html__('Random', 'g5plus-arvo') => 'rand',
				esc_html__('Sales', 'g5plus-arvo') => 'sales'
			),
			'description' => esc_html__('Select how to sort retrieved products.', 'g5plus-arvo'),
			'dependency' => array('element' => 'show','value' => array('all', 'sale', 'featured')),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Sort order', 'g5plus-arvo'),
			'param_name' => 'order',
			'value' => array(
				esc_html__('Descending', 'g5plus-arvo') => 'DESC',
				esc_html__('Ascending', 'g5plus-arvo') => 'ASC'
			),
			'description' => esc_html__('Designates the ascending or descending order.', 'g5plus-arvo'),
			'dependency' => array('element' => 'show','value' => array('all', 'sale', 'featured')),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),

		gf_vc_map_add_slider(),
		gf_vc_map_add_pagination(),
		gf_vc_map_add_navigation(),
		gf_vc_map_add_navigation_position(),
		gf_vc_map_add_navigation_style(),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Rows', 'g5plus-arvo'),
			'param_name' => 'rows',
			'value' => array( '1' => 1, '2' => 2 , '3' => 3,'4' => 4),
			'std' => 1,
			'dependency' => array('element' => 'is_slider', 'value' => 'true'),
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Medium devices', 'g5plus-arvo'),
			'param_name' => 'columns_md',
			'description' => esc_html__('Browser Width >= 992px and < 1200px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => 0, '1' => 1, '2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 0,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Small devices', 'g5plus-arvo'),
			'param_name' => 'columns_sm',
			'description' => esc_html__('Browser Width >= 768px and < 991px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => 0, '1' => 1, '2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 0,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Extra small devices', 'g5plus-arvo'),
			'param_name' => 'columns_xs',
			'description' => esc_html__('Browser Width >= 480px and < 767px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => 0, '1' => 1, '2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 0,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Mobile', 'g5plus-arvo'),
			'param_name' => 'columns_mb',
			'description' => esc_html__('Browser Width < 480px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => 0, '1' => 1, '2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 0,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		gf_vc_map_add_css_editor()
	)
);