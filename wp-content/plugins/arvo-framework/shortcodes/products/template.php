<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $show
 * @var $slugs
 * @var $category
 * @var $number
 * @var $columns
 * @var $orderby
 * @var $order
 * @var $is_slider
 * @var $dots
 * @var $nav
 * @var $nav_position
 * @var $nav_style
 * @var $rows
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * @var $columns_md
 * @var $columns_sm
 * @var $columns_xs
 * @var $columns_mb
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Products
 */

$show = $slugs = $category =  $number = $columns = $orderby = $order = $is_slider = $dots = $nav = $nav_position = $nav_style =  $rows = $css_animation = $animation_duration = $animation_delay = $el_class = $css = $columns_md = $columns_sm = $columns_xs = $columns_mb =  '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$styles = array();

$wrapper_classes = array(
	'woocommerce',
	'g5plus-products',
	'clearfix',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
	$styles = $animation_style;
}

if ( $styles ) {
	$wrapper_attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}

// get sources
$query_args = array(
	'post_type'           => 'product',
	'post_status'         => 'publish',
	'ignore_sticky_posts' => 1,
	'posts_per_page'      => $number,
	'meta_query'          => WC()->query->get_meta_query()
);

if (!empty($category) && ($show != 'products')) {
	$query_args['tax_query'] = array(
		array(
			'taxonomy' 		=> 'product_cat',
			'terms' 		=>  explode(',',$category),
			'field' 		=> 'slug',
			'operator' 		=> 'IN'
		)
	);
}

switch($show) {
	case 'sale':
		$product_ids_on_sale = wc_get_product_ids_on_sale();
		$query_args['post__in'] = array_merge( array( 0 ), $product_ids_on_sale );
		break;
	case 'new-in':
		$query_args['orderby'] = 'DESC';
		$query_args['order'] = 'date';
		break;
	case 'featured':
		$query_args['meta_query'][] = array(
			'key'   => '_featured',
			'value' => 'yes'
		);
		break;
	case 'top-rated':
		$query_args['meta_key'] = '_wc_average_rating';
		$query_args['orderby'] = 'meta_value_num';
		$query_args['order'] = 'DESC';
		$query_args['tax_query'] = WC()->query->get_tax_query();
		break;
	case 'recent-review':
		add_filter( 'posts_clauses', array($this, 'order_by_comment_date_post_clauses' ) );
		break;
	case 'best-selling' :
		$query_args['meta_key'] = 'total_sales';
		$query_args['orderby'] = 'meta_value_num';
		break;
	case 'products':
		$product_names = explode( ',', $slugs );
		$query_args['post_name__in'] = $product_names;
		$query_args['posts_per_page'] = -1;
		add_filter( 'posts_orderby' , 'gf_order_by_slug',10,2 );
		break;
}

if (in_array($show,array('all','sale','featured'))) {
	$query_args['order'] = $order;
	switch ( $orderby ) {
		case 'price' :
			$query_args['meta_key'] = '_price';
			$query_args['orderby']  = 'meta_value_num';
			break;
		case 'rand' :
			$query_args['orderby']  = 'rand';
			break;
		case 'sales' :
			$query_args['meta_key'] = 'total_sales';
			$query_args['orderby']  = 'meta_value_num';
			break;
		default :
			$query_args['orderby']  = 'date';
	}
}

$r = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts ) );

if($show =='recent-review' ){
	remove_filter( 'posts_clauses', array($this, 'order_by_comment_date_post_clauses' )  );
}

if($show =='products' ){
	remove_filter( 'posts_orderby','gf_order_by_slug');
}

$totalRecord = $r->post_count;
$per_page = $rows * $columns;
$index = 0;
$index_sub = 0;





$g5plus_woocommerce_loop = &gf_get_woocommerce_loop();
$g5plus_woocommerce_loop['columns'] = $columns;
$g5plus_woocommerce_loop['columns_md'] = $columns_md;
$g5plus_woocommerce_loop['columns_sm'] = $columns_sm;
$g5plus_woocommerce_loop['columns_xs'] = $columns_xs;
$g5plus_woocommerce_loop['columns_mb'] = $columns_mb;


$product_listing_class = array('clearfix');
if ($is_slider && ($totalRecord > $per_page)) {
	$g5plus_woocommerce_loop['layout'] = 'slider';
	$g5plus_woocommerce_loop['dots'] = ($dots ? 'true' : 'false');
	$g5plus_woocommerce_loop['nav'] = ($nav ? 'true' : 'false');
	$g5plus_woocommerce_loop['nav_position'] = $nav_position;
	$g5plus_woocommerce_loop['nav_style'] = $nav_style;
	if ($rows > 1) {
		$g5plus_woocommerce_loop['rows'] = $rows;
		$columns_md = ($columns_md == 0) ? (($columns > 4) ? 4 : $columns) : $columns_md;
		$columns_sm = ($columns_sm == 0) ? (($columns > 3) ? 3 : $columns) : $columns_sm;
		$columns_xs = ($columns_xs == 0) ? (($columns > 2) ? 2 : $columns) : $columns_xs;
		$columns_mb = ($columns_mb == 0) ? (1) : $columns_mb;
		$product_listing_class[] = 'columns-' . $columns ;
		$product_listing_class[] = 'columns-md-' . $columns_md;
		$product_listing_class[] = 'columns-sm-' . $columns_sm;
		$product_listing_class[] = 'columns-xs-' . $columns_xs;
		$product_listing_class[] = 'columns-mb-' . $columns_mb;
	}
}

$class_to_filter = implode(' ', array_filter($wrapper_classes) );
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode( ' ', $wrapper_attributes ); ?>>
	<?php if ($r->have_posts()): ?>
		<?php woocommerce_product_loop_start(); ?>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<?php if (($is_slider) && ($totalRecord > $per_page) && ($rows > 1) && (($index % $per_page) === 0)): ?>
				<?php $index_sub = 0; ?>
				<div class="<?php echo join(' ',$product_listing_class); ?>">
			<?php endif; ?>
			<?php wc_get_template_part( 'content', 'product' ); ?>
			<?php if (($is_slider) && ($totalRecord > $per_page) && ($rows > 1) && ($index_sub == ($per_page - 1))) : ?>
				</div><!-- End Loop -->
			<?php endif; ?>
			<?php
				$index_sub++;
				$index++;
			?>
		<?php endwhile; // end of the loop. ?>
		<?php if (($is_slider) && ($totalRecord > $per_page) && ($rows > 1) && ($index_sub != $per_page) && ($index > 0)) : ?>
			</div><!-- End Loop -->
		<?php endif; ?>
		<?php woocommerce_product_loop_end(); ?>
	<?php else: ?>
		<div class="item-not-found"><?php esc_html_e('No item found','g5plus-arvo'); ?></div>
	<?php endif; ?>
</div>
<?php wp_reset_postdata(); ?>