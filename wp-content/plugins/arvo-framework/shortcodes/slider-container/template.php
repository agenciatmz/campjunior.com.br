<?php
/**
 * Shortcode attributes
 * @var $content
 * @var $atts
 * @var $color_scheme
 * @var $nav
 * @var $nav_style
 * @var $nav_position
 * @var $dots
 * @var $dot_position
 * @var $margin
 * @var $auto_play
 * @var $auto_play_time
 * @var $loop
 * @var $items_lg
 * @var $items_md
 * @var $items_sm
 * @var $items_xs
 * @var $items_mb
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Slider_Container
 */
$color_scheme = $nav = $nav_style = $nav_position = $dots = $dot_position = $margin = $auto_play = $auto_play_time = $loop = $items_lg = $items_md = $items_sm = $items_xs = $items_mb = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$wrapper_attributes = array();
$wrapper_styles = array();

$wrapper_classes = array(
	'g5plus-slider-container',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}
if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', $wrapper_styles) . '"';
}
$wrapper_classes[] = 'owl-carousel';

if ($nav) {
	$wrapper_classes[] = 'owl-nav-'. $nav_position;
	$wrapper_classes[] = 'owl-nav-' . $nav_style;
}

if ($dots) {
	$wrapper_classes[] = 'owl-dot-' . $dot_position;
}

if ($color_scheme == 'dark') {
	$wrapper_classes[] = 'owl-dark';
}

$owl_responsive_attributes = array();

// Mobile <= 480px
$owl_responsive_attributes[] = '"0" : {"items" : '. $items_mb .', "margin": 0}';

// Extra small devices ( < 768px)
$owl_responsive_attributes[] = '"481" : {"items" : '. $items_xs .', "margin": '. $margin .'}';

// Small devices Tablets ( < 992px)
$owl_responsive_attributes[] = '"768" : {"items" : '. $items_sm .', "margin": '. $margin .'}';

// Medium devices ( < 1199px)
$owl_responsive_attributes[] = '"992" : {"items" : '. $items_md .', "margin": '. $margin .'}';

// Medium devices ( > 1199px)
$owl_responsive_attributes[] = '"1200" : {"items" : '. $items_lg .', "margin": '. $margin .'}';

$owl_attributes = array(
	'"autoHeight": true',
	'"dots": ' . ($dots ? 'true' : 'false'),
	'"nav": ' . ($nav ? 'true' : 'false'),
	'"responsive": {'. implode(', ', $owl_responsive_attributes) . '}'
);
if ($auto_play === 'true') {
	$owl_attributes[] = '"autoplay": true';
	$owl_attributes[] = '"autoplayTimeout":' . $auto_play_time;
}

if ($loop === 'true') {
	$owl_attributes[] = '"loop": true';
}

$wrapper_attributes[] = "data-plugin-options='{". implode(', ', $owl_attributes) ."}'";

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<?php echo do_shortcode($content) ?>
</div>