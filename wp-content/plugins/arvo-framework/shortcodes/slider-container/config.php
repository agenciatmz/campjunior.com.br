<?php
return array(
	'name' => esc_html__('Slider Container', 'g5plus-arvo'),
	'base' => 'g5plus_slider_container',
	'icon' => 'fa fa-ellipsis-h',
	'category' => GF_SHORTCODE_CATEGORY,
	'as_parent' => array('except' => 'g5plus_slider_container'),
	'content_element' => true,
	'show_settings_on_create' => true,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
			'param_name' => 'color_scheme',
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'dark',
				esc_html__('Light', 'g5plus-arvo') => 'light'),
			'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'std' => 'light',
		),
		array_merge(gf_vc_map_add_navigation(),array(
			'dependency' => array(),
			'edit_field_class' => 'vc_col-sm-12 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_style(),array(
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_position(),array(
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination(),array(
			'dependency' => array(),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination_position(),array(
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Auto Play', 'g5plus-arvo'),
			'param_name' => 'auto_play',
			'std' => 'false',
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Auto Play Time', 'g5plus-arvo'),
			'description' => esc_html__('Autoplay interval timeout (millisecond)', 'g5plus-arvo'),
			'param_name' => 'auto_play_time',
			'std' => '5000',
			'dependency' => array('element' => 'auto_play', 'value' => 'true'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Loop', 'g5plus-arvo'),
			'param_name' => 'loop',
			'std' => 'false',
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Margin', 'g5plus-arvo' ),
			'param_name' => 'margin',
			'value' => array(
				'0px' => '0',
				'10px' => '10',
				'20px' => '20',
				'30px' => '30',
			),
			'std' => '30',
			'description' => esc_html__( 'Margin-right(px) on item.', 'g5plus-arvo' ),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Desktop', 'g5plus-arvo'),
			'param_name' => 'items_lg',
			'description' => esc_html__('Browser Width > 1199', 'g5plus-arvo'),
			'value' => '',
			'std' => '5',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Desktop Small', 'g5plus-arvo'),
			'param_name' => 'items_md',
			'description' => esc_html__('Browser Width < 1199', 'g5plus-arvo'),
			'value' => '',
			'std' => '4',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Tablet', 'g5plus-arvo'),
			'param_name' => 'items_sm',
			'description' => esc_html__('Browser Width < 992', 'g5plus-arvo'),
			'value' => '',
			'std' => '3',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Tablet Small', 'g5plus-arvo'),
			'param_name' => 'items_xs',
			'description' => esc_html__('Browser Width < 768', 'g5plus-arvo'),
			'value' => '',
			'std' => '2',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Mobile', 'g5plus-arvo'),
			'param_name' => 'items_mb',
			'description' => esc_html__('Browser Width < 480', 'g5plus-arvo'),
			'value' => '',
			'std' => '1',
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	),
	'js_view' => 'VcColumnView'
);