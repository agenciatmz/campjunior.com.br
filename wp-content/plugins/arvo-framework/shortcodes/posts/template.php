<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $layout
 * @var $no_padding
 * @var $category
 * @var $source
 * @var $number
 * @var $columns
 * @var $is_excerpt
 * @var $is_slider
 * @var $dots
 * @var $nav
 * @var $nav_position
 * @var $nav_style
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $items_md
 * @var $items_sm
 * @var $items_xs
 * @var $items_mb
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Posts
 */

$layout = $no_padding = $category = $source = $number = $columns = $is_slider = $dots = $nav = $nav_position = $nav_style = $css_animation = $animation_duration = $animation_delay = $el_class = $items_md = $items_sm = $items_xs = $items_mb = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$styles = array();
// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
	$styles = $animation_style;
}

$wrapper_classes = array(
	'g5plus-posts',
	'clearfix',
	'posts-'. $layout,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);


if ( $styles ) {
	$wrapper_attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}

if ($items_md == -1) {
	$items_md = $columns;
}

if ($items_sm == -1) {
	$items_sm = $columns;
}

if ($items_xs == -1) {
	$items_xs = $columns;
}

if ($items_mb == -1) {
	$items_mb = $columns;
}


if ($is_slider){
	$wrapper_classes[] = 'owl-carousel manual';

	if ($nav) {
		$wrapper_classes[] = 'owl-nav-'. $nav_position;
		$wrapper_classes[] = 'owl-nav-' . $nav_style;
	}

	$owl_responsive_attributes = array();
	$owl_margin = 30;
	if ($no_padding) {
		$owl_margin = 0;
		if ($layout != 'carousel') {
			$wrapper_classes[] = 'blog-equal-height-carousel';
		}
	}
	// Mobile <= 480px
	$owl_responsive_attributes[] = '"0" : {"items" : '. $items_mb .', "margin": 0}';

	// Extra small devices ( < 768px)
	$owl_responsive_attributes[] = '"481" : {"items" : '. $items_xs .', "margin": '. $owl_margin .'}';

	// Small devices Tablets ( < 992px)
	$owl_responsive_attributes[] = '"768" : {"items" : '. $items_sm .', "margin": '. $owl_margin .'}';

	// Medium devices ( < 1199px)
	$owl_responsive_attributes[] = '"992" : {"items" : '. $items_md .', "margin": '. $owl_margin .'}';

	// Medium devices ( > 1199px)
	$owl_responsive_attributes[] = '"1200" : {"items" : '. $columns .', "margin": '. $owl_margin .'}';

	$owl_attributes = array(
		'"autoHeight": true',
		'"dots": ' . ($dots ? 'true' : 'false'),
		'"nav": ' . ($nav ? 'true' : 'false'),
		'"responsive": {'. implode(', ', $owl_responsive_attributes) . '}'
	);
	$wrapper_attributes[] = "data-plugin-options='{". implode(', ', $owl_attributes) ."}'";
} else {
	$wrapper_classes[] = 'columns-' . $columns;
	$wrapper_classes[] = 'columns-md-' . $items_md;
	$wrapper_classes[] = 'columns-sm-' . $items_sm;
	$wrapper_classes[] = 'columns-xs-' . $items_xs;
	$wrapper_classes[] = 'columns-mb-' . $items_mb;
	if (!$no_padding) {
		$wrapper_classes[] = 'row';
	}
	if ($no_padding) {
		if ($layout != 'carousel') {
			$wrapper_classes[] = 'blog-equal-height';
		}
	}
}
if ($no_padding) {
	$wrapper_classes[] = 'blog-no-padding';
}
// get sources
$query_args = array(
	'posts_per_page' => $number,
	'no_found_rows' => true,
	'post_status' => 'publish',
	'ignore_sticky_posts' => true,
	'post_type' => 'post',
	'tax_query' => array(
		array(
			'taxonomy' => 'post_format',
			'field' => 'slug',
			'terms' => array('post-format-quote', 'post-format-link', 'post-format-audio'),
			'operator' => 'NOT IN'
		)
	)
);
switch ($source) {
	case 'random' :
		$query_order_args = array(
			'orderby' => 'rand',
			'order' => 'DESC',
		);
		break;
	case 'popular':
		$query_order_args = array(
			'orderby' => 'comment_count',
			'order' => 'DESC',
		);
		break;
	case 'recent':
		$query_order_args = array(
			'orderby' => 'post_date',
			'order' => 'DESC',
		);
		break;
	case 'oldest':
		$query_order_args = array(
			'orderby' => 'post_date',
		);
		break;
}
$query_args = array_merge($query_args,$query_order_args);
if (!empty($category)) {
	$query_args['tax_query'][] = array(
		'taxonomy' => 'category',
		'terms' => explode(',', $category),
		'field' => 'slug',
		'operator' => 'IN'
	);
}
$r = new WP_Query($query_args);

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'posts', plugins_url(GF_PLUGIN_NAME . '/shortcodes/posts/assets/css/posts.min.css'), array(), false, 'all');
}

$min_suffix_js = gf_get_option('enable_minifile_js',0) == 1 ? '.min' : '';
wp_enqueue_script(GF_PLUGIN_PREFIX . 'posts', plugins_url(GF_PLUGIN_NAME . '/shortcodes/posts/assets/js/posts' . $min_suffix_js . '.js'), false, true);
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode( ' ', $wrapper_attributes ); ?>>
	<?php if ($r->have_posts()): ?>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<?php gf_get_template('shortcodes/posts/content',array('is_slider' => $is_slider,'layout' => $layout,'no_padding' => $no_padding)); ?>
		<?php endwhile; ?>
	<?php else: ?>
		<div class="item-not-found"><?php esc_html_e('No item found','g5plus-arvo'); ?></div>
	<?php endif; ?>
</div>
<?php wp_reset_postdata(); ?>