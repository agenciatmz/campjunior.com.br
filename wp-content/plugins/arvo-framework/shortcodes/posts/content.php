<?php
/**
 * The template for displaying content
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 * @var $is_slider
 * @var $layout
 * @var $no_padding
 */
$size = 'small-image';
$excerpt =  get_the_excerpt();
$post_class = array('clearfix');
if (!$is_slider) {
	$post_class[] = 'gf-item-wrap';
	if ($no_padding) {
		$post_class[] = 'gf-item-wrap-no-padding';
	}
}
if (in_array($layout,array('grid','grid-no-image'))) {
	$post_class[] = 'post-grid';
} else {
	$post_class[] = 'post-carousel';
	$size = 'size-640x420';
}

if ($layout == 'grid-no-image') {
	$post_class[] = 'text-center';
}
?>

<article <?php post_class($post_class); ?>>
	<?php if ($layout != 'grid-no-image'){
		gf_get_post_thumbnail($size,1);
	} ?>
	<div class="entry-content-wrap">
		<?php if ($layout == 'carousel') : ?>
			<div class="entry-meta-date">
				<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"> <?php echo  get_the_date(get_option('date_format'));?> </a>
			</div>
			<h3 class="entry-post-title"><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php else: ?>
			<h3 class="entry-post-title"><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<?php get_template_part('templates/archive/post-meta'); ?>
			<?php if ($excerpt !== ''): ?>
				<div class="entry-excerpt">
					<?php the_excerpt(); ?>
				</div>
			<?php endif; ?>
			<a href="<?php the_permalink(); ?>" class="btn btn-link btn-black"><?php esc_html_e('Read More', 'g5plus-arvo'); ?></a>
		<?php endif; ?>
	</div>
</article>
