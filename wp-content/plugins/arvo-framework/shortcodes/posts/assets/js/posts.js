var G5Plus_Posts = {};
(function ($) {
	"use strict";
	var $body = $('body'),
		isRTL = $body.hasClass('rtl');

	G5Plus_Posts = {
		init: function(){
			this.equalHeight();
			this.events();
			this.owlCarousel();
		},
		events: function(){
			$(window).on('resize', function(){
				G5Plus_Posts.equalHeight();
			});
		},
		equalHeight: function(){
			$('.blog-equal-height').each(function() {
				G5Plus_Posts.fixHeight('.entry-content-wrap',$(this));
			});
		},
		equalHeightCarousel : function(event){
			var $this   = $(event.target);
			if ($this.hasClass('blog-equal-height-carousel')) {
				G5Plus_Posts.fixHeight('.owl-item.active .entry-content-wrap',$this);
				setTimeout(function(){
					G5Plus_Posts.fixHeight('.owl-item.active .entry-content-wrap',$this);
				},1000);
				setTimeout(function(){
					var $slider = $this.data('owl.carousel');
					if (typeof($slider) != 'undefined') {
						$slider.refresh();
					}
				},2000);

			}
		},
		fixHeight : function($element,$wrap){
			var maxHeight = 0;
			$($element,$wrap).css('height','auto');
			$($element,$wrap).each(function(){
				var height = $(this).outerHeight();
				if (height > maxHeight) {
					maxHeight = height;
				}
			});
			$($element,$wrap).css('height', maxHeight+'px');
		},
		owlCarousel: function () {
			$('.g5plus-posts.owl-carousel:not(.owl-loaded)').each(function () {
				var slider = $(this);
				var defaults = {
					items: 4,
					nav: false,
					navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
					dots: false,
					loop: false,
					center: false,
					mouseDrag: true,
					touchDrag: true,
					pullDrag: true,
					freeDrag: false,
					margin: 0,
					stagePadding: 0,
					merge: false,
					mergeFit: true,
					autoWidth: false,
					startPosition: 0,
					rtl: isRTL,
					smartSpeed: 250,
					fluidSpeed: false,
					dragEndSpeed: false,
					autoplayHoverPause: true,
					onInitialized: G5Plus_Posts.equalHeightCarousel,
					onResized: G5Plus_Posts.equalHeightCarousel,
					onChanged: G5Plus_Posts.equalHeightCarousel

				};
				var config = $.extend({}, defaults, slider.data("plugin-options"));
				// Initialize Slider
				slider.owlCarousel(config);
			});
		}
	};
	$(document).ready(function () {
		G5Plus_Posts.init();
	});
})(jQuery);