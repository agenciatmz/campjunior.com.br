<?php
return array(
	'base' => 'g5plus_posts',
	'name' => esc_html__('Posts','g5plus-arvo'),
	'icon' => 'fa fa-file-text',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout', 'g5plus-arvo'),
			'param_name' => 'layout',
			'value' => array(
				esc_html__('Grid','g5plus-arvo') => 'grid',
				esc_html__('Grid No Image','g5plus-arvo') => 'grid-no-image',
				esc_html__('Carousel', 'g5plus-arvo' ) => 'carousel',
			),
			'admin_label' => true,
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('No Padding?', 'g5plus-arvo' ),
			'param_name' => 'no_padding',
		),
		gf_vc_map_add_narrow_category(),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Source', 'g5plus-arvo'),
			'param_name' => 'source',
			'value' => array(
				esc_html__('Random','g5plus-arvo') => 'random',
				esc_html__('Popular','g5plus-arvo') => 'popular',
				esc_html__('Recent', 'g5plus-arvo' ) => 'recent',
				esc_html__('Oldest','g5plus-arvo') => 'oldest'
			),
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Number of posts', 'g5plus-arvo' ),
			'description' => esc_html__('Enter number of posts to display.', 'g5plus-arvo' ),
			'param_name' => 'number',
			'value' => 5,
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns', 'g5plus-arvo'),
			'param_name' => 'columns',
			'value' => array('1' => 1, '2' => 2 , '3' => 3),
			'std' => 3,
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		),
		gf_vc_map_add_slider(),
		gf_vc_map_add_pagination(),
		gf_vc_map_add_navigation(),
		array_merge(gf_vc_map_add_navigation_position(),array('edit_field_class' => 'vc_col-sm-4 vc_column')),
		array_merge(gf_vc_map_add_navigation_style(),array('edit_field_class' => 'vc_col-sm-4 vc_column')),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Medium devices', 'g5plus-arvo'),
			'param_name' => 'items_md',
			'description' => esc_html__('Browser Width >= 992px and < 1200px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => -1,'1' => 1, '2' => 2 , '3' => 3),
			'std' => -1,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Small devices', 'g5plus-arvo'),
			'param_name' => 'items_sm',
			'description' => esc_html__('Browser Width >= 768px and < 991px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => -1,'1' => 1, '2' => 2 , '3' => 3),
			'std' => 2,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Extra small devices', 'g5plus-arvo'),
			'param_name' => 'items_xs',
			'description' => esc_html__('Browser Width >= 480px and < 767px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => -1,'1' => 1, '2' => 2 , '3' => 3),
			'std' => 2,
			'group' => esc_html__('Responsive','g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Items Mobile', 'g5plus-arvo'),
			'param_name' => 'items_mb',
			'description' => esc_html__('Browser Width < 480px', 'g5plus-arvo'),
			'value' => array( esc_html__('Default','g5plus-arvo') => -1,'1' => 1, '2' => 2 , '3' => 3),
			'std' => 1,
			'group' => esc_html__('Responsive','g5plus-arvo')
		)
	)
);