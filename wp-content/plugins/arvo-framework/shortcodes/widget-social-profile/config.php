<?php
return array(
	'base' => 'g5plus_widget_social_profile',
	'name' => esc_html__('Widget Social Profile','g5plus-arvo'),
	'icon' => 'fa fa-share-alt',
	'category' => GF_SHORTCODE_WIDGET_CATEGORY,
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Widget title', 'g5plus-arvo' ),
			'param_name' => 'title',
			'value' => '',
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Follow Text', 'g5plus-arvo' ),
			'param_name' => 'follow_text',
			'value' => ''
		),
		array(
			'type' => 'select2',
			'heading' => esc_html__('Values','g5plus-arvo'),
			'param_name' => 'icons',
			'options' => get_profiles(),
			'multiple' => true,
		),
		array(
			'type' => 'dropdown',
			'heading'     => esc_html__('Style', 'g5plus-arvo'),
			'param_name'  => 'style',
			'value'       => array(
				esc_html__('Classic', 'g5plus-arvo')      => 'classic',
				esc_html__('Circle Outline', 'g5plus-arvo') => 'circle',
				esc_html__('Circle Fill Color', 'g5plus-arvo') => 'circle-fill'
			)
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	),
);