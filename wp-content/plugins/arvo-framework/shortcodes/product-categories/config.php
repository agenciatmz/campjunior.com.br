<?php
return array(
	'base' => 'g5plus_product_categories',
	'name' => esc_html__('Product Categories','g5plus-arvo'),
	'icon' => 'fa fa-product-hunt',
	'category' => GF_SHORTCODE_CATEGORY,
	'description' => esc_html__('Display product categories loop','g5plus-arvo'),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Show', 'g5plus-arvo'),
			'param_name' => 'show',
			"admin_label" => true,
			'value' => array(
				esc_html__('All', 'g5plus-arvo') => 'all',
				esc_html__('Narrow Categories', 'g5plus-arvo') => 'categories'
			)
		),
		array(
			'type' => 'autocomplete',
			'heading' => esc_html__( 'Narrow Categories', 'g5plus-arvo' ),
			'param_name' => 'category',
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'save_always' => true,
			'description' => esc_html__( 'Enter List of Product Categories', 'g5plus-arvo' ),
			'dependency' => array('element' => 'show', 'value' => 'categories'),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__('Number of product categories', 'g5plus-arvo' ),
			'description' => esc_html__('Enter number of product categories to display.', 'g5plus-arvo' ),
			'param_name' => 'number',
			'value' => 8,
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'show', 'value' => 'all'),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns', 'g5plus-arvo'),
			'param_name' => 'columns',
			'value' => array( '1' => 1, '2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 4,
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Order by', 'g5plus-arvo'),
			'param_name' => 'orderby',
			'value' => array(
				esc_html__('Name', 'g5plus-arvo') => 'name',
				esc_html__('Order', 'g5plus-arvo') => 'order'
			),
			'description' => esc_html__('Select how to sort retrieved product categories.', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'show', 'value' => 'all'),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Sort order', 'g5plus-arvo'),
			'param_name' => 'order',
			'value' => array(
				esc_html__('Descending', 'g5plus-arvo') => 'DESC',
				esc_html__('Ascending', 'g5plus-arvo') => 'ASC'
			),
			'description' => esc_html__('Designates the ascending or descending order.', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'show', 'value' => 'all'),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Hide Empty?', 'g5plus-arvo' ),
			'param_name' => 'hide_empty',
			'admin_label' => true,
		),

		gf_vc_map_add_slider(),
		gf_vc_map_add_pagination(),
		gf_vc_map_add_navigation(),
		gf_vc_map_add_navigation_position(),
		gf_vc_map_add_navigation_style(),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Rows', 'g5plus-arvo'),
			'param_name' => 'rows',
			'value' => array( '1' => 1, '2' => 2 , '3' => 3,'4' => 4),
			'std' => 1,
			'dependency' => array('element' => 'is_slider', 'value' => 'true'),
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class()
	)
);