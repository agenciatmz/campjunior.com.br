<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $layout
 * @var $columns
 * @var $max_items
 * @var $post_paging
 * @var $posts_per_page
 * @var $orderby
 * @var $order
 * @var $meta_key
 * @var $category
 * @var $el_class
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Blog
 */

$layout = $columns = $max_items = $post_paging = $posts_per_page = $orderby = $order = $meta_key = $category = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

global $wp_query;
$wrapper_classes = array(
	'archive-wrap',
	'clearfix',
	$this->getExtraClass( $el_class )
);

if (is_front_page()) {
	$paged   = get_query_var( 'page' ) ? intval( get_query_var( 'page' ) ) : 1;
} else {
	$paged   = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
}

$posts_per_page = $max_items > 0 ? $max_items : $posts_per_page;

$args = array(
	'post_type'=> 'post',
	'paged' => $paged,
	'ignore_sticky_posts' => true,
	'posts_per_page' => $posts_per_page,
	'orderby' => $orderby,
	'order' => $order,
	'meta_key' => $orderby == 'meta_key' ? $meta_key : '',
);

if ($post_paging == 'all' && $max_items == -1) {
	$args['nopaging'] = true;
}

if (!empty($category)) {
	$args['tax_query'] = array(
		array(
			'taxonomy' 		=> 'category',
			'terms' 		=>  explode(',',$category),
			'field' 		=> 'slug',
			'operator' 		=> 'IN'
		)
	);
}
query_posts($args);

$blog_wrap_classes = array('blog-wrap clearfix');
$blog_wrap_classes[] = 'blog-'.$layout;
if (in_array($layout,array('grid','masonry','1-large-image-masonry'))) {
	$page_layouts = &gf_get_page_layout_settings();
	$blog_wrap_classes[] = 'row';
	$blog_wrap_classes[] = 'columns-'.$columns;
	if ($page_layouts['has_sidebar']) {
		$blog_wrap_classes[] = 'columns-md-2';
	} else {
		$blog_wrap_classes[] = 'columns-md-'.$columns;
	}
	$blog_wrap_classes[] = 'columns-sm-2';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);
?>
<div class="<?php echo esc_attr($css_class) ?>">
	<div class="<?php echo esc_attr(join(' ',$blog_wrap_classes));?>">
		<?php
		if ( have_posts() ) :
			// Start the Loop.
			while ( have_posts() ) : the_post();
				$index = $wp_query->current_post;
				if ($post_paging !== 'navigation') {
					$index = $wp_query->current_post + (($paged - 1) * $posts_per_page);
				}
				/*
				 * Include the post format-specific template for the content. If you want to
				 * use this in a child theme, then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				if ($layout == '1-large-image-masonry') {
					if(($wp_query->current_post == 0) && (($paged == 1) || ($post_paging == 'navigation'))) {
						get_template_part('templates/archive/content-large-image-2');
					} else {
						g5plus_get_template('archive/content-masonry-2',array('index' => ($index - 1), 'columns' => $columns));
					}
				} else {
					get_template_part('templates/archive/content', $layout);
				}
			endwhile;
		else :
			// If no content, include the "No posts found" template.
			get_template_part('templates/archive/content', 'none');
		endif;
		?>
	</div>
	<?php if ($wp_query->max_num_pages > 1 && $max_items == -1) {
		get_template_part('templates/paging/'.$post_paging);
	}?>
</div>
<?php wp_reset_query(); ?>