<?php
return array(
	'base' => 'g5plus_blog',
	'name' => esc_html__('Blog','g5plus-arvo'),
	'icon' => 'fa fa-file-text',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout', 'g5plus-arvo'),
			'param_name' => 'layout',
			'value' => array(
				esc_html__('Large Image','g5plus-arvo') => 'large-image',
				esc_html__('Grid','g5plus-arvo') => 'grid',
				esc_html__('Masonry', 'g5plus-arvo' ) => 'masonry',
				esc_html__('1 Large Image Then Masonry Layout', 'g5plus-arvo' ) => '1-large-image-masonry',
				esc_html__('Large Image Center', 'g5plus-arvo' ) => 'large-image-2'
			),
			'admin_label' => true,
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns', 'g5plus-arvo'),
			'param_name' => 'columns',
			'value' => array('2' => 2 , '3' => 3),
			'dependency' => array('element' => 'layout', 'value' => array('masonry','grid','1-large-image-masonry') ),
		),

		array(
			'type' => 'number',
			'heading' => esc_html__('Number of posts', 'g5plus-arvo' ),
			'description' => esc_html__('Enter number of posts to display.', 'g5plus-arvo' ),
			'param_name' => 'max_items',
			'value' => -1,
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Post Paging', 'g5plus-arvo'),
			'param_name' => 'post_paging',
			'value' => array(
				esc_html__('Show all', 'g5plus-arvo') => 'all',
				esc_html__('Navigation', 'g5plus-arvo') => 'navigation',
				esc_html__('Load More', 'g5plus-arvo') => 'load-more',
				esc_html__('Infinite Scroll', 'g5plus-arvo') => 'infinite-scroll',
			),
			'std' => 'all',
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'max_items','value' => '-1'),
			'admin_label' => true,
		),

		array(
			"type" => "number",
			"heading" => esc_html__("Posts per page", 'g5plus-arvo'),
			"param_name" => "posts_per_page",
			"value" => get_option('posts_per_page'),
			"description" => esc_html__('Number of items to show per page', 'g5plus-arvo'),
			'dependency' => array('element' => 'post_paging','value' => array('navigation', 'load-more', 'infinite-scroll')),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'admin_label' => true,
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Order by', 'g5plus-arvo'),
			'param_name' => 'orderby',
			'value' => array(
				esc_html__('Date', 'g5plus-arvo') => 'date',
				esc_html__('Order by post ID', 'g5plus-arvo') => 'ID',
				esc_html__('Author', 'g5plus-arvo') => 'author',
				esc_html__('Title', 'g5plus-arvo') => 'title',
				esc_html__('Last modified date', 'g5plus-arvo') => 'modified',
				esc_html__('Post/page parent ID', 'g5plus-arvo') => 'parent',
				esc_html__('Number of comments', 'g5plus-arvo') => 'comment_count',
				esc_html__('Menu order/Page Order', 'g5plus-arvo') => 'menu_order',
				esc_html__('Meta value', 'g5plus-arvo') => 'meta_value',
				esc_html__('Meta value number', 'g5plus-arvo') => 'meta_value_num',
				esc_html__('Random order', 'g5plus-arvo') => 'rand',
			),
			'description' => esc_html__('Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'g5plus-arvo'),
			'group' => esc_html__('Data Settings', 'g5plus-arvo'),
			'param_holder_class' => 'vc_grid-data-type-not-ids',
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Sorting', 'g5plus-arvo'),
			'param_name' => 'order',
			'group' => esc_html__('Data Settings', 'g5plus-arvo'),
			'value' => array(
				esc_html__('Descending', 'g5plus-arvo') => 'DESC',
				esc_html__('Ascending', 'g5plus-arvo') => 'ASC',
			),
			'param_holder_class' => 'vc_grid-data-type-not-ids',
			'description' => esc_html__('Select sorting order.', 'g5plus-arvo'),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__('Meta key', 'g5plus-arvo'),
			'param_name' => 'meta_key',
			'description' => esc_html__('Input meta key for grid ordering.', 'g5plus-arvo'),
			'group' => esc_html__('Data Settings', 'g5plus-arvo'),
			'param_holder_class' => 'vc_grid-data-type-not-ids',
			'dependency' => array(
				'element' => 'orderby',
				'value' => array('meta_value', 'meta_value_num'),
			),
		),
		gf_vc_map_add_narrow_category(),
		gf_vc_map_add_extra_class()
	)
);