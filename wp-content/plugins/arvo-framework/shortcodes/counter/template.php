<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $counter_title
 * @var $counter_value
 * @var $underline_value
 * @var $color_scheme
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Counter
 */

$counter_title = $counter_value = $underline_value = $color_scheme = $css_animation = $animation_duration = $animation_delay = $el_class = $css =  '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$styles = array();

// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
    $styles = $animation_style;
}
if($underline_value) {
	$underline_value='underline-value';
}
else{
	$underline_value='';
}
$wrapper_classes = array(
    'g5plus-counter',
	$underline_value,
	$color_scheme,
    $this->getExtraClass( $el_class ),
    $this->getCSSAnimation( $css_animation )
);

if ( $styles ) {
    $wrapper_attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX  . 'counter', plugins_url(GF_PLUGIN_NAME . '/shortcodes/counter/assets/css/counter.min.css'), array(), false, 'all');
}

?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode( ' ', $wrapper_attributes ); ?>>
    <div class="ct-content">
		<?php if(!empty($counter_value)): ?>
            <span class="ct-value counter s-font fs-40 fw-bold" ><?php echo wp_kses_post($counter_value) ?></span>
        <?php endif;
        if(!empty($counter_title)): ?>
            <p class="ct-title fs-13 fw-bold"><?php echo wp_kses_post($counter_title) ?></p>
        <?php endif; ?>
    </div>
</div>