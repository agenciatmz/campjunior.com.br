<?php
return array(
    'name' => esc_html__('Counter', 'g5plus-arvo'),
    'base' => 'g5plus_counter',
    'class' => '',
    'icon' => 'fa fa-tachometer',
    'category' => GF_SHORTCODE_CATEGORY,
    'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Value', 'g5plus-arvo'),
			'param_name' => 'counter_value',
			'value' => '',
			'admin_label' => true,
		),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Title', 'g5plus-arvo'),
            'param_name' => 'counter_title',
            'value' => '',
            'admin_label' => true,
        ),
		array(
			'type' => 'checkbox',
            'heading' => esc_html__('Underline value ?', 'g5plus-arvo'),
            'param_name' => 'underline_value',
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
            'param_name' => 'color_scheme',
            'value' => array(
                esc_html__('Dark', 'g5plus-arvo') => 'ct-dark',
                esc_html__('Light', 'g5plus-arvo') => 'ct-light'),
            'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'std' => 'ct-dark',
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        vc_map_add_css_animation(),
        gf_vc_map_add_animation_duration(),
        gf_vc_map_add_animation_delay(),
        gf_vc_map_add_extra_class(),
        gf_vc_map_add_css_editor()
    ),
);