<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $text
 * @var $image
 * @var $link
 * @var $is_new
 * @var $is_coming_soon
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_View_Demo
 */
$text = $image = $link = $is_new = $is_coming_soon = $el_class = $css_animation = $duration = $delay = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$wrapper_attributes = array();
$wrapper_styles = array();
$is_coming_soon = $is_coming_soon ? 'coming-soon' : '';
$wrapper_classes = array(
	'g5plus-view-demo',
	$is_new,
	$is_coming_soon,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation ),
);
// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}

if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', $wrapper_styles) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX  . 'view-demo', plugins_url(GF_PLUGIN_NAME . '/shortcodes/view-demo/assets/css/view-demo.min.css'), array(), false, 'all');
}

$img_html = '';
$image_src = '';
if (!empty($image)) {
	$image_id = preg_replace( '/[^\d]/', '', $image );
	$image_srcs = wp_get_attachment_image_src( $image_id, 'full' );
	if ( ! empty( $image_srcs[0] ) ) {
		$image_src = $image_srcs[0];
		$img_html = '<img alt="'. the_title_attribute(array('post' => $image_id,'echo' => false )) .'" src="'. esc_url($image_src) .'">';
	}
} else {
	$img_html = '';
}

//parse link
$link_attributes = $title_attributes = array();
$link = ( '||' === $link ) ? '' : $link;
$link = vc_build_link( $link );
$use_link = false;
if($is_coming_soon != 'coming-soon') {
	if ( strlen( $link['url'] ) > 0 ) {
		$use_link = true;
		$link_attributes[] = 'href="' . esc_url( trim($link['url']) ) . '"';
		if(strlen($link['target']) >0) {
			$link_attributes[] = 'target="' . trim($link['target']) . '"';
		}
		if(strlen($link['rel']) >0) {
			$link_attributes[] = 'rel="' . trim($link['rel']) . '"';
		}
		if(strlen($link['title']) >0) {
			$link_attributes[] = 'title="' . trim($link['title']) . '"';
		}
	}
}
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<?php if($is_coming_soon != 'coming-soon'):?>
		<div class="view-demo-inner">
			<?php if($use_link): ?>
				<a <?php echo implode(' ', $link_attributes ); ?>></a>
			<?php endif;?>
			<div class="view-demo-image">
				<?php if($img_html != ''): ?>
					<?php echo wp_kses_post( $img_html );?>
				<?php endif; ?>
			</div>
			<?php if($is_new):?>
				<div class="new-demo p-font"><span><?php echo esc_html__( 'New', 'g5plus-arvo' )?></span></div>
			<?php endif;?>
		</div>
		<?php if(strlen( $link['title'] ) > 0): ?>
			<a <?php echo implode(' ', $link_attributes ); ?> class="s-font view-demo-title"><?php echo esc_html( $link['title'] )?></a>
		<?php endif; ?>
	<?php else:?>
		<div class="view-demo-inner">
			<div class="block-center">
				<div class="block-center-inner">
					<p class="coming-soon-text s-font"><?php echo esc_html__( 'COMING SOON', 'g5plus-arvo' );?></p>
				</div>
			</div>
			<div class="view-demo-image">
				<?php if($img_html != ''): ?>
					<?php echo wp_kses_post( $img_html );?>
				<?php endif; ?>
			</div>
		</div>
		<span class="s-font view-demo-title"><?php echo esc_html__('AND MORE', 'g5plus-arvo')?></span>
	<?php endif;?>
</div>