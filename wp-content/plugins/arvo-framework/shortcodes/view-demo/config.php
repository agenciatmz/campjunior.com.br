<?php
return array(
	'name' => esc_html__('View Demo', 'g5plus-arvo'),
    'base' => 'g5plus_view_demo',
    'icon' => 'fa fa-eye',
    'category' => GF_SHORTCODE_CATEGORY,
    'params' => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_html__('Image:', 'g5plus-arvo'),
			'param_name' => 'image',
			'value' => '',
		),
	    array(
		    'type'       => 'checkbox',
		    'heading'    => esc_html__('Is New', 'g5plus-arvo'),
		    'param_name' => 'is_new',
		    'edit_field_class' => 'vc_col-sm-6 vc_column'
	    ),
	    array(
		    'type'       => 'checkbox',
		    'heading'    => esc_html__('Is Coming Soon', 'g5plus-arvo'),
		    'param_name' => 'is_coming_soon',
		    'edit_field_class' => 'vc_col-sm-6 vc_column'
	    ),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__('Link (url)', 'g5plus-arvo'),
			'param_name' => 'link',
			'value' => ''
		),
	    vc_map_add_css_animation(),
	    gf_vc_map_add_animation_duration(),
	    gf_vc_map_add_animation_delay(),
	    gf_vc_map_add_extra_class(),
	    gf_vc_map_add_css_editor()
    )
);