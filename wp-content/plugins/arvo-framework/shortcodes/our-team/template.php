<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $member_name
 * @var $member_job
 * @var $member_avatar
 * @var $avatar_size
 * @var $social_profiles
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $css
 * @var $el_class
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Our_Team
 */
$member_name = $member_job = $member_avatar = $avatar_size = $social_profiles = $css_animation = $animation_duration = $animation_delay = $css = $el_class = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);

extract($atts);

$wrapper_attributes = array();
$wrapper_styles = array();

$wrapper_classes = array(
	'g5plus-ourteam',
	'clearfix',
	'text-center',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation($css_animation)
);

$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}
if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', $wrapper_styles) . '"';
}

$social_profiles = (array) vc_param_group_parse_atts( $social_profiles );

// icon html
$image_html = '';
if (!empty($member_avatar)) {
	$image_id = preg_replace( '/[^\d]/', '', $member_avatar );
	$image_full = wp_get_attachment_image_src( $image_id, 'full' );
	$image_src = '';
	if(sizeof( $image_full ) > 0) {
		$image_src = $image_full[0];
	}
	$image_html = '<img alt="'. the_title_attribute(array('post' => $image_id,'echo' => false )) .'" src="'. esc_url($image_src) .'">';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX. 'our-team', plugins_url(GF_PLUGIN_NAME . '/shortcodes/our-team/assets/css/ourteam.min.css'), array(), false);
}
?>
<div class="<?php echo esc_attr($css_class); ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<div class="ourteam-avatar">
		<?php if (!empty($member_avatar)):
			echo wp_kses_post( $image_html );
		endif;?>
	</div>
	<div class="ourteam-content">
		<?php if(count( $social_profiles ) > 0 ): ?>
			<div class="social-wrap">
				<div class="social-share-icon">
					<i class="fa fa-share-alt"></i>
				</div>
				<div class="social-profiles">
					<div class="social-profiles-inner">
						<?php foreach ($social_profiles as $social_profile):
							$link_attributes = $title_attributes = array();
							$link = ( '||' === $social_profile['link'] ) ? '' : $social_profile['link'];
							$link = vc_build_link( $link );
							if ( strlen( $link['url'] ) > 0 ) {
							$link_attributes[] = 'href="' . esc_url( trim($link['url']) ) . '"';
							if(strlen($link['target']) >0) {
							$link_attributes[] = 'target="' . trim($link['target']) . '"';
							}
							if(strlen($link['rel']) >0) {
							$link_attributes[] = 'rel="' . trim($link['rel']) . '"';
							}
							if(strlen($link['title']) >0) {
							$link_attributes[] = 'title="' . trim($link['title']) . '"';
							}?>
							<a <?php echo implode(' ', $link_attributes ); ?>>
								<i class="<?php echo esc_attr($social_profile['icon_font'])?>"></i>
							</a>
							<?php } ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if(!empty( $member_name )): ?><span class="s-font"><?php echo esc_attr( $member_name ) ?></span><?php endif; ?>
		<?php if(!empty( $member_job )): ?><p><?php echo esc_attr( $member_job ) ?></p><?php endif; ?>
	</div>
</div>

