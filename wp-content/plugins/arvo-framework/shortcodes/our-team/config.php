<?php
return array(
	'name'     => esc_html__('Our Team', 'g5plus-arvo'),
	'base'     => 'g5plus_our_team',
	'class'    => '',
	'icon'     => 'fa fa-users',
	'category' => GF_SHORTCODE_CATEGORY,
	'params'   => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Member Name', 'g5plus-arvo' ),
			'param_name' => 'member_name',
			'value' => '',
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Member Job', 'g5plus-arvo' ),
			'param_name' => 'member_job',
			'value' => '',
			'admin_label' => true,
		),
		array(
			'type' => 'attach_image',
			'heading'    => esc_html__('Select Member Avatar', 'g5plus-arvo'),
			'param_name' => 'member_avatar',
			'value' => '',
		),
		array(
			'type' => 'param_group',
			'heading' => esc_html__('Social Profiles','g5plus-arvo'),
			'param_name' => 'social_profiles',
			'description' => esc_html__('Enter Social Profiles For This Member - Title, Icon And Link','g5plus-arvo'),
			'value' => '',
			'params' => array(
				array_merge( \gf_vc_map_add_icon_font(),array(
					'admin_label' => true
				)),
				array(
					'type' => 'vc_link',
					'heading' => esc_html__('Link (url)', 'g5plus-arvo'),
					'param_name' => 'link',
					'value' => '',
				),
			)
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);