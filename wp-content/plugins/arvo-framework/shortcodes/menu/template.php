<?php
/**
 * Created by PhpStorm.
 * User: Kaga
 * Date: 13/8/2016
 * Time: 4:49 PM
 */

/**
 * Shortcode attributes
 * @var $atts
 * @var $layout_style
 * @var $images
 * @var $name
 * @var $price
 * @var $category
 * @var $columns
 * @var $color_scheme
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Menu
 */
$menu = $layout_style = $images = $name = $price = $category = $columns = $color_scheme = $css_animation = $animation_duration = $animation_delay = $el_class = $css ='';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$wrapper_attributes = array();
$styles = array();

$menu = (array) vc_param_group_parse_atts( $menu );

$wrapper_classes = array(
	'g5plus-menu',
	$layout_style,
	$color_scheme,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation ),
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}
$content_classes='';

if ($layout_style == 'menu-image')
{
	$columns_md = 'columns-md-3';
	$columns_sm = 'columns-sm-3';
	$columns_xs = 'columns-xs-2';
	if($columns == 2){
		$columns_sm = 'columns-sm-2';
	}
	if($columns == 1){
		$columns_sm = '';
		$columns_xs = '';
	}
	$content_classes = 'columns-'.esc_attr($columns).' '.$columns_md.' '.$columns_sm.' '.$columns_xs;
}
if ( $styles ) {
	$wrapper_attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX  . 'menu', plugins_url(GF_PLUGIN_NAME . '/shortcodes/menu/assets/css/menu.min.css'), array(), false, 'all');
}

$i=0;
$images_id = rand();
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<ul class="<?php echo esc_attr($content_classes)?>">
	<?php foreach ($menu as $data):
			$name = isset( $data['name'] ) ? $data['name'] : '';
			$price = isset( $data['price'] ) ? $data['price'] : '';
			$category = isset( $data['category'] ) ? $data['category'] : '';
			$images = isset( $data['images'] ) ? $data['images'] : '';

			$image_src = wp_get_attachment_image_src( $images, 'full' );
			if(sizeof( $image_src ) > 0) {
				$image_src = $image_src[0];
			}
			$image_thumb = wp_get_attachment_image_src($images);
			$image_thumb_link = '';
			if (sizeof($image_thumb) > 0) {
				$image_thumb_link = $image_thumb['0'];
			}
		?>
		<li class="gf-item-wrap">
			<?php if ($layout_style == 'menu-image'): ?>
				<div class="thumb">
					<a href="<?php echo esc_url($image_src) ?>"
					   data-thumb-src="<?php echo esc_url( $image_thumb_link ); ?>" data-rel="lightGallery" data-gallery-id="<?php echo esc_html( $images_id ) ?>">
						<i class="fa fa-search"></i>
					</a>
					<img src="<?php echo esc_url($image_src) ?>" alt="<?php echo esc_attr($name)?>"
						 title="<?php echo esc_attr($name)?>">
				</div>
			<?php endif; ?>
			<div class="menu-title">
				<h4 class="menu-name"><span><?php echo esc_attr($name)?></span></h4>
				<span class="menu-value"><?php echo esc_attr($price)?></span>
			</div>
			<p class="menu-category"><?php echo esc_attr($category)?></p>
		</li>
	<?php endforeach; ?>
	</ul>
</div>