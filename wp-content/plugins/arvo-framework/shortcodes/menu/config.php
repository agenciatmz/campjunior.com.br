<?php
/**
 * Created by PhpStorm.
 * User: Kaga
 * Date: 13/8/2016
 * Time: 4:48 PM
 */
return array(
	'name' => esc_html__('Menu', 'g5plus-arvo'),
	'base' => 'g5plus_menu',
	'icon' => 'fa fa-cutlery',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo' ),
			'param_name' => 'layout_style',
			'admin_label' => true,
			'value' => array(
				esc_html__('Menu Image', 'g5plus-arvo') => 'menu-image',
				esc_html__('Menu Lists', 'g5plus-arvo') => 'menu-lists',
			),
		),
		array(
			'type' => 'param_group',
			'heading' => esc_html__( 'Menu', 'g5plus-arvo' ),
			'param_name' => 'menu',
			'value' => urlencode( json_encode( array(
				array(
					'label' => esc_html__( 'Menu', 'g5plus-arvo' ),
					'value' => '',
				),
			) ) ),
			'params' => array(
				array(
					'type' => 'attach_images',
					'heading' => esc_html__('Images', 'g5plus-arvo'),
					'param_name' => 'images',
					'value' => '',
					'description' => esc_html__('Select images ( Only applies to Menu Image )', 'g5plus-arvo'),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Name', 'g5plus-arvo'),
					'param_name' => 'name',
					'admin_label' => true,
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Price', 'g5plus-arvo'),
					'param_name' => 'price',
					'admin_label' => true,
				),
				array(
					'type' => 'textarea',
					'heading' => esc_html__('Categories', 'g5plus-arvo'),
					'param_name' => 'category',
					'description' => esc_html__('Product categories', 'g5plus-arvo'),
				),
			),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns Show ?', 'g5plus-arvo' ),
			'param_name' => 'columns',
			'value' => array(
				'1 Columns' => '',
				'2 Columns' => '2',
				'3 Columns' => '3',
				'4 Columns' => '4',
				'5 Columns' => '5',
				'6 Columns' => '6',
			),
			'std' => '3',
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'layout_style', 'value' => 'menu-image'),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
			'param_name' => 'color_scheme',
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'menu-dark',
				esc_html__('Light', 'g5plus-arvo') => 'menu-light'),
			'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'std' => 'menu-dark',
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);