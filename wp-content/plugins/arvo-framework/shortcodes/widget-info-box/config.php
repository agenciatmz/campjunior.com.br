<?php
return array(
	'base' => 'g5plus_widget_info_box',
	'name' => esc_html__('Widget Info Box','g5plus-arvo'),
	'icon' => 'fa fa-info-circle',
	'category' => GF_SHORTCODE_WIDGET_CATEGORY,
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Widget title', 'g5plus-arvo' ),
			'param_name' => 'title',
			'description' => esc_html__('What text use as a widget title. Leave blank to use default widget title.', 'g5plus-arvo' ),
			'value' => '',
			'admin_label' => true
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout', 'g5plus-arvo'),
			'param_name' => 'layout',
			'value' => array(
				esc_html__('Vertical', 'g5plus-arvo') => 'vertical',
				esc_html__('Horizontal', 'g5plus-arvo') => 'horizontal',
			),
			'std' => 'vertical',
			'admin_label' => true
		),
		array(
			'type' => 'param_group',
			'heading' => esc_html__('Values','g5plus-arvo'),
			'param_name' => 'values',
			'description' => esc_html__('Enter values for icon - title and description','g5plus-arvo'),
			'value' => '',
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Title', 'g5plus-arvo' ),
					'param_name' => 'text',
					'value' => '',
					'admin_label' => true
				),
				array_merge(gf_vc_map_add_icon_font(),array('param_name' => 'icon'))
			)
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	),
);