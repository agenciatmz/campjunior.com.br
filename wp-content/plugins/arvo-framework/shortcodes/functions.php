<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 4/5/2016
 * Time: 8:06 AM
 */



//////////////////////////////////////////////////////////////////
// Add theme icon
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_add_theme_icon')) {
	function gf_add_theme_icon($icons){
		$icons['Font Theme Icon'] = &gf_get_theme_font();
		$icons['Font pe 7 stroke'] = &gf_get_font_pe_icon_7_stroke();
		return $icons;
	}
	add_filter('vc_iconpicker-type-fontawesome','gf_add_theme_icon');
}

//////////////////////////////////////////////////////////////////
// Animation Duration Param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_animation_duration')) {
	function gf_vc_map_add_animation_duration(){
		return array(
			'type' => 'textfield',
			'heading' => esc_html__('Animation Duration', 'g5plus-arvo'),
			'param_name' => 'animation_duration',
			'value' => '',
			'description' => wp_kses_post(__('Duration in seconds. You can use decimal points in the value. Use this field to specify the amount of time the animation plays. <em>The default value depends on the animation, leave blank to use the default.</em>', 'g5plus-arvo')),
			'dependency' => array('element' => 'css_animation', 'value' => array('top-to-bottom','bottom-to-top','left-to-right','right-to-left','appear')),
		);
	}
}

//////////////////////////////////////////////////////////////////
// Animation Delay Param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_animation_delay')) {
	function gf_vc_map_add_animation_delay(){
		return array(
			'type' => 'textfield',
			'heading' => esc_html__('Animation Delay', 'g5plus-arvo'),
			'param_name' => 'animation_delay',
			'value' => '',
			'description' => esc_html__('Delay in seconds. You can use decimal points in the value. Use this field to delay the animation for a few seconds, this is helpful if you want to chain different effects one after another above the fold.', 'g5plus-arvo'),
			'dependency' => array('element' => 'css_animation', 'value' => array('top-to-bottom','bottom-to-top','left-to-right','right-to-left','appear')),
		);
	}
}

//////////////////////////////////////////////////////////////////
// Extra Class Param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_extra_class')) {
	function gf_vc_map_add_extra_class(){
		return array(
			'type' => 'textfield',
			'heading' => esc_html__('Extra class name', 'g5plus-arvo'),
			'param_name' => 'el_class',
			'description' => esc_html__('Style particular content element differently - add a class name and refer to it in custom CSS.', 'g5plus-arvo'),
		);
	}
}

//////////////////////////////////////////////////////////////////
// Css Editor Param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_css_editor')) {
	function gf_vc_map_add_css_editor(){
		return array(
			'type' => 'css_editor',
			'heading' => esc_html__('CSS box', 'g5plus-arvo'),
			'param_name' => 'css',
			'group' => esc_html__('Design Options', 'g5plus-arvo'),
		);
	}
}

//////////////////////////////////////////////////////////////////
// Icon Type Param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_icon_type')) {
	function gf_vc_map_add_icon_type($dependency = array()){
		return array(
			'type' => 'dropdown',
			'heading' => esc_html__('Icon library', 'g5plus-arvo'),
			'value' => array(
				esc_html__('Icon', 'g5plus-arvo') => 'icon',
				esc_html__('Image', 'g5plus-arvo') => 'image',
			),
			'param_name' => 'icon_type',
			'description' => esc_html__('Select icon library.', 'g5plus-arvo'),
			'dependency' => $dependency,
			'edit_field_class' => 'vc_col-sm-6 vc_column',
		);
	}
}

//////////////////////////////////////////////////////////////////
// Icon Font Awesome
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_icon_font')) {
	function gf_vc_map_add_icon_font($dependency = array()){
		if (count($dependency) == 0) {
			$dependency = array('element' => 'icon_type','value' => 'icon');
		}
		return  array(
			'type' => 'iconpicker',
			'heading' => esc_html__('Icon', 'g5plus-arvo'),
			'param_name' => 'icon_font',
			'value' => 'fa fa-adjust', // default value to backend editor admin_label
			'settings' => array(
				'emptyIcon' => false,
				// default true, display an "EMPTY" icon?
				'iconsPerPage' => 100,
				'type' => 'fontawesome'
				// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
			),
			'dependency' => $dependency,
			'description' => esc_html__('Select icon from library.', 'g5plus-arvo')
		);

	}
}

//////////////////////////////////////////////////////////////////
// Icon Images
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_icon_image')) {
	function gf_vc_map_add_icon_image(){
		return array(
			'type' => 'attach_image',
			'heading' => esc_html__('Upload Image Icon:', 'g5plus-arvo'),
			'param_name' => 'icon_image',
			'value' => '',
			'description' => esc_html__('Upload the custom image icon.', 'g5plus-arvo'),
			'dependency' => array('element' => 'icon_type','value' => 'image'),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
		);
	}
}

//////////////////////////////////////////////////////////////////
// Narrow Category
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_narrow_category')){
	function gf_vc_map_add_narrow_category(){
		$category = array();
		$categories = get_categories();
		if (is_array($categories)) {
			foreach ($categories as $cat) {
				$category[$cat->name] = $cat->slug;
			}
		}
		return array(
			'type' => 'select2',
			'heading' => esc_html__('Narrow Category', 'g5plus-arvo'),
			'param_name' => 'category',
			'options' => $category,
			'multiple' => true,
			'description' => esc_html__( 'Enter categories by names to narrow output (Note: only listed categories will be displayed, divide categories with linebreak (Enter)).', 'g5plus-arvo' ),
			"admin_label" => true,
			'std' => ''
		);
	}
}

//////////////////////////////////////////////////////////////////
// Narrow Portfolio Category
//////////////////////////////////////////////////////////////////
if (!function_exists('g5plus_vc_map_add_narrow_portfolio_category')){
	function g5plus_vc_map_add_narrow_portfolio_category(){
		$category = array();
		$categories = get_categories(array('taxonomy' => G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY , 'hide_empty' => 0, 'orderby' => 'ASC'));
		if (is_array($categories)) {
			foreach ($categories as $cat) {
				$category[$cat->name] = $cat->slug;
			}
		}
		return array(
			'type' => 'select2',
			'heading' => esc_html__('Narrow Category', 'g5plus-arvo'),
			'param_name' => 'category',
			'options' => $category,
			'multiple' => true,
			'description' => esc_html__( 'Enter categories by names to narrow output (Note: only listed categories will be displayed, divide categories with linebreak (Enter)).', 'g5plus-arvo' ),
			'std' => ''
		);
	}
}

//////////////////////////////////////////////////////////////////
// Custom params vc_row
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_vc_row')){
	function gf_custom_param_vc_row(){
		$vc_row = WPBMap::getShortCode('vc_row');
		$vc_row_params = $vc_row['params'];
		$index = 100;
		$background_overlay_index = 0;
		foreach($vc_row_params as $key => $param){
			$param['weight'] = $index;
			if ($param['param_name'] == 'parallax_speed_bg') {
				$background_overlay_index = $index - 1;
				$index = $index - 1;
			}
			vc_update_shortcode_param( 'vc_row', $param );
			$index--;
		}
		$params = array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show background overlay', 'g5plus-arvo'),
				'param_name' => 'overlay_mode',
				'description' => esc_html__('Hide or Show overlay on background images.', 'g5plus-arvo'),
				'value' => array(
					esc_html__('Hide, please', 'g5plus-arvo') => '',
					esc_html__('Show Overlay Color', 'g5plus-arvo') => 'color',
					esc_html__('Show Overlay Image', 'g5plus-arvo') => 'image',
				),
				'weight' => $background_overlay_index
			),
			array(
				'type' => 'colorpicker',
				'heading' => esc_html__('Overlay color', 'g5plus-arvo'),
				'param_name' => 'overlay_color',
				'description' => esc_html__('Select color for background overlay.', 'g5plus-arvo'),
				'value' => '',
				'dependency' => array('element' => 'overlay_mode', 'value' => array('color')),
				'weight' => ($background_overlay_index - 1)
			),
			array(
				'type' => 'attach_image',
				'heading' => esc_html__('Image Overlay:', 'g5plus-arvo'),
				'param_name' => 'overlay_image',
				'value' => '',
				'description' => esc_html__('Upload image overlay.', 'g5plus-arvo'),
				'dependency' => array('element' => 'overlay_mode', 'value' => array('image')),
				'weight' => ($background_overlay_index - 2)
			),
			array(
				'type' => 'number',
				'class' => '',
				'heading' => esc_html__('Overlay opacity', 'g5plus-arvo'),
				'param_name' => 'overlay_opacity',
				'value' => '50',
				'min' => '1',
				'max' => '100',
				'suffix' => '%',
				'description' => esc_html__('Select opacity for overlay.', 'g5plus-arvo'),
				'dependency' => array('element' => 'overlay_mode', 'value' => array('image')),
				'weight' => ($background_overlay_index - 3)
			),
		);
		vc_add_params( 'vc_row', $params );

		$full_width = array(
			esc_html__('Default (no paddings)','g5plus-arvo') => 'row_content_no_spaces'
		);
		$param_full_width = WPBMap::getParam('vc_row','full_width');
		$param_full_width['value'] = array_merge($param_full_width['value'],$full_width);
		$param_full_width['std'] = '';
		vc_update_shortcode_param( 'vc_row', $param_full_width );

	}
	add_action( 'vc_after_init', 'gf_custom_param_vc_row' );
}

//////////////////////////////////////////////////////////////////
// Custom VC Columns
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_vc_column')) {
	function gf_custom_param_vc_column(){
		vc_add_param( 'vc_column', vc_map_add_css_animation() );
		vc_add_param( 'vc_column_inner', vc_map_add_css_animation() );
	}
	add_action( 'vc_after_init', 'gf_custom_param_vc_column' );
}

if (!function_exists('gf_custom_vc_column_animation')) {
	function gf_custom_vc_column_animation($css_classes,$base,$atts){
		if ($base == 'vc_column' || $base == 'vc_column_inner') {
			$css_animation = $atts['css_animation'];
			if (!empty($css_animation)) {
				wp_enqueue_script( 'waypoints' );
				$css_classes .= ' wpb_animate_when_almost_visible wpb_' . $css_animation;
			}
		}
		return $css_classes;
	}
	add_filter(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG,'gf_custom_vc_column_animation',10,3);
}

//////////////////////////////////////////////////////////////////
// Custom vc_tta_accordion param
//////////////////////////////////////////////////////////////////
if (!function_exists('g5plus_custom_param_vc_tta_accordion')) {
	function g5plus_custom_param_vc_tta_accordion(){
		$styles = array(
			esc_html__('Classic No Fill Color','g5plus-arvo') => 'classic vc_tta-no-fill-color'
		);
		$param_style = WPBMap::getParam('vc_tta_accordion','style');
		$param_style['value'] = array_merge($param_style['value'],$styles);
		$param_style['std'] = 'classic';
		vc_update_shortcode_param( 'vc_tta_accordion', $param_style );

		$colors = array(
			esc_html__( 'Primary', 'g5plus-arvo' ) => 'primary'
		);
		$param_color = WPBMap::getParam('vc_tta_accordion','color');
		$param_color['value'] = array_merge($colors,$param_color['value']);
		vc_update_shortcode_param( 'vc_tta_accordion', $param_color );

	}
	add_action( 'vc_after_init', 'g5plus_custom_param_vc_tta_accordion' );
}

//////////////////////////////////////////////////////////////////
// Custom vc_toggle param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_vc_toggle')) {
	function gf_custom_param_vc_toggle() {
		$style =  array(
			esc_html__('Rounded','g5plus-arvo') => 'rounded',
			esc_html__('Rounded Outline','g5plus-arvo') => 'rounded_outline',
			esc_html__('Square','g5plus-arvo') => 'square',
			esc_html__('Square Outline','g5plus-arvo') => 'square_outline',
			esc_html__('Text Only','g5plus-arvo') => 'text_only',
		);

		$param_style = WPBMap::getParam('vc_toggle','style');
		$param_style['value'] = $style;
		$param_style['std'] = 'square_outline';
		vc_update_shortcode_param( 'vc_toggle', $param_style );

		$colors = array(
			esc_html__( 'Primary', 'g5plus-arvo' ) => 'primary'
		);
		$param_color = WPBMap::getParam('vc_toggle','color');
		$param_color['value'] = array_merge($colors,$param_color['value']);
		$param_color['heading'] = esc_html__('Color','g5plus-arvo');
		$param_color['std'] = 'default';
		vc_update_shortcode_param( 'vc_toggle', $param_color );

	}
	add_action( 'vc_after_init', 'gf_custom_param_vc_toggle' );
}

//////////////////////////////////////////////////////////////////
// Custom param vc_tta_tabs
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_vc_tta_tabs')) {
	function gf_custom_param_vc_tta_tabs()
	{
		$styles = array(
			esc_html__('Vertical Bar', 'g5plus-arvo') => 'vertical-bar',
			esc_html__('Border Top Active', 'g5plus-arvo') => 'border-top-active'
		);
		$param_style = WPBMap::getParam('vc_tta_tabs', 'style');
		$param_style['value'] = array_merge($param_style['value'], $styles);
		$param_style['std'] = 'classic';
		vc_update_shortcode_param('vc_tta_tabs', $param_style);

		$shapes = array(
			esc_html__('None', 'g5plus-arvo') => 'none',
		);

		$param_shape = WPBMap::getParam('vc_tta_tabs', 'shape');
		$param_shape['value'] = array_merge($param_shape['value'], $shapes);
		vc_update_shortcode_param('vc_tta_tabs', $param_shape);

		$colors = array(
			esc_html__('Primary', 'g5plus-arvo') => 'primary'
		);
		$param_color = WPBMap::getParam('vc_tta_tabs', 'color');
		$param_color['value'] = array_merge($colors, $param_color['value']);
		$param_color['heading'] = esc_html__('Color', 'g5plus-arvo');
		$param_color['std'] = 'primary';
		vc_update_shortcode_param('vc_tta_tabs', $param_color);
	}

	add_action( 'vc_after_init', 'gf_custom_param_vc_tta_tabs' );
}

//////////////////////////////////////////////////////////////////
// Custom icon param
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_icon')) {
	function gf_custom_param_icon() {
		$icons = array(
			'icon_fontawesome',
			'icon_openiconic',
			'icon_entypo',
			'icon_linecons',
			'icon_monosocial'
		);
		$shortcodes = array(
			'vc_tta_section'
		);
		foreach ($shortcodes as $shortcode) {
			foreach ($icons as $icon) {
				${$icon} = WPBMap::getParam($shortcode,'i_' . $icon);
				${$icon}['settings']['iconsPerPage'] = 50;
				vc_update_shortcode_param( $shortcode, ${$icon} );
			}
		}
	}
	add_action( 'vc_after_init', 'gf_custom_param_icon' );
}

//////////////////////////////////////////////////////////////////
// Get Slider
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_slider')) {
	function gf_vc_map_add_slider(){
		return array(
			'type' => 'checkbox',
			'heading' => esc_html__('Display Slider?', 'g5plus-arvo' ),
			'param_name' => 'is_slider',
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		);
	}
}

//////////////////////////////////////////////////////////////////
// Get Pagination
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_pagination')) {
	function gf_vc_map_add_pagination(){
		return array(
			'type' => 'checkbox',
			'heading' => esc_html__('Show pagination control', 'g5plus-arvo'),
			'param_name' => 'dots',
			'std' => 'false',
			'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		);
	}
}

//////////////////////////////////////////////////////////////////
// Get  Pagination Style
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_pagination_style')) {
	function gf_vc_map_add_pagination_style(){
		return array(
			'type' => 'dropdown',
			'heading' => esc_html__('Pagination Style', 'g5plus-arvo'),
			'param_name' => 'dot_style',
			'value' => array(
				esc_html__('Round','g5plus-arvo') => 'round',
				esc_html__('Line','g5plus-arvo') => 'line'
			),
			'std' => 'line',
			'dependency' => array('element' => 'dots', 'value' => 'true'),
		);
	}
}

//////////////////////////////////////////////////////////////////
// Get  Pagination Style
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_pagination_position')) {
	function gf_vc_map_add_pagination_position(){
		return array(
			'type' => 'dropdown',
			'heading' => esc_html__('Pagination Position', 'g5plus-arvo'),
			'param_name' => 'dot_position',
			'value' => array(
				esc_html__('Vertical Center','g5plus-arvo') => 'vertical',
				esc_html__('Bottom','g5plus-arvo') => 'bottom'
			),
			'std' => 'bottom',
			'dependency' => array('element' => 'dots', 'value' => 'true'),
		);
	}
}

//////////////////////////////////////////////////////////////////
// Get Navigation
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_navigation')) {
	function gf_vc_map_add_navigation(){
		return array(
			'type' => 'checkbox',
			'heading' => esc_html__('Show navigation control', 'g5plus-arvo'),
			'param_name' => 'nav',
			'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			'std' => 'true',
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		);
	}
}
//////////////////////////////////////////////////////////////////
// Get Navigation Position
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_navigation_position')) {
	function gf_vc_map_add_navigation_position(){
		return array(
			'type' => 'dropdown',
			'heading' => esc_html__('Navigation Position', 'g5plus-arvo'),
			'param_name' => 'nav_position',
			'value' => array(
				esc_html__('Top (100px)','g5plus-arvo') => 'top',
				esc_html__('Top (68px)','g5plus-arvo') => 'top-68',
				esc_html__('Center','g5plus-arvo') => 'center',
				esc_html__('Center Inner','g5plus-arvo') => 'center-inner',
				esc_html__('Bottom','g5plus-arvo') => 'bottom'
			),
			'std' => 'center',
			'dependency' => array('element' => 'nav', 'value' => 'true'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		);
	}
}


//////////////////////////////////////////////////////////////////
// Get Navigation Style
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_navigation_style')) {
	function gf_vc_map_add_navigation_style(){
		return array(
			'type' => 'dropdown',
			'heading' => esc_html__('Navigation Style', 'g5plus-arvo'),
			'param_name' => 'nav_style',
			'value' => array(
				esc_html__('Classic','g5plus-arvo') => 'classic',
				esc_html__('Round','g5plus-arvo') => 'round',
				esc_html__('Round Large','g5plus-arvo') => 'round-large'
			),
			'std' => 'round',
			'dependency' => array('element' => 'nav', 'value' => 'true'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		);
	}
}
//////////////////////////////////////////////////////////////////
// Custom vc_icon param
//////////////////////////////////////////////////////////////////

if (!function_exists('gf_custom_param_vc_icon')) {
	function gf_custom_param_vc_icon() {
		$align = array(
			esc_html__( 'Inline', 'g5plus-arvo' ) => 'inline',
			esc_html__( 'Inline Block', 'g5plus-arvo' ) => 'inline-block'
		);
		$param_align = WPBMap::getParam('vc_icon','align');
		$param_align['value'] = array_merge($align,$param_align['value']);
		vc_update_shortcode_param( 'vc_icon', $param_align );

	}
	add_action( 'vc_after_init', 'gf_custom_param_vc_icon' );
}

//////////////////////////////////////////////////////////////////
// Custom vc_progress_bars
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_vc_progress_bar')){
	function gf_custom_param_vc_progress_bar(){
		$vc_progress_bar = WPBMap::getShortCode('vc_progress_bar');
		$vc_progress_bar_params = $vc_progress_bar['params'];
		$index = 100;
		$background_overlay_index = 0;
		foreach($vc_progress_bar_params as $key => $param){
			$param['weight'] = $index;
			if ($param['param_name'] == 'bgcolor') {
				$background_overlay_index = $index - 1;
				$index = $index - 1;
			}
			vc_update_shortcode_param( 'vc_progress_bar', $param );
			$index--;
		}
		$params = array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Layout Style', 'g5plus-arvo'),
				'param_name' => 'layout_style',
				'value' => array(
					esc_html__('Normal', 'g5plus-arvo') => '',
					esc_html__('Value move', 'g5plus-arvo') => 'prb_vl_move',
					esc_html__('Value right position', 'g5plus-arvo') => 'prb_vl_right'
				),
				'admin_label' => true,
				'description' => esc_html__('Select Layout Style.', 'g5plus-arvo'),
				'weight' => ($background_overlay_index + $background_overlay_index),
			),
		);
		vc_add_params( 'vc_progress_bar', $params );
	}
	add_action( 'vc_after_init', 'gf_custom_param_vc_progress_bar' );
}

//////////////////////////////////////////////////////////////////
// Custom vc_mesage_box
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_custom_param_vc_message')){
	function gf_custom_param_vc_message(){
		$param_color = WPBMap::getParam('vc_message','color');
		$param_color_options = array();
		foreach ($param_color['options'] as $option) {
			if ($option['value'] == 'info') {
				$option['params'] = array_merge($option['params'],array('icon_fontawesome' => 'fa fa-info-circle'));
			}
			if ($option['value'] == 'warning') {
				$option['params'] = array_merge($option['params'],array('icon_fontawesome' => 'fa fa-exclamation-triangle'));
			}
			if ($option['value'] == 'success') {
				$option['params'] = array_merge($option['params'],array('icon_fontawesome' => 'fa fa-check'));
			}
			if ($option['value'] == 'danger') {
				$option['params'] = array_merge($option['params'],array('icon_fontawesome' => 'fa fa-exclamation-circle'));
			}
			$param_color_options[] = $option;
		}
		$param_color['options'] = $param_color_options;
		vc_update_shortcode_param( 'vc_message', $param_color );
	}
	add_action( 'vc_after_init', 'gf_custom_param_vc_message' );
}


//////////////////////////////////////////////////////////////////
// Get Portfolio ids by slugs
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_portfolio_ids_by_slugs')) {
	function gf_get_portfolio_ids_by_slugs($slugs){
		global $wpdb;
		$slugs = preg_split("/[\s,]+/",$slugs);
		$format = implode(', ', array_fill(0, count($slugs), '%s'));
		$query = "SELECT p.ID as ID
		FROM {$wpdb->posts} as p
		WHERE (p.post_type = 'portfolio')
			  AND (p.post_status = 'publish')
			  AND (p.post_name IN({$format}))";
		$portfolio = $wpdb->get_results( $wpdb->prepare($query, $slugs) );
		$portfolio_ids = array_unique( array_map( 'absint', array_merge( wp_list_pluck( $portfolio, 'ID' ) ) ) );
		return $portfolio_ids;
	}
}


//////////////////////////////////////////////////////////////////
// Get Product Id by slug
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_portfolio_id_by_slug')) {
	function gf_get_portfolio_id_by_slug( $slug ) {
		global $wpdb;
		$portfolio_id = $wpdb->get_var( $wpdb->prepare( "
		SELECT posts.ID
		FROM $wpdb->posts AS posts
		WHERE posts.post_type = 'portfolio'
		AND posts.post_name = '%s'
		LIMIT 1
	 ", $slug ) );
		return ( $portfolio_id ) ? intval( $portfolio_id ) : 0;
	}
}

//////////////////////////////////////////////////////////////////
// Order By Slug
//////////////////////////////////////////////////////////////////
if(!function_exists('gf_order_by_slug')){
	function gf_order_by_slug($orderby,$query) {
		global $wpdb;
		$post_name_in = implode("','",$query->query['post_name__in']);
		$post_name_in = str_replace( ' ', '' , $post_name_in );
		$orderby = "FIELD( {$wpdb->posts}.post_name, '$post_name_in')";
		return $orderby;
	}
}

//////////////////////////////////////////////////////////////////
// Narrow Product Category
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_vc_map_add_narrow_product_category')){
	function gf_vc_map_add_narrow_product_category(){
		$category = array();
		$categories = get_categories(array('taxonomy' => 'product_cat', 'hide_empty' => false));
		if (is_array($categories)) {
			foreach ($categories as $cat) {
				$category[$cat->name] = $cat->slug;
			}
		}
		return array(
			'type' => 'select2',
			'heading' => esc_html__('Narrow Category', 'g5plus-arvo'),
			'param_name' => 'category',
			'options' => $category,
			'multiple' => true,
			'description' => esc_html__( 'Enter categories by names to narrow output (Note: only listed categories will be displayed, divide categories with linebreak (Enter)).', 'g5plus-arvo' ),
			"admin_label" => true,
			'std' => ''
		);
	}
}

//////////////////////////////////////////////////////////////////
// Get Product ids by slugs
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_product_ids_by_slugs')) {
	function gf_get_product_ids_by_slugs($slugs){
		global $wpdb;
		$slugs = preg_split("/[\s,]+/",$slugs);
		$format = implode(', ', array_fill(0, count($slugs), '%s'));
		$query = "SELECT p.ID as ID
		FROM {$wpdb->posts} as p
		WHERE (p.post_type = 'product')
			  AND (p.post_status = 'publish')
			  AND (p.post_name IN({$format}))";
		$products = $wpdb->get_results( $wpdb->prepare($query, $slugs) );
		$product_ids = array_unique( array_map( 'absint', array_merge( wp_list_pluck( $products, 'ID' ) ) ) );
		return $product_ids;
	}
}

//////////////////////////////////////////////////////////////////
// Get Product Id by slug
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_product_id_by_slug')) {
	function gf_get_product_id_by_slug( $slug ) {
		global $wpdb;
		$product_id = $wpdb->get_var( $wpdb->prepare( "
		SELECT posts.ID
		FROM $wpdb->posts AS posts
		WHERE posts.post_type = 'product'
		AND posts.post_name = '%s'
		LIMIT 1
	 ", $slug ) );
		return ( $product_id ) ? intval( $product_id ) : 0;
	}
}


//////////////////////////////////////////////////////////////////
// Get Woocommerce Loop Variable
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_woocommerce_loop')) {
	function &gf_get_woocommerce_loop(){
		if (class_exists('G5Plus_Woocommerce')) {
			return G5Plus_Woocommerce::get_woocommerce_loop();
		}
		return array(
			'layout' => '',
			'columns' => '',
			'columns_md' => '',
			'columns_sm' => '',
			'columns_xs' => '',
			'columns_mb' => '',
			'rows' => '',
			'dots' => 'false',
			'dot_style' => '',
			'nav' => 'true',
			'nav_position' => 'top',
			'nav_style' => '',
			'owl_scheme' => ''
		);
	}
}

//////////////////////////////////////////////////////////////////
// Get Woocommerce Loop Variable
//////////////////////////////////////////////////////////////////
if (!function_exists('get_profiles')) {
	function get_profiles() {
		$ret      = array();
		$profiles = gf_get_social_profiles();
		foreach ( $profiles as $profile ) {
			$ret[ $profile['title'] ] = $profile['id'];
		}

		return $ret;
	}
}
