<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $values
 * @var $layout_style
 * @var $color_scheme
 * @var $nav
 * @var $nav_position
 * @var $nav_style
 * @var $dots
 * @var $dot_style
 * @var $dot_position
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Testimonials
 */

$values = $layout_style = $color_scheme = $nav = $nav_position = $nav_style = $dots = $dot_style = $dot_position = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$values = (array) vc_param_group_parse_atts( $values );

$wrapper_attributes = array();
$wrapper_styles = array();

if($layout_style=='tt_grid_bg'){
	$color_scheme = '';
}
$wrapper_classes = array(
	'g5plus-testimonials',
	$layout_style,
	$color_scheme,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation ),
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}

if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', $wrapper_styles) . '"';
}
	$content_classes[] = 'owl-carousel';

	if ($nav) {
		$content_classes[] = 'owl-nav-'. $nav_position;
		$content_classes[] = 'owl-nav-' . $nav_style;
	}

	if ($dots) {
		$content_classes[] = 'owl-dot-' . $dot_style;
		$content_classes[] = 'owl-dot-' . $dot_position;
	}

	$owl_responsive_attributes = array();
	// Mobile <= 480px
	$owl_responsive_attributes[] = '"0" : {"items" : ' . $items_mb . '}';

	// Extra small devices ( < 768px)
	$owl_responsive_attributes[] = '"481" : {"items" : ' . $items_xs . '}';

	// Small devices Tablets ( < 992px)
	$owl_responsive_attributes[] = '"768" : {"items" : ' . $items_sm . '}';

	// Medium devices ( < 1199px)
	$owl_responsive_attributes[] = '"992" : {"items" : ' . $items_md . '}';

	// Medium devices ( > 1199px)
	$owl_responsive_attributes[] = '"1200" : {"items" : ' . $items_lg . '}';

if($layout_style=='tt_scroller'){
	$owl_attributes = array(
		'"dots": ' . ($dots ? 'true' : 'false'),
		'"nav": ' . ($nav ? 'true' : 'false'),
		'"items": 1',
		'"animateOut": "fadeOut"'
	);
}
else
{
	$owl_attributes = array(
		'"autoHeight": true',
		'"dots": ' . ($dots ? 'true' : 'false'),
		'"nav": ' . ($nav ? 'true' : 'false'),
		'"responsive": {'. implode(', ', $owl_responsive_attributes) . '}',
		'"animateOut": "fadeOut"'
	);
}

$content_attributes[] = "data-plugin-options='{". implode(', ', $owl_attributes) ."}'";

$css_content_class = implode(' ', array_filter($content_classes));

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX  . 'testimonials', plugins_url(GF_PLUGIN_NAME . '/shortcodes/testimonials/assets/css/testimonials.min.css'), array(), false, 'all');
}
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<?php if ($layout_style == 'tt_grid_bg'): ?>
		<div class="row">
	<?php endif; ?>
			<div class="<?php echo esc_attr($css_content_class);?>" <?php echo implode(' ', $content_attributes) ?>>
				<?php foreach ($values as $data): ?>
					<?php $avatar = isset( $data['avatar'] ) ? $data['avatar'] : ''; ?>
					<?php $quote = isset( $data['quote'] ) ? $data['quote'] : ''; ?>
					<?php $author = isset( $data['author'] ) ? $data['author'] : ''; ?>
					<?php $author_info = isset( $data['author_info'] ) ? $data['author_info'] : ''; ?>
					<?php $image = wp_get_attachment_image_src( $avatar, 'full' ); ?>
					<div class="testimonial-item">
							<?php if (!empty($image[0])): ?>
								<div class="avatar">
									<img alt="<?php echo esc_attr($author)?>" src="<?php echo esc_url($image[0])?>"/>
								</div>
							<?php endif; ?>
						<div class="info">
							<?php if ($layout_style != 'tt_grid'): ?>
								<p><?php echo esc_html($quote)?></p>
							<?php endif; ?>
							<?php if (!empty($author)): ?>
								<h4><?php echo esc_html($author)?></h4>
							<?php endif; ?>
							<?php if (!empty($author_info)): ?>
								<span><?php echo wp_kses_post($author_info); ?></span>
							<?php endif; ?>
							<?php if ($layout_style == 'tt_grid'): ?>
								<p><?php echo esc_html($quote)?></p>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
	<?php if ($layout_style == 'tt_grid_bg'): ?>
		</div>
	<?php endif; ?>
</div>