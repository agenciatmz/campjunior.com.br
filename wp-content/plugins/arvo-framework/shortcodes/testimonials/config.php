<?php
return array(
    'name' => esc_html__( 'Testimonials', 'g5plus-arvo' ),
    'base' => 'g5plus_testimonials',
    'icon' => 'fa fa-user',
    'category' => GF_SHORTCODE_CATEGORY,
    'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo'),
			'param_name' => 'layout_style',
			'admin_label' => true,
			'value' => array(
				esc_html__('Testimonials Scroller', 'g5plus-arvo') => 'tt_scroller',
				esc_html__('Testimonials Grid', 'g5plus-arvo') => 'tt_grid',
				esc_html__('Testimonials Grid Background Item', 'g5plus-arvo') => 'tt_grid_bg'),
			'description' => esc_html__('Select Layout Style.', 'g5plus-arvo')
		),
        array(
            'type' => 'param_group',
            'heading' => esc_html__( 'Testimonials', 'g5plus-arvo' ),
            'param_name' => 'values',
            'value' => urlencode( json_encode( array(
                array(
                    'label' => esc_html__( 'Author', 'g5plus-arvo' ),
                    'value' => '',
                ),
            ) ) ),
            'params' => array(
				array(
					'type' => 'attach_image',
					'heading' => esc_html__('Avatar:', 'g5plus-arvo'),
					'param_name' => 'avatar',
					'value' => '',
					'description' => esc_html__('Choose the author picture.', 'g5plus-arvo'),
					'dependency' => array('element' => 'layout_style','value' => array( 'tt_scroller','tt_grid' ))
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Author Name', 'g5plus-arvo'),
					'param_name' => 'author',
					'admin_label' => true,
					'description' => esc_html__('Enter Author information.', 'g5plus-arvo'),
					'dependency' => array('element' => 'layout_style','value' => array( 'tt_scroller','tt_grid' ))
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Author Info', 'g5plus-arvo'),
					'param_name' => 'author_info',
					'admin_label' => true,
					'description' => esc_html__('Enter Author information.', 'g5plus-arvo'),
					'dependency' => array('element' => 'layout_style','value' => array( 'tt_scroller','tt_grid' ))
				),
				array(
					'type' => 'textarea',
					'heading' => esc_html__('Quote from author', 'g5plus-arvo'),
					'param_name' => 'quote',
					'value' => ''
				),
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
            'param_name' => 'color_scheme',
            'value' => array(
				esc_html__('Light', 'g5plus-arvo') => 'color-light',
				esc_html__('Dark', 'g5plus-arvo') => 'color-dark'),
            'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'dependency' => array('element' => 'layout_style','value' => array( 'tt_scroller','tt_grid' ))
        ),
		array_merge(gf_vc_map_add_navigation(),array(
			'dependency' => array(),
		)),
		array_merge(gf_vc_map_add_navigation_style(),array(
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_position(),array(
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination(),array(
			'dependency' => array(),
		)),
		array_merge(gf_vc_map_add_pagination_style(),array(
			'edit_field_class' => 'vc_col-sm-4 vc_column'
		)),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Desktop', 'g5plus-arvo'),
			'param_name' => 'items_lg',
			'description' => esc_html__('Browser Width > 1199', 'g5plus-arvo'),
			'value' => '',
			'std' => '2',
			'group'=> 'Reponsive',
			'dependency' => array('element' => 'layout_style','value' => array( 'tt_grid_bg','tt_grid' ))
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Desktop Small', 'g5plus-arvo'),
			'param_name' => 'items_md',
			'description' => esc_html__('Browser Width < 1199', 'g5plus-arvo'),
			'value' => '',
			'std' => '2',
			'group'=> 'Reponsive',
			'dependency' => array('element' => 'layout_style','value' => array( 'tt_grid_bg','tt_grid' ))
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Tablet', 'g5plus-arvo'),
			'param_name' => 'items_sm',
			'description' => esc_html__('Browser Width < 992', 'g5plus-arvo'),
			'value' => '',
			'std' => '1',
			'group'=> 'Reponsive',
			'dependency' => array('element' => 'layout_style','value' => array( 'tt_grid_bg','tt_grid' ))
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Tablet Small', 'g5plus-arvo'),
			'param_name' => 'items_xs',
			'description' => esc_html__('Browser Width < 768', 'g5plus-arvo'),
			'value' => '',
			'std' => '1',
			'group'=> 'Reponsive',
			'dependency' => array('element' => 'layout_style','value' => array( 'tt_grid_bg','tt_grid' ))
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Items Mobile', 'g5plus-arvo'),
			'param_name' => 'items_mb',
			'description' => esc_html__('Browser Width < 480', 'g5plus-arvo'),
			'value' => '',
			'std' => '1',
			'group'=> 'Reponsive',
			'dependency' => array('element' => 'layout_style','value' => array( 'tt_grid_bg','tt_grid' ))
		),
        vc_map_add_css_animation(),
        gf_vc_map_add_animation_duration(),
        gf_vc_map_add_animation_delay(),
        gf_vc_map_add_extra_class(),
        gf_vc_map_add_css_editor()
    )
);
