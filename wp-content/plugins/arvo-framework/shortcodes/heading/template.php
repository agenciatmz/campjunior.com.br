<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $des_title
 * @var $align
 * @var $color
 * @var $google_fonts
 * @var $fs
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * @var $google_fonts_data - returned from $this->getAttributes
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Heading
 */

$title = $des_title = $align = $color = $css_animation = $animation_duration = $animation_delay = $el_class = $css = $google_fonts_data = $fs = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$wrapper_attributes = array();
$styles = array();
$inline_styles = array();
$inline_style = '';


// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$styles = $animation_style;
}

$wrapper_classes = array(
	'g5plus-heading',
	'mg-bottom-50',
	$align,
	$color,
	$this->getExtraClass($el_class),
	$this->getCSSAnimation($css_animation)
);

if ('true' === $atts['use_theme_fonts']) {
	$google_fonts_obj = new Vc_Google_Fonts();
	$google_fonts_field_settings = isset($google_fonts_field['settings'], $google_fonts_field['settings']['fields']) ? $google_fonts_field['settings']['fields'] : array();
	$google_fonts_data = strlen($google_fonts) > 0 ? $google_fonts_obj->_vc_google_fonts_parse_attributes($google_fonts_field_settings, $google_fonts) : '';

	if ((isset($atts['use_theme_fonts'])) && !empty($google_fonts_data) && isset($google_fonts_data['values'], $google_fonts_data['values']['font_family'], $google_fonts_data['values']['font_style'])) {
		$google_fonts_family = explode(':', $google_fonts_data['values']['font_family']);
		$inline_styles[] = 'font-family:' . $google_fonts_family[0];
		$google_fonts_styles = explode(':', $google_fonts_data['values']['font_style']);
		$inline_styles[] = 'font-weight:' . $google_fonts_styles[1];
		$inline_styles[] = 'font-style:' . $google_fonts_styles[2];
		if(!empty($fs)){
			$inline_styles[] = 'font-size:' . $fs . 'px';
		}
	}

	$settings = get_option('wpb_js_google_fonts_subsets');
	if (is_array($settings) && !empty($settings)) {
		$subsets = '&subset=' . implode(',', $settings);
	} else {
		$subsets = '';
	}

	if (isset($google_fonts_data['values']['font_family'])) {
		wp_enqueue_style('vc_google_fonts_' . vc_build_safe_css_class($google_fonts_data['values']['font_family']), '//fonts.googleapis.com/css?family=' . $google_fonts_data['values']['font_family'] . $subsets);
	}

	if (!empty($inline_styles)) {
		$inline_style = 'style="' . esc_attr(implode(';', $inline_styles)) . '"';
	}
}

if (!empty($styles)) {
	$wrapper_attributes[] = 'style="' . implode(' ', $styles) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'heading', plugins_url(GF_PLUGIN_NAME . '/shortcodes/heading/assets/css/heading.min.css'), array(), false, 'all');
}

?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<?php if (!empty($title)): ?>
		<h2 <?php echo $inline_style ?>><?php echo esc_html($title); ?></h2>
	<?php endif; ?>
	<?php if (!empty($des_title)): ?>
		<span><?php echo esc_html($des_title); ?></span>
	<?php endif; ?>
</div>
