<?php
return array(
	'base' => 'g5plus_heading',
	'name' => esc_html__('Heading','g5plus-arvo'),
	'icon' => 'fa fa-header',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Heading', 'g5plus-arvo' ),
			'param_name' => 'title',
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Description Title', 'g5plus-arvo' ),
			'param_name' => 'des_title'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Text Align', 'g5plus-arvo' ),
			'param_name' => 'align',
			'description' => esc_html__('Select heading alignment.', 'g5plus-arvo' ),
			'value' => array(
				esc_html__('Left', 'g5plus-arvo' ) => 'text-left',
				esc_html__('Center', 'g5plus-arvo' ) => 'text-center',
				esc_html__('Right', 'g5plus-arvo' ) => 'text-right',
			),
			'admin_label' => true,
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color', 'g5plus-arvo'),
			'param_name' => 'color',
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'hd-dark',
				esc_html__('Light', 'g5plus-arvo') => 'hd-light'),
			'description' => esc_html__('Select Color Heading.', 'g5plus-arvo'),
			'std' => 'hd-dark',
			'admin_label' => true,
		),
		array(
			'type' => 'checkbox',
			'heading' => __( 'Use custom font?', 'g5plus-arvo' ),
			'param_name' => 'use_theme_fonts',
			'description' => __( 'Enable Google fonts.', 'g5plus-arvo' ),
		),
		array(
			'type' => 'google_fonts',
			'param_name' => 'google_fonts',
			'value' => 'font_family:Abril%20Fatface%3Aregular|font_style:400%20regular%3A400%3Anormal',
			'settings' => array(
				'fields' => array(
					'font_family_description' => __( 'Select font family.', 'g5plus-arvo' ),
					'font_style_description' => __( 'Select font styling.', 'g5plus-arvo' ),
				),
			),
			'dependency' => array('element' => 'use_theme_fonts', 'value' => 'true'),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Font size', 'g5plus-arvo' ),
			'param_name' => 'fs',
			'dependency' => array('element' => 'use_theme_fonts', 'value' => 'true'),
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	),
);