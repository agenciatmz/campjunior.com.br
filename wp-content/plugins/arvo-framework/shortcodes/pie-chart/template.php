<?php
/**
 * Shortcode attributes
 * @var $atts
 * Shortcode class
 * @var $this WPBakeryShortCode_Vc_Pie
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'pie_chart_css', plugins_url(GF_PLUGIN_NAME . '/shortcodes/pie-chart/assets/css/pie-chart.min.css'), array(), false);
}

$min_suffix_js = gf_get_option('enable_minifile_js', 0) == 1 ? '.min' : '';
wp_enqueue_script(GF_PLUGIN_PREFIX . 'pie_chart_js', plugins_url(GF_PLUGIN_NAME . '/shortcodes/pie-chart/assets/js/jquery.vc_chart' . $min_suffix_js . '.js'), array(), false, true);

$colors = array(
    'primary-color' => '#3ab8bd',
    'blue' => '#5472d2',
    'turquoise' => '#00c1cf',
    'pink' => '#fe6c61',
    'violet' => '#8d6dc4',
    'peacoc' => '#4cadc9',
    'chino' => '#cec2ab',
    'mulled-wine' => '#50485b',
    'vista-blue' => '#75d69c',
    'orange' => '#f7be68',
    'sky' => '#5aa1e3',
    'green' => '#6dab3c',
    'juicy-pink' => '#f4524d',
    'sandy-brown' => '#f79468',
    'purple' => '#b97ebb',
    'black' => '#2a2a2a',
    'grey' => '#ebebeb',
    'white' => '#ffffff',
);

if ('custom' === $atts['color']) {
    $atts['color'] = $atts['custom_color'];
} else {
    $atts['color'] = isset($colors[$atts['color']]) ? $colors[$atts['color']] : '';
}

if (!$atts['color']) {
    $atts['color'] = $colors['grey'];
}
if ('bar_custom' === $atts['bar_color']) {
    $atts['bar_color'] = $atts['bar_custom_color'];
} else {
    $atts['bar_color'] = isset($colors[$atts['bar_color']]) ? $colors[$atts['bar_color']] : '';
}

if (!$atts['bar_color']) {
    $atts['bar_color'] = $colors['grey'];
}
$pie_type = ($atts['layout_style'] == 'pie_icon') ? 1 : 0;

$wrapper_classes = array(
    'g5plus-pie-chart',
    'vc_pie_chart wpb_content_element',
    $atts['layout_style'],
    $this->getExtraClass($atts['el_class'])
);
// icon html
$icon_html = '';
if ($atts['icon_type'] == 'image') {
    if (!empty($atts['icon_image'])) {
        $icon_image_id = preg_replace( '/[^\d]/', '', $atts['icon_image'] );
        $icon_image_src = wp_get_attachment_image_src( $icon_image_id, 'full' );
        if ( ! empty( $icon_image_src[0] ) ) {
            $icon_image_src = $icon_image_src[0];
            $icon_html = '<img alt="'. the_title_attribute(array('post' => $icon_image_id,'echo' => false )) .'" src="'. esc_url($icon_image_src) .'">';
        }

    }
} else {
    $icon_attributes=array();
    if($atts['value_color']!=='')
    {
        $icon_attributes[]=' style="color:'.esc_attr($atts['value_color']).'"';
    }
    $icon_html = '<i '. implode(' ',$icon_attributes) .' class="'. esc_attr($atts['icon_font']).'"></i>';
}
$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($atts['css'], ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);
$bar_attributes=array();
if($atts['bar_color']!=='')
{
    $bar_attributes[]=' style="border-color:'.esc_attr($atts['bar_color']).'"';
}
$value_attributes=array();
if($atts['value_color']!=='')
{
    $value_attributes[]=' style="color:'.esc_attr($atts['value_color']).'"';
}
?>
<div class="<?php echo esc_attr($css_class) ?>"
     data-pie-value="<?php echo esc_attr($atts['value']); ?>" data-pie-label-value="<?php echo esc_attr($atts['label_value']) ?>"
     data-pie-units="<?php echo esc_attr($atts['units']) ?>" data-pie-color="<?php echo esc_attr($atts['color']) ?>"
     data-pie-icon="<?php echo esc_attr($pie_type) ?>">
    <div class="wpb_wrapper">
        <div class="vc_pie_wrapper">
            <span class="vc_pie_chart_back"<?php echo implode(' ',$bar_attributes); ?>></span>
            <?php if ($atts['layout_style'] != 'pie_icon'):?>
                <span<?php echo implode(' ',$value_attributes); ?> class="vc_pie_chart_value"></span>
            <?php else: ?>
                <span class="vc_pie_chart_value">
                <?php echo wp_kses_post($icon_html); ?>
                </span>
            <?php endif;?>
            <canvas width="101" height="101"></canvas>
        </div>
    </div>
</div>
<?php if ($atts['title'] !==''):
    $title_attributes=array();
    if($atts['title_color']!=='')
    {
        $title_attributes[]=' style="color:'.esc_attr($atts['title_color']).'"';
    }
    ?>
    <h4 class="wpb_pie_chart_heading fw-medium"<?php echo implode(' ',$title_attributes); ?>><?php echo esc_html($atts['title']); ?></h4>
<?php endif; ?>