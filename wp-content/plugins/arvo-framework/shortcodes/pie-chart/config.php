<?php
return array(
	'name' => esc_html__('Pie Chart', 'g5plus-arvo'),
	'base' => 'g5plus_pie_chart',
	'class' => '',
	'icon' => 'fa fa-pie-chart',
	'category' => GF_SHORTCODE_CATEGORY,
	'description' => esc_html__('Animated pie chart', 'g5plus-arvo'),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo'),
			'param_name' => 'layout_style',
			'admin_label' => true,
			'value' => array(esc_html__('Normal', 'g5plus-arvo') => 'pie_text', esc_html__('Pie Icon', 'g5plus-arvo') => 'pie_icon'),
			'description' => esc_html__('Select Layout Style.', 'g5plus-arvo'),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Title', 'g5plus-arvo' ),
			'param_name' => 'title',
			'description' => __( 'Enter text used as widget title (Note: located above content element).', 'g5plus-arvo' ),
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Value', 'g5plus-arvo'),
			'param_name' => 'value',
			'description' => esc_html__('Enter value for graph (Note: choose range from 0 to 100).', 'g5plus-arvo'),
			'value' => '50',
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Label value', 'g5plus-arvo'),
			'param_name' => 'label_value',
			'description' => esc_html__('Enter label for pie chart (Note: leaving empty will set value from "Value" field).', 'g5plus-arvo'),
			'value' => ''
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Units', 'g5plus-arvo'),
			'param_name' => 'units',
			'description' => esc_html__('Enter measurement units (Example: %, px, points, etc. Note: graph value and units will be appended to graph title).', 'g5plus-arvo')
		),
		gf_vc_map_add_icon_type(Array('element' => 'layout_style', 'value' => array('pie_icon'))),
		gf_vc_map_add_icon_font(),
		gf_vc_map_add_icon_image(),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__('Text value / Icon color', 'g5plus-arvo'),
			'param_name' => 'value_color',
			'description' => esc_html__('Select value/icon color.', 'g5plus-arvo'),
		),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__('Title color', 'g5plus-arvo'),
			'param_name' => 'title_color',
			'description' => esc_html__('Select title color.', 'g5plus-arvo'),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Bar color', 'g5plus-arvo'),
			'param_name' => 'bar_color',
			'value' => array(esc_html__('Primary color', 'g5plus-arvo') => 'primary-color') + getVcShared('colors-dashed') + array(esc_html__('Custom', 'g5plus-arvo') => 'custom'),
			'description' => esc_html__('Select pie chart color.', 'g5plus-arvo'),
			'param_holder_class' => 'vc_colored-dropdown',
			'std' => 'grey'
		),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__('Custom color', 'g5plus-arvo'),
			'param_name' => 'bar_custom_color',
			'description' => esc_html__('Select custom bar color.', 'g5plus-arvo'),
			'dependency' => array(
				'element' => 'bar_color',
				'value' => array('custom')
			),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Bar value color', 'g5plus-arvo'),
			'param_name' => 'color',
			'value' => array(esc_html__('Primary color', 'g5plus-arvo') => 'primary-color') + getVcShared('colors-dashed') + array(esc_html__('Custom', 'g5plus-arvo') => 'custom'),
			'description' => esc_html__('Select bar value color.', 'g5plus-arvo'),
			'param_holder_class' => 'vc_colored-dropdown',
			'std' => 'primary-color'
		),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__('Custom color', 'g5plus-arvo'),
			'param_name' => 'custom_color',
			'description' => esc_html__('Select custom bar value color.', 'g5plus-arvo'),
			'dependency' => array(
				'element' => 'color',
				'value' => array('custom')
			),
		),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);