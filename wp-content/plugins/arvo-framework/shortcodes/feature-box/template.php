<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $image
 * @var $video_url
 * @var $title
 * @var $description
 * @var $link
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Feature_Box
 */

$image = $video_url = $title = $description = $link = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

//parse link
$link = ( '||' === $link ) ? '' : $link;
$link = vc_build_link( $link );
$use_link = false;
if ( strlen( $link['url'] ) > 0 ) {
	$use_link = true;
	$a_href = $link['url'];
	$a_title = $link['title'];
	$a_target = $link['target'];
}else {
	$a_href = '#';
}
if(empty($a_target)){
	$a_target='_self';
}

$wrapper_attributes = array();
$wrapper_styles = array();

$wrapper_classes = array(
	'g5plus-feature-box',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation ),
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}
if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', $wrapper_styles) . '"';
}
// get image src
if (!empty($image)) {
	$image_id = preg_replace('/[^\d]/', '', $image);
	$image_src = wp_get_attachment_image_src($image_id, 'full');
	if (!empty($image_src[0])) {
		$image_src = $image_src[0];
	}
}
$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'feature-box', plugins_url(GF_PLUGIN_NAME . '/shortcodes/feature-box/assets/css/feature-box.min.css'), array(), false, 'all');
}
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<div class="feature-thumb">
		<?php if($video_url != ''):?>
			<a title="<?php echo esc_attr($title); ?>" class="view-video" data-src="<?php echo esc_url($video_url); ?>">
				<img alt="<?php echo esc_attr($title); ?>" src="<?php echo esc_url($image_src) ?>">
				<i class="fa fa-play-circle-o fs-30"></i>
			</a>
		<?php else:?>
			<a title="<?php echo esc_attr($title); ?>" target="<?php echo esc_attr($a_target) ?>" href="<?php echo esc_url($a_href); ?>">
				<img alt="<?php echo esc_attr($title); ?>" src="<?php echo esc_url($image_src) ?>">
			</a>
		<?php endif;?>
	</div>
	<div class="feature-content">
		<?php if($title!=''):?>
			<a target="<?php echo esc_attr($a_target) ?>" href="<?php echo esc_url($a_href); ?>"><?php echo esc_html($title); ?></a>
		<?php endif;
		if($description!=''): ?>
			<p><?php echo wp_kses_post($description); ?></p>
		<?php endif;?>
	</div>
</div>