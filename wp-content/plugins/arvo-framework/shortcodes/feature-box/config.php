<?php
return array(
	'name' => esc_html__('Feature Box', 'g5plus-arvo'),
	'base' => 'g5plus_feature_box',
	'class' => '',
	'icon' => 'fa fa-newspaper-o',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_html__('Image:', 'g5plus-arvo'),
			'param_name' => 'image',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Video Url', 'g5plus-arvo'),
			'param_name' => 'video_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Title', 'g5plus-arvo'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textarea',
			'heading' => esc_html__('Description', 'g5plus-arvo'),
			'param_name' => 'description',
			'value' => '',
		),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__('Link (url)', 'g5plus-arvo'),
			'param_name' => 'link',
			'value' => '',
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);