<?php
return array(
	'base' => 'g5plus_dropcaps',
	'name' => esc_html__('Drop Caps', 'g5plus-arvo'),
	'icon' => 'fa fa-adn',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo' ),
			'param_name' => 'layout_style',
			'value' => array(
				esc_html__('Classic', 'g5plus-arvo' ) => 'dc-classic',
				esc_html__('Outline', 'g5plus-arvo' ) => 'dc-outline',
				esc_html__('Background', 'g5plus-arvo' ) => 'dc-background',
			),
			'admin_label' => true,
			'std' => 'dc-classic'
		),
		array(
			'type' => 'textarea_html',
			'heading' => esc_html__( 'Text', 'g5plus-arvo' ),
			'param_name' => 'content'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
			'param_name' => 'color_scheme',
			'value' => array(
				esc_html__('Primary', 'g5plus-arvo') => 'dc-scheme-accent',
				esc_html__('Dark', 'g5plus-arvo') => 'dc-scheme-dark',
				esc_html__('Light', 'g5plus-arvo') => 'dc-scheme-light'
			),
			'admin_label' => true,
			'std' => 'dc-scheme-accent'
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);