<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $layout_style
 * @var $content
 * @var $color_scheme
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Dropcaps
 */

$layout_style = $color_scheme = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$styles = array();

$wrapper_classes = array(
	'g5plus-drop-caps',
	$layout_style,
	$color_scheme,
	'clearfix',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
	$styles = $animation_style;
}

if ( $styles ) {
	$wrapper_attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}

$class_to_filter = implode(' ',array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);
if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'cta', plugins_url(GF_PLUGIN_NAME . '/shortcodes/dropcaps/assets/css/dropcaps.min.css'), array(), false, 'all');
}
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode( ' ', $wrapper_attributes ); ?>>
	<?php $dropcap_content = wpb_js_remove_wpautop( $content, true );?>
	<div class="drop-caps-content">
		<p><span class="first-letter"><?php echo strtoupper( $dropcap_content[3] ) ?></span>
			<?php echo wp_kses_post( substr( $dropcap_content, 4, strlen( $dropcap_content ) - 9 ) )?></p>
	</div>
</div>
