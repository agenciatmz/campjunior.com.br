<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $values
 * @var $layout_style
 * @var $prt_style
 * @var $features_style
 * @var $icon_font
 * @var $color
 * @var $name
 * @var $price
 * @var $features
 * @var $title
 * @var $link
 * @var $recommend
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Pricing_Tables
 */

$values = $layout_style = $prt_style = $features_style = $icon_font = $color = $name  = $price = $features = $title = $link = $recommend = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
$values = (array) vc_param_group_parse_atts( $values );

$wrapper_attributes = array();
$wrapper_styles = array();

$wrapper_classes = array(
	'g5plus-pricing-tables',
	$layout_style,
	$prt_style,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation ),
);
// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}
//parse link
$link = ( '||' === $link ) ? '' : $link;
$link = vc_build_link( $link );
$use_link = false;
if ( strlen( $link['url'] ) > 0 ) {
	$use_link = true;
	$a_href = $link['url'];
	$a_title = $link['title'];
	$a_target = $link['target'];
}
$button_class= 'btn btn-md btn-round';

if ( 'true' === $recommend) {
	$wrapper_classes[] = 'recommend';
}

if ( $wrapper_styles ) {
	$wrapper_attributes[] = 'style="' . implode( ' ', $wrapper_styles ) . '"';
}

$button_attributes = array();
if ( $use_link ) {
	$button_attributes[] = 'href="' . esc_url( trim( $a_href ) ) . '"';
	if(empty($a_title)) {
		$a_title = $title;
	}
	$button_attributes[] = 'title="' . esc_attr( trim( $a_title ) ) . '"';
	if ( ! empty( $a_target ) ) {
		$button_attributes[] = 'target="' . esc_attr( trim( $a_target ) ) . '"';
	}
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX  . 'pricing-tables', plugins_url(GF_PLUGIN_NAME . '/shortcodes/pricing-tables/assets/css/pricing-tables.min.css'), array(), false, 'all');
}

?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<h4 class="pricing-name s-font fw-bold"><?php echo wp_kses_post($name)?></h4>
	<span class="price-value fw-bold"><?php echo wp_kses_post($price)?></span>
	<div class="pricing-features">
		<ul>
			<?php foreach ($values as $data): ?>
				<?php $features = isset( $data['features'] ) ? $data['features'] : ''; ?>
				<?php $features_style = isset( $data['features_style'] ) ? $data['features_style'] : ''; ?>
				<?php $color = isset( $data['color'] ) ? $data['color'] : ''; ?>
				<?php if (!empty($features)||!empty($features_style)): ?>
					<li>
						<?php if ($features_style == 'icon') : ?>
							<i class="<?php echo esc_attr($data['icon_font']);?> <?php echo esc_attr($color);?>"></i>
						<?php else: ?>
							<?php echo wp_kses_post($data['features'])?>
						<?php endif; ?>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php if (!empty($title)): ?>
		<div class="pricing-button">
			<a class="<?php echo esc_attr($button_class) ?>" <?php echo implode( ' ', $button_attributes );?>><?php echo esc_html($title) ?></a>
		</div>
	<?php endif; ?>
</div>