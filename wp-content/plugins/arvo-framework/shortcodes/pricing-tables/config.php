<?php
return array(
	'name' => esc_html__( 'Pricing Tables', 'g5plus-arvo' ),
	'base' => 'g5plus_pricing_tables',
	'icon' => 'fa fa-usd',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo'),
			'param_name' => 'layout_style',
			'admin_label' => true,
			'value' => array(
				esc_html__('Pricing Table', 'g5plus-arvo') => 'prt_table',
				esc_html__('Pricing Package Features', 'g5plus-arvo') => 'prt_package'),
				'description' => esc_html__('Select Layout Style.', 'g5plus-arvo')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Pricing Table Style', 'g5plus-arvo'),
			'param_name' => 'prt_style',
			'value' => array(
				esc_html__('Features Style Line', 'g5plus-arvo') => 'features_line',
				esc_html__('Features Style Box', 'g5plus-arvo') => 'features_box'),
			'description' => esc_html__('Select Layout Style.', 'g5plus-arvo')
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Name', 'g5plus-arvo'),
			'param_name' => 'name',
			'admin_label' => true,
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Price', 'g5plus-arvo'),
			'param_name' => 'price',
			'admin_label' => true,
			'dependency' => array('element' => 'layout_style','value' => array( 'prt_table' ))
		),
		array(
			'type' => 'param_group',
			'heading' => esc_html__( 'Pricing Tables', 'g5plus-arvo' ),
			'param_name' => 'values',
			'value' => urlencode( json_encode( array(
				array(
					'label' => esc_html__( 'Features', 'g5plus-arvo' ),
					'value' => '',
				),
			) ) ),
			'params' => array(
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Features Style', 'g5plus-arvo'),
					'param_name' => 'features_style',
					'value' => array(
						esc_html__('Text','g5plus-arvo') => 'text',
						esc_html__('Icon','g5plus-arvo') => 'icon',
					),
					'admin_label' => true,
				),
				gf_vc_map_add_icon_font(array('element' => 'features_style','value' => 'icon')),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Icon color', 'g5plus-arvo' ),
					'param_name' => 'color',
					'value' => array( esc_html__( 'Default', 'g5plus-arvo' ) => '',esc_html__( 'Primary', 'g5plus-arvo' ) => 'primary' ) + getVcShared( 'colors' ),
					'std' => '',
					'description' => esc_html__( 'Select Icon color.', 'g5plus-arvo' ),
					'param_holder_class' => 'vc_colored-dropdown',
					'dependency' => array('element' => 'features_style','value' => array( 'icon' )),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Features', 'g5plus-arvo'),
					'param_name' => 'features',
					'admin_label' => true,
					'dependency' => array('element' => 'features_style','value' => array( 'text' )),
					'value' =>''
				),
			),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Button Text', 'g5plus-arvo' ),
			'param_name' => 'title',
			'dependency' => array('element' => 'layout_style','value' => array( 'prt_table' ))
		),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__('URL (Link)', 'g5plus-arvo' ),
			'param_name' => 'link',
			'description' => esc_html__('Add link to button.', 'g5plus-arvo' ),
			'dependency' => array('element' => 'layout_style','value' => array( 'prt_table' ))
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Recommend ?', 'g5plus-arvo' ),
			'param_name' => 'recommend',
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element' => 'layout_style','value' => array( 'prt_table' ))
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);
