<?php
return array(
	'name' => esc_html__('Google Map', 'g5plus-arvo'),
	'base' => 'g5plus_google_map',
	'icon' => 'fa fa-map-marker',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'param_group',
			'heading' => esc_html__( 'Markers', 'g5plus-arvo' ),
			'param_name' => 'markers',
			'value' => urlencode( json_encode( array(
				array(
					'label' => esc_html__( 'Title', 'g5plus-arvo' ),
					'value' => '',
				),
			) ) ),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Latitude ', 'g5plus-arvo'),
					'param_name' => 'lat',
					'admin_label' => true,
					'value' => '',
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Longitude ', 'g5plus-arvo'),
					'param_name' => 'lng',
					'admin_label' => true,
					'value' => '',
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Title', 'g5plus-arvo'),
					'param_name' => 'title',
					'admin_label' => true,
					'value' => '',
				),
				array(
					'type' => 'textarea',
					'heading' => esc_html__('Description', 'g5plus-arvo'),
					'param_name' => 'description',
					'value' => ''
				),
				array(
					'type' => 'attach_image',
					'heading' => esc_html__( 'Marker Icon', 'g5plus-arvo' ),
					'param_name' => 'icon',
					'value' => '',
					'description' => esc_html__( 'Select an image from media library.', 'g5plus-arvo' ),
				),
			),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('API Url', 'g5plus-arvo'),
			'param_name' => 'api_url',
			'std' => 'http://maps.googleapis.com/maps/api/js?key=AIzaSyAwey_47Cen4qJOjwHQ_sK1igwKPd74J18',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Map height (px or %)', 'g5plus-arvo'),
			'param_name' => 'map_height',
			'edit_field_class' => 'vc_col-sm-6',
			'std' => '500px',
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color Scheme', 'g5plus-arvo'),
			'param_name' => 'color_scheme',
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'map-dark',
				esc_html__('Light', 'g5plus-arvo') => 'map-light'),
			'admin_label' => true,
			'edit_field_class' => 'vc_col-sm-6',
		),
	vc_map_add_css_animation(),
	gf_vc_map_add_animation_duration(),
	gf_vc_map_add_animation_delay(),
	gf_vc_map_add_extra_class(),
	gf_vc_map_add_css_editor()
	)
);