<?php
$dependency_add_icon = array(
	'element' => 'add_icon',
	'value' => 'true',
);
return array(
	'base' => 'g5plus_button',
	'name' => esc_html__('Button','g5plus-arvo'),
	'icon' => 'fa fa-bold',
	'category' =>  GF_SHORTCODE_CATEGORY,
	'description' => esc_html__('Eye catching button', 'g5plus-arvo' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Text', 'g5plus-arvo' ),
			'param_name' => 'title',
			'value' => esc_html__('Text on the button', 'g5plus-arvo' ),
			'admin_label' => true,
		),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__('URL (Link)', 'g5plus-arvo' ),
			'param_name' => 'link',
			'description' => esc_html__('Add link to button.', 'g5plus-arvo' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Style', 'g5plus-arvo' ),
			'description' => esc_html__('Select button display style.', 'g5plus-arvo' ),
			'param_name' => 'style',
			'value' => array(
				esc_html__('Classic', 'g5plus-arvo' ) => 'classic',
				esc_html__('Outline', 'g5plus-arvo' ) => 'outline',
				esc_html__('Link Text', 'g5plus-arvo' ) => 'link',
			),
			'std' => 'classic',
			'admin_label' => true,
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Shape', 'g5plus-arvo' ),
			'description' => esc_html__( 'Select button shape.', 'g5plus-arvo' ),
			'param_name' => 'shape',
			'value' => array(
				esc_html__( 'Square', 'g5plus-arvo' ) => 'square',
				esc_html__( 'Round', 'g5plus-arvo' ) => 'round',
			),
			'std' => 'round',
			'dependency' => array('element' => 'style','value_not_equal_to' => 'link')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Color', 'g5plus-arvo' ),
			'param_name' => 'color',
			'description' => esc_html__('Select button color.', 'g5plus-arvo' ),
			'value' => array(
					esc_html__('Primary', 'g5plus-arvo' ) => 'primary',
					esc_html__('Gray', 'g5plus-arvo' ) => 'gray',
					esc_html__('Black', 'g5plus-arvo' ) => 'black',
					esc_html__('White', 'g5plus-arvo' ) => 'white',
				),
			'std' => 'primary',
			'admin_label' => true,
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Scheme', 'g5plus-arvo'),
			'param_name' => 'scheme',
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'dark',
				esc_html__('Light', 'g5plus-arvo') => 'light'),
			'description' => esc_html__('Select Color Scheme.', 'g5plus-arvo'),
			'std' => 'light',
			'dependency' => array('element' => 'style','value_not_equal_to' => 'link')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Size', 'g5plus-arvo' ),
			'param_name' => 'size',
			'description' => esc_html__('Select button display size.', 'g5plus-arvo' ),
			'std' => 'sm',
			'value' => array(
				esc_html__('Mini','g5plus-arvo') => 'xs', // 41px
				esc_html__('Small','g5plus-arvo') => 'sm', // 45px
				esc_html__('Normal','g5plus-arvo') => 'md', // 48px
				esc_html__('Large','g5plus-arvo') => 'lg', //50px
			),
			'admin_label' => true,
			'dependency' => array('element' => 'style','value_not_equal_to' => 'link')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Alignment', 'g5plus-arvo' ),
			'param_name' => 'align',
			'description' => esc_html__('Select button alignment.', 'g5plus-arvo' ),
			'value' => array(
				esc_html__('Inline', 'g5plus-arvo' ) => 'inline',
				esc_html__('Left', 'g5plus-arvo' ) => 'left',
				esc_html__('Right', 'g5plus-arvo' ) => 'right',
				esc_html__('Center', 'g5plus-arvo' ) => 'center',
			),
			'admin_label' => true,
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Set full width button?', 'g5plus-arvo' ),
			'param_name' => 'button_block',
			'dependency' => array(
				'element' => 'align',
				'value_not_equal_to' => 'inline',
			),
			'admin_label' => true,
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Add icon?', 'g5plus-arvo' ),
			'param_name' => 'add_icon',
			'admin_label' => true,
			'dependency' => array('element' => 'style','value_not_equal_to' => 'link')
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Icon Alignment', 'g5plus-arvo' ),
			'description' => esc_html__('Select icon alignment.', 'g5plus-arvo' ),
			'param_name' => 'icon_align',
			'value' => array(
				esc_html__('Left', 'g5plus-arvo' ) => 'left',
				// default as well
				esc_html__('Right', 'g5plus-arvo' ) => 'right',
			),
			'dependency' => $dependency_add_icon,
		),
		gf_vc_map_add_icon_font($dependency_add_icon),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	),
);