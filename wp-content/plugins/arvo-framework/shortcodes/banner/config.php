<?php
return array(
    'base' => 'g5plus_banner',
    'name' => esc_html__('Banner','g5plus-arvo'),
    'icon' => 'fa fa-picture-o',
    'category' => GF_SHORTCODE_CATEGORY,
    'params' => array(
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Hover effect', 'g5plus-arvo'),
			'param_name' => 'hover_effect',
			'std'        => '',
			'value'      => array(
				esc_html__('Normal', 'g5plus-arvo')    => '',
				esc_html__('Suprema', 'g5plus-arvo') => 'suprema-effect',
				esc_html__('Layla', 'g5plus-arvo')   => 'layla-effect',
				esc_html__('Bubba', 'g5plus-arvo')   => 'bubba-effect',
				esc_html__('Jazz', 'g5plus-arvo')    => 'jazz-effect',
			),
			'admin_label' => true
		),
	    array(
		    'type'       => 'dropdown',
		    'heading'    => esc_html__('Hover Overlay Scheme', 'g5plus-arvo'),
		    'param_name' => 'hover_overlay_scheme',
		    'value'      => array(
			    esc_html__('Dark', 'g5plus-arvo')    => 'overlay-dark',
			    esc_html__('Light', 'g5plus-arvo') => 'overlay-light'
		    ),
		    'std' => 'overlay-light',
		    'admin_label' => true,
		    'dependency' => array('element'=>'hover_effect', 'value'=>array('suprema-effect', 'layla-effect', 'bubba-effect', 'jazz-effect'))
	    ),
        array(
            'type' => 'attach_image',
			'heading'    => esc_html__('Select Banner image', 'g5plus-arvo'),
			'param_name' => 'banner_image',
            'value' => '',
        ),
	    array(
		    'type' => 'textarea_html',
		    'heading' => esc_html__( 'Text', 'g5plus-arvo' ),
		    'param_name' => 'content'
	    ),
	    array(
		    'type'       => 'dropdown',
		    'heading'    => esc_html__('Text Place', 'g5plus-arvo'),
		    'param_name' => 'text_place',
		    'std'        => '',
		    'value'      => array(
			    esc_html__('Top Center', 'g5plus-arvo')    => 'text-top-center',
			    esc_html__('Middle Center', 'g5plus-arvo')    => 'text-middle-center',
			    esc_html__('Bottom Left', 'g5plus-arvo')    => 'text-bottom-left',
			    esc_html__('Bottom Center', 'g5plus-arvo')    => 'text-bottom-center',
			    esc_html__('Bottom Right', 'g5plus-arvo')    => 'text-bottom-right'
		    )
	    ),
        array(
            'type' => 'vc_link',
            'heading' => esc_html__('Link (url)', 'g5plus-arvo'),
            'param_name' => 'link',
            'value' => '',
        ),
	    vc_map_add_css_animation(),
	    gf_vc_map_add_animation_duration(),
	    gf_vc_map_add_animation_delay(),
	    gf_vc_map_add_extra_class(),
	    gf_vc_map_add_css_editor()
    ),
);