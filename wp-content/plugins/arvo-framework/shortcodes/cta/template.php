<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $content
 * @var $button_title
 * @var $button_link
 * @var $button_style
 * @var $button_shape
 * @var $button_color
 * @var $button_scheme
 * @var $button_size
 * @var $button_align
 * @var $button_block
 * @var $button_add_icon
 * @var $button_icon_align
 * @var $icon_font
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Cta
 */

$button_title = $button_link = $button_style = $button_shape = $button_color
	= $button_scheme = $button_size = $button_align = $button_block = $button_add_icon
	= $button_icon_align = $icon_font = $css_animation = $animation_duration = $animation_delay = $el_class = $css = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$styles = array();

$wrapper_classes = array(
	'g5plus-cta',
	'clearfix',
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
	$styles = $animation_style;
}

if ( $styles ) {
	$wrapper_attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);
if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'cta', plugins_url(GF_PLUGIN_NAME . '/shortcodes/cta/assets/css/cta.min.css'), array(), false, 'all');
}
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode( ' ', $wrapper_attributes ); ?>>
	<div class="cta-content">
		<?php echo wpb_js_remove_wpautop( $content, true ); ?>
	</div>
	<?php if (!empty($button_title)): ?>
		<div class="cta-action">
			<?php echo do_shortcode( '[g5plus_button title="'.$button_title.'" style="'.$button_style.'" shape="'.$button_shape.'"
			 color="'.$button_color.'" scheme="'.$button_scheme.'" size="'.$button_size.'" 
			 align="'.$button_align.'" icon_align="'.$button_icon_align.'" icon_font="'.$icon_font.'" 
			 link="'.$button_link.'" button_block="'.$button_block.'" add_icon="'.$button_add_icon.'"]' ); ?>
		</div>
	<?php endif; ?>
</div>
