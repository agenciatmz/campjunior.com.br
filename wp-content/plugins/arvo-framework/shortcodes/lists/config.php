<?php
$styles = array(
	esc_html__('Simple','g5plus-arvo') => 'simple',
	esc_html__('Round','g5plus-arvo') => 'round',
);

$sizes = array(
	esc_html__('Small','g5plus-arvo') => 'sm',
	esc_html__('Medium','g5plus-arvo') => 'md',
	esc_html__('Large','g5plus-arvo') => 'lg',
);

return array(
	'base' => 'g5plus_lists',
	'name' => esc_html__('Lists','g5plus-arvo'),
	'icon' => 'fa fa-list-ol',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Style', 'g5plus-arvo'),
			'param_name' => 'style',
			'value' => $styles,
			'description' => esc_html__( 'Select lists design style.', 'g5plus-arvo' ),
			'admin_label' => true,
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Bullet', 'g5plus-arvo'),
			'param_name' => 'bullet',
			'value' => array(
				esc_html__('Number','g5plus-arvo') => 'number',
				esc_html__('Icon','g5plus-arvo') => 'icon',
			),
			'admin_label' => true,
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Bullet Color', 'g5plus-arvo' ),
			'param_name' => 'color',
			'value' => array( esc_html__( 'Default', 'g5plus-arvo' ) => '',esc_html__( 'Primary', 'g5plus-arvo' ) => 'primary' ) + getVcShared( 'colors' ),
			'std' => 'grey',
			'description' => esc_html__( 'Select bullet color.', 'g5plus-arvo' ),
			'param_holder_class' => 'vc_colored-dropdown',
		),
		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Values Color', 'g5plus-arvo' ),
			'param_name' => 'value_color',
			'description' => esc_html__( 'Select Values color.', 'g5plus-arvo' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Bullet Size', 'g5plus-arvo' ),
			'param_name' => 'size',
			'value' => $sizes,
			'std' => 'md',
			'description' => esc_html__( 'Select bullet size', 'g5plus-arvo' ),
		),
		array(
			'type' => 'param_group',
			'heading' => esc_html__('Values','g5plus-arvo'),
			'param_name' => 'values',
			'description' => esc_html__('Enter values for list - icon and text','g5plus-arvo'),
			'value' => '',
			'params' => array(
				gf_vc_map_add_icon_font(array('element' => 'bullet','value' => 'icon')) + array('admin_label' => true),
				array(
					'type' => 'textarea',
					'heading' => esc_html__( 'Label', 'g5plus-arvo' ),
					'param_name' => 'label'
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Bullet color', 'g5plus-arvo' ),
					'param_name' => 'color',
					'value' => array( esc_html__( 'Default', 'g5plus-arvo' ) => '',esc_html__( 'Primary', 'g5plus-arvo' ) => 'primary' ) + getVcShared( 'colors' ),
					'std' => '',
					'description' => esc_html__( 'Select bullet color.', 'g5plus-arvo' ),
					'param_holder_class' => 'vc_colored-dropdown',
				)
			)
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Gap', 'g5plus-arvo' ),
			'param_name' => 'gap',
			'value' => array(
				'30px' => 'gap-30',
				'35px' => 'gap-35',
			),
			'std' => 'gap-30',
			'description' => esc_html__( 'Gap on item.', 'g5plus-arvo' ),
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);