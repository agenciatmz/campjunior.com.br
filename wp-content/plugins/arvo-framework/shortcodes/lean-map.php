<?php
function gf_lean_map() {
	$lean_maps = array(
		'g5plus_banner',
		'g5plus_blog',
		'g5plus_button',
		'g5plus_clients',
		'g5plus_countdown',
		'g5plus_counter',
		'g5plus_cta',
		'g5plus_dropcaps',
		'g5plus_feature_box',
		'g5plus_gallery',
		'g5plus_google_map',
		'g5plus_heading',
		'g5plus_icon_box',
		'g5plus_lists',
		'g5plus_menu',
		'g5plus_our_team',
		'g5plus_pie_chart',
		'g5plus_posts',
		'g5plus_pricing_tables',
		'g5plus_progress',
		'g5plus_slider_container',
		'g5plus_testimonials',
		'g5plus_video',
		'g5plus_view_demo',
		'g5plus_widget_info_box',
		'g5plus_widget_social_profile'
	);
	$cpt_disable = gf_get_option('cpt_disable',array());
	if ((array_key_exists('portfolio', $cpt_disable) && ($cpt_disable['portfolio'] == '0' || $cpt_disable['portfolio'] == ''))) {
		$lean_maps[] = 'g5plus_portfolio';
	}

	if (class_exists('WooCommerce')) {
		$lean_maps[] = 'g5plus_products';
		$lean_maps[] = 'g5plus_product_categories';
	}

	foreach ($lean_maps as $key){
		$directory = preg_replace('/^g5plus_/', '', $key);
		vc_lean_map( $key, null, GF_PLUGIN_DIR . 'shortcodes/' . str_replace('_', '-', $directory) . '/config.php' );
	}
}
add_action('vc_before_mapping', 'gf_lean_map');