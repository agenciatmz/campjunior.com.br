<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $layout_style
 * @var $time
 * @var $text_color
 * @var $show_month
 * @var $url_redirect
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Countdown
 */

$layout_style = $time = $text_color = $show_month = $url_redirect = $css_animation = $animation_duration = $animation_delay = $el_class = $css =  '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$wrapper_attributes = array();
$wrapper_styles = array();

$wrapper_classes = array(
	'g5plus-countdown',
	$layout_style,
	$text_color,
	$show_month,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation( $css_animation )
);
// animation
$animation_style = $this->getStyleAnimation($animation_duration,$animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}

if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', array_filter($wrapper_styles) ) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($atts['css'], ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX . 'countdown', plugins_url(GF_PLUGIN_NAME . '/shortcodes/countdown/assets/css/countdown.min.css'), array(), false, 'all');
}

if ( !empty($time)) {
	$time =  mysql2date( 'Y/m/d H:i:s', $time );

	?>
	<div class="<?php echo esc_attr($css_class) ?>" data-url-redirect="<?php echo esc_attr($url_redirect)?>" data-date-end="<?php echo esc_attr($time); ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
		<div class="g5plus-countdown-inner">
			<div class="countdown-section">
				<span class="countdown-value countdown-day">00</span>
				<span class="countdown-text"><?php esc_html_e('DAYS','g5plus-arvo'); ?></span>
			</div>
			<div class="countdown-section">
				<span class="countdown-value countdown-hours">00</span>
				<span class="countdown-text"><?php esc_html_e('HOURS','g5plus-arvo'); ?></span>
			</div>
			<div class="countdown-section">
				<span class="countdown-value countdown-minutes">00</span>
				<span class="countdown-text"><?php esc_html_e('MINUTES','g5plus-arvo'); ?></span>
			</div>
			<div class="countdown-section">
				<span class="countdown-value countdown-seconds">00</span>
				<span class="countdown-text"><?php esc_html_e('SECONDS','g5plus-arvo'); ?></span>
			</div>
		</div>
	</div>
<?php
}