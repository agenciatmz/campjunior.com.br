<?php
/**
 * Created by PhpStorm.
 * User: Kaga
 * Date: 20/5/2016
 * Time: 3:57 PM
 */
return array(
	'name' => esc_html__('Countdown', 'g5plus-arvo'),
	'base' => 'g5plus_countdown',
	'class' => '',
	'icon' => 'fa fa-clock-o',
	'category' => GF_SHORTCODE_CATEGORY,
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Layout Style', 'g5plus-arvo'),
			'param_name' => 'layout_style',
			'admin_label' => true,
			'value' => array(
				esc_html__('Vertical', 'g5plus-arvo') => 'cd-vertical',
				esc_html__('Horizontal', 'g5plus-arvo') => 'cd-horizontal'),
			'description' => esc_html__('Select the type of display content', 'g5plus-arvo'),
			'admin_label' => true,
		),
		array(
			'type' => 'datetimepicker',
			'heading' => esc_html__('Time Off', 'g5plus-arvo'),
			'param_name' => 'time',
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Text Color', 'g5plus-arvo'),
			'param_name' => 'text_color',
			'admin_label' => true,
			'value' => array(
				esc_html__('Dark', 'g5plus-arvo') => 'text-dark',
				esc_html__('Light', 'g5plus-arvo') => 'text-light'),
			'description' => esc_html__('Select Color Text Time.', 'g5plus-arvo'),
			'std' => 'text-dark',
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__('Url Redirect', 'g5plus-arvo'),
			'param_name' => 'url_redirect',
			'value' => '',
			'description' => esc_html__('Url link open when countdown end', 'g5plus-arvo'),
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);