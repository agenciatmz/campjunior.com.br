<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $layout_style
 * @var $nav
 * @var $nav_style
 * @var $nav_position
 * @var $dots
 * @var $dot_position
 * @var $data_source
 * @var $category
 * @var $names
 * @var $hover_effect
 * @var $is_filter
 * @var $active_cate_color
 * @var $show_paging
 * @var $item_amount
 * @var $columns
 * @var $image_size
 * @var $columns_gap
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $css
 * @var $el_class
 * @var $current_page
 * @var $post_not_in
 * @var $archive_category
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Portfolio
 */
$layout_style = $nav = $nav_style = $nav_position = $dots = $dot_position =
	$data_source = $category = $names = $hover_effect = $is_filter = $active_cate_color = $show_paging =
	$item_amount = $columns = $image_size = $columns_gap = $css_animation = $animation_duration =
	$animation_delay = $css = $el_class = $current_page = $post_not_in = $archive_category = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);

extract($atts);

$wrapper_attributes = array();
$wrapper_styles = array();
$filter_attributes = array();
$filter_class = array('hidden-mb portfolio-filter-content');
$content_class = array('portfolio-content');
$content_attributes = array();

if($is_filter) {
	$filter_attributes[] = "data-source='".$data_source."'";
	$filter_attributes[] = "data-names='".$names."'";
	$filter_attributes[] = "data-item-amount='".$item_amount."'";
	$filter_attributes[] = "data-image-size='".$image_size."'";
	$filter_attributes[] = "data-hover-effect='".$hover_effect."'";
	$filter_attributes[] = 'data-item=".portfolio-item"';
	$content_attributes[] = 'data-filter-content="filter"';
} else {
	$active_cate_color = '';
}

if(strpos($layout_style, 'metro') !== false) {
	$columns_gap = 'col-gap-0';
}
if($layout_style == 'portfolio-masonry-metro-style4') {
	$columns_gap = 'col-gap-15';
}
$owl_attributes = array();
$wrapper_classes = array(
	'g5plus-portfolio',
	'clearfix',
	'text-center',
	$layout_style,
	$active_cate_color,
	$columns_gap,
	$this->getExtraClass( $el_class ),
	$this->getCSSAnimation($css_animation)
);

$portfolio_item_class = array('portfolio-item', $hover_effect);
$owl_attribute = array();
if($columns_gap == 'col-gap-30') {
	$col_gap = 30;
} elseif($columns_gap == 'col-gap-10') {
	$col_gap = 10;
} else {
	$col_gap = 0;
}
if($layout_style == 'portfolio-slider') {
	$content_attributes[] = 'data-type="carousel"';
	$show_paging = '';
	$content_class[] = 'owl-carousel';
	if ($nav) {
		$content_class[] = 'owl-nav-'. $nav_position;
		$content_class[] = 'owl-nav-' . $nav_style;
	}
	if ($dots) {
		$content_class[] = 'owl-dot-' . $dot_position;
	}
	$owl_attributes = array(
		'"autoHeight": true',
		'"dots": '.($dots ? 'true' : 'false'),
		'"nav": '. ($nav ? 'true' : 'false'),
		'"responsive": {"0" : {"items" : 1, "margin": 0}, "481" : {"items" : '. (($columns >= 2) ? '2' : $columns) .
			', "margin": '. (($columns >= 2) ? $col_gap : '0') .'}, "768" : {"items" : '. (($columns >= 3) ? '3' : $columns) .
			', "margin": '.$col_gap.'}, "992" : {"items" : '. $columns .', "margin": '.$col_gap.'}}'
	);
	$owl_attribute[] = "data-plugin-options='{". implode(', ', $owl_attributes) ."}'";
	if($is_filter) {
		$filter_class[] = 'portfolio-filter-carousel';
		$filter_attributes[] = 'data-filter-type="carousel"';
		$content_attributes[] = 'data-layout="filter"';
	}
} else {
	if($columns_gap=='col-gap-30') {
		$portfolio_item_class[] = 'mg-bottom-30';
	} elseif($columns_gap=='col-gap-10') {
		$portfolio_item_class[] = 'mg-bottom-10';
	}  elseif($columns_gap=='col-gap-15') {
		$portfolio_item_class[] = 'mg-bottom-15';
	}

	if($layout_style == 'portfolio-grid') {
		$content_attributes[] = 'data-type="grid"';
		$content_class[] = 'row';
		$content_class[] = 'columns-'.$columns;
		$content_class[] = 'columns-md-'.$columns;
		$content_class[] = 'columns-sm-'.(($columns == 2)? '2': '3');
		$content_class[] = 'columns-xs-2';
		$portfolio_item_class[] = 'gf-item-wrap';
		if($is_filter) {
			$content_attributes[] = 'data-layout="masonry"';
		}
	}
	if($is_filter) {
		$filter_attributes[] = 'data-filter-type="filter"';
	}
}

// Masonry Style
$matrix = null;
$image_size_masonry = ',';
if($layout_style != 'portfolio-grid' && $layout_style != 'portfolio-slider') {
	$content_attributes[] = 'data-type="masonry"';
	if($layout_style == 'portfolio-masonry-normal') {
		if($columns == "2") {
			$matrix = array(1, 1.5, 1.5, 1);
			$image_size_masonry = '640,480';
		} elseif ($columns == "3") {
			$matrix = array(1.2, 1.5, 1, 1.5, 1.2, 1);
			$image_size_masonry = '586,391';
		} elseif ($columns == "4") {
			$matrix = array(1.2, 1.5, 1, 1.5, 1.5, 1.5, 1.2, 1);
			$image_size_masonry = '430,287';
		} else {
			$matrix = array(1, 1.9, 1.3, 1.9, 1, 1.3, 1.9, 1, 1.3, 1.3, 1.3, 1.9, 1.3, 1, 1.9, 1.9, 1, 1.3, 1, 1);
			$image_size_masonry = '348,270';
		}
		$content_attributes[] = 'data-layout="masonry"';
	} else {
		if($layout_style == 'portfolio-masonry-metro-style1') {
			$matrix = array(2, 1, 4, 1, 1, 1);
			$image_size_masonry = '384,384';
		} elseif($layout_style == 'portfolio-masonry-metro-style2') {
			$matrix = array(1, 1, 4, 1, 2, 1);
			$image_size_masonry = '384,384';
		} elseif($layout_style == 'portfolio-masonry-metro-style3') {
			$matrix = array(1.5, 1.5, 1, 1, 1);
			$image_size_masonry = '640,640';
		} else {
			$matrix = array('270,400','590,400','270,400','270,400', '400,400','270,400','270,400','400,400','590,400','270,400','590,400','270,400','400,400','270,400','270,400');
		}
		if($layout_style != 'portfolio-masonry-metro-style3' && $layout_style != 'portfolio-masonry-metro-style4' ) {
			$content_attributes[] = 'data-layout="metro"';
			$content_attributes[] = 'data-col-width=".portfolio-col-width20"';
		} else {
			$content_attributes[] = 'data-layout="fitRows"';
		}
	}
}

$args = array(
	'post__not_in' => array($post_not_in),
	'posts_per_page' => (strlen($item_amount) > 0) ? $item_amount : -1,
	'post_type' => G5PLUS_PORTFOLIO_POST_TYPE,
	'orderby' => 'date',
	'paged' => $current_page,
	'order' => 'ASC',
	'post_status' => 'publish'
);
if ($category != '') {
	$args['tax_query'] = array(
		array(
			'taxonomy' => G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY,
			'field' => 'slug',
			'terms' => explode(',', $category),
			'operator' => 'IN'
		)
	);
}
if($data_source == 'list_id' && !empty( $names )) {
	$portfolio_names = explode( ',', $names );
	$args['post_name__in'] = $portfolio_names;
	add_filter( 'posts_orderby' , 'gf_order_by_slug',10,2 );
	if($is_filter) {
		$portfolio_ids = gf_get_portfolio_ids_by_slugs($names);
		$category_obj = wp_get_object_terms( $portfolio_ids,  G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY );
		$category = '';
		foreach ($category_obj as $cate) {
			$category .= $cate->slug . ',';
		}
		$category = rtrim( $category, ',' );
	}
}
$data = new WP_Query($args);
$total_item = $data->found_posts;

if($data_source == 'list_id' && !empty( $names )) {
	remove_filter('posts_orderby', 'gf_order_by_slug', 10);
}
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
	$wrapper_styles = $animation_style;
}
if ($wrapper_styles) {
	$wrapper_attributes[] = 'style="' . implode('; ', $wrapper_styles) . '"';
}

$class_to_filter = implode(' ', array_filter($wrapper_classes));
$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX. 'portfolio', plugins_url(GF_PLUGIN_NAME . '/shortcodes/portfolio/assets/css/portfolio.min.css'), array(), false);
}
$min_suffix_js = gf_get_option('enable_minifile_js',0) == 1 ? '.min' : '';
wp_enqueue_script(GF_PLUGIN_PREFIX . 'portfolio', plugins_url(GF_PLUGIN_NAME . '/shortcodes/portfolio/assets/js/portfolio' . $min_suffix_js . '.js'), false, true);
wp_localize_script(GF_PLUGIN_PREFIX . 'portfolio', 'g5plus_portfolio_meta', array(
	'ajax_url' => admin_url('admin-ajax.php?activate-multi=true')
));
?>
<div class="<?php echo esc_attr($css_class); ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<?php if($is_filter):
		$filter_id = rand(); ?>
		<div data-filter_id="<?php echo esc_attr( $filter_id ); ?>" <?php echo implode( ' ', $filter_attributes ); ?>
			class="<?php echo implode( ' ', $filter_class ); ?>"
			<?php if(!empty( $archive_category )): ?> data-active-category=".<?php echo esc_attr( $archive_category )?>" <?php endif; ?>>
				<a data-filter="*" class="filter-category active-filter" style="cursor: not-allowed">
					<?php esc_html_e('ALL', 'g5plus-arvo') ?></a>
			<?php if($category != '') {
				$categories = explode(',', $category);
				foreach ($categories as $cat) {?>
					<a class="filter-category" data-filter=".<?php echo esc_attr($cat); ?>">
						<?php echo strtoupper(get_term_by('slug', $cat, G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY)->name); ?></a>
				<?php }
			} else {
				$categories = get_categories(array('taxonomy' => G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY,'hide_empty' => 0, 'orderby' => 'ASC'));
				if (is_array($categories)) {
					foreach ($categories as $cat) {
						$filter_id = rand();?>
							<a class="filter-category" data-filter=".<?php echo esc_attr($cat->slug); ?>">
								<?php echo strtoupper($cat->name) ?></a>
					<?php }
				}
			} ?>
		</div>
		<select class="active-mb portfolio-filter-mb">
			<option value="*"><?php esc_html_e('ALL', 'g5plus-arvo') ?></option>
			<?php if ($category != '') {
				$categories = explode(',', $category);
				foreach ($categories as $cat) {
					?>
					<option
						value=".<?php echo esc_attr($cat); ?>"><?php echo strtoupper(get_term_by('slug', $cat, G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY)->name); ?></option>
					<?php
				}
			} else {
				$categories = get_categories(array('taxonomy' => G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY,'hide_empty' => 0, 'orderby' => 'ASC'));
				if (is_array($categories)) {
					foreach ($categories as $cat) {
						?>
						<option value=".<?php echo esc_attr($cat->slug); ?>"><?php echo strtoupper($cat->name) ?></option>
						<?php
					}
				}
			}?>
		</select>
	<?php endif; ?>
	<div class="<?php echo implode(' ', $content_class); ?>" <?php echo implode(' ', $owl_attribute); ?> <?php echo implode( ' ', $content_attributes ); ?>>
		<?php $i = 0;
			if ($data->have_posts()) :
			while ($data->have_posts()): $data->the_post();
				$category1 = get_the_terms( get_the_ID(), G5PLUS_PORTFOLIO_CATEGORY_TAXONOMY );
				$cate_fitter = array();
				$cate_names = array();
				if($category1) {
					foreach($category1 as $str) {
						$cate_fitter[] = $str->slug;
						$cate_names[] = $str->name;
					}
				}
				if($is_filter) {
					$portfolio_item_classes = array_merge($portfolio_item_class, $cate_fitter);
				} else {
					$portfolio_item_classes = $portfolio_item_class;
				}
				$attach_id = get_post_thumbnail_id();

				$image_src = '';
				$item_class = '';
				if($layout_style != 'portfolio-grid' && $layout_style != 'portfolio-slider') {
					if($i == (count( $matrix))) {
						$i = 0;
					}
					if($layout_style == 'portfolio-masonry-metro-style4') {
						$image_size_masonry = $matrix[$i];
					}
					$img_size = explode(',',$image_size_masonry);
					if($layout_style == 'portfolio-masonry-normal') {
						if($columns == "2") {
							$item_class = 'portfolio-width50';
						} elseif ($columns == "3") {
							$item_class = 'portfolio-width33';
						} elseif ($columns == "4") {
							$item_class = 'portfolio-width25';
						} else {
							$item_class = 'portfolio-width20';
						}
						$image_src = wpb_resize($attach_id,null,$img_size[0], $img_size[1]*$matrix[$i],true);
					} else {
						if($layout_style == 'portfolio-masonry-metro-style1' || $layout_style == 'portfolio-masonry-metro-style2') {
							if($matrix[$i] != 4) {
								$image_src = wpb_resize($attach_id,null,$img_size[0]*$matrix[$i], $img_size[1],true);
								$item_class = 'portfolio-width'.(20*$matrix[$i]);
							} else {
								$image_src = wpb_resize($attach_id,null,$img_size[0]*2, $img_size[1]*2,true);
								$item_class = 'portfolio-width40';
							}
							if($layout_style == 'portfolio-masonry-metro-style1' && $i == 3) {
								$item_class.= ' portfolio-col-width20';
							}
							if($layout_style == 'portfolio-masonry-metro-style2' && $i == 0) {
								$item_class.= ' portfolio-col-width20';
							}
						} elseif($layout_style == 'portfolio-masonry-metro-style3') {
							if($matrix[$i] != 1) {
								$item_class = 'portfolio-width50';
								$image_src = wpb_resize($attach_id,null,960, 550,true);
							} else {
								$item_class = 'portfolio-width33';
								$image_src = wpb_resize($attach_id,null,$img_size[0], $img_size[1],true);
							}
						} else {
							$image_src = wpb_resize($attach_id,null,$img_size[0], $img_size[1],true);
							if($img_size[0] == '270') {
								$item_class = 'portfolio-width15';
							} elseif ($img_size[0] == '590') {
								$item_class = 'portfolio-width32';
							} else {
								$item_class = 'portfolio-width22';
							}
						}
					}
					$i++;

					if(sizeof( $image_src) > 0) {
						$image_src = $image_src['url'];
					}
					$portfolio_item_classes[] = $item_class;
				} else {
					$image_src = g5plus_get_image_src($attach_id, $image_size);
				}

				$image_full = wp_get_attachment_image_src( $attach_id, 'full' );
				if(sizeof( $image_full ) > 0) {
					$image_full = $image_full[0];
				}
				$image_thumb = wp_get_attachment_image_src($attach_id);
				$image_thumb_link = '';
				if (sizeof($image_thumb) > 0) {
					$image_thumb_link = $image_thumb['0'];
				}
				$gallery_id = rand();
				
				$portfolio_link = get_post_meta(get_the_ID(), 'portfolio-link', true);
				$portfolio_open_single_page = get_post_meta(get_the_ID(), 'portfolio-open-single-page', true);
				if(!isset( $portfolio_open_single_page ) || $portfolio_open_single_page == '') {
					$portfolio_open_single_page = '1';
				}
				if(empty( $portfolio_link ) || $portfolio_open_single_page == '1') {
					$portfolio_link = get_the_permalink();
				}
				?>
				<div class="<?php echo join(' ', $portfolio_item_classes); ?>">
					<div class="portfolio-inner">
						<div class="portfolio-avatar effect-content">
							<?php if (!empty($image_src)):?>
									<img src="<?php echo esc_url($image_src) ?>" alt="<?php the_title(); ?>"
								     title="<?php the_title(); ?>">
							<?php endif;?>
						</div>
						<div class="portfolio-item-content block-center">
							<div class="block-center-inner">
								<a href="<?php echo esc_url( $portfolio_link ); ?>" title="<?php the_title(); ?>" class="title s-font"><?php the_title() ?></a>
								<?php if (!empty($cate_names)): ?>
									<p class="portfolio-category"><?php echo join('/ ', $cate_names); ?></p>
								<?php endif;?>
								<a href="javascript:;" class="view-gallery" data-post-id="<?php the_ID(); ?>" >
									<i class="fa fa-search"></i>
								</a>
								<a href="<?php echo esc_url( $portfolio_link ); ?>" class="link-single" title="<?php the_title(); ?>">
									<i class="fa fa-share"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile;
		else: ?>
			<div class="item-not-found"><?php esc_html_e('No item found','g5plus-arvo'); ?></div><?php
		endif;?>
	</div>
	<?php if($show_paging == 'load-more'&& (($item_amount * $current_page < $total_item))) :?>
		<div class="clearfix text-center">
			<button data-layout_style="<?php echo esc_attr($layout_style)?>"
			        data-source="<?php echo esc_attr( $data_source ) ?>"
			        data-names="<?php echo esc_attr( $names )?>"
			        data-is_filter="<?php echo esc_attr( $is_filter ) ?>"
			        data-columns_gap="<?php echo esc_attr( $columns_gap ) ?>"
			        data-current_page="<?php echo (esc_attr($current_page) + 1) ?>"
			        data-columns="<?php echo esc_attr($columns)?>"
			        data-category="<?php echo esc_attr($category) ?>"
			        data-item_per_page="<?php echo esc_attr($item_amount)?>"
			        data-image_size="<?php echo esc_attr($image_size)?>"
			        data-hover-effect="<?php echo esc_attr($hover_effect)?>"
			        data-loading-text="<span class='fa fa-spinner fa-spin'></span> <?php esc_html_e('Loading...','g5plus-arvo'); ?>"
			        class="portfolio-load-more btn btn-md btn-outline btn-round">
				<?php esc_html_e('VIEW MORE WORK','g5plus-arvo'); ?>
			</button>
		</div>
	<?php endif;  ?>
</div>
<?php
wp_reset_postdata();
