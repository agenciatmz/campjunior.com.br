<?php
return array(
	'name'     => esc_html__('Portfolio', 'g5plus-arvo'),
	'base'     => 'g5plus_portfolio',
	'class'    => '',
	'icon'     => 'fa fa-windows',
	'category' => GF_SHORTCODE_CATEGORY,
	'params'   => array(
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Layout Style', 'g5plus-arvo'),
			'param_name' => 'layout_style',
			'value'      => array(
				esc_html__('Grid', 'g5plus-arvo') => 'portfolio-grid',
				esc_html__('Slider', 'g5plus-arvo') => 'portfolio-slider',
				esc_html__('Masonry Normal', 'g5plus-arvo') => 'portfolio-masonry-normal',
				esc_html__('Masonry Metro Style 1', 'g5plus-arvo') => 'portfolio-masonry-metro-style1',
				esc_html__('Masonry Metro Style 2', 'g5plus-arvo') => 'portfolio-masonry-metro-style2',
				esc_html__('Masonry Metro Style 3', 'g5plus-arvo') => 'portfolio-masonry-metro-style3',
				esc_html__('Masonry Metro Style 4', 'g5plus-arvo') => 'portfolio-masonry-metro-style4'
			),
			'admin_label' => true
		),
		array_merge(gf_vc_map_add_navigation(),array(
			'dependency' => array('element' => 'layout_style', 'value' => 'portfolio-slider'),
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-12 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_style(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_position(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination(),array(
			'dependency' => array('element' => 'layout_style', 'value' => 'portfolio-slider'),
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination_position(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array(
			'type' => 'dropdown',
			'heading'     => esc_html__('Source', 'g5plus-arvo'),
			'param_name'  => 'data_source',
			'admin_label' => true,
			'value'       => array(
				esc_html__('From Category', 'g5plus-arvo')      => '',
				esc_html__('From Portfolio IDs', 'g5plus-arvo') => 'list_id'
			)
		),
		array_merge(g5plus_vc_map_add_narrow_portfolio_category(), array(
			'dependency' => array('element' => 'data_source', 'value' => array(''))
		)),
		array(
			'type' => 'autocomplete',
			'heading' => esc_html__( 'Narrow Portfolio', 'g5plus-arvo' ),
			'param_name' => 'names',
			'settings' => array(
				'multiple' => true,
				'sortable' => true,
				'unique_values' => true,
			),
			'save_always' => true,
			'description' => esc_html__( 'Enter List of Portfolio', 'g5plus-arvo' ),
			'dependency' => array('element' => 'data_source','value' => 'list_id')
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Hover effect', 'g5plus-arvo'),
			'param_name' => 'hover_effect',
			'value'      => array(
				esc_html__('Default', 'g5plus-arvo') => 'default-effect',
				esc_html__('Layla', 'g5plus-arvo')   => 'layla-effect',
				esc_html__('Bubba', 'g5plus-arvo')   => 'bubba-effect',
				esc_html__('Jazz', 'g5plus-arvo')    => 'jazz-effect',
			)
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__('Show Category Filter', 'g5plus-arvo'),
			'param_name' => 'is_filter',
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array(
				'element' => 'layout_style',
				'value' => array('portfolio-grid', 'portfolio-masonry-normal', 'portfolio-slider', 'portfolio-masonry-metro-style1', 'portfolio-masonry-metro-style2', 'portfolio-masonry-metro-style3')
			),
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Active Category Color', 'g5plus-arvo'),
			'param_name' => 'active_cate_color',
			'value'      => array(
				esc_html__('Primary', 'g5plus-arvo')  => 'active-cate-accent',
				esc_html__('Dark', 'g5plus-arvo')  => 'active-cate-dark'),
			'dependency' => array('element' => 'is_filter', 'value' => array(true, 'true', 1)),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Show Paging', 'g5plus-arvo'),
			'param_name' => 'show_paging',
			'value'      => array(
				esc_html__('Show All', 'g5plus-arvo')  => '',
				esc_html__('Load More', 'g5plus-arvo')  => 'load-more'),
			'dependency' => array(
				'element' => 'layout_style',
				'value' => array('portfolio-grid', 'portfolio-masonry-normal', 'portfolio-masonry-metro-style1', 'portfolio-masonry-metro-style2', 'portfolio-masonry-metro-style3')),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type'       => 'textfield',
			'heading'    => esc_html__('Items Amount', 'g5plus-arvo'),
			'param_name' => 'item_amount',
			'std'      => 8,
			'value' => '8',
			'description' => esc_html__('Enter Items Amount or Items Per Page If Load More', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns', 'g5plus-arvo'),
			'param_name' => 'columns',
			'value' => array('2' => 2 , '3' => 3,'4' => 4, '5' => 5),
			'std' => 4,
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element'=>'layout_style', 'value'=>array('portfolio-grid', 'portfolio-slider', 'portfolio-masonry-normal'))
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Image Size', 'g5plus-arvo'),
			'param_name' => 'image_size',
			'value'      => array(
				'370x370' => 'portfolio-size-xs',
				'480x480' => 'portfolio-size-sm',
				'585x585' => 'portfolio-size-md',
				'640x427' => 'portfolio-size-lg',
				'640x480' => 'portfolio-size-xl',
				'640x640' => 'portfolio-size-xxl'
			),
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'description' => esc_html__('Select Image Size', 'g5plus-arvo'),
			'dependency' => array('element'=>'layout_style', 'value'=>array('portfolio-grid', 'portfolio-slider'))
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns Gap', 'g5plus-arvo'),
			'param_name' => 'columns_gap',
			'value' => array(
				'30px' => 'col-gap-30',
				'10px' => 'col-gap-10',
				'0px' => 'col-gap-0'
			),
			'std' => 'col-gap-0',
			'edit_field_class' => 'vc_col-sm-6 vc_column',
			'dependency' => array('element'=>'layout_style', 'value'=>array('portfolio-masonry-normal', 'portfolio-grid', 'portfolio-slider'))
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor(),
		array(
			'type' => 'hidden',
			'param_name' => 'current_page',
			'value' => 1
		),
		array(
			'type' => 'hidden',
			'param_name' => 'post_not_in',
			'value' => ''
		),
		array(
			'type' => 'hidden',
			'param_name' => 'archive_category',
			'value' => ''
		)
	)
);