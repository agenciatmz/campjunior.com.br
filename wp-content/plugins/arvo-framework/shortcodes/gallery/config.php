<?php
return array(
	'name'     => esc_html__('Gallery', 'g5plus-arvo'),
	'base'     => 'g5plus_gallery',
	'class'    => '',
	'icon'     => 'vc_general vc_element-icon icon-wpb-images-stack',
	'category' => GF_SHORTCODE_CATEGORY,
	'params'   => array(
		array(
			'type'       => 'checkbox',
			'heading'    => esc_html__('Slider style', 'g5plus-arvo'),
			'param_name' => 'is_slider',
			'admin_label' => true
		),
		array_merge(gf_vc_map_add_navigation(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-12 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_style(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_navigation_position(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array_merge(gf_vc_map_add_pagination_style(),array(
			'group' => esc_html__('Slider Options', 'g5plus-arvo'),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		)),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Hover effect', 'g5plus-arvo'),
			'param_name' => 'hover_effect',
			'std' => 'default-effect',
			'value'      => array(
				esc_html__('Default', 'g5plus-arvo') => 'default-effect',
				esc_html__('Layla', 'g5plus-arvo')   => 'layla-effect',
				esc_html__('Bubba', 'g5plus-arvo')   => 'bubba-effect',
				esc_html__('Jazz', 'g5plus-arvo')    => 'jazz-effect',
			)
		),
		array(
			'type' => 'attach_images',
			'heading' => esc_html__('Images', 'g5plus-arvo'),
			'param_name' => 'images',
			'value' => '',
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns', 'g5plus-arvo'),
			'param_name' => 'columns',
			'value' => array('2' => 2 , '3' => 3,'4' => 4, '5'=> 5, '6' => 6),
			'std' => 4,
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__('Image size', 'g5plus-arvo'),
			'param_name' => 'image_size',
			'value'      => array(
				'320x220' => 'gallery-size-sm',
				'430x430' => 'gallery-size-md',
			),
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		array(
			'type' => 'dropdown',
			'heading' => esc_html__('Columns Gap', 'g5plus-arvo'),
			'param_name' => 'columns_gap',
			'value' => array(
				'30px' => 'col-gap-30',
				'0px' => 'col-gap-0'
			),
			'std' => 'col-gap-30',
			'edit_field_class' => 'vc_col-sm-6 vc_column'
		),
		vc_map_add_css_animation(),
		gf_vc_map_add_animation_duration(),
		gf_vc_map_add_animation_delay(),
		gf_vc_map_add_extra_class(),
		gf_vc_map_add_css_editor()
	)
);