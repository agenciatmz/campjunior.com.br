<?php
/**
 * Created by PhpStorm.
 * User: Kaga
 * Date: 9/5/2016
 * Time: 4:44 PM
 */
return array(
		'name' => esc_html__('Clients', 'g5plus-arvo'),
		'base' => 'g5plus_clients',
		'icon' => 'fa fa-exchange',
		'category' => GF_SHORTCODE_CATEGORY,
		'params' => array(
			array(
				'type' => 'attach_images',
				'heading' => esc_html__('Images', 'g5plus-arvo'),
				'param_name' => 'images',
				'value' => '',
				'description' => esc_html__('Select images clients.', 'g5plus-arvo')
			),
			array(
				'type' => 'number',
				'class' => '',
				'heading' => esc_html__('Images opacity', 'g5plus-arvo'),
				'param_name' => 'images_opacity',
				'value' => '100',
				'min' => '1',
				'max' => '100',
				'suffix' => '%',
				'description' => esc_html__('Select opacity for images.', 'g5plus-arvo'),
			),
			array(
				'type' => 'exploded_textarea',
				'heading' => esc_html__('Custom links', 'g5plus-arvo'),
				'param_name' => 'custom_links',
				'description' => esc_html__('Enter links for each slide here. Divide links with linebreaks (Enter) . ', 'g5plus-arvo'),
			),
			array(
				'type' => 'checkbox',
				'heading' => esc_html__('Open link in a new tab ?', 'g5plus-arvo' ),
				'param_name' => 'custom_links_target',
				'std' => 'false',
				'edit_field_class' => 'vc_col-sm-4 vc_column'
			),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Display Show', 'g5plus-arvo' ),
				'param_name' => 'is_slider',
				'admin_label' => true,
				'edit_field_class' => 'vc_col-sm-6 vc_column',
				'value' => array(
					esc_html__('Slider', 'g5plus-arvo') => 'true',
					esc_html__('Grid', 'g5plus-arvo') => 'false',
				),
			),
			array_merge(gf_vc_map_add_navigation(),array(
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			)),
			array_merge(gf_vc_map_add_navigation_style(),array(
				'edit_field_class' => 'vc_col-sm-4 vc_column'
			)),
			array_merge(gf_vc_map_add_navigation_position(),array(
				'edit_field_class' => 'vc_col-sm-4 vc_column'
			)),
			array_merge(gf_vc_map_add_pagination(),array(
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			)),
			array_merge(gf_vc_map_add_pagination_style(),array(
				'edit_field_class' => 'vc_col-sm-4 vc_column'
			)),
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Columns Show ?', 'g5plus-arvo' ),
				'param_name' => 'items',
				'value' => array(
					'1 Columns' => '',
					'2 Columns' => '2',
					'3 Columns' => '3',
					'4 Columns' => '4',
					'5 Columns' => '5',
					'6 Columns' => '6',
				),
				'std' => '3',
				'edit_field_class' => 'vc_col-sm-6 vc_column',
				'dependency' => array('element' => 'is_slider', 'value' => 'false'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Items Desktop', 'g5plus-arvo'),
				'param_name' => 'items_lg',
				'description' => esc_html__('Browser Width > 1199', 'g5plus-arvo'),
				'value' => '',
				'std' => '5',
				'group'=> 'Reponsive',
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Items Desktop Small', 'g5plus-arvo'),
				'param_name' => 'items_md',
				'description' => esc_html__('Browser Width < 1199', 'g5plus-arvo'),
				'value' => '',
				'std' => '4',
				'group'=> 'Reponsive',
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Items Tablet', 'g5plus-arvo'),
				'param_name' => 'items_sm',
				'description' => esc_html__('Browser Width < 992', 'g5plus-arvo'),
				'value' => '',
				'std' => '3',
				'group'=> 'Reponsive',
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Items Tablet Small', 'g5plus-arvo'),
				'param_name' => 'items_xs',
				'description' => esc_html__('Browser Width < 768', 'g5plus-arvo'),
				'value' => '',
				'std' => '2',
				'group'=> 'Reponsive',
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			),
			array(
				'type' => 'textfield',
				'heading' => esc_html__('Items Mobile', 'g5plus-arvo'),
				'param_name' => 'items_mb',
				'description' => esc_html__('Browser Width < 480', 'g5plus-arvo'),
				'value' => '',
				'std' => '1',
				'group'=> 'Reponsive',
				'dependency' => array('element' => 'is_slider', 'value' => 'true'),
			),
			vc_map_add_css_animation(),
			gf_vc_map_add_animation_duration(),
			gf_vc_map_add_animation_delay(),
			gf_vc_map_add_extra_class(),
			gf_vc_map_add_css_editor()
	)
);