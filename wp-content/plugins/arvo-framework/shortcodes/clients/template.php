<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $images
 * @var $images_opacity
 * @var $custom_links
 * @var $custom_links_target
 * @var $is_slider
 * @var $nav
 * @var $nav_style
 * @var $nav_position
 * @var $dots
 * @var $dot_style
 * @var $dot_position
 * @var $items
 * @var $items_lg
 * @var $items_md
 * @var $items_sm
 * @var $items_xs
 * @var $items_mb
 * @var $css_animation
 * @var $animation_duration
 * @var $animation_delay
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_G5Plus_Clients
 */
$images = $images_opacity = $custom_links = $custom_links_target = $is_slider = $nav = $nav_style = $nav_position = $dots = $dot_style = $dot_position = $items = $items_lg = $items_md = $items_sm = $items_xs = $items_mb = $css_animation = $animation_duration = $animation_delay = $el_class = $css ='';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$custom_links = explode(',', $custom_links);
$images = explode(',', $images);

$wrapper_attributes = array();
$wrapper_styles = array();
$content_attributes = array();

$wrapper_classes = array(
'g5plus-clients',
$this->getExtraClass( $el_class ),
$this->getCSSAnimation( $css_animation ),
);

// animation
$animation_style = $this->getStyleAnimation($animation_duration, $animation_delay);
if (sizeof($animation_style) > 0) {
$wrapper_styles = $animation_style;
}
if ($wrapper_styles) {
	$wrapper_attributes[] = ' style="' . implode('; ', $wrapper_styles) . '"';
}

if ($is_slider=='true') {
	$content_classes[] = 'owl-carousel';

	if ($nav) {
		$content_classes[] = 'owl-nav-'. $nav_position;
		$content_classes[] = 'owl-nav-' . $nav_style;
	}

	if ($dots) {
		$content_classes[] = 'owl-dot-' . $dot_style;
		$content_classes[] = 'owl-dot-' . $dot_position;
	}

	$owl_responsive_attributes = array();
	// Mobile <= 480px
	$owl_responsive_attributes[] = '"0" : {"items" : ' . $items_mb . '}';

	// Extra small devices ( < 768px)
	$owl_responsive_attributes[] = '"481" : {"items" : ' . $items_xs . '}';

	// Small devices Tablets ( < 992px)
	$owl_responsive_attributes[] = '"768" : {"items" : ' . $items_sm . '}';

	// Medium devices ( < 1199px)
	$owl_responsive_attributes[] = '"992" : {"items" : ' . $items_md . '}';

	// Medium devices ( > 1199px)
	$owl_responsive_attributes[] = '"1200" : {"items" : ' . $items_lg . '}';

	$owl_attributes = array(
		'"autoHeight": true',
		'"dots": ' . ($dots ? 'true' : 'false'),
		'"nav": ' . ($nav ? 'true' : 'false'),
		'"responsive": {'. implode(', ', $owl_responsive_attributes) . '}'
	);
	$content_attributes[] = "data-plugin-options='{". implode(', ', $owl_attributes) ."}'";
}
else{
	$columns_md = 'columns-md-3';
	$columns_sm = 'columns-sm-3';
	$columns_xs = 'columns-xs-2';
	if($items == 2){
		$columns_md = 'columns-md-2';
		$columns_sm = 'columns-sm-2';
	}
	if($items == 1){
		$columns_sm = '';
		$columns_xs = '';
	}
	$content_classes[] = 'clients-columns columns-'.esc_attr($items).' '.$columns_md.' '.$columns_sm.' '.$columns_xs.' col-mb-12';
}

$css_content_class = implode(' ', array_filter($content_classes));
$class_to_filter = implode(' ', array_filter($wrapper_classes));

$class_to_filter .= vc_shortcode_custom_css_class($css, ' ');
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts);

if ($custom_links_target=='true') {
	$custom_links_target='_blank';
}
else{
	$custom_links_target='_self';
}

if (!(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG)) {
	wp_enqueue_style(GF_PLUGIN_PREFIX  . 'clients', plugins_url(GF_PLUGIN_NAME . '/shortcodes/clients/assets/css/clients.min.css'), array(), false, 'all');
}

$i=0;
?>
<div class="<?php echo esc_attr($css_class) ?>" <?php echo implode(' ', $wrapper_attributes); ?>>
	<div class="<?php echo esc_attr($css_content_class);?>" <?php echo implode(' ', $content_attributes) ?>>
		<?php foreach ($images as $attach_id):
			$i++;
			$class_border='';
			if(($i==1)||($i%$items==1)){
				$class_border ='no-border-left';
			}
			if($i%$items==0){
				$class_border .=' no-border-right';
			}
			if($i<=$items){
				$class_border .=' no-border-top';
			}
			else{
				$class_border .=' no-border-bottom';
			}
			$images_id = preg_replace( '/[^\d]/', '', $attach_id );
			$images_src = wp_get_attachment_image_src( $images_id, 'full' );
			$images_alt = the_title_attribute(array('post' => $images_id,'echo' => false ));
			if($images_src !='') {?>
				<?php if ($is_slider == 'false'): ?>
					<div class='clients-item text-center gf-item-wrap <?php echo esc_html($class_border) ?>' style="opacity: <?php echo esc_attr($images_opacity / 100) ?>">
				<?php else: ?>
					<div class='clients-item text-center' style="opacity: <?php echo esc_attr($images_opacity / 100) ?>">
				<?php endif; ?>
					<?php if (isset($custom_links[$i]) && $custom_links[$i] != ''): ?>
						<a title="<?php echo esc_attr($images_alt); ?>" href="<?php echo esc_url($custom_links[$i]) ?>"
						   target="<?php echo esc_attr($custom_links_target) ?>">
							<img alt="<?php echo esc_attr($images_alt) ?>" src="<?php echo esc_url($images_src[0]) ?>"/>
						</a>
					<?php else: ?>
						<img alt="<?php echo esc_attr($images_alt) ?>" src="<?php echo esc_url($images_src[0]) ?>"/>
					<?php endif;?>
				</div>
			<?php }
		endforeach; ?>
	</div>
</div>