<?php
    /**
     * The template for the header sticky bar.
     * Override this template by specifying the path where it is stored (templates_path) in your Redux config.
     *
     * @author        Redux Framework
     * @package       ReduxFramework/Templates
     * @version:      3.5.7.8
     */
$current_preset = isset($_GET['preset_id']) ? $_GET['preset_id'] : '';

$args = array(
	'posts_per_page' => 1000,
	'post_type' => 'gf_preset'
);
$presets = get_posts( $args );
?>
<div id="redux-sticky">
    <div id="info_bar">

        <a href="javascript:void(0);" class="expand_options<?php echo esc_attr(( $this->parent->args['open_expanded'] ) ? ' expanded' : ''); ?>"<?php echo esc_attr($this->parent->args['hide_expand'] ? ' style="display: none;"' : '');?>>
            <?php esc_attr_e( 'Expand', 'redux-framework' ); ?>
        </a>
	    <div class="preset-wrapper">
		    <select name="" id="gf_select_preset">
			    <option value="" <?php echo ($current_preset == '' ? 'selected' : ''); ?>><?php esc_html_e( '--[Select Preset]--', 'g5plus-arvo' ); ?></option>
			    <?php foreach ( $presets as $post ) : ?>
			        <option value="<?php echo esc_attr($post->ID) ?>" <?php echo ($current_preset == $post->ID ? 'selected' : ''); ?>><?php echo esc_html($post->post_title) ?></option>
			    <?php endforeach;?>
			    <?php wp_reset_postdata(); ?>
		    </select>
		    <button id="gf_choose_preset" type="button" class="button"><?php esc_html_e( 'Load Preset', 'g5plus-arvo' ); ?></button>
	    </div>
        <div class="redux-action_bar">
            <span class="spinner"></span>
            <?php if ( false === $this->parent->args['hide_save'] ) { ?>
                <?php submit_button( esc_attr__( 'Save Changes', 'redux-framework' ), 'primary', 'redux_save', false ); ?>
            <?php } ?>
            
            <?php if ( false === $this->parent->args['hide_reset'] ) { ?>
                <?php submit_button( esc_attr__( 'Reset Section', 'redux-framework' ), 'secondary', $this->parent->args['opt_name'] . '[defaults-section]', false, array( 'id' => 'redux-defaults-section' ) ); ?>
                <?php submit_button( esc_attr__( 'Reset All', 'redux-framework' ), 'secondary', $this->parent->args['opt_name'] . '[defaults]', false, array( 'id' => 'redux-defaults' ) ); ?>
            <?php } ?>
        </div>
        <div class="redux-ajax-loading" alt="<?php esc_attr_e( 'Working...', 'redux-framework' ) ?>">&nbsp;</div>
        <div class="clear"></div>
    </div>

    <!-- Notification bar -->
    <div id="redux_notification_bar">
        <?php $this->notification_bar(); ?>
    </div>


</div>