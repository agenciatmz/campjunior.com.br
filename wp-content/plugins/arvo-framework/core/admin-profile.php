<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 8/9/2016
 * Time: 11:29 AM
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if (!class_exists('GF_Admin_Profile')) {
	class GF_Admin_Profile {
		/**
		 * Hook in tabs.
		 */
		public function __construct() {
			add_action( 'show_user_profile', array( $this, 'add_customer_meta_fields' ) );
			add_action( 'edit_user_profile', array( $this, 'add_customer_meta_fields' ) );

			add_action( 'personal_options_update', array( $this, 'save_customer_meta_fields' ) );
			add_action( 'edit_user_profile_update', array( $this, 'save_customer_meta_fields' ) );
		}

		public function add_customer_meta_fields( $user ) {
			$show_fields = array();
			$profiles = gf_get_social_profiles();
			foreach ($profiles as $value) {
				$show_fields[$value['id']] = array(
					'title' => $value['title'],
					'icon' => $value['icon'],
					'type' => $value['link-type'],
					'description' => $value['subtitle']
				);
			}


			$social_profiles_sorter = get_user_meta( $user->ID, 'social_profiles_sorter', true );
			if (!isset($social_profiles_sorter) || ($social_profiles_sorter == null)) {
				$social_profiles_sorter = '';
			}
			$fields_sorted = explode(',', $social_profiles_sorter);
			foreach ($show_fields as $key => $field ) {
				if (!in_array($key, $fields_sorted) && ($field['type'] == 'link')) {
					$fields_sorted[] = $key;
				}
			}
			?>
			<h2><?php esc_html_e('Social Profiles', 'g5plus-arvo') ?></h2>
			<p><?php esc_html_e('Drag and Drop to change social order', 'g5plus-arvo') ?></p>
			<input type="hidden" id="social_profiles_sorter" name="social_profiles_sorter" value="<?php echo esc_attr($social_profiles_sorter) ?>"/>
			<table class="form-table social-profiles">
				<?php
				foreach ($fields_sorted as $key) :
					if (!isset($show_fields[$key])) {
						continue;
					}
					$field = $show_fields[$key];
					?>
					<tr>
						<th><label for="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $field['title'] ); ?></label></th>
						<td>
							<input type="text" name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $key ); ?>" value="<?php echo esc_attr( get_user_meta( $user->ID, $key, true ) ); ?>" class="regular-text" />
							<br/>
							<span class="description"><?php echo wp_kses_post( $field['description'] ); ?></span>
						</td>
					</tr>
					<?php
				endforeach;
				?>
			</table>

			<?php
		}

		public function save_customer_meta_fields( $user_id ) {
			if ( !current_user_can( 'edit_user', $user_id ) ) {
				return false;
			}
			$profiles = gf_get_social_profiles();
			foreach ($profiles as $key => $field ) {
				if ( isset( $_POST[ $field['id']] ) ) {
					update_user_meta( $user_id, $field['id'], sanitize_text_field( $_POST[ $field['id'] ] ) );
				}
			}
			if (isset( $_POST[ 'social_profiles_sorter' ] )) {
				update_user_meta( $user_id, 'social_profiles_sorter', sanitize_text_field( $_POST[ 'social_profiles_sorter' ] ) );
			}
			return true;
		}
	}
}
return new GF_Admin_Profile();