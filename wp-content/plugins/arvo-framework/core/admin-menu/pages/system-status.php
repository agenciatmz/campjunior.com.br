<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 6/4/2016
 * Time: 10:29 AM
 */

$system_infos = array(
    'php_version'        => array(
        'label'            => esc_html__('PHP Version', 'g5plus-arvo'),
        'help_tip'         => esc_html__('The version of PHP that running on your hosting.', 'g5plus-arvo'),
        'error'            => '%s - We recommend use php version at least %s. Please <a href="http://php.net/downloads.php" target="_blank">download and update latest version</a>',
        'require_value'    => 5.4,
        'value'            => PHP_VERSION,
        'convert_function' => 'floatval'
    ),
    'wp_version'         => array(
        'label'            => esc_html__('WP Version', 'g5plus-arvo'),
        'help_tip'         => esc_html__('The version of WordPress installed on your site.', 'g5plus-arvo'),
        'error'            => '%s - We recommend use WordPress at least %s. Please <a href="https://wordpress.org/download/" target="_blank">download latest version</a>',
        'require_value'    => 4.2,
        'value'            => get_bloginfo('version'),
        'convert_function' => 'floatval'
    ),
    'wp_multisite'       => array(
        'label'    => esc_html__('WP Multisite', 'g5plus-arvo'),
        'help_tip' => esc_html__('Whether or not you have WordPress Multisite enabled.', 'g5plus-arvo'),
        'value'    => (is_multisite() ? 'Yes' : 'No')
    ),
    'memory_limit'       => array(
        'label'            => esc_html__('WP Memory Limit', 'g5plus-arvo'),
        'help_tip'         => esc_html__('The maximum amount of memory (RAM) that your site can use at one time.', 'g5plus-arvo'),
        'error'            => '%s - We recommend setting memory to at least %s. See: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">Increasing memory allocated to PHP</a>',
        'require_value'    => '128M',
        'value'            => WP_MEMORY_LIMIT,
        'convert_function' => 'gf_memory_convert'
    ),
    'post_max_size'      => array(
        'label'            => esc_html__('Post Max Size', 'g5plus-arvo'),
        'help_tip'         => esc_html__('The maximum amount of data that your site can process on one request submit.', 'g5plus-arvo'),
        'error'            => '%s - We recommend setting memory to at least %s. See: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">Increasing memory allocated to PHP</a>',
        'require_value'    => '64M',
        'value'            => WP_MEMORY_LIMIT,
        'convert_function' => 'gf_memory_convert'
    ),
    'max_input_vars'     => array(
        'label'            => esc_html__('Max Input Vars', 'g5plus-arvo'),
        'help_tip'         => esc_html__('The maximum amount of data that your site can process on one request submit.', 'g5plus-arvo'),
        'error'            => '%s - We recommend setting max_input_var to at least %s. See: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">Increasing memory allocated to PHP</a>',
        'require_value'    => 3000,
        'value'            => ini_get('max_input_vars'),
        'convert_function' => 'floatval'
    ),
    'max_execution_time' => array(
        'label'            => esc_html__('Max Execution Time', 'g5plus-arvo'),
        'help_tip'         => esc_html__('The maximum execution time that your site waiting execute a request before throw timeout exception.', 'g5plus-arvo'),
        'error'            => '%s - We recommend setting execution time to at least %s milisecond. See: <a href="http://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">Increasing memory allocated to PHP</a>',
        'require_value'    => 300,
        'value'            => ini_get('max_input_vars'),
        'convert_function' => 'floatval'
    ),
    'language'           => array(
        'label'    => esc_html__('Language', 'g5plus-arvo'),
        'help_tip' => esc_html__('The current language used by WordPress. Default = English', 'g5plus-arvo'),
        'value'    => get_locale()
    ),
);

if(!function_exists('gf_memory_convert')){
    function gf_memory_convert($size)
    {
        $l = substr($size, -1);
        $ret = substr($size, 0, -1);
        switch (strtoupper($l)) {
            case 'P':
                $ret *= 1024;
            case 'T':
                $ret *= 1024;
            case 'G':
                $ret *= 1024;
            case 'M':
                $ret *= 1024;
            case 'K':
                $ret *= 1024;
        }
        return $ret;
    }
}

$require_value = $current_value = 0;
?>
<ul class="table-info">
    <?php foreach ($system_infos as $system_info) { ?>
        <li>
            <span class="label"><?php echo wp_kses_post($system_info['label']); ?> :</span>
            <span class="content">
                <?php if (isset($system_info['help_tip'])): ?>
                    <a href="javascript:;" class="help_tip"
                       title="<?php echo wp_kses_post($system_info['help_tip']); ?>">[?]</a>
                <?php endif; ?>
                <?php if (isset($system_info['require_value'])) : ?>
                    <?php
                    $require_value = $system_info['require_value'];
                    $current_value = $system_info['value'];
                    if (isset($system_info['convert_function'])) {
                        $require_value = call_user_func($system_info['convert_function'], $require_value);
                        $current_value = call_user_func($system_info['convert_function'], $current_value);
                    }
                    if ($current_value < $require_value):?>
                        <mark class="error">
                            <?php echo sprintf($system_info['error'], $system_info['value'], $system_info['require_value']) ?>
                        </mark>

                    <?php else: ?>
                        <mark class="yes"> <?php echo wp_kses_post($system_info['value']) ?></mark>
                    <?php endif;
                else: ?>
                    <mark class="yes"> <?php echo wp_kses_post($system_info['value']) ?></mark>
                <?php endif; ?>
            </span>
        </li>
    <?php } ?>
</ul>
