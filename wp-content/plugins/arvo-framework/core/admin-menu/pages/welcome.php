<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 6/4/2016
 * Time: 10:29 AM
 */


$theme = wp_get_theme();
if (!isset($theme)) {
    return;
}

$our_themes = array(

	array(
		'name'         => 'Mowasalat',
		'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/mowasalat.jpg',
		'link-preview' => 'https://themeforest.net/item/mowasalat-logistic-and-transports-wp-theme/17387412'
	),
	array(
		'name'         => 'Rica',
		'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/rica.jpg',
		'link-preview' => 'https://themeforest.net/item/rica-plus-a-delicious-restaurant-cafe-pub-wp-theme/17443393'
	),
	array(
		'name'         => 'Orson',
		'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/orson.jpg',
		'link-preview' => 'https://themeforest.net/item/orson-innovative-ecommerce-wordpress-theme-for-online-stores/16361340'
	),
	array(
		'name'         => 'Pithree',
		'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/pithree.jpg',
		'link-preview' => 'https://themeforest.net/item/pithree-construction-building-wordpress-theme/16467862'
	),
	array(
		'name'         => 'Porton',
		'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/porton.jpg',
		'link-preview' => 'https://themeforest.net/item/porton-responsive-multipurpose-wordpress-theme/16124830'
	),
    array(
        'name'         => 'Hearthstone',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/hearthstone.jpg',
        'link-preview' => 'http://themeforest.net/item/hearthstone-responsive-wordpress-blog-theme/15374354'
    ),
    array(
        'name'         => 'Academia',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/academia.jpg',
        'link-preview' => 'http://themeforest.net/item/academia-education-center-wordpress-theme/14806196'
    ),
    array(
        'name'         => 'Darna',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/darna.jpg',
        'link-preview' => 'http://themeforest.net/item/darna-building-construction-wordpress-theme/12271216'
    ),
    array(
        'name'         => 'Handmade',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/handmade.jpg',
        'link-preview' => 'http://themeforest.net/item/handmade-shop-wordpress-woocommerce-theme/13307231'
    ),
    array(
        'name'         => 'Innovation',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/innovation.jpg',
        'link-preview' => 'http://themeforest.net/item/innovation-construction-building-wordpress-theme/11967531'
    ),
    array(
        'name'         => 'Megatron',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/megatron.jpg',
        'link-preview' => 'http://themeforest.net/item/megatron-responsive-multipurpose-wordpress-theme/14063654'
    ),
    array(
        'name'         => 'Wolverine',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/wolverine.jpg',
        'link-preview' => 'http://themeforest.net/item/wolverine-responsive-multipurpose-theme/12789221'
    ),
    array(
        'name'         => 'Grove',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/grove.jpg',
        'link-preview' => 'http://themeforest.net/item/grove-responsive-multipurpose-wordpress-theme/11373752'
    ),
    array(
        'name'         => 'Zorka',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/zorka.jpg',
        'link-preview' => 'http://themeforest.net/item/zorka-wonderful-fashion-woocommerce-theme/11257557'
    ),
    array(
        'name'         => 'Cupid',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/cupid.jpg',
        'link-preview' => 'http://themeforest.net/item/cupid-adorable-kindergarten-wordpress-theme/10762082'
    ),
    array(
        'name'         => '',
        'thumb'        => GF_PLUGIN_URL . 'core/admin-menu/assets/img/more_theme.jpg',
        'description'  => '',
        'link-preview' => 'http://themeforest.net/user/g5theme/portfolio'
    )

)
?>
<div class="col-left">
    <h2 class="theme-info">
        Welcome to
        <span class="name"><?php echo wp_kses_post($theme->get('Name')); ?> </span>
        <span class="version"><?php echo wp_kses_post($theme->get('Version')); ?> </span>
    </h2>
    <div class="description">
	    <p>
		    <?php echo wp_kses_post($theme->get('Description')); ?>
	    </p>
        <p style="font-style: italic;color:#D6224C">
            Before use theme, please check that all the requirements on <a href="javascript:;" data-external="1"
                                                                           data-section-id="system-status">'System
                status'</a> tab are fulfilled and labeled in green.
        </p>
        <p>
            Once again, thank you for purchasing our theme. If you have any questions please feel free to us via email
            or create support ticket at <a href="http://support.g5plus.net/" target="_blank">support.g5plus.net</a>.
            Thanks so much!
        </p>

    </div>
</div>
<div class="col-right">
    <h2 class="theme-info text-center">
        Maybe you interest
    </h2>
    <div id="our-themes" class="tf_container">
        <div class="slider-wrap tf_slider">
            <div id="mac-book">
                <div class="ratio"></div>
                <div id="mac-screen">
                    <?php
                    $index = 0;
                    foreach ($our_themes as $our_theme) { ?>
                        <div class="hgh-linner hgi" data-index="<?php echo $index++; ?>">
                            <img src="<?php echo esc_url($our_theme['thumb']); ?>"
                                 class="attachment-highlight wp-post-image" alt="screen"/>
                            <div class="theme-overlay-wrap transition-10">
                                <div class="theme-overlay">
                                    <div class="bt-view-now">
                                        <a class="transition-10" target="_blank"
                                           href="<?php echo esc_url($our_theme['link-preview']); ?>">View more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <span id="left"></span>
                    <span id="right"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear: both"></div>