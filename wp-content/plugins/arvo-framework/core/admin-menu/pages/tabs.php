<?php
/**
 * Created by PhpStorm.
 * User: phuongth
 * Date: 6/4/2016
 * Time: 9:16 AM
 */


wp_enqueue_style('g5plus-admin-menu', GF_PLUGIN_URL . 'core/admin-menu/assets/style.min.css');
wp_enqueue_style('powertip', GF_PLUGIN_URL . 'core/admin-menu/assets/powertip/jquery.powertip.min.css');
wp_enqueue_style('animate', GF_PLUGIN_URL . 'core/admin-menu/assets/zenith/css/animate.min.css');
wp_enqueue_style('zenith', GF_PLUGIN_URL . 'core/admin-menu/assets/zenith/css/style.min.css');

wp_enqueue_script('g5plus-powertip', GF_PLUGIN_URL . 'core/admin-menu/assets/powertip/jquery.powertip.min.js', false, true);
wp_enqueue_script('hammer', GF_PLUGIN_URL . 'core/admin-menu/assets/zenith/js/hammer.min.js', false, true);
wp_enqueue_script('zenith', GF_PLUGIN_URL . '/core/admin-menu/assets/zenith/js/zenith.min.js', false, true);
wp_enqueue_script('g5plus-admin-menu', GF_PLUGIN_URL . 'core/admin-menu/assets/app.min.js', false, true);

$theme_name = 'arvo';
?>
<div class="welcome-wrap">
    <!-- Nav tabs -->
    <nav>
        <ul class="nav-tabs">
            <li class="tab-current">
                <a class="transition-10" href="javascript:;" data-section-id="welcome">
                    <i class="fa fa-home" ></i>
                    <span><?php esc_html_e('Welcome', 'g5plus-arvo'); ?></span>
                </a>
            </li>
            <li >
                <a class="transition-10" href="#system-status" data-section-id="system-status">
                    <i class="fa fa-television" ></i>
                    <span><?php esc_html_e('System status', 'g5plus-arvo'); ?></span>
                </a>
            </li>
            <li >
                <a class="transition-10" href="<?php echo esc_url(admin_url().'admin.php?page=g5plus_install_demo') ?>" >
                    <i class="fa fa-cogs" ></i>
                    <span><?php esc_html_e('Install demo', 'g5plus-arvo'); ?></span>
                </a>
            </li>
            <li >
                <a class="transition-10" href="<?php echo esc_url(admin_url().'admin.php?page=_options') ?>">
                    <i class="fa fa-sliders" ></i>
                    <span><?php esc_html_e('Theme Options', 'g5plus-arvo'); ?></span>
                </a>
            </li>
            <li ><a class="transition-10" href="<?php echo sprintf('http://themes.g5plus.net/documentation/%s/',$theme_name); ?>" target="_blank">
                    <i class="fa fa-book" ></i>
                    <span><?php esc_html_e('Documentation', 'g5plus-arvo'); ?></span></a>
            </li>
            <li ><a class="transition-10" href="http://support.g5plus.net/faq/" target="_blank">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <span><?php esc_html_e('FAQ', 'g5plus-arvo'); ?></span></a>
            </li>
        </ul>
    </nav>

    <!-- Tab panes -->
    <div class="content-wrap">
        <section id="welcome" class="content-current" >
            <?php gf_get_template('core/admin-menu/pages/welcome'); ?>
        </section>
        <section id="system-status" >
            <?php gf_get_template('core/admin-menu/pages/system-status'); ?>
        </section>
    </div>
</div>
