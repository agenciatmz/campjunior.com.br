/**
 * Created by phuongth on 6/4/2016.
 */
"use strict";
(function ($) {
    $(document).ready(function(){

        var $tab_wrap_class = '.welcome-wrap .nav-tabs';
        var $content_wrap_class = '.welcome-wrap .content-wrap';

        /** handler change tab **/
        $('a', '.welcome-wrap').click(function () {
            var $section_id = $(this).attr('data-section-id');
            if (typeof $section_id != 'undefined') {
                $('li', $tab_wrap_class).removeClass('tab-current');
                var $data_external = $(this).attr('data-external');
                if(typeof $data_external !='undefined'){
                    $('a[data-section-id="' + $section_id + '"]',$tab_wrap_class).parent().addClass('tab-current');
                }else{
                    $(this).parent().addClass('tab-current');
                }

                $('section', $content_wrap_class).hide();
                $('#' + $section_id, $content_wrap_class).show();
            }
        });

        /** handler hash **/
        var $hash = window.location.hash.substr(1);
        if(typeof $hash != 'undefined' && $('section[id="' + $hash + '"]', $content_wrap_class).length > 0){
            $('section', $content_wrap_class).hide();
            $('#' + $hash, $content_wrap_class).show();
            $('li', $tab_wrap_class).removeClass('tab-current');
            $('a[href="#' + $hash + '"]').parent().addClass('tab-current');
        }

        /** show tooltip **/
        $('.help_tip').powerTip({
            placement: 'ne' // north-east tooltip position
        });

        /** init our themes slider **/
        $('#our-themes').zenith({
            layout: 'screen',
            arrows: true,
            markup: ['.tf_container', '.tf_slide'],
            autoplaySpeed :	5000
        });

    })
})(jQuery)