<?php
/**
 *    Plugin Name: Arvo Framework
 *    Plugin URI: http://g5plus.net
 *    Description: The Arvo Framework plugin.
 *    Version: 1.4
 *    Author: g5plus
 *    Author URI: http://g5plus.net
 *
 *    Text Domain: arvo-framework
 *    Domain Path: /languages/
 *
 * @package G5Plus Framework
 * @category Core
 * @author g5plus
 *
 **/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('GF_Loader')) {
    class GF_Loader
    {
        public function __construct()
        {
            $this->define_constants();
            $this->includes();

            add_action('admin_enqueue_scripts', array(&$this, 'enqueue_admin_resources'));
            add_action('wp_enqueue_scripts', array(&$this, 'enqueue_frontend_resources'),100);
	        add_action('admin_enqueue_scripts',array($this,'dequeue_style'),100);

            add_action('load-post.php', array(&$this, 'enqueue_meta_box_resource'));
            add_action('load-post-new.php', array(&$this, 'enqueue_meta_box_resource'));

            add_filter('admin_enqueue_scripts', array(&$this, 'enqueue_redux_resource'));

            add_action( 'plugins_loaded',array(&$this, 'define_plugin_version') );
            add_action( 'plugins_loaded',array(&$this, 'include_vc_shortcode') );

            add_action('wp_ajax_popup_icon', array(&$this,'popup_icon'));

	        add_action( 'wp_footer', array($this,'enqueue_custom_script'));
        }

        //==============================================================================
        // Define constant
        //==============================================================================
        private function define_constants()
        {
            $plugin_dir_name = dirname(__FILE__);
            $plugin_dir_name = str_replace('\\', '/', $plugin_dir_name);
            $plugin_dir_name = explode('/', $plugin_dir_name);
            $plugin_dir_name = end($plugin_dir_name);

            if (!defined('GF_PLUGIN_NAME')) {
                define('GF_PLUGIN_NAME', $plugin_dir_name);
            }

            if (!defined('GF_PLUGIN_DIR')) {
                define('GF_PLUGIN_DIR', plugin_dir_path(__FILE__));
            }
            if (!defined('GF_PLUGIN_URL')) {
                define('GF_PLUGIN_URL', trailingslashit(plugins_url(GF_PLUGIN_NAME)));
            }

            if (!defined('GF_PLUGIN_PREFIX')) {
                define('GF_PLUGIN_PREFIX', 'arvo_framework_');
            }

            if (!defined('GF_METABOX_PREFIX')) {
                define('GF_METABOX_PREFIX', 'arvo_framework_');
            }

            if (!defined('GF_OPTIONS_NAME')) {
                define('GF_OPTIONS_NAME', 'arvo_options');
            }
        }

        //==============================================================================
        // Include library for plugin
        //==============================================================================
        private function includes()
        {
	        include_once GF_PLUGIN_DIR . 'core/wpalchemy/MetaBox.php';



	        /**
	         * Custom CSS Class
	         */
	        include_once GF_PLUGIN_DIR . 'core/widget-custom-class.php';

            /**
             * custom post type
             */
            include_once GF_PLUGIN_DIR . 'cpt/cpt.php';

            /**
             * Include less library
             */
            include_once GF_PLUGIN_DIR . 'core/less/Less.php';

            /**
             * Include less functions
             */
            include_once GF_PLUGIN_DIR . 'inc/less-functions.php';

            /**
             * Include functions
             */
            include_once GF_PLUGIN_DIR . 'inc/functions.php';

            /**
             * Plugin Admin Menu
             */
            include_once GF_PLUGIN_DIR . 'core/admin-menu/menu.php';

            /**
             * Include Action
             */
            include_once GF_PLUGIN_DIR . 'inc/action.php';

            /**
             * Include Filters
             */
            include_once GF_PLUGIN_DIR . 'inc/filter.php';

            /**
             * Include Post Type
             */
            include_once GF_PLUGIN_DIR . 'inc/post-type.php';

            /**
             * Include install demo data
             */
            include_once GF_PLUGIN_DIR . 'core/install-demo/install-demo.php';

            /**
             * Include theme-options
             */
            include_once GF_PLUGIN_DIR . 'core/theme-options/framework.php';
            include_once GF_PLUGIN_DIR . 'inc/options-functions.php';
            include_once GF_PLUGIN_DIR . 'inc/options-config.php';

            /**
             * Include MetaBox
             * *******************************************************
             */
            include_once GF_PLUGIN_DIR . 'core/meta-box/meta-box.php';
            include_once GF_PLUGIN_DIR . 'inc/meta-boxes.php';

	        /**
	         * Include MetaBox For Term
	         * *******************************************************
	         */
	        include_once GF_PLUGIN_DIR . 'core/tax-meta-class/tax-meta-class.php';
	        include_once GF_PLUGIN_DIR . 'inc/tax-meta.php';



	        /**
	         * Include XMENU
	         */
	        include_once GF_PLUGIN_DIR . 'core/xmenu/xmenu.php';





            /**
             * Include widget
             */
            include_once GF_PLUGIN_DIR . 'widgets/widgets.php';

	        /**
	         * Admin Profile
	         */
	        include_once GF_PLUGIN_DIR . 'core/admin-profile.php';

        }

        //==============================================================================
        // Define Plugin Version
        //==============================================================================
        public function define_plugin_version()
        {
	        $plugin_version = '1.0';
	        if (function_exists('get_plugin_data')) {
		        $plugin_data = get_plugin_data(__FILE__);
		        $plugin_version = $plugin_data['Version'];
	        }
	        if (!defined('GF_PLUGIN_VERSION')) {
		        define('GF_PLUGIN_VERSION', $plugin_version);
	        }
        }

	    //////////////////////////////////////////////////////////////////
	    // Dequeue Style Woocomerce
	    //////////////////////////////////////////////////////////////////
	    public function dequeue_style(){
		    $screen         = get_current_screen();
		    $screen_id      = $screen ? $screen->id : '';
		    $screen_ids   = array(
			    'widgets',
			    'toplevel_page__options'
		    );

		    if ( in_array( $screen_id, $screen_ids ) ) {
			    wp_dequeue_style( 'woocommerce_admin_styles' );
			    wp_dequeue_style('yith_wcan_admin');
			    wp_dequeue_style('jquery-ui-style');
			    wp_dequeue_style('yit-jquery-ui-style');
			    wp_dequeue_style('jquery-ui-overcast');
			    wp_dequeue_script('woocommerce_settings');
		    }
	    }

        //==============================================================================
        // Enqueue admin resources
        //==============================================================================
        public function enqueue_admin_resources()
        {

	        $screen         = get_current_screen();
	        $screen_id      = $screen ? $screen->id : '';
	        if ( $screen_id === 'toplevel_page__options' ) {
		        return;
	        }

            add_thickbox();
            // select2
	        $min_css_suffix = !(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG) ? '.min' : '';
            $min_suffix = gf_get_option('enable_minifile_js') ? '.min' : '';
	        wp_enqueue_style('select2',plugins_url(GF_PLUGIN_NAME. '/assets/plugins/jquery.select2/css/select2.min.css'),array(),'4.0.3','all');
	        wp_enqueue_script('select2',plugins_url(GF_PLUGIN_NAME . '/assets/plugins/jquery.select2/js/select2.full.min.js'),array('jquery'),'4.0.3',true);
            // datetimepicker
            wp_enqueue_style('rwmb-datetimepicker',plugins_url(GF_PLUGIN_NAME. '/assets/plugins/datetimepicker/css/datetimepicker.min.css'),array(),false,'all');

            wp_enqueue_style(GF_PLUGIN_PREFIX . 'admin', plugins_url(GF_PLUGIN_NAME . '/assets/css/admin'. $min_css_suffix .'.css'), array(), false, 'all');
            wp_enqueue_script(GF_PLUGIN_PREFIX.'popup-icon',plugins_url(GF_PLUGIN_NAME . '/assets/js/popup-icon'. $min_suffix .'.js'),array(),false,true);
            wp_localize_script(GF_PLUGIN_PREFIX . 'popup-icon', 'g5plus_framework_meta', array(
                'ajax_url' => admin_url('admin-ajax.php')
            ));


        }

        //==============================================================================
        // Enqueue frontend resources
        //==============================================================================
        public function enqueue_frontend_resources()
        {
	        $min_suffix = !(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG) ? '.min' : '';
            wp_enqueue_style(GF_PLUGIN_PREFIX . 'frontend', plugins_url(GF_PLUGIN_NAME . '/assets/css/frontend'.$min_suffix.'.css'), array(), false, 'all');
        }

	    public function enqueue_custom_script(){
		    $custom_js = gf_get_option('custom_js', '');
		    if ( $custom_js ) {
			    echo sprintf('<script type="text/javascript">%s</script>',$custom_js);
		    }
	    }

        public function enqueue_meta_box_resource()
        {
	        $min_css_suffix = !(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG) ? '.min' : '';
	        $min_suffix = gf_get_option('enable_minifile_js') ? '.min' : '';
            wp_enqueue_script(GF_PLUGIN_PREFIX . 'meta_box', GF_PLUGIN_URL . 'assets/js/meta-box.app'. $min_suffix .'.js', array(), GF_PLUGIN_VERSION, true);
            wp_enqueue_style(GF_PLUGIN_PREFIX . 'meta_box', GF_PLUGIN_URL . 'assets/css/meta-box'.$min_css_suffix.'.css', false, GF_PLUGIN_VERSION);
            wp_localize_script(GF_PLUGIN_PREFIX . 'meta_box', 'meta_box_prefix', GF_METABOX_PREFIX);
        }

        public function enqueue_redux_resource($hook)
        {
            if (preg_match('/_page__options$/', $hook, $matches)) {
	            $min_css_suffix = !(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG) ? '.min' : '';
	            $min_suffix = gf_get_option('enable_minifile_js') ? '.min' : '';
                wp_enqueue_script(GF_PLUGIN_PREFIX . 'redux_admin', GF_PLUGIN_URL . 'assets/js/redux.app'.$min_suffix.'.js', array(), GF_PLUGIN_VERSION, true);
                wp_enqueue_style(GF_PLUGIN_PREFIX . 'redux_admin', GF_PLUGIN_URL . 'assets/css/redux-admin'.$min_css_suffix.'.css', false, GF_PLUGIN_VERSION);
            }

        }

        public function include_vc_shortcode(){
            /**
             * Include shortcodes
             */
            if (class_exists('Vc_Manager')) {
                include_once GF_PLUGIN_DIR . 'shortcodes/shortcodes.php';
            }
        }

        public function popup_icon(){
            $font_awesome = &gf_get_font_awesome();
	        $font_pe_7_stroke = &gf_get_font_pe_icon_7_stroke();
            $font_theme_icon = &gf_get_theme_font();

            ob_start();
            ?>
            <div id="g5plus-framework-popup-icon-wrapper">
                <div class="popup-icon-wrapper">
                    <div class="popup-content">
                        <div class="popup-search-icon">
                            <input placeholder="Search" type="text" id="txtSearch">
                            <div class="preview">
                                <span></span> <a id="iconPreview" href="javascript:"><i class="fa fa-home"></i></a>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div class="list-icon">
                            <h3><?php esc_html_e('Font Theme Icon','g5plus-arvo'); ?></h3>
                            <ul id="group-1">
                                <?php foreach ($font_theme_icon as $icon) {
                                    $arrkey=array_keys($icon);
                                    ?>
                                    <li><a title="<?php echo esc_attr($arrkey[0]); ?>" href="javascript:"><i class="<?php echo esc_attr($arrkey[0]); ?>"></i></a></li>
                                    <?php

                                } ?>
                            </ul>
                            <br>
                            <h3>Font Awesome</h3>
                            <ul id="group-2">
                                <?php foreach ($font_awesome as $icon) {
                                    $arrkey=array_keys($icon);
                                    ?>
                                    <li><a title="<?php echo esc_attr($arrkey[0]); ?>" href="javascript:"><i class="<?php echo esc_attr($arrkey[0]); ?>"></i></a></li>
                                    <?php

                                } ?>
                            </ul>
	                        <br>
	                        <h3>Font Pe 7 Stroke</h3>
	                        <ul id="group-3">
		                        <?php foreach ($font_pe_7_stroke as $icon) {
			                        $arrkey=array_keys($icon);
			                        ?>
			                        <li><a title="<?php echo esc_attr($arrkey[0]); ?>" href="javascript:"><i class="<?php echo esc_attr($arrkey[0]); ?>"></i></a></li>
			                        <?php

		                        } ?>
	                        </ul>
                        </div>
                    </div>
                    <div class="popup-bottom">
                        <a id="btnSave" href="javascript:" class="button button-primary">Insert Icon</a>
                    </div>
                </div>
            </div>
            <?php
            die();

        }
    }


    /**
     * Instantiate the G5PLUS FRAMEWORK loader class.
     */
    $gf_loader = new GF_Loader();
}


