<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/18/2016
 * Time: 11:36 AM
 */
//////////////////////////////////////////////////////////////////
// Get Page Layout
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_page_layout')) {
    function gf_get_page_layout() {
        return apply_filters('gf_page_layout', array(
            'full'      => esc_html__('Full Width', 'g5plus-arvo'),
            'container' => esc_html__('Container', 'g5plus-arvo'),
	        'container-fluid' => esc_html__('Container Fluid', 'g5plus-arvo')
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Sidebar Layout
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_sidebar_layout')) {
    function gf_get_sidebar_layout()
    {
        return apply_filters('gf_sidebar_layout', array(
            'none'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/sidebar-none.png'),
            'left'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/sidebar-left.png'),
            'right' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/sidebar-right.png'),
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Sidebar Width
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_sidebar_width')) {
    function gf_get_sidebar_width($default = false)
    {
        $result = apply_filters('gf_sidebar_width', array(
            'small' => esc_html__('Small (1/4)', 'g5plus-arvo'),
            'large' => esc_html__('Large (1/3)', 'g5plus-arvo')
        ));

        if ($default) {
            $result = array(-1 => esc_html__('Default', 'g5plus-arvo')) + $result;
        }

        return $result;


    }
}

//////////////////////////////////////////////////////////////////
// Get Page Title Layout
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_page_title_layout')) {
    function gf_get_page_title_layout($default = false)
    {
        $result = apply_filters('gf_page_title_layout', array(
            'normal' => esc_html__('Title Left & Breadcrums Right', 'g5plus-arvo'),
            'centered' => esc_html__('Centered', 'g5plus-arvo'),
            'large-title' => esc_html__('Large Title', 'g5plus-arvo'),
        ));
        if ($default) {
            $result = array(-1 => esc_html__('Default', 'g5plus-arvo')) + $result;
        }
        return $result;
    }
}

//////////////////////////////////////////////////////////////////
// Get Layout Styles
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_layout_style')) {
    function gf_get_layout_style()
    {
        return apply_filters('gf_layout_style', array(
            'wide'  => array('title' => 'Wide', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/layout-wide.png'),
            'boxed' => array('title' => 'Boxed', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/layout-boxed.png'),
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Post Layout
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_post_layout')) {
    function gf_get_post_layout()
    {
        return apply_filters('gf_post_layout', array(
            'large-image'  => esc_html__('Large Image', 'g5plus-arvo'),
            'grid'      => esc_html__('Grid', 'g5plus-arvo'),
	        'masonry'      => esc_html__('Masonry', 'g5plus-arvo'),
	        '1-large-image-masonry' => esc_html__('1 Large Image Then Masonry Layout','g5plus-arvo'),
	        'large-image-2' => esc_html__('Large Image Center','g5plus-arvo'),
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Post Columns
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_post_columns')) {
    function gf_get_post_columns()
    {
        return apply_filters('gf_post_columns', array(
            2 => '2',
            3 => '3'
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Post Paging
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_paging_style')) {
    function gf_get_paging_style()
    {
        return apply_filters('gf_paging_style', array(
            'navigation'      => esc_html__('Navigation', 'g5plus-arvo'),
            'load-more'       => esc_html__('Load More', 'g5plus-arvo'),
            'infinite-scroll' => esc_html__('Infinite Scroll', 'g5plus-arvo')
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Toggle
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_toggle')) {
    function gf_get_toggle($default = false)
    {
        $result = array(
            1 => esc_html__('On', 'g5plus-arvo'),
            0 => esc_html__('Off', 'g5plus-arvo')

        );
        if ($default) {
            $result = array(-1 => esc_html__('Default', 'g5plus-arvo')) + $result;
        }

        return $result;
    }
}

//////////////////////////////////////////////////////////////////
// Get Product Image Hover Effect
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_product_image_hover_effect')) {
    function gf_get_product_image_hover_effect()
    {
        return apply_filters('gf_product_image_hover_effect', array(
            'none'         => esc_html__('None', 'g5plus-arvo'),
            'change-image' => esc_html__('Change Image', 'g5plus-arvo'),
            'flip-back'    => esc_html__('Flip Back', 'g5plus-arvo')
        ));
    }
}

//////////////////////////////////////////////////////////////////
// Get Search Ajax Post Type
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_post_type_search')) {
	function gf_get_post_type_search(){
		return apply_filters('gf_post_type_search',array(
			'post'      => esc_html__('Post','g5plus-arvo'),
			'page'      => esc_html__('Page','g5plus-arvo'),
			'product'   => esc_html__('Product','g5plus-arvo')
		));
	}
}

//==============================================================================
// Get Toggle Color
//==============================================================================
if (!function_exists('gf_get_toggle_color')) {
    function gf_get_toggle_color($inherit = false)
    {
        $result = array(
            0 => esc_html__('Default', 'g5plus-arvo'),
            1 => esc_html__('Customize', 'g5plus-arvo')
        );
        if ($inherit) {
            $result = array(-1 => esc_html__('Inherit', 'g5plus-arvo')) + $result;
        }

        return $result;
    }
}

//==============================================================================
// Get list social profiles
//==============================================================================
if (!function_exists('gf_get_social_profiles')) {
    function gf_get_social_profiles()
    {
        return apply_filters('gf_get_social_profiles', array(
            array(
                'id'        => 'twitter_url',
                'type'      => 'text',
                'title'     => esc_html__('Twitter', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Twitter', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-twitter',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'facebook_url',
                'type'      => 'text',
                'title'     => esc_html__('Facebook', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your facebook page/profile url', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-facebook',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'dribbble_url',
                'type'      => 'text',
                'title'     => esc_html__('Dribbble', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Dribbble', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-dribbble',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'vimeo_url',
                'type'      => 'text',
                'title'     => esc_html__('Vimeo', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Vimeo', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-vimeo',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'tumblr_url',
                'type'      => 'text',
                'title'     => esc_html__('Tumblr', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Tumblr', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-tumblr',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'skype_username',
                'type'      => 'text',
                'title'     => esc_html__('Skype', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Skype username', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-skype',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'linkedin_url',
                'type'      => 'text',
                'title'     => esc_html__('LinkedIn', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your LinkedIn page/profile url', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-linkedin',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'googleplus_url',
                'type'      => 'text',
                'title'     => esc_html__('Google+', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Google+ page/profile URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-google-plus',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'flickr_url',
                'type'      => 'text',
                'title'     => esc_html__('Flickr', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Flickr page url', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-flickr',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'youtube_url',
                'type'      => 'text',
                'title'     => esc_html__('YouTube', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your YouTube URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-youtube',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'pinterest_url',
                'type'      => 'text',
                'title'     => esc_html__('Pinterest', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Pinterest', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-pinterest',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'foursquare_url',
                'type'      => 'text',
                'title'     => esc_html__('Foursquare', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Foursqaure URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-foursquare',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'instagram_url',
                'type'      => 'text',
                'title'     => esc_html__('Instagram', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Instagram', 'g5plus-arvo'),
                'default'   => '',
	                'icon'      => 'fa fa-instagram',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'github_url',
                'type'      => 'text',
                'title'     => esc_html__('GitHub', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your GitHub URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-github',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'xing_url',
                'type'      => 'text',
                'title'     => esc_html__('Xing', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Xing URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-xing',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'behance_url',
                'type'      => 'text',
                'title'     => esc_html__('Behance', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Behance URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-behance',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'deviantart_url',
                'type'      => 'text',
                'title'     => esc_html__('Deviantart', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Deviantart URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-deviantart',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'soundcloud_url',
                'type'      => 'text',
                'title'     => esc_html__('SoundCloud', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your SoundCloud URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-soundcloud',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'yelp_url',
                'type'      => 'text',
                'title'     => esc_html__('Yelp', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your Yelp URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-yelp',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'rss_url',
                'type'      => 'text',
                'title'     => esc_html__('RSS Feed', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your RSS Feed URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-rss',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'vk_url',
                'type'      => 'text',
                'title'     => esc_html__('VK', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your VK URL', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-vk',
                'link-type' => 'link',
            ),
            array(
                'id'        => 'email_address',
                'type'      => 'text',
                'title'     => esc_html__('Email address', 'g5plus-arvo'),
                'subtitle'  => esc_html__('Your email address', 'g5plus-arvo'),
                'default'   => '',
                'icon'      => 'fa fa-envelope',
                'link-type' => 'email',
            ),
        ));
    }
}


//////////////////////////////////////////////////////////////////
// Get Search Type
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_search_type')) {
    function gf_get_search_type() {
        return apply_filters('gf_search_type', array(
            'button'           => esc_html__('Button', 'g5plus-arvo'),
            'box'              => esc_html__('Box', 'g5plus-arvo'),
        ));
    }
}


//////////////////////////////////////////////////////////////////
// Get Custom Page Layout
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_custom_page_layout')){
	function gf_get_custom_page_layout(){
		$settings = apply_filters('gf_custom_page_layout', array(
			'page' => array(
				'title' => esc_html__('Pages','g5plus-arvo'),
			),
			'blog' => array(
				'title' => esc_html__('Blog','g5plus-arvo'),
			),
			'single_blog' => array(
				'title' => esc_html__('Single Blog','g5plus-arvo'),
				'is_single' => true,
				'post_type' => 'post'
			),
			'product' => array(
				'title' => esc_html__('Archive Product','g5plus-arvo'),
				'category' => 'product_cat',
				'tag' => 'product_tag',
				'is_archive' => true,
				'post_type' => 'product'
			),
			'single_product' => array(
				'title' => esc_html__('Single Product','g5plus-arvo'),
				'is_single' => true,
				'post_type' => 'product'
			)
		));
		return $settings;
	}
}

//////////////////////////////////////////////////////////////////
// Get Custom Page Title
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_custom_page_title')){
	function gf_get_custom_page_title(){
		$settings = apply_filters('gf_custom_page_title', array(
			'blog' => array(
				'title' => esc_html__('Blog','g5plus-arvo'),
			),
			'single_blog' => array(
				'title' => esc_html__('Single Blog','g5plus-arvo'),
				'is_single' => true,
				'post_type' => 'post'
			),
			'product' => array(
				'title' => esc_html__('Archive Product','g5plus-arvo'),
				'category' => 'product_cat',
				'tag' => 'product_tag',
				'is_archive' => true,
				'post_type' => 'product'
			),
			'single_product' => array(
				'title' => esc_html__('Single Product','g5plus-arvo'),
				'is_single' => true,
				'post_type' => 'product'
			)
		));
		return $settings;
	}
}