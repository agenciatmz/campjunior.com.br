<?php
/**
 * COMMON FUNCTION FOR PLUGIN FRAMEWORK
 */

/**
 * GET Plugin template
 * *******************************************************
 */
function gf_get_template($slug, $args = array())
{
    if ($args && is_array($args)) {
        extract($args);
    }
    $located = GF_PLUGIN_DIR . $slug . '.php';
    if (!file_exists($located)) {
        _doing_it_wrong(__FUNCTION__, sprintf('<code>%s</code> does not exist.', $slug), '1.0');
        return;
    }
    include($located);
}

/**
 * GET Meta Box Value
 * *******************************************************
 */
if (!function_exists('gf_get_rwmb_meta')) {
    function gf_get_rwmb_meta($key, $args = array(), $post_id = null)
    {
        return rwmb_meta(GF_METABOX_PREFIX . $key, $args, $post_id);
    }
}

/**
 * GET Meta Box Image Value
 * *******************************************************
 */
if (!function_exists('gf_get_rwmb_meta_image')) {
    function gf_get_rwmb_meta_image($key, $post_id = null)
    {
        $metabox_id = gf_get_rwmb_meta($key, array(), $post_id);
        $metabox_value = gf_get_rwmb_meta($key, 'type=image_advanced', $post_id);
        if (($metabox_id === '') || ($metabox_id === false) || ($metabox_value === '') || ($metabox_value === false) || ($metabox_value === array())) {
            return '';
        }
        $image_url = '';
        if ($metabox_value !== array() && isset($metabox_value[$metabox_id]) && isset($metabox_value[$metabox_id]['full_url'])) {
            $image_url = $metabox_value[$metabox_id]['full_url'];
        }
        return $image_url;
    }
}

/**
 * GET theme option value
 * *******************************************************
 */
if (!function_exists('gf_get_option')) {
    function gf_get_option($key, $default = '')
    {
        return isset($GLOBALS[GF_OPTIONS_NAME][$key]) ? $GLOBALS[GF_OPTIONS_NAME][$key] : $default;
    }
}

/**
 * CONVERT hexa color to rgba color
 * *******************************************************
 */
if (!function_exists('gf_hex2rgba')) {
	function gf_hex2rgba($hex, $opacity) {
		if ($opacity > 1) {
			$opacity = $opacity / 100;
		}
		if (strtolower($hex) == 'transparent') {
			return 'transparent';
		}
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} elseif(strlen($hex) == 6) {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		else {
			$r = 0;
			$g = 0;
			$b = 0;
			$opacity = 0;
		}
		return sprintf('rgba(%s,%s,%s,%s)', $r, $g, $b, $opacity);
	}
}

/**
 * Load Preset Into Theme Options
 * *******************************************************
 */
if (!function_exists('gf_load_preset_into_theme_options')) {
	function &gf_load_preset_into_theme_options(&$options, $current_preset) {
		$color_fields = &gf_get_color_fields();
		$image_fields = &gf_get_image_fields();
		foreach ($options as $key => $value) {
			$field_type = isset(Redux::$fields[GF_OPTIONS_NAME])
			&& isset(Redux::$fields[GF_OPTIONS_NAME][$key])
			&& isset(Redux::$fields[GF_OPTIONS_NAME][$key]['type']) ? Redux::$fields[GF_OPTIONS_NAME][$key]['type'] : '';
			switch ($field_type) {
				case 'media':
					if (isset($image_fields[$key])) {
						$check = true;
						foreach ($image_fields[$key] as $condition_key =>  $condition_value) {
							$condition = gf_get_rwmb_meta($condition_key, array(), $current_preset);
							if ($condition != $condition_value) {
								$check = false;
								break;
							}
						}
						if ($check) {
							$meta_value = gf_get_rwmb_meta_image($key, $current_preset);
							$thumbnail_url = '';
							if ($meta_value !== false) {
								$meta_id = gf_get_rwmb_meta($key, array(), $current_preset);
								$thumbnail_url = wp_get_attachment_image_url($meta_id);
								$options[$key]['id'] = $meta_id;
							}
							$options[$key]['thumbnail'] = $thumbnail_url;
							$options[$key]['url'] = $meta_value;
						}
					} else {
						$meta_value = gf_get_rwmb_meta_image($key, $current_preset);
						$thumbnail_url= '';
						if ($meta_value !== false) {
							$meta_id = gf_get_rwmb_meta($key, array(), $current_preset);
							$thumbnail_url = wp_get_attachment_image_url($meta_id);
							$options[$key]['id'] = $meta_id;
						}
						$options[$key]['thumbnail'] = $thumbnail_url;
						$options[$key]['url'] = $meta_value;
					}
					break;
				case 'spacing':
					$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
					if ($meta_value !== '') {
						$options[$key]['padding-top'] = isset($meta_value['top']) && ($meta_value['top'] !== '') ? $meta_value['top'] . 'px' : '';
						$options[$key]['padding-bottom'] = isset($meta_value['bottom']) && ($meta_value['bottom'] !== '') ? $meta_value['bottom'] . 'px' : '';
					}
					break;
				case 'dimensions':
					$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
					if ($meta_value !== '') {
						$options[$key]['height'] = $meta_value;
					}
					break;
				case 'sorter':
					$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
					if ($meta_value !== '') {
						$enable_arr = isset($meta_value['enable']) ? $meta_value['enable'] : '';
						$enable_arr = explode('||', $enable_arr);
						$sorter_arr = array();
						if (isset($options[$key]['enabled']) && isset($options[$key]['disabled']) && is_array($options[$key]['enabled']) && is_array($options[$key]['enabled'])) {
							$sorter_arr = array_merge($options[$key]['enabled'], $options[$key]['disabled']);
						}
						$options[$key]['enabled'] = array();
						$options[$key]['disabled'] = array();

						foreach ($enable_arr as $sorter_key) {
							$sorter_value = isset($sorter_arr[$sorter_key]) ? $sorter_arr[$sorter_key] : '';
							$options[$key]['enabled'][$sorter_key] = $sorter_value;
						}
						foreach ($sorter_arr as $sorter_key => $sorter_value) {
							if (!in_array($sorter_key, $enable_arr)) {
								$options[$key]['disabled'][$sorter_key] = $sorter_value;
							}
						}
					}
					break;
				case 'label_value':
					$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
					if ($meta_value !== '') {
						$options[$key]['label'] = isset($meta_value['label']) ? $meta_value['label'] : '';
						$options[$key]['value'] = isset($meta_value['text']) ? $meta_value['text'] : '';
					}
					break;
				case 'background':
				case 'color_rgba':
				case 'typography':
					// NONE
					break;
				case 'color':
				case 'slider':
					if (isset($color_fields[$key])) {
						$condition_value = gf_get_rwmb_meta($color_fields[$key], array(), $current_preset);
						if ($condition_value) {
							$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
							if ($meta_value !== '') {
								$options[$key] = $meta_value;
							}
						}
					}
					else {
						$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
						if ($meta_value !== '') {
							$options[$key] = $meta_value;
						}
					}
					break;
				default:
					$meta_value = gf_get_rwmb_meta($key, array(), $current_preset);
					if ($meta_value !== '') {
						$options[$key] = $meta_value;
					}
					break;
			}

		}
		return $options;
	}
}

/**
 * Get Current Preset ID
 * *******************************************************
 */
if (!function_exists('gf_get_current_preset')) {
	function gf_get_current_preset() {
		global $post;
		$post_type = get_post_type($post);
		$preset_id = 0;
		$list_post_type = apply_filters('gf_post_type_preset', array());
		foreach ($list_post_type as $post_type_name => $post_type_value) {
			if (is_post_type_archive( $post_type_name ) || (isset( $post_type_value['category'] ) && is_tax($post_type_value['category'])) || (isset( $post_type_value['tag'] ) && is_tax($post_type_value['tag']))) {
				$page_preset = gf_get_option($post_type_name . '_preset');
				if ($page_preset) {
					$preset_id = $page_preset;
				}
			}
			/**
			 * Single Post Type
			 */
			elseif (is_singular($post_type_name)) {
				$page_preset = gf_get_option($post_type_name . '_single_preset');
				if ($page_preset) {
					$preset_id = $page_preset;
				}
			}
			if ($preset_id) {
				break;
			}
		}
		if ($preset_id) {}
		/**
		 * Product Page
		 */
		elseif (is_post_type_archive( 'product' ) || is_tax('product_cat') || is_tax('product_tag')) {
			$page_preset = gf_get_option('product_preset');
			if ($page_preset) {
				$preset_id = $page_preset;
			}
		}
		/**
		 * Single Product
		 */
		elseif (is_singular('product')) {
			$page_preset = gf_get_option('product_single_preset');
			if ($page_preset) {
				$preset_id = $page_preset;
			}
		}
		/**
		 * Blog
		 */
		elseif (is_home() || is_category() || is_tag() || is_search() || (is_archive() && $post_type == 'post')) {
			$page_preset = gf_get_option('blog_preset');
			if ($page_preset) {
				$preset_id = $page_preset;
			}
		}
		/**
		 * Blog Single
		 */
		elseif (is_singular('post')) {
			$page_preset = gf_get_option('blog_single_preset');
			if ($page_preset) {
				$preset_id = $page_preset;
			}
		}
		/**
		 * 404 Page
		 */
		elseif (is_404()) {
			$page_preset = gf_get_option('page_404_preset');
			if ($page_preset) {
				$preset_id = $page_preset;
			}
		}


		/**
		 * Single Page
		 */
		if (is_singular()) {
			/**
			 * Get Preset
			 */
			$page_preset = gf_get_rwmb_meta('page_preset');
			if ($page_preset) {
				$preset_id = $page_preset;
			}

			if (is_singular('gf_preset')) {
				$preset_id = get_the_ID();
			}
		}
		return $preset_id;
	}
}

/**
 * Get Preset Dir
 * *******************************************************
 */
if (!function_exists('gf_get_preset_dir')) {
	function gf_get_preset_dir() {
		return trailingslashit(get_template_directory()) . 'assets/preset/';
	}
}

/**
 * Get Preset Url
 * *******************************************************
 */
if (!function_exists('gf_get_preset_url')) {
	function gf_get_preset_url() {
		return trailingslashit(get_template_directory_uri()) . 'assets/preset/';
	}
}

/**
 * Enqueue Preset Style
 * @preset_type: style, rtl, tta
 * *******************************************************
 */
if (!function_exists('gf_enqueue_preset_style')) {
	function gf_enqueue_preset_style($preset_id, $preset_type) {
		$filename = $preset_id . '.' . $preset_type . '.min.css';
		if (!file_exists(gf_get_preset_dir() . $filename)) {
			gf_generate_less($preset_id);
			if (!file_exists(gf_get_preset_dir() . $filename)) {
				return false;
			}
		}
		wp_enqueue_style(GF_PLUGIN_PREFIX . '_preset_' . $preset_type, gf_get_preset_url() . $filename);
		return true;
	}
}

/**
 * Get Fonts Awesome Array
 * *******************************************************
 */
if (!function_exists('gf_get_font_awesome')) {
	function &gf_get_font_awesome() {
		if (function_exists('g5plus_get_font_awesome')) {
			return g5plus_get_font_awesome();
		}
		$fonts = array();
		return $fonts;
	}
}

/**
 * Get Fonts Pe 7 Stroke Array
 * *******************************************************
 */
if (!function_exists('gf_get_font_pe_icon_7_stroke')) {
	function &gf_get_font_pe_icon_7_stroke() {
		if (function_exists('g5plus_get_font_pe_icon_7_stroke')) {
			return g5plus_get_font_pe_icon_7_stroke();
		}
		$fonts = array();
		return $fonts;
	}
}



/**
 * Get Theme Font Icon
 * *******************************************************
 */
if (!function_exists('gf_get_theme_font')) {
	function &gf_get_theme_font() {
		if (function_exists('g5plus_get_theme_font')) {
			return g5plus_get_theme_font();
		}
		$fonts = array();
		return $fonts;
	}
}

/**
 * Get Post Thumbnail
 * *******************************************************
 */
if (!function_exists('gf_get_post_thumbnail')) {
	function gf_get_post_thumbnail($size, $noImage = 0, $is_single = false) {
		if (function_exists('g5plus_get_post_thumbnail')) {
			g5plus_get_post_thumbnail($size, $noImage, $is_single);
		}
	}
}

/**
 * Get Color Fields
 * *******************************************************
 */
if (!function_exists('gf_get_color_fields')) {
	function &gf_get_color_fields() {
		if (isset($GLOBALS['gf_color_field_setting'])) {
			return $GLOBALS['gf_color_field_setting'];
		}
		$GLOBALS['gf_color_field_setting'] = array(
			'accent_color' => 'custom_color_general',
			'foreground_accent_color' => 'custom_color_general',
			'text_color' => 'custom_color_general',
			'border_color' => 'custom_color_general',
			'text_color_bold' => 'custom_color_general',


			'top_drawer_bg_color' => 'custom_color_top_drawer',
			'top_drawer_text_color' => 'custom_color_top_drawer',

			'header_bg_color' => 'custom_color_header',
			'header_overlay' => 'custom_color_header',
			'header_text_color' => 'custom_color_header',
			'header_border_color' => 'custom_color_header',
			'header_above_border_color' => 'custom_color_header',

			'top_bar_bg_color' => 'custom_color_top_bar',
			'top_bar_overlay' => 'custom_color_top_bar',
			'top_bar_text_color' => 'custom_color_top_bar',
			'top_bar_border_color' => 'custom_color_top_bar',

			'navigation_bg_color' => 'custom_color_navigation',
			'navigation_overlay' => 'custom_color_navigation',
			'navigation_text_color' => 'custom_color_navigation',
			'navigation_text_color_hover' => 'custom_color_navigation',
			'navigation_customize_text_color' => 'custom_color_navigation',

			'footer_bg_color' => 'custom_color_footer',
			'footer_bg_overlay' => 'custom_color_footer',
			'footer_text_color' => 'custom_color_footer',
			'footer_widget_title_color' => 'custom_color_footer',
			'footer_border_color' => 'custom_color_footer',

			'bottom_bar_bg_color' => 'custom_color_bottom_bar',
			'bottom_bar_overlay' => 'custom_color_bottom_bar',
			'bottom_bar_text_color' => 'custom_color_bottom_bar',
			'bottom_bar_border_color' => 'custom_color_bottom_bar',
		);
		return $GLOBALS['gf_color_field_setting'];
	}
}


//////////////////////////////////////////////////////////////////
// Get Image Fields
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_image_fields')) {
	function &gf_get_image_fields(){
		if (isset($GLOBALS['gf_image_field_setting'])) {
			return $GLOBALS['gf_image_field_setting'];
		}
		$GLOBALS['gf_image_field_setting'] = array(
			'page_title_bg_image' => array(
				'page_title_enable' =>  '1',
				'custom_page_title_bg_image_enable' => '1'
			),
			'logo' => array(
				'custom_logo_enable' => '1'
			),
			'logo_retina' => array(
				'custom_logo_enable' => '1'
			),
			'sticky_logo' => array(
				'custom_logo_enable' => '1'
			),
			'sticky_logo_retina' => array(
				'custom_logo_enable' => '1'
			),
			'mobile_logo' => array(
				'custom_logo_mobile_enable' => '1'
			),
			'mobile_logo_retina' => array(
				'custom_logo_mobile_enable' => '1'
			),
			'footer_bg_image' => array(
				'custom_footer_bg_image_enable' => '1'
			),
		);
		return $GLOBALS['gf_image_field_setting'];

	}
}

//////////////////////////////////////////////////////////////////
// Get Page Layout Settings
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_page_layout_settings')) {
	function &gf_get_page_layout_settings(){
		$key_page_layout_settings = 'g5plus_page_layout_settings';
		if (isset($GLOBALS[$key_page_layout_settings]) && is_array($GLOBALS[$key_page_layout_settings])) {
			return $GLOBALS[$key_page_layout_settings];
		}
		$GLOBALS[$key_page_layout_settings] = array(
			'layout'                 => gf_get_option( 'layout','container' ),
			'sidebar_layout'         => gf_get_option( 'sidebar_layout','right' ),
			'sidebar'                => gf_get_option( 'sidebar','main-sidebar' ),
			'sidebar_width'          => gf_get_option( 'sidebar_width','small' ),
			'sidebar_mobile_enable'  => gf_get_option( 'sidebar_mobile_enable',1 ),
			'sidebar_mobile_canvas'  => gf_get_option( 'sidebar_mobile_canvas',1 ),
			'padding'                => gf_get_option( 'content_padding',array('padding-top' => '70px', 'padding-bottom' => '70px', 'units' => 'px') ),
			'padding_mobile'         => gf_get_option( 'content_padding_mobile',array('padding-top' => '30px', 'padding-bottom' => '30px', 'units' => 'px') ),
			'remove_content_padding' => 0,
			'has_sidebar' => 1
		);
		return $GLOBALS[$key_page_layout_settings];
	}
}

//////////////////////////////////////////////////////////////////
// Get Post Layout Settings
//////////////////////////////////////////////////////////////////
if (!function_exists('gf_get_post_layout_settings')){
	function &gf_get_post_layout_settings(){
		$key_post_layout_settings = 'g5plus_post_layout_settings';
		if (isset($GLOBALS[$key_post_layout_settings]) && is_array($GLOBALS[$key_post_layout_settings])) {
			return $GLOBALS[$key_post_layout_settings];
		}

		$GLOBALS[$key_post_layout_settings] = array(
			'layout'      => gf_get_option('post_layout','large-image'),
			'columns' => gf_get_option('post_column',3),
			'paging'      => gf_get_option('post_paging','navigation'),
			'slider'      => false
		);

		return $GLOBALS[$key_post_layout_settings];
	}
}