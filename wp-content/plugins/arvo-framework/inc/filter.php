<?php
/**
 * G5PLUS FRAMEWORK PLUGIN ACTION
 * *******************************************************
 */

/**
 * Add safe_style_css
 * *******************************************************
 */
if (!function_exists('gf_safe_style_css')) {
	function gf_safe_style_css($args) {
		$args [] = 'max-width';
		return $args;
	}
	add_filter( 'safe_style_css', 'gf_safe_style_css');
}

/**
 * Change Redux Url
 * *******************************************************
 */
if (!function_exists('gf_change_redux_url')) {
	function gf_change_redux_url($url) {
		return GF_PLUGIN_URL . 'core/theme-options/';
	}
	add_filter('redux/_url', 'gf_change_redux_url');
}

/**
 * Change Redux Url
 * *******************************************************
 */
if (!function_exists('gf_change_redux_extension_customizer_url')) {
	function gf_change_redux_extension_customizer_url($url) {
		return GF_PLUGIN_URL . 'core/theme-options/inc/extensions/customizer/';
	}
	add_filter('redux_extension_customizer/_url', 'gf_change_redux_extension_customizer_url');
}



/**
 * Set Preset Setting to Theme Options
 * *******************************************************
 */
if (!function_exists('gf_set_preset_to_theme_option')) {
	function gf_set_preset_to_theme_option($options) {
		$page = isset($_GET['page']) ? $_GET['page'] : '';
		if ($page === '_options') {
			$current_preset = isset($_GET['preset_id']) ? $_GET['preset_id'] : '';
			if ($current_preset) {
				$options = &gf_load_preset_into_theme_options($options, $current_preset);
			}
		}
		return $options;
	}
	add_filter("redux/options/" . GF_OPTIONS_NAME . "/options", 'gf_set_preset_to_theme_option');
}

/**
 * Move cat_count category into tag A
 * *******************************************************
 */
if (!function_exists('gf_cat_count_span')) {
	function gf_cat_count_span($links) {
		$links = str_replace('</a> (', ' (', $links);
		$links = str_replace(')', ')</a>', $links);
		return $links;
	}
	add_filter('wp_list_categories', 'gf_cat_count_span');
}

/**
 * This code filters the Archive widget to include the post count inside the link
 * *******************************************************
 */
if (!function_exists('gf_archive_count_span')) {
	function gf_archive_count_span($links) {
		$links = str_replace('</a>&nbsp;(', ' (', $links);
		$links = str_replace(')', ')</a>', $links);
		return $links;
	}
	add_filter('get_archives_link', 'gf_archive_count_span');
}

/**
 * Set Onepage menu
 * *******************************************************
 */
if (!function_exists('gf_main_menu_one_page_filter')) {
	function gf_main_menu_one_page_filter($args) {
		if (isset($args['theme_location']) && !in_array($args['theme_location'], apply_filters('xmenu_location_apply', array('primary', 'mobile')))) {
			return $args;
		}
		$is_one_page = gf_get_option('is_one_page', 0);
		if ($is_one_page == '1') {
			$args['menu_class'] .= ' menu-one-page';
		}
		return $args;
	}
	add_filter('wp_nav_menu_args','gf_main_menu_one_page_filter', 20);
}

/**
 * Add Custom MIME
 * *******************************************************
 */
if (!function_exists('gf_add_custom_mime_types')) {
	function gf_add_custom_mime_types($mimes)
	{
		return array_merge($mimes, array(
			'eot'  => 'application/vnd.ms-fontobject',
			'woff' => 'application/x-font-woff',
			'ttf'  => 'application/x-font-truetype',
			'svg' => 'image/svg+xml'
		));
	}
	add_filter('upload_mimes', 'gf_add_custom_mime_types');
}
