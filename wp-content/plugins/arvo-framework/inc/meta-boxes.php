<?php
/**
 * REGISTER META BOX FOR PRESET
 * *******************************************************
 */
if (!function_exists('gf_register_meta_boxes')) {
	function gf_register_meta_boxes()
	{
		$prefix = GF_METABOX_PREFIX;

		$post_type_meta_box = apply_filters('gf_post_setting_apply', array('post', 'page'));

		/**
		 * Get Theme Options Value
		 */
		$options = array();
		if (isset($GLOBALS[GF_OPTIONS_NAME])) {
			$options = &$GLOBALS[GF_OPTIONS_NAME];
		}

		/* PAGE MENU */
		$user_menus = get_categories(array('taxonomy' => 'nav_menu', 'hide_empty' => false ));
		$menu_list = array();
		foreach ( $user_menus as $menu ) {
			$menu_list[ $menu->term_id ] = $menu->name;
		}

		/* PRESET */
		$preset_list = array();
		$args = array(
			'posts_per_page' => 1000,
			'post_type' => 'gf_preset'
		);
		$presets = get_posts( $args );
		foreach ( $presets as $post ) {
			$preset_list[$post->ID] = $post->post_title;
		}
		wp_reset_postdata();


		$meta_boxes = array();

		/**
		 * Page Layout
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'layout_meta_box',
			'title'      => esc_html__('Page Layout', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name'     => esc_html__('Layout Style', 'g5plus-arvo'),
					'id'       => $prefix . 'layout_style',
					'type'     => 'button_set',
					'options'  => array(
						'boxed' => esc_html__('Boxed', 'g5plus-arvo'),
						'wide'  => esc_html__('Wide', 'g5plus-arvo')
					),
					'std'      => isset($options['layout_style']) ? $options['layout_style'] : 'wide',
					'multiple' => false,
				),
				array(
					'name'     => esc_html__('Layout', 'g5plus-arvo'),
					'id'       => $prefix . 'layout',
					'type'     => 'button_set',
					'options'  => gf_get_page_layout(),
					'std'      => isset($options['layout']) ? $options['layout'] : 'container',
					'multiple' => false,
				),
				array(
					'name'       => esc_html__('Sidebar Layout', 'g5plus-arvo'),
					'id'         => $prefix . 'sidebar_layout',
					'type'       => 'image_set',
					'options'    => array(
						'none'  => GF_PLUGIN_URL . '/assets/images/theme-options/sidebar-none.png',
						'left'  => GF_PLUGIN_URL . '/assets/images/theme-options/sidebar-left.png',
						'right' => GF_PLUGIN_URL . '/assets/images/theme-options/sidebar-right.png'
					),
					'std'        => isset($options['sidebar_layout']) ? $options['sidebar_layout'] : 'none',
					'multiple'   => false,

				),
				array(
					'name'           => esc_html__('Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'sidebar',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['sidebar']) ? $options['sidebar'] : 'main-sidebar',
					'js_options' => array('allowClear'  => false),
					'required-field' => array($prefix . 'sidebar_layout', '=', array('', 'right', 'left')),
				),

				array(
					'name'           => esc_html__('Sidebar Width', 'g5plus-arvo'),
					'id'             => $prefix . 'sidebar_width',
					'type'           => 'button_set',
					'options'        => gf_get_sidebar_width(),
					'std'            => isset($options['sidebar_width']) ? $options['sidebar_width'] : 'small',
					'multiple'       => false,
					'required-field' => array($prefix . 'sidebar_layout', '<>', 'none'),
				),

				array(
					'name'           => esc_html__('Sidebar Mobile', 'g5plus-arvo'),
					'id'             => $prefix . 'sidebar_mobile_enable',
					'type'           => 'button_set',
					'options'        => gf_get_toggle(),
					'std'            => isset($options['sidebar_mobile_enable']) ? $options['sidebar_mobile_enable'] : '1',
					'multiple'       => false,
					'required-field' => array($prefix . 'sidebar_layout', '<>', 'none'),
				),



				array(
					'id'    => $prefix . 'content_padding',
					'name'  => esc_html__('Content Padding', 'g5plus-arvo'),
					'desc'  => esc_html__('Set content top/bottom padding. Do not include units (empty to set default). Allow values (0,5,10,15....100)', 'g5plus-arvo'),
					'type'  => 'padding',
					'allow' => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['content_padding']) && isset($options['content_padding']['padding-top']) ? str_replace('px','',$options['content_padding']['padding-top']) : '',
						'bottom' => isset($options['content_padding']) && isset($options['content_padding']['padding-bottom']) ? str_replace('px','',$options['content_padding']['padding-bottom']) : '',
					)
				),

				array(
					'id'    => $prefix . 'content_padding_mobile',
					'name'  => esc_html__('Content Padding Mobile', 'g5plus-arvo'),
					'desc'  => esc_html__('Set content top/bottom padding mobile. Do not include units (empty to set default). Allow values (0,5,10,15....100)', 'g5plus-arvo'),
					'type'  => 'padding',
					'allow' => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['content_padding_mobile']) && isset($options['content_padding_mobile']['padding-top']) ? str_replace('px','',$options['content_padding_mobile']['padding-top']) : '',
						'bottom' => isset($options['content_padding_mobile']) && isset($options['content_padding_mobile']['padding-bottom']) ? str_replace('px','',$options['content_padding_mobile']['padding-bottom']) : '',
					)
				),


			)
		);

		/**
		 * Page Title
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'title_meta_box',
			'title'      => esc_html__('Page Title', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name'    => esc_html__('Show/Hide Page Title?', 'g5plus-arvo'),
					'id'      => $prefix . 'page_title_enable',
					'type'    => 'button_set',
					'std'     => isset($options['page_title_enable']) ? $options['page_title_enable'] : '1',
					'options' => gf_get_toggle()
				),

				array(
					'name'           => esc_html__('Page Title Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'page_title_layout',
					'type'           => 'button_set',
					'std'            => isset($options['page_title_layout']) ? $options['page_title_layout'] : 'centered',
					'options'        => gf_get_page_title_layout(),
					'required-field' => array($prefix . 'page_title_enable', '=', '1'),
				),

				array(
					'name'           => esc_html__('Page Title', 'g5plus-arvo'),
					'id'             => $prefix . 'page_title',
					'desc'           => esc_html__("Enter a custom page title if you'd like.", 'g5plus-arvo'),
					'type'           => 'text',
					'std'            => '',
					'required-field' => array($prefix . 'page_title_enable', '=', '1'),
				),

				array(
					'name'           => esc_html__('Page Subtitle', 'g5plus-arvo'),
					'id'             => $prefix . 'page_sub_title',
					'desc'           => esc_html__("Enter a custom page title if you'd like.", 'g5plus-arvo'),
					'type'           => 'text',
					'std'            => isset($options['page_sub_title']) ? $options['page_sub_title'] : '',
					'required-field' => array(
						array($prefix . 'page_title_enable', '=', '1'),
					),
				),

				array(
					'id'             => $prefix . 'page_title_padding',
					'name'           => esc_html__('Padding', 'g5plus-arvo'),
					'desc'           => esc_html__('Enter a page title padding top value (not include unit)', 'g5plus-arvo'),
					'type'           => 'padding',
					'allow'          => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['page_title_padding']) && isset($options['page_title_padding']['padding-top']) ? str_replace('px','',$options['page_title_padding']['padding-top']) : '',
						'bottom' => isset($options['page_title_padding']) && isset($options['page_title_padding']['padding-bottom']) ? str_replace('px','',$options['page_title_padding']['padding-bottom']) : '',
					),
					'required-field' => array($prefix . 'page_title_enable', '=', '1'),
				),


				// BACKGROUND IMAGE
				array(
					'id' => $prefix . 'custom_page_title_bg_image_enable',
					'name' => esc_html__('Custom Background Image','g5plus-arvo'),
					'desc' => esc_html__('Turn on this option if you want to enable custom background image of page title','g5plus-arvo'),
					'type' => 'button_set',
					'options' => gf_get_toggle(),
					'std' => 0,
					'required-field'   => array(
						array($prefix . 'page_title_enable', '=', '1'),
					),
				),
				array(
					'id'               => $prefix . 'page_title_bg_image',
					'name'             => esc_html__('Background Image', 'g5plus-arvo'),
					'desc'             => esc_html__('Background Image for page title.', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field'   => array(
						array($prefix . 'page_title_enable', '=', '1'),
						array($prefix. 'custom_page_title_bg_image_enable', '=' , '1')
					),
				),
				array(
					'name'           => esc_html__('Page Title Overlay Scheme', 'g5plus-arvo'),
					'id'             => $prefix . 'page_title_overlay_scheme',
					'type'           => 'button_set',
					'options'        => array(
						'page-title-overlay-dark' => esc_html__( 'Dark', 'g5plus-arvo' ),
						'page-title-overlay-light' => esc_html__( 'Light', 'g5plus-arvo' )
					),
					'std' => isset($options['page_title_overlay_scheme']) ? $options['page_title_overlay_scheme'] : 'page-title-overlay-dark',
					'required-field'   => array($prefix . 'page_title_enable', '=', '1' ),
				),
				array(
					'name'           => esc_html__('Page Title Parallax', 'g5plus-arvo'),
					'id'             => $prefix . 'page_title_parallax',
					'desc'           => esc_html__("Enable Page Title Parallax", 'g5plus-arvo'),
					'type'           => 'button_set',
					'options'        => gf_get_toggle(),
					'std'            => isset($options['page_title_parallax']) ? $options['page_title_parallax'] : 0,
					'required-field'   => array($prefix . 'page_title_enable', '=', '1'),
				),

				// Breadcrumbs in Page Title
				array(
					'name'           => esc_html__('Breadcrumbs Enable', 'g5plus-arvo'),
					'id'             => $prefix . 'breadcrumbs_enable',
					'desc'           => esc_html__("Show/Hide Breadcrumbs", 'g5plus-arvo'),
					'type'           => 'button_set',
					'options'        => gf_get_toggle(),
					'std'            => isset($options['breadcrumbs_enable']) ? $options['breadcrumbs_enable'] : 1,
					'required-field' => array($prefix . 'page_title_enable', '<>', 0),
				),
			)
		);

		/**
		 * Logo
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'logo_meta_box',
			'title'      => esc_html__('Logo', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name' => esc_html__('Desktop Settings', 'g5plus-arvo'),
					'id'   => $prefix . 'page_logo_section_1',
					'type' => 'section',
					'std'  => '',
				),
				array(
					'id' => $prefix . 'custom_logo_enable',
					'name' => esc_html__('Custom Logo','g5plus-arvo'),
					'desc' => esc_html__('Turn on this option if you want to enable custom logo','g5plus-arvo'),
					'type' => 'button_set',
					'options' => gf_get_toggle(),
					'std' => 0
				),
				array(
					'id'               => $prefix . 'logo',
					'name'             => esc_html__('Header Logo', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload custom logo in header.', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field' => array($prefix . 'custom_logo_enable', '=', '1'),
				),
				array(
					'id'               => $prefix . 'logo_retina',
					'name'             => esc_html__('Header Logo Retina', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload custom logo retina in header.', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field' => array($prefix . 'custom_logo_enable', '=', '1'),
				),
				array(
					'id'               => $prefix . 'sticky_logo',
					'name'             => esc_html__('Sticky Logo', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload sticky logo in header (empty to default)', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field' => array($prefix . 'custom_logo_enable', '=', '1'),
				),
				array(
					'id'               => $prefix . 'sticky_logo_retina',
					'name'             => esc_html__('Sticky Logo Retina', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload sticky logo retina in header (empty to default)', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field' => array($prefix . 'custom_logo_enable', '=', '1'),
				),
				array(
					'id'   => $prefix . 'logo_max_height',
					'name' => esc_html__('Logo max height', 'g5plus-arvo'),
					'desc' => esc_html__('Logo max height (px). Do not include units (empty to set default)', 'g5plus-arvo'),
					'type' => 'text',
					'std'  => isset($options['logo_max_height']) && isset($options['logo_max_height']['height']) ? str_replace('px','',$options['logo_max_height']['height']) : '',
				),
				array(
					'id'    => $prefix . 'logo_padding',
					'name'  => esc_html__('Logo padding', 'g5plus-arvo'),
					'desc'  => esc_html__('Logo padding top. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'  => 'padding',
					'allow' => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['logo_padding']) && isset($options['logo_padding']['padding-top']) ? str_replace('px','',$options['logo_padding']['padding-top']) : '',
						'bottom' => isset($options['logo_padding']) && isset($options['logo_padding']['padding-bottom']) ? str_replace('px','',$options['logo_padding']['padding-bottom']) : '',
					),
				),

				array(
					'name' => esc_html__('Mobile Settings', 'g5plus-arvo'),
					'id'   => $prefix . 'page_logo_section_2',
					'type' => 'section',
					'std'  => '',
				),
				array(
					'id' => $prefix . 'custom_logo_mobile_enable',
					'name' => esc_html__('Custom Mobile Logo','g5plus-arvo'),
					'desc' => esc_html__('Turn on this option if you want to enable custom mobile logo','g5plus-arvo'),
					'type' => 'button_set',
					'options' => gf_get_toggle(),
					'std' => 0,

				),
				array(
					'id'               => $prefix . 'mobile_logo',
					'name'             => esc_html__('Mobile Logo', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload mobile logo in header.', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field' => array($prefix . 'custom_logo_mobile_enable', '=', '1'),
				),
				array(
					'id'               => $prefix . 'mobile_logo_retina',
					'name'             => esc_html__('Mobile Logo Retina', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload mobile logo retina in header.', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field' => array($prefix . 'custom_logo_mobile_enable', '=', '1'),
				),
				array(
					'id'   => $prefix . 'mobile_logo_max_height',
					'name' => esc_html__('Mobile Logo Max Height', 'g5plus-arvo'),
					'desc' => esc_html__('Logo max height (px). Do not include units (empty to set default)', 'g5plus-arvo'),
					'type' => 'text',
					'std'  => isset($options['mobile_logo_max_height']) && isset($options['mobile_logo_max_height']['height']) ? str_replace('px','',$options['mobile_logo_max_height']['height']) : '',
				),
				array(
					'id'    => $prefix . 'mobile_logo_padding',
					'name'  => esc_html__('Mobile logo padding', 'g5plus-arvo'),
					'desc'  => esc_html__('Mobile logo padding top. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'  => 'padding',
					'allow' => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['mobile_logo_padding']) && isset($options['mobile_logo_padding']['padding-top']) ? str_replace('px','',$options['mobile_logo_padding']['padding-top']) : '',
						'bottom' => isset($options['mobile_logo_padding']) && isset($options['mobile_logo_padding']['padding-bottom']) ? str_replace('px','',$options['mobile_logo_padding']['padding-bottom']) : '',
					),
				),
			)
		);

		/**
		 * TOP DRAWER
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'top_drawer_meta_box',
			'title'      => esc_html__('Top Drawer', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name'    => esc_html__('Top Drawer Type', 'g5plus-arvo'),
					'id'      => $prefix . 'top_drawer_type',
					'type'    => 'button_set',
					'std'     => isset($options['top_drawer_type']) ? $options['top_drawer_type'] : 'none',
					'options' => array(
						'none'   => esc_html__('Disable', 'g5plus-arvo'),
						'show'   => esc_html__('Always Show', 'g5plus-arvo'),
						'toggle' => esc_html__('Toggle', 'g5plus-arvo')
					),
					'desc'    => esc_html__('Top drawer type', 'g5plus-arvo'),
				),
				array(
					'name'           => esc_html__('Top Drawer Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'top_drawer_sidebar',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['top_drawer_sidebar']) ? $options['top_drawer_sidebar'] : '',
					'js_options' => array('allowClear'  => false),
					'required-field' => array($prefix . 'top_drawer_type', '<>', 'none'),
				),

				array(
					'name'           => esc_html__('Top Drawer Wrapper Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'top_drawer_wrapper_layout',
					'type'           => 'button_set',
					'std'            => isset($options['top_drawer_wrapper_layout']) ? $options['top_drawer_wrapper_layout'] : 'container',
					'options'        => array(
						'full'            => esc_html__('Full Width', 'g5plus-arvo'),
						'container'       => esc_html__('Container', 'g5plus-arvo'),
						'container-fluid' => esc_html__('Container Fluid', 'g5plus-arvo')
					),
					'required-field' => array($prefix . 'top_drawer_type', '<>', 'none'),
				),

				array(
					'name'           => esc_html__('Top Drawer hide on mobile', 'g5plus-arvo'),
					'id'             => $prefix . 'top_drawer_hide_mobile',
					'type'           => 'button_set',
					'std'            => isset($options['top_drawer_hide_mobile']) ? $options['top_drawer_hide_mobile'] : '1',
					'options'        => array(
						'1'  => esc_html__('Show on mobile', 'g5plus-arvo'),
						'0'  => esc_html__('Hide on mobile', 'g5plus-arvo'),
					),
					'required-field' => array($prefix . 'top_drawer_type', '<>', 'none'),
				),
				array(
					'id'    => $prefix . 'top_drawer_padding',
					'name'  => esc_html__('Top drawer padding', 'g5plus-arvo'),
					'desc'  => esc_html__('Top drawer padding top. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'  => 'padding',
					'allow' => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['top_drawer_padding']) && isset($options['top_drawer_padding']['padding-top']) ? str_replace('px','',$options['top_drawer_padding']['padding-top']) : '',
						'bottom' => isset($options['top_drawer_padding']) && isset($options['top_drawer_padding']['padding-bottom']) ? str_replace('px','',$options['top_drawer_padding']['padding-bottom']) : '',
					),
					'required-field' => array($prefix . 'top_drawer_type', '<>', 'none'),
				),
			)
		);

		/**
		 * TOP BAR
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'top_bar_meta_box',
			'title'      => esc_html__('Top Bar', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name' => esc_html__('Desktop Settings', 'g5plus-arvo'),
					'id'   => $prefix . 'top_bar_desktop_section',
					'type' => 'section',
					'std'  => 0,
				),
				array(
					'name'    => esc_html__('Show/Hide Top Bar', 'g5plus-arvo'),
					'id'      => $prefix . 'top_bar_enable',
					'type'    => 'button_set',
					'std'     => isset($options['top_bar_enable']) ? $options['top_bar_enable'] : '0',
					'options' => array(
						'1'  => esc_html__('Show', 'g5plus-arvo'),
						'0'  => esc_html__('Hide', 'g5plus-arvo')
					),
					'desc'    => esc_html__('Show/Hide Top Bar.', 'g5plus-arvo'),
				),

				array(
					'name'           => esc_html__('Top Bar Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_layout',
					'type'           => 'image_set',
					'width'          => '80px',
					'std'            => isset($options['top_bar_layout']) ? $options['top_bar_layout'] : 'top-bar-1',
					'options'        => array(
						'top-bar-1' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-1.jpg',
						'top-bar-2' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-2.jpg',
						'top-bar-3' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-3.jpg',
						'top-bar-4' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-4.jpg'
					),
					'required-field' => array(
						array($prefix . 'top_bar_enable', '<>', '0'),
					),
				),

				array(
					'name'           => esc_html__('Top Left Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_left_sidebar',
					'type'           => 'sidebars',
					'std'            => isset($options['top_bar_left_sidebar']) ? $options['top_bar_left_sidebar'] : 'top_bar_left',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'top_bar_enable', '<>', '0'),
					),
				),

				array(
					'name'           => esc_html__('Top Right Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_right_sidebar',
					'type'           => 'sidebars',
					'std'            => isset($options['top_bar_right_sidebar']) ? $options['top_bar_right_sidebar'] : 'top_bar_right',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'top_bar_enable', '<>', '0'),
						array($prefix . 'top_bar_layout', '<>', 'top-bar-4'),
					),
				),

				array(
					'name'           => esc_html__('Top Bar Border', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_border',
					'type'           => 'button_set',
					'std'            => isset($options['top_bar_border']) ? $options['top_bar_border'] : 'none',
					'options'        => array(
						'none'             => esc_html__('None', 'g5plus-arvo'),
						'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
						'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
					),
					'desc'           => esc_html__('Show Hide Top Bar.', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'top_bar_enable', '<>', '0'),
					),
				),
				array(
					'id'             => $prefix . 'top_bar_padding',
					'name'           => esc_html__('Top bar padding', 'g5plus-arvo'),
					'desc'           => esc_html__('Top bar padding top. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'           => 'padding',
					'allow'          => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['top_bar_padding']) && isset($options['top_bar_padding']['padding-top']) ? str_replace('px','',$options['top_bar_padding']['padding-top']) : '',
						'bottom' => isset($options['top_bar_padding']) && isset($options['top_bar_padding']['padding-bottom']) ? str_replace('px','',$options['top_bar_padding']['padding-bottom']) : '',
					),
					'required-field' => array(
						array($prefix . 'top_bar_enable', '<>', '0'),
					),
				),

				array(
					'name' => esc_html__('Mobile Settings', 'g5plus-arvo'),
					'id'   => $prefix . 'top_bar_mobile_section',
					'type' => 'section',
					'std'  => '',
				),
				array(
					'name'    => esc_html__('Show/Hide Top Bar', 'g5plus-arvo'),
					'id'      => $prefix . 'top_bar_mobile_enable',
					'type'    => 'button_set',
					'options' => array(
						'1'  => esc_html__('Show', 'g5plus-arvo'),
						'0'  => esc_html__('Hide', 'g5plus-arvo')
					),
					'std'            => isset($options['top_bar_mobile_enable']) ? $options['top_bar_mobile_enable'] : '0',
					'desc'    => esc_html__('Show/Hide Top Bar.', 'g5plus-arvo'),
				),
				array(
					'name'           => esc_html__('Top Bar Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_mobile_layout',
					'type'           => 'image_set',
					'width'          => '80px',
					'std'            => isset($options['top_bar_mobile_layout']) ? $options['top_bar_mobile_layout'] : 'top-bar-1',
					'options'        => array(
						'top-bar-1' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-1.jpg',
						'top-bar-2' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-2.jpg',
						'top-bar-3' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-3.jpg',
						'top-bar-4' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-4.jpg'
					),
					'required-field' => array(
						array($prefix . 'top_bar_mobile_enable', '<>', '0'),
					),
				),

				array(
					'name'           => esc_html__('Top Left Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_mobile_left_sidebar',
					'type'           => 'sidebars',
					'std'            => isset($options['top_bar_mobile_left_sidebar']) ? $options['top_bar_mobile_left_sidebar'] : 'top_bar_left',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'top_bar_mobile_enable', '<>', '0'),
					),
				),

				array(
					'name'           => esc_html__('Top Right Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_mobile_right_sidebar',
					'type'           => 'sidebars',
					'std'            => isset($options['top_bar_mobile_right_sidebar']) ? $options['top_bar_mobile_right_sidebar'] : 'top_bar_right',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'top_bar_mobile_enable', '<>', '0'),
						array($prefix . 'top_bar_mobile_layout', '<>', 'top-bar-4'),
					),
				),
				array(
					'name'           => esc_html__('Top Bar Border', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_mobile_border',
					'type'           => 'button_set',
					'std'            => isset($options['top_bar_mobile_border']) ? $options['top_bar_mobile_border'] : 'none',
					'options'        => array(
						'none'             => esc_html__('None', 'g5plus-arvo'),
						'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
						'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
					),
					'desc'           => esc_html__('Show Hide Top Bar.', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'top_bar_mobile_enable', '<>', '0'),
					),
				),
			)
		);

		/**
		 * Header
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'header_meta_box',
			'title'      => esc_html__('Header', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name' => esc_html__('Header On/Off?', 'g5plus-arvo'),
					'id'   => $prefix . 'header_show_hide',
					'type' => 'checkbox',
					'desc' => esc_html__('Turn ON/Off Header', 'g5plus-arvo'),
					'std'  => '1',
				),
				array(
					'name'           => esc_html__('Desktop Settings', 'g5plus-arvo'),
					'id'             => $prefix . 'page_header_customize_enable',
					'type'           => 'section',
					'std'            => '',
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'name'           => esc_html__('Header Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'header_layout',
					'type'           => 'image_set',
					'std'            => isset($options['header_layout']) ? $options['header_layout'] : 'header-1',
					'options'        => array(
						'header-1'  => GF_PLUGIN_URL . '/assets/images/theme-options/header-1.png',
						'header-2'  => GF_PLUGIN_URL . '/assets/images/theme-options/header-2.png',
						'header-3'  => GF_PLUGIN_URL . '/assets/images/theme-options/header-3.png',
						'header-4'  => GF_PLUGIN_URL . '/assets/images/theme-options/header-4.png',
						'header-5'  => GF_PLUGIN_URL . '/assets/images/theme-options/header-5.png',
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'id'             => $prefix . 'header_container_layout',
					'name'           => esc_html__('Container Layout', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['header_container_layout']) ? $options['header_container_layout'] : 'container',
					'options'        => array(
						'container'      => esc_html__('Container', 'g5plus-arvo'),
						'container-fluid' => esc_html__('Container Fluid', 'g5plus-arvo'),
					),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '=', array('header-1','header-2','header-5')),
					),
				),
				array(
					'id'             => $prefix . 'header_float',
					'name'           => esc_html__('Header Float', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['header_float']) ? $options['header_float'] : '0',
					'options'        => gf_get_toggle(),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '<>', 'header-4'),
					),
				),
				array(
					'id'             => $prefix . 'header_sticky',
					'name'           => esc_html__('Header Sticky', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['header_sticky']) ? $options['header_sticky'] : '0',
					'options'        => array(
						'1'  => esc_html__('On', 'g5plus-arvo'),
						'0'  => esc_html__('Off', 'g5plus-arvo')
					),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '<>', 'header-4'),
					),
				),
				array(
					'id'             => $prefix . 'header_sticky_change_style',
					'name'           => esc_html__('Header Sticky change style', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['header_sticky_change_style']) ? $options['header_sticky_change_style'] : '1',
					'options'        => gf_get_toggle(),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_float', '=', '1'),
						array($prefix . 'header_sticky', '=', '1'),
						array($prefix . 'header_layout', '<>', 'header-4'),
					),
				),
				array(
					'id'             => $prefix . 'header_border_bottom',
					'name'           => esc_html__('Header border bottom', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['header_border_bottom']) ? $options['header_border_bottom'] : 'none',
					'options'        => array(
						'none'              => esc_html__('None','g5plus-arvo'),
						'full-border'       => esc_html__('Full Border','g5plus-arvo'),
						'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
					),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '=', array('header-1','header-2','header-5')),
					),
				),
				array(
					'id'             => $prefix . 'header_above_border_bottom',
					'name'           => esc_html__('Header above border bottom', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['header_above_border_bottom']) ? $options['header_above_border_bottom'] : 'none',
					'options'        => array(
						'none'              => esc_html__('None','g5plus-arvo'),
						'full-border'       => esc_html__('Full Border','g5plus-arvo'),
						'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
					),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '=', array('header-3')),
					),
				),
				array(
					'id'             => $prefix . 'header_padding',
					'name'           => esc_html__('Header padding', 'g5plus-arvo'),
					'desc'           => esc_html__('Header padding top. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'           => 'padding',
					'allow'          => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['header_padding']) && isset($options['header_padding']['padding-top']) ? str_replace('px','',$options['header_padding']['padding-top']) : '',
						'bottom' => isset($options['header_padding']) && isset($options['header_padding']['padding-bottom']) ? str_replace('px','',$options['header_padding']['padding-bottom']) : '',
					),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '<>', 'header-4'),
					),
				),

				array(
					'id'             => $prefix . 'navigation_float',
					'name'           => esc_html__('Navigation Float', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['navigation_float']) ? $options['navigation_float'] : '1',
					'options'        => gf_get_toggle(),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '=', 'header-3'),
					),
				),

				array(
					'id'             => $prefix . 'navigation_height',
					'name'           => esc_html__('Navigation height', 'g5plus-arvo'),
					'type'           => 'text',
					'desc'           => esc_html__('Set header navigation height (px). Do not include unit.', 'g5plus-arvo'),
					'std'            => isset($options['navigation_height']) && isset($options['navigation_height']['height']) ? str_replace('px','',$options['navigation_height']['height']) : '',
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
					),
				),

				array(
					'id'             => $prefix . 'navigation_spacing',
					'name'           => esc_html__('Navigation Spacing (px)', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['navigation_spacing']) ? str_replace('px','', $options['navigation_spacing']) : '30',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'required-field' => array(
						array($prefix . 'header_show_hide', '<>', '0'),
						array($prefix . 'header_layout', '<>', 'header-4'),
					),
				),


				//-----------------------------------------------------------------------
				array(
					'name'           => esc_html__('Page Header Mobile', 'g5plus-arvo'),
					'id'             => $prefix . 'page_header_section_2',
					'type'           => 'section',
					'std'            => '',
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'name'           => esc_html__('Header Mobile Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'mobile_header_layout',
					'type'           => 'image_set',
					'allowClear'     => true,
					'std'            => isset($options['mobile_header_layout']) ? $options['mobile_header_layout'] : 'header-mobile-1',
					'options'        => array(
						'header-mobile-1' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-1.png',
						'header-mobile-2' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-2.png',
						'header-mobile-3' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-3.png',
						'header-mobile-4' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-4.png',
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'id'             => $prefix . 'mobile_header_menu_drop',
					'name'           => esc_html__('Menu Drop Type', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['mobile_header_menu_drop']) ? $options['mobile_header_menu_drop'] : 'menu-drop-fly',
					'options'        => array(
						'menu-drop-fly'      => esc_html__('Fly Menu', 'g5plus-arvo'),
						'menu-drop-dropdown' => esc_html__('Dropdown Menu', 'g5plus-arvo'),
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'id'             => $prefix . 'mobile_header_stick',
					'name'           => esc_html__('Header mobile sticky', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['mobile_header_stick']) ? $options['mobile_header_stick'] : '0',
					'options'        => array(
						'1'  => esc_html__('Enable', 'g5plus-arvo'),
						'0'  => esc_html__('Disable', 'g5plus-arvo'),
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'name'           => esc_html__('Mobile Header Search Box', 'g5plus-arvo'),
					'id'             => $prefix . 'mobile_header_search_box',
					'type'           => 'button_set',
					'std'            => isset($options['mobile_header_search_box']) ? $options['mobile_header_search_box'] : '1',
					'options'        => array(
						'1'  => esc_html__('Show', 'g5plus-arvo'),
						'0'  => esc_html__('Hide', 'g5plus-arvo')
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),

				array(
					'name'           => esc_html__('Mobile Header Shopping Cart', 'g5plus-arvo'),
					'id'             => $prefix . 'mobile_header_shopping_cart',
					'type'           => 'button_set',
					'std'            => isset($options['mobile_header_shopping_cart']) ? $options['mobile_header_shopping_cart'] : '1',
					'options'        => array(
						'1'  => esc_html__('Show', 'g5plus-arvo'),
						'0'  => esc_html__('Hide', 'g5plus-arvo')
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
				array(
					'id'             => $prefix . 'mobile_header_border_bottom',
					'name'           => esc_html__('Mobile header border bottom', 'g5plus-arvo'),
					'type'           => 'button_set',
					'std'            => isset($options['mobile_header_border_bottom']) ? $options['mobile_header_border_bottom'] : 'none',
					'options'        => array(
						'none'     => esc_html__('None', 'g5plus-arvo'),
						'bordered' => esc_html__('Bordered', 'g5plus-arvo'),
					),
					'required-field' => array($prefix . 'header_show_hide', '<>', '0'),
				),
			)
		);

		/**
		 * Header Customize
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'header_customize_meta_box',
			'title'      => esc_html__('Header Customize', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				//------------------------------------------------------------------
				array(
					'name'   => esc_html__('Header Customize Navigation', 'g5plus-arvo'),
					'id'     => $prefix . 'enable_header_customize_nav',
					'type'   => 'section',

				),
				array(
					'name'           => esc_html__('Header Customize Navigation', 'g5plus-arvo'),
					'id'             => $prefix . 'header_customize_nav',
					'type'           => 'sorter',
					'std'            => gf_get_sorter_option_value('header_customize_nav'),
					'desc'           => esc_html__('Select element for header customize navigation. Drag to change element order', 'g5plus-arvo'),
					'options'        => array(
						'shopping-cart' => esc_html__('Shopping Cart', 'g5plus-arvo'),
						'search'        => esc_html__('Search Button', 'g5plus-arvo'),
						'canvas-menu' => esc_html__('Canvas Menu','g5plus-arvo'),
						'custom-text'   => esc_html__('Custom Text', 'g5plus-arvo'),
					),
				),
				array(
					'name'           => esc_html__('Search Type', 'g5plus-arvo'),
					'id'             => $prefix . 'header_customize_nav_search',
					'type'           => 'button_set',
					'std'            => isset($options['header_customize_nav_search']) ? $options['header_customize_nav_search'] : 'button',
					'options'        => gf_get_search_type(),
				),
				array(
					'name'           => esc_html__('Custom text content', 'g5plus-arvo'),
					'id'             => $prefix . 'header_customize_nav_text',
					'type'           => 'textarea',
					'std'            => isset($options['header_customize_nav_text']) ? $options['header_customize_nav_text'] : '',
				),
				array(
					'id'             => $prefix . 'header_customize_nav_spacing',
					'name'           => esc_html__('Navigation Item Spacing (px)', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['header_customize_nav_spacing']) ? str_replace('px','', $options['header_customize_nav_spacing']) : '20',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
				),
				array(
					'name'           => esc_html__('Custom CSS class', 'g5plus-arvo'),
					'id'             => $prefix . 'header_customize_nav_css_class',
					'type'           => 'text',
					'std'            => isset($options['header_customize_nav_css_class']) ? $options['header_customize_nav_css_class'] : '',
				),
			)
		);

		/**
		 * Menu
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'menu_meta_box',
			'title'      => esc_html__('Menu', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name'        => esc_html__('Page menu', 'g5plus-arvo'),
					'id'          => $prefix . 'page_menu',
					'type'        => 'select_advanced',
					'options'     => $menu_list,
					'placeholder' => esc_html__('Select Menu', 'g5plus-arvo'),
					'std'         => '',
					'multiple'    => false,
					'desc'        => esc_html__('Optionally you can choose to override the menu that is used on the page', 'g5plus-arvo'),
				),

				array(
					'name'        => esc_html__('Page menu mobile', 'g5plus-arvo'),
					'id'          => $prefix . 'page_menu_mobile',
					'type'        => 'select_advanced',
					'options'     => $menu_list,
					'placeholder' => esc_html__('Select Menu', 'g5plus-arvo'),
					'std'         => '',
					'multiple'    => false,
					'desc'        => esc_html__('Optionally you can choose to override the menu mobile that is used on the page', 'g5plus-arvo'),
				),

				array(
					'name' => esc_html__('Is One Page', 'g5plus-arvo'),
					'id'   => $prefix . 'is_one_page',
					'type' => 'checkbox',
					'std'  => '0',
					'desc' => esc_html__('Set page style is One Page', 'g5plus-arvo'),
				),
			)
		);

		/**
		 * Footer
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'footer_meta_box',
			'title'      => esc_html__('Footer', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				array(
					'name'           => esc_html__('Footer Parallax', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_parallax',
					'type'           => 'button_set',
					'std'            => isset($options['footer_parallax']) ? $options['footer_parallax'] : '0',
					'options'        => gf_get_toggle(),
					'desc'           => esc_html__('Enable Footer Parallax', 'g5plus-arvo'),
				),
				array(
					'id' => $prefix . 'custom_footer_bg_image_enable',
					'name' => esc_html__('Custom Background Image','g5plus-arvo'),
					'desc' => esc_html__('Turn on this option if you want to enable custom background image of footer','g5plus-arvo'),
					'type' => 'button_set',
					'options' => gf_get_toggle(),
					'std' => 0
				),
				array(
					'id'               => $prefix . 'footer_bg_image',
					'name'             => esc_html__('Background Image', 'g5plus-arvo'),
					'desc'             => esc_html__('Set footer background image', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'std'              => '',
					'required-field' => array($prefix. 'custom_footer_bg_image_enable','=','1')
				),
				array(
					'name'           => esc_html__('Footer Image apply for', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_bg_image_apply_for',
					'type'           => 'button_set',
					'std'            => isset($options['footer_bg_image_apply_for']) ? $options['footer_bg_image_apply_for'] : 'footer.main-footer-wrapper',
					'options'  => array(
						'footer.main-footer-wrapper'    => esc_html__('Footer Wrapper', 'g5plus-arvo'),
						'footer .main-footer'           => esc_html__('Main Footer', 'g5plus-arvo'),
					),
					'desc'           => esc_html__('Select region apply for footer image', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'footer_bg_image', '<>', ''),
					),
				),
				array(
					'name' => esc_html__('Footer Custom', 'g5plus-arvo'),
					'id'   => $prefix . 'set_footer_custom',
					'type' => 'post',
					'post_type' => 'gf_footer',
					'placeholder' => esc_html__('Set Custom Footer', 'g5plus-arvo'),
					'std'   => '',
					'desc'  => esc_html__('Select one to apply to the page footer', 'g5plus-arvo'),
				),


				//--------------------------------------------------------------------
				array(
					'name' => esc_html__('Main Footer Settings', 'g5plus-arvo'),
					'id'   => $prefix . 'page_footer_section_2',
					'type' => 'section',
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
					),
				),
				array(
					'name' => esc_html__('Show/Hide Footer', 'g5plus-arvo'),
					'id'   => $prefix . 'footer_show_hide',
					'type' => 'checkbox',
					'std'  => '1',
					'desc' => esc_html__('Show/hide footer', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
					),
				),
				array(
					'name'           => esc_html__('Footer Container Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_container_layout',
					'type'           => 'button_set',
					'std'            => isset($options['footer_container_layout']) ? $options['footer_container_layout'] : 'Container',
					'options'        => gf_get_page_layout(),
					'desc'           => esc_html__('Select Footer Container Layout', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
					),
				),
				array(
					'name'           => esc_html__('Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_layout',
					'type'           => 'image_set',
					'width'          => '80px',
					'std'            => isset($options['footer_layout']) ? $options['footer_layout'] : 'footer-1',
					'options'        => array(
						'footer-1' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-1.jpg',
						'footer-2' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-2.jpg',
						'footer-3' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-3.jpg',
						'footer-4' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-4.jpg',
						'footer-5' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-5.jpg',
						'footer-6' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-6.jpg',
						'footer-7' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-7.jpg',
						'footer-8' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-8.jpg',
						'footer-9' => GF_PLUGIN_URL . '/assets/images/theme-options/footer-layout-9.jpg',
					),
					'desc'           => esc_html__('Select Footer Layout (Not set to default).', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
					),
				),
				array(
					'name'           => esc_html__('Sidebar 1', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_sidebar_1',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['footer_sidebar_1']) ? $options['footer_sidebar_1'] : 'footer-1',
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
						array($prefix . 'footer_layout', '=', array('footer-1', 'footer-2', 'footer-3', 'footer-4', 'footer-5', 'footer-6', 'footer-7', 'footer-8', 'footer-9'))
					),
				),

				array(
					'name'           => esc_html__('Sidebar 2', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_sidebar_2',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['footer_sidebar_2']) ? $options['footer_sidebar_2'] : 'footer-2',
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
						array($prefix . 'footer_layout', '=', array('footer-1', 'footer-2', 'footer-3', 'footer-4', 'footer-5', 'footer-6', 'footer-7', 'footer-8'))
					),
				),

				array(
					'name'           => esc_html__('Sidebar 3', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_sidebar_3',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['footer_sidebar_3']) ? $options['footer_sidebar_3'] : 'footer-3',
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
						array($prefix . 'footer_layout', '=', array('footer-1', 'footer-2', 'footer-3', 'footer-5', 'footer-8'))
					),
				),

				array(
					'name'           => esc_html__('Sidebar 4', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_sidebar_4',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['footer_sidebar_4']) ? $options['footer_sidebar_4'] : 'footer-4',
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
						array($prefix . 'footer_layout', '=', array('footer-1'))
					),
				),

				array(
					'name'           => esc_html__('Collapse footer on mobile device', 'g5plus-arvo'),
					'id'             => $prefix . 'collapse_footer',
					'type'           => 'button_set',
					'std'            => isset($options['collapse_footer']) ? $options['collapse_footer'] : '0',
					'options'        => array(
						'1'  => 'On',
						'0'  => 'Off'
					),
					'desc'           => esc_html__('Enable collapse footer', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
					),
				),
				array(
					'id'             => $prefix . 'footer_padding',
					'name'           => esc_html__('Footer padding', 'g5plus-arvo'),
					'desc'           => esc_html__('Footer padding top. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'           => 'padding',
					'allow'          => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['footer_padding']) && isset($options['footer_padding']['padding-top']) ? str_replace('px','',$options['footer_padding']['padding-top']) : '',
						'bottom' => isset($options['footer_padding']) && isset($options['footer_padding']['padding-bottom']) ? str_replace('px','',$options['footer_padding']['padding-bottom']) : '',
					),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'footer_show_hide', '=', '1'),
					),
				),

				//--------------------------------------------------------------------
				array(
					'name' => esc_html__('Bottom Bar Settings', 'g5plus-arvo'),
					'id'   => $prefix . 'page_footer_section_3',
					'type' => 'section',
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
					),
				),
				array(
					'name' => esc_html__('Show/Hide Bottom Bar', 'g5plus-arvo'),
					'id'   => $prefix . 'bottom_bar_visible',
					'type' => 'checkbox',
					'std'  => '1',
					'desc' => esc_html__('Turn ON/OFF Bottom Bar.', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
					),
				),

				array(
					'name'           => esc_html__('Bottom Bar Container Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_container_layout',
					'type'           => 'button_set',
					'std'            => isset($options['bottom_bar_container_layout']) ? $options['bottom_bar_container_layout'] : 'container',
					'options'        => gf_get_page_layout(),
					'desc'           => esc_html__('Select Bottom Bar Container Layout', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'bottom_bar_visible', '<>', '0')
					),
				),
				array(
					'name'           => esc_html__('Bottom Bar Layout', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_layout',
					'type'           => 'image_set',
					'width'          => '80px',
					'std'            => isset($options['bottom_bar_layout']) ? $options['bottom_bar_layout'] : 'bottom-bar-1',
					'options'        => array(
						'bottom-bar-1' => GF_PLUGIN_URL . '/assets/images/theme-options/bottom-bar-layout-1.jpg',
						'bottom-bar-2' => GF_PLUGIN_URL . '/assets/images/theme-options/bottom-bar-layout-2.jpg',
						'bottom-bar-3' => GF_PLUGIN_URL . '/assets/images/theme-options/bottom-bar-layout-3.jpg',
						'bottom-bar-4' => GF_PLUGIN_URL . '/assets/images/theme-options/bottom-bar-layout-4.jpg',
					),
					'desc'           => esc_html__('Bottom bar layout.', 'g5plus-arvo'),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'bottom_bar_visible', '<>', '0')
					),
				),

				array(
					'name'           => esc_html__('Bottom Bar Left Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_left_sidebar',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['bottom_bar_left_sidebar']) ? $options['bottom_bar_left_sidebar'] : 'bottom_bar_left',
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'bottom_bar_visible', '<>', '0')
					),
				),

				array(
					'name'           => esc_html__('Bottom Bar Right Sidebar', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_right_sidebar',
					'type'           => 'sidebars',
					'placeholder'    => esc_html__('Select Sidebar', 'g5plus-arvo'),
					'std'            => isset($options['bottom_bar_right_sidebar']) ? $options['bottom_bar_right_sidebar'] : 'bottom_bar_right',
					'js_options' => array('allowClear'  => false),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'bottom_bar_visible', '<>', '0'),
						array($prefix . 'bottom_bar_layout', '<>', 'bottom-bar-4')
					),
				),
				array(
					'name'           => esc_html__('Bottom Bar Border Top', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_border_top',
					'type'           => 'button_set',
					'std'            => isset($options['bottom_bar_border_top']) ? $options['bottom_bar_border_top'] : 'none',
					'options'        => array(
						'none'             => esc_html__('None', 'g5plus-arvo'),
						'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
						'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
					),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'bottom_bar_visible', '<>', '0')
					),
				),
				array(
					'id'             => $prefix . 'bottom_bar_padding',
					'name'           => esc_html__('Bottom bar padding', 'g5plus-arvo'),
					'desc'           => esc_html__('Set bottom bar padding. Do not include units (empty to set default)', 'g5plus-arvo'),
					'type'           => 'padding',
					'allow'          => array(
						'left'  => false,
						'right' => false
					),
					'std' => array(
						'top' => isset($options['bottom_bar_padding']) && isset($options['bottom_bar_padding']['padding-top']) ? str_replace('px','',$options['bottom_bar_padding']['padding-top']) : '',
						'bottom' => isset($options['bottom_bar_padding']) && isset($options['bottom_bar_padding']['padding-bottom']) ? str_replace('px','',$options['bottom_bar_padding']['padding-bottom']) : '',
					),
					'required-field' => array(
						array($prefix . 'set_footer_custom', '=', ''),
						array($prefix . 'bottom_bar_visible', '<>', '0')
					),
				),
			)
		);

		/**
		 * Color Setting
		 * *******************************************************
		 */
		$meta_boxes[] = array(
			'id'         => $prefix . 'color_meta_box',
			'title'      => esc_html__('Colors Setting', 'g5plus-arvo'),
			'post_types' => array('gf_preset'),
			'fields'     => array(
				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_general',
					'name'   => esc_html__('General', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Accent color', 'g5plus-arvo'),
					'id'             => $prefix . 'accent_color',
					'type'           => 'color',
					'std'            => isset($options['accent_color']) ? $options['accent_color'] : '#1086df',
					'required-field' => array($prefix .  'custom_color_general', '=', '1'),
				),
				array(
					'name'           => esc_html__('Foreground Accent color', 'g5plus-arvo'),
					'id'             => $prefix . 'foreground_accent_color',
					'type'           => 'color',
					'std'            => isset($options['foreground_accent_color']) ? $options['foreground_accent_color'] : '#fff',
					'required-field' => array($prefix .  'custom_color_general', '=', '1'),
				),
				array(
					'name'           => esc_html__('Text color', 'g5plus-arvo'),
					'id'             => $prefix . 'text_color',
					'type'           => 'color',
					'std'            => isset($options['text_color']) ? $options['text_color'] : '#727272',
					'required-field' => array($prefix .  'custom_color_general', '=', '1'),
				),
				array(
					'name'           => esc_html__('Text Color Bolder', 'g5plus-arvo'),
					'id'             => $prefix . 'text_color_bold',
					'type'           => 'color',
					'std'            => isset($options['text_color_bold']) ? $options['text_color_bold'] : '#222',
					'required-field' => array($prefix .  'custom_color_general', '=', '1'),
				),
				array(
					'name'           => esc_html__('Border color', 'g5plus-arvo'),
					'id'             => $prefix . 'border_color',
					'type'           => 'color',
					'std'            => isset($options['border_color']) ? $options['border_color'] : '#eee',
					'required-field' => array($prefix .  'custom_color_general', '=', '1'),
				),


				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_top_drawer',
					'name'   => esc_html__('Top Drawer', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Top drawer background color', 'g5plus-arvo'),
					'id'             => $prefix . 'top_drawer_bg_color',
					'type'           => 'color',
					'std'            => isset($options['top_drawer_bg_color']) ? $options['top_drawer_bg_color'] : '#2f2f2f',
					'required-field' => array($prefix . 'custom_color_top_drawer', '=', '1'),
				),
				array(
					'name'           => esc_html__('Top drawer text color', 'g5plus-arvo'),
					'id'             => $prefix . 'top_drawer_text_color',
					'type'           => 'color',
					'std'            => isset($options['top_drawer_text_color']) ? $options['top_drawer_text_color'] : '#c5c5c5',
					'required-field' => array($prefix . 'custom_color_top_drawer', '=', '1'),
				),

				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_top_bar',
					'name'   => esc_html__('Top Bar', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Top bar background color', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_bg_color',
					'type'           => 'color',
					'std'            => isset($options['top_bar_bg_color']) ? $options['top_bar_bg_color'] : '#eee',
					'required-field' => array($prefix . 'custom_color_top_bar', '=', '1'),
				),
				array(
					'name'           => esc_html__('Top bar overlay', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_overlay',
					'desc'           => esc_html__('Set the opacity level of the top bar background color (apply for header float)', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['top_bar_overlay']) ? $options['top_bar_overlay'] : '0',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'required-field' => array(
						array($prefix . 'header_float', '=', '1'),
						array($prefix . 'custom_color_top_bar', '=', '1'),
					),
				),
				array(
					'name'           => esc_html__('Top bar text color', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_text_color',
					'type'           => 'color',
					'std'            => isset($options['top_bar_text_color']) ? $options['top_bar_text_color'] : '#777',
					'required-field' => array($prefix . 'custom_color_top_bar', '=', '1'),
				),
				array(
					'name'           => esc_html__('Top bar border color', 'g5plus-arvo'),
					'id'             => $prefix . 'top_bar_border_color',
					'type'           => 'color',
					'std'            => isset($options['top_bar_border_color']) ? $options['top_bar_border_color'] : '#eee',
					'required-field' => array($prefix . 'custom_color_top_bar', '=', '1'),
				),

				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_header',
					'name'   => esc_html__('Header', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Header background color', 'g5plus-arvo'),
					'id'             => $prefix . 'header_bg_color',
					'type'           => 'color',
					'std'            => isset($options['header_bg_color']) ? $options['header_bg_color'] : '#fff',
					'required-field' => array($prefix . 'custom_color_header', '=', '1'),
				),
				array(
					'name'           => esc_html__('Header overlay', 'g5plus-arvo'),
					'id'             => $prefix . 'header_overlay',
					'desc'           => esc_html__('Set the opacity level of the header background color (apply for header float)', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['header_overlay']) ? $options['header_overlay'] : '0',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'required-field' => array(
						array($prefix . 'header_float', '=', '1'),
						array($prefix . 'custom_color_header', '=', '1'),
					),
				),
				array(
					'name'           => esc_html__('Header text color', 'g5plus-arvo'),
					'id'             => $prefix . 'header_text_color',
					'type'           => 'color',
					'std'            => isset($options['header_text_color']) ? $options['header_text_color'] : '#212121',
					'required-field' => array($prefix . 'custom_color_header', '=', '1'),
				),
				array(
					'name'           => esc_html__('Header border color', 'g5plus-arvo'),
					'id'             => $prefix . 'header_border_color',
					'type'           => 'color',
					'std'            => isset($options['header_border_color']) ? $options['header_border_color'] : '#eee',
					'required-field' => array($prefix . 'custom_color_header', '=', '1'),
				),
				array(
					'name'           => esc_html__('Header above border color', 'g5plus-arvo'),
					'id'             => $prefix . 'header_above_border_color',
					'type'           => 'color',
					'std'            => isset($options['header_above_border_color']) ? $options['header_above_border_color'] : '#eee',
					'required-field' => array(
						array($prefix . 'custom_color_header', '=', '1'),
						array($prefix . 'header_layout', '=', 'header-2')
					) ,
				),



				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_navigation',
					'name'   => esc_html__('Navigation', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Navigation background color', 'g5plus-arvo'),
					'id'             => $prefix . 'navigation_bg_color',
					'type'           => 'color',
					'std'            => isset($options['navigation_bg_color']) ? $options['navigation_bg_color'] : '#eee',
					'required-field' => array($prefix . 'custom_color_navigation', '=', '1'),
				),
				array(
					'name'           => esc_html__('Navigation overlay', 'g5plus-arvo'),
					'id'             => $prefix . 'navigation_overlay',
					'desc'           => esc_html__('Set the opacity level of the navigation background color (apply for header float)', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['navigation_overlay']) ? $options['navigation_overlay'] : '0',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'required-field' => array(
						array($prefix . 'header_float', '=', '1'),
						array($prefix . 'custom_color_navigation', '=', '1'),
					),
				),
				array(
					'name'           => esc_html__('Navigation text color', 'g5plus-arvo'),
					'id'             => $prefix . 'navigation_text_color',
					'type'           => 'color',
					'std'            => isset($options['navigation_text_color']) ? $options['navigation_text_color'] : '#858585',
					'required-field' => array($prefix . 'custom_color_navigation', '=', '1'),
				),
				array(
					'name'           => esc_html__('Navigation text hover color', 'g5plus-arvo'),
					'id'             => $prefix . 'navigation_text_color_hover',
					'type'           => 'color',
					'std'            => isset($options['navigation_text_color_hover']) ? $options['navigation_text_color_hover'] : '#3AB8BD',
					'required-field' => array($prefix . 'custom_color_navigation', '=', '1'),
				),
				array(
					'name'           => esc_html__('Navigation Customize text color', 'g5plus-arvo'),
					'id'             => $prefix . 'navigation_customize_text_color',
					'type'           => 'color',
					'std'            => isset($options['navigation_customize_text_color']) ? $options['navigation_customize_text_color'] : '#BBB',
					'required-field' => array($prefix . 'custom_color_navigation', '=', '1'),
				),

				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_footer',
					'name'   => esc_html__('Footer', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Footer background color', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_bg_color',
					'type'           => 'color',
					'std'            => isset($options['footer_bg_color']) ? $options['footer_bg_color'] : '#222222',
					'required-field' => array($prefix . 'custom_color_footer', '=', '1'),
				),

				array(
					'name'           => esc_html__('Footer background overlay', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_bg_overlay',
					'desc'           => esc_html__('Set the opacity level of the footer background color', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['footer_bg_overlay']) ? $options['footer_bg_overlay'] : '100',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'required-field' => array($prefix . 'custom_color_footer', '=', '1'),
				),


				array(
					'name'           => esc_html__('Footer text color', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_text_color',
					'type'           => 'color',
					'std'            => isset($options['footer_text_color']) ? $options['footer_text_color'] : '#b2b2b2',
					'required-field' => array($prefix . 'custom_color_footer', '=', '1'),
				),
				array(
					'name'           => esc_html__('Footer widget title color', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_widget_title_color',
					'type'           => 'color',
					'std'            => isset($options['footer_widget_title_color']) ? $options['footer_widget_title_color'] : '#fff',
					'required-field' => array($prefix . 'custom_color_footer', '=', '1'),
				),
				array(
					'name'           => esc_html__('Footer border color', 'g5plus-arvo'),
					'id'             => $prefix . 'footer_border_color',
					'type'           => 'color',
					'std'            => isset($options['footer_border_color']) ? $options['footer_border_color'] : '#eee',
					'required-field' => array($prefix . 'custom_color_footer', '=', '1'),
				),

				//------------------------------------------------------------------------
				array(
					'id'     => $prefix . 'custom_color_bottom_bar',
					'name'   => esc_html__('Bottom Bar', 'g5plus-arvo'),
					'type'   => 'section',
					'std'         => '0',
					'switch'      => true,
				),
				array(
					'name'           => esc_html__('Bottom bar background color', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_bg_color',
					'type'           => 'color',
					'std'            => isset($options['bottom_bar_bg_color']) ? $options['bottom_bar_bg_color'] : '#333',
					'required-field' => array($prefix . 'custom_color_bottom_bar', '=', '1'),
				),
				array(
					'name'           => esc_html__('Bottom bar overlay', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_overlay',
					'desc'           => esc_html__('Set the opacity level of the bottom bar background color', 'g5plus-arvo'),
					'clone'          => false,
					'type'           => 'slider',
					'prefix'         => '',
					'std'            => isset($options['bottom_bar_overlay']) ? $options['bottom_bar_overlay'] : '100',
					'js_options'     => array(
						'min'  => 0,
						'max'  => 100,
						'step' => 1,
					),
					'required-field' => array($prefix . 'custom_color_bottom_bar', '=', '1'),
				),
				array(
					'name'           => esc_html__('Bottom bar text color', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_text_color',
					'type'           => 'color',
					'std'            => isset($options['bottom_bar_text_color']) ? $options['bottom_bar_text_color'] : '#777',
					'required-field' => array($prefix . 'custom_color_bottom_bar', '=', '1'),
				),
				array(
					'name'           => esc_html__('Bottom bar border color', 'g5plus-arvo'),
					'id'             => $prefix . 'bottom_bar_border_color',
					'type'           => 'color',
					'std'            => isset($options['bottom_bar_border_color']) ? $options['bottom_bar_border_color'] : '#454545',
					'required-field' => array($prefix . 'custom_color_bottom_bar', '=', '1'),
				),
			)
		);

		//==============================================================================
		// META BOX FOR ALL POST TYPE
		//==============================================================================
		$meta_boxes[] = array(
			'id'         => $prefix . 'page_custom_meta_box_general',
			'title'      => esc_html__('General', 'g5plus-arvo'),
			'post_types' => $post_type_meta_box,
			'fields'     => array(
				array(
					'name'        => esc_html__('Preset', 'g5plus-arvo'),
					'id'          => $prefix . 'page_preset',
					'type'        => 'select_advanced',
					'options'     => $preset_list,
					'placeholder' => esc_html__('Select Preset', 'g5plus-arvo'),
					'std'         => '',
					'multiple'    => false,
					'desc'        => esc_html__('Optionally you can choose to override the setting that is used on the page', 'g5plus-arvo'),
				),
				array(
					'name'        => esc_html__('Custom Css Class', 'g5plus-arvo'),
					'id'          => $prefix . 'custom_page_css_class',
					'type'        => 'text',
					'std'         => '',
					'desc'        => esc_html__('Enter custom class for this page', 'g5plus-arvo'),
				),
			)
		);

		$meta_boxes[] = array(
			'id'         => $prefix . 'page_custom_meta_box_layout',
			'title'      => esc_html__('Page Layout', 'g5plus-arvo'),
			'post_types' => $post_type_meta_box,
			'fields'     => array(
				array(
					'name'       => esc_html__('Sidebar Layout', 'g5plus-arvo'),
					'id'         => $prefix . 'custom_page_sidebar_layout',
					'type'       => 'image_set',
					'options'    => array(
						'none'  => GF_PLUGIN_URL . '/assets/images/theme-options/sidebar-none.png',
						'left'  => GF_PLUGIN_URL . '/assets/images/theme-options/sidebar-left.png',
						'right' => GF_PLUGIN_URL . '/assets/images/theme-options/sidebar-right.png'
					),
					'std'        => '',
					'multiple'   => false,
					'allowClear'     => true,
				),
				array(
					'name' => esc_html__('Remove Content Padding', 'g5plus-arvo'),
					'id'   => $prefix . 'remove_content_padding',
					'type' => 'checkbox',
					'std'  => '0',
				),
			)
		);

		$meta_boxes[] = array(
			'id'         => $prefix . 'page_custom_meta_box_title',
			'title'      => esc_html__('Page Title', 'g5plus-arvo'),
			'post_types' => $post_type_meta_box,
			'fields'     => array(
				array(
					'name'        => esc_html__('Show/Hide Page Title', 'g5plus-arvo'),
					'id'          => $prefix . 'custom_page_title_visible',
					'type'        => 'button_set',
					'std'         => '-1',
					'options'      => array(
						'-1'     => esc_html__('Default', 'g5plus-arvo'),
						'1'     => esc_html__('Show', 'g5plus-arvo'),
						'0'     => esc_html__('Hide', 'g5plus-arvo'),
					),
				),
				array(
					'name'        => esc_html__('Show/Hide Breadcrumbs', 'g5plus-arvo'),
					'id'          => $prefix . 'custom_breadcrumbs_visible',
					'type'        => 'button_set',
					'std'         => '-1',
					'options'      => array(
						'-1'     => esc_html__('Default', 'g5plus-arvo'),
						'1'     => esc_html__('Show', 'g5plus-arvo'),
						'0'     => esc_html__('Hide', 'g5plus-arvo'),
					),
					'required-field'    => array(
						array($prefix . 'custom_page_title_visible', '<>', '0'),
					),
				),
				array(
					'name'        => esc_html__('Custom Page Title?', 'g5plus-arvo'),
					'id'          => $prefix . 'is_custom_page_title',
					'type'        => 'section',
					'std'         => '0',
					'switch'      => true,
					'required-field'    => array(
						array($prefix . 'custom_page_title_visible', '<>', '0'),
					),
				),

				array(
					'name'        => esc_html__('Page Title', 'g5plus-arvo'),
					'id'          => $prefix . 'custom_page_title',
					'type'        => 'text',
					'std'         => '',
					'required-field'    => array(
						array($prefix . 'is_custom_page_title', '=', '1'),
						array($prefix . 'custom_page_title_visible', '<>', '0'),
					),
				),
				array(
					'name'        => esc_html__('Page Sub Title', 'g5plus-arvo'),
					'id'          => $prefix . 'custom_page_sub_title',
					'type'        => 'text',
					'std'         => '',
					'required-field'    => array(
						array($prefix . 'is_custom_page_title', '=', '1'),
						array($prefix . 'custom_page_title_visible', '<>', '0'),
					),
				),
				array(
					'name'        => esc_html__('Custom Page Title Background?', 'g5plus-arvo'),
					'id'          => $prefix . 'is_custom_page_title_bg',
					'type'        => 'section',
					'std'         => '0',
					'switch'      => true,
					'required-field'    => array(
						array($prefix . 'custom_page_title_visible', '<>', '0'),
					),
				),
				array(
					'id'               => $prefix . 'custom_page_title_bg_image',
					'name'             => esc_html__('Page Title Background Image', 'g5plus-arvo'),
					'desc'             => esc_html__('Upload custom page title background image', 'g5plus-arvo'),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'required-field'    => array(
						array($prefix . 'custom_page_title_visible', '<>', '0'),
						array($prefix . 'is_custom_page_title_bg', '=', '1'),
					),
				),
			)
		);


		/**
		 * Add Meta Box
		 * *******************************************************
		 */
		if (class_exists('RW_Meta_Box')) {
			foreach ($meta_boxes as $meta_box) {
				new RW_Meta_Box($meta_box);
			}
		}
	}
	add_action('admin_init', 'gf_register_meta_boxes');
}

/**
 * HELPER
 * *******************************************************
 */
if (!function_exists('gf_get_sorter_option_value')) {
	function gf_get_sorter_option_value($key) {
		$options = array();
		if (isset($GLOBALS[GF_OPTIONS_NAME])) {
			$options = &$GLOBALS[GF_OPTIONS_NAME];
		}
		$arr = array();
		if (isset($options[$key]) && is_array($options[$key]['enabled'])) {
			foreach ($options[$key]['enabled'] as $key => $value) {
				$arr[] = $key;
			}
			return $arr;
		}
		return array();
	}
}