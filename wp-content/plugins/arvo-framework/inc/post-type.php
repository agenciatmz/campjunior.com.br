<?php
/**
 * Register gf_preset POST TYPE
 * *******************************************************
 */
if (!function_exists('gf_register_preset_post_type')) {
    function gf_register_preset_post_type()
    {

        /**
         * Preset
         */
        $name = esc_html__('Preset', 'g5plus-arvo');
        register_post_type('gf_preset',
            array(
                'label'             => esc_html__('Preset', 'g5plus-arvo'),
                'description'       => esc_html__('Preset', 'g5plus-arvo'),
                'labels'            => array(
                    'name'               => $name,
                    'singular_name'      => $name,
                    'menu_name'          => $name,
                    'parent_item_colon'  => esc_html__('Parent Item:', 'g5plus-arvo'),
                    'all_items'          => sprintf(esc_html__('All %s', 'g5plus-arvo'), $name),
                    'view_item'          => esc_html__('View Item', 'g5plus-arvo'),
                    'add_new_item'       => sprintf(esc_html__('Add New  %s', 'g5plus-arvo'), $name),
                    'add_new'            => esc_html__('Add New', 'g5plus-arvo'),
                    'edit_item'          => esc_html__('Edit Preset', 'g5plus-arvo'),
                    'update_item'        => esc_html__('Update Preset', 'g5plus-arvo'),
                    'search_items'       => esc_html__('Search Preset', 'g5plus-arvo'),
                    'not_found'          => esc_html__('Not found', 'g5plus-arvo'),
                    'not_found_in_trash' => esc_html__('Not found in Trash', 'g5plus-arvo'),
                ),
                'supports'          => array('title'),
                'public'            => true,
                'show_ui'           => true,
                '_builtin'          => false,
                'has_archive'       => false,
                'show_in_menu'      => false,
                'show_in_nav_menus' => false,
                'menu_icon'         => 'dashicons-screenoptions'
            )
        );

        /**
         * Custom Footer
         */
        $name = esc_html__('Custom Footer', 'g5plus-arvo');
        register_post_type('gf_footer',
            array(
                'label'             => $name,
                'description'       => $name,
                'labels'            => array(
                    'name'               => $name,
                    'singular_name'      => $name,
                    'menu_name'          => $name,
                    'parent_item_colon'  => esc_html__('Parent Item:', 'g5plus-arvo'),
                    'all_items'          => sprintf(esc_html__('All %s', 'g5plus-arvo'), $name),
                    'view_item'          => esc_html__('View Item', 'g5plus-arvo'),
                    'add_new_item'       => sprintf(esc_html__('Add New  %s', 'g5plus-arvo'), $name),
                    'add_new'            => esc_html__('Add New', 'g5plus-arvo'),
                    'edit_item'          => esc_html__('Edit Item', 'g5plus-arvo'),
                    'update_item'        => esc_html__('Update Item', 'g5plus-arvo'),
                    'search_items'       => esc_html__('Search Item', 'g5plus-arvo'),
                    'not_found'          => esc_html__('Not found', 'g5plus-arvo'),
                    'not_found_in_trash' => esc_html__('Not found in Trash', 'g5plus-arvo'),
                ),
                'supports'          => array('title', 'editor'),
                'public'            => true,
                'show_ui'           => true,
                '_builtin'          => false,
                'has_archive'       => false,
                'show_in_menu'      => false,
                'show_in_nav_menus' => false,
                'menu_icon'         => 'dashicons-screenoptions'
            )
        );
    }

    add_action('init', 'gf_register_preset_post_type');
}

/**
 * Preset Single
 * *******************************************************
 */
if (!function_exists('gf_preset_single_template')) {
	function gf_preset_single_template($single) {
		if (is_singular(array('gf_preset', 'gf_footer'))) {
			$single = GF_PLUGIN_DIR . '/inc/templates/single-preset.php';
		}
		return $single;
	}
	add_filter('single_template', 'gf_preset_single_template');
}