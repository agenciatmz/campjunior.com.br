<?php
//include the main class file
if (is_admin()){
    /*
     * prefix of meta keys, optional
     */
    $prefix = GF_METABOX_PREFIX;

    /*
     * configure your meta box
     */
    $config_page_title = array(
        'id' => 'page_title_meta_box',          // meta box id, unique per meta box
        'title' => esc_html__('Page Title','g5plus-arvo') ,          // meta box title
        'pages' => array('category','product_cat'),        // taxonomy name, accept categories, post_tag and custom taxonomies
        'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
        'fields' => array(),            // list of meta fields (can be added by field arrays)
        'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
        'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
    );

	/*
     * Initiate your meta box
     */
	$page_title_meta =  new Tax_Meta_Class($config_page_title);
	/*
	 * Add fields to your meta box
	 */
	$page_title_meta->addRadio(
		$prefix.'page_title_enable',
		gf_get_toggle(1),
		array(
			'name' => esc_html__('Show/Hide Page Title?','g5plus-arvo'),
			'std' => -1
		)
	);

	//Image field
	$page_title_meta->addImage(
		$prefix.'page_title_bg_image',
		array(
			'name'=> esc_html__('Page Title Background Image','g5plus-arvo')
		)
	);
	$page_title_meta->Finish();
}
