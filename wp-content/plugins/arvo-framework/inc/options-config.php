<?php
/**
 * THEME OPTIONS CONFIG FILE
 * *******************************************************
 */
if (!class_exists('Redux')) {
    return;
}

if (!class_exists('GF_Options_Config')) {
    class GF_Options_Config
    {
        public function __construct()
        {
            $this->initSettings();
        }

        public function initSettings()
        {
            // Set the default arguments
            $this->setArguments();
            // Create the sections and fields
            $this->setSections();
        }

        /**
         * HELPER
         * *******************************************************
         */
        private function get_section_start($id, $title)
        {
            return array(
                'id'     => $id . '_start',
                'type'   => 'section',
                'title'  => $title,
                'indent' => true
            );
        }

        private function get_section_end($id)
        {
            return array(
                'id'     => $id . '_end',
                'type'   => 'section',
                'indent' => false
            );
        }

        private function get_page_layout($id, $required = array(), $default = 'container')
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Layout', 'g5plus-arvo'),
                'subtitle' => esc_html__('Select Page Layout', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_page_layout(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_sidebar_layout($id, $required = array(), $default = 'right')
        {
            return array(
                'id'       => $id,
                'type'     => 'image_select',
                'title'    => esc_html__('Sidebar Layout', 'g5plus-arvo'),
                'subtitle' => esc_html__('Set Sidebar Layout', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_sidebar_layout(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_sidebar_width($id, $required = array(), $default = 'small')
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Sidebar Width', 'g5plus-arvo'),
                'subtitle' => esc_html__('Set Sidebar width', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_sidebar_width(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_sidebar_mobile_enable($id, $required = array(), $default = 1)
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Sidebar Mobile', 'g5plus-arvo'),
                'subtitle' => esc_html__('Turn Off this option if you want to disable sidebar on mobile', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_toggle(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_sidebar_mobile_canvas( $id, $required = array(), $default = 1 ) {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__( 'Canvas Sidebar Mobile', 'g5plus-arvo' ),
                'subtitle' => esc_html__( 'Turn Off this option if you want to disable canvas sidebar on mobile', 'g5plus-arvo' ),
                'desc'     => '',
                'options'  => gf_get_toggle(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_sidebar($id, $required = array(), $default = 'main-sidebar')
        {
            return array(
                'id'       => $id,
                'type'     => 'select',
                'title'    => esc_html__('Sidebar', 'g5plus-arvo'),
                'subtitle' => '',
                'data'     => 'sidebars',
                'desc'     => '',
                'default'  => $default,
	            'select2'  => array('allowClear' => false),
                'required' => $required,
            );
        }

        private function get_content_padding($id, $required = array(), $default = array('padding-top' => '100px', 'padding-bottom' => '100px', 'units' => 'px'))
        {
            return array(
                'id'             => $id,
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => 'px',
                'units_extended' => 'false',
                'title'          => esc_html__('Content Padding', 'g5plus-arvo'),
                'subtitle'       => esc_html__('Set content top/bottom padding.', 'g5plus-arvo'),
                'desc'           => esc_html__('Allow values (0,5,10,15....100)', 'g5plus-arvo'),
                'left'           => false,
                'right'          => false,
                'default'        => $default,
                'required'       => $required,
            );
        }

        private function get_content_padding_mobile($id, $required = array(), $default = array('padding-top' => '', 'padding-bottom' => '', 'units' => 'px'))
        {
            return array(
                'id'             => $id,
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => 'px',
                'units_extended' => 'false',
                'title'          => esc_html__('Content Padding Mobile', 'g5plus-arvo'),
                'subtitle'       => esc_html__('Set content top/bottom padding mobile.', 'g5plus-arvo'),
                'desc'           => esc_html__('Allow values (0,5,10,15....100)', 'g5plus-arvo'),
                'left'           => false,
                'right'          => false,
                'default'        => $default,
                'required'       => $required,
            );
        }

        private function get_page_title_enable($id, $required = array(), $default = 1)
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Page Title Enable', 'g5plus-arvo'),
                'subtitle' => esc_html__('Enable/Disable Page Title', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_toggle(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_page_title_layout($id, $required = array(), $default = 'centered')
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Page Title Layout', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_page_title_layout(),
                'default'  => $default,
                'required' => $required
            );
        }

        private function get_page_sub_title($id, $required = array())
        {
            return array(
                'id'       => $id,
                'type'     => 'text',
                'title'    => esc_html__('Page Sub Title', 'g5plus-arvo'),
                'subtitle' => '',
                'desc'     => '',
                'default'  => '',
                'required' => $required,
            );
        }

        private function get_page_title_padding($id, $required = array(), $default = array('padding-top' => '', 'padding-bottom' => '', 'units' => 'px'))
        {
            return array(
                'id'             => $id,
                'type'           => 'spacing',
                'mode'           => 'padding',
                'units'          => 'px',
                'units_extended' => 'false',
                'title'          => esc_html__('Padding', 'g5plus-arvo'),
                'subtitle'       => esc_html__('Set page title top/bottom padding.', 'g5plus-arvo'),
                'desc'           => '',
                'left'           => false,
                'right'          => false,
                'default'        => $default,
                'required'       => $required,
            );
        }


        private function get_page_title_background_image($id, $required = array(), $default = array())
        {
            return array(
                'id'       => $id,
                'type'     => 'media',
                'url'      => true,
                'title'    => esc_html__('Background Image', 'g5plus-arvo'),
                'subtitle' => esc_html__('Upload page title background.', 'g5plus-arvo'),
                'desc'     => '',
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_page_title_overlay_scheme($id, $required = array(), $default = 'page-title-overlay-dark')
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Page Title Overlay Scheme', 'g5plus-arvo'),
                'subtitle' => esc_html__('Enable Page Title Parallax', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => array(
                    'page-title-overlay-dark' => esc_html__( 'Dark', 'g5plus-arvo' ),
                    'page-title-overlay-light' => esc_html__( 'Light', 'g5plus-arvo' )
                ),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_page_title_parallax($id, $required = array(), $default = 0)
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Page Title Parallax', 'g5plus-arvo'),
                'subtitle' => esc_html__('Enable Page Title Parallax', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_toggle(),
                'default'  => $default,
                'required' => $required,
            );
        }

        private function get_breadcrumb_enable($id, $required = array(), $default = 1)
        {
            return array(
                'id'       => $id,
                'type'     => 'button_set',
                'title'    => esc_html__('Breadcrumbs Enable', 'g5plus-arvo'),
                'subtitle' => esc_html__('Enable/Disable Breadcrumbs In Pages Title', 'g5plus-arvo'),
                'desc'     => '',
                'options'  => gf_get_toggle(),
                'default'  => $default,
                'required' => $required
            );
        }

        /**
         * END: HELPER
         * *******************************************************
         */

        /**
         * Set Arguments
         */
        public function setArguments()
        {
            // This is your option name where all the Redux data is stored.
            $opt_name = GF_OPTIONS_NAME;

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'             => $opt_name,
                // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'         => $theme->get('Name'),
                // Name that appears at the top of your panel
                'display_version'      => $theme->get('Version'),
                // Version that appears at the top of your panel
                'menu_type'            => 'menu',
                //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'       => false,
                // Show the sections below the admin menu item or not
                'menu_title'           => __('Theme Options', 'g5plus-arvo'),
                'page_title'           => __('Theme Options', 'g5plus-arvo'),
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key'       => '',
                // Set it you want google fonts to update weekly. A google_api_key value is required.
                'google_update_weekly' => false,
                // Must be defined to add google fonts to the typography module
                'async_typography'     => true,
                // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'            => true,
                // Show the panel pages on the admin bar
                'admin_bar_icon'       => 'dashicons-portfolio',
                // Choose an icon for the admin bar menu
                'admin_bar_priority'   => 50,
                // Choose an priority for the admin bar menu
                'global_variable'      => '',
                // Set a different name for your global variable other than the opt_name
                'dev_mode'             => false,
                // Show the time the page took to load, etc
                'update_notice'        => true,
                // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                'customizer'           => true,
                // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'        => null,
                // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'          => 'themes.php',
                // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'     => 'manage_options',
                // Permissions needed to access the options panel.
                'menu_icon'            => '',
                // Specify a custom URL to an icon
                'last_tab'             => '',
                // Force your panel to always open to a specific tab (by id)
                'page_icon'            => 'icon-themes',
                // Icon displayed in the admin panel next to your menu_title
                'page_slug'            => '_options',
                // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
                'save_defaults'        => true,
                // On load save the defaults to DB before user clicks save or not
                'default_show'         => false,
                // If true, shows the default value next to each field that is not the default value.
                'default_mark'         => '',
                // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export'   => true,
                // Shows the Import/Export panel when not used as a field.

                // CAREFUL -> These options are for advanced use only
                'transient_time'       => 60 * MINUTE_IN_SECONDS,
                'output'               => true,
                // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'           => true,
                // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'             => '',
                // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'use_cdn'              => true,
                // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

                // HINTS
                'hints'                => array(
                    'icon'          => 'el el-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'   => 'red',
                        'shadow'  => true,
                        'rounded' => false,
                        'style'   => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show' => array(
                            'effect'   => 'slide',
                            'duration' => '500',
                            'event'    => 'mouseover',
                        ),
                        'hide' => array(
                            'effect'   => 'slide',
                            'duration' => '500',
                            'event'    => 'click mouseleave',
                        ),
                    ),
                )
            );

            Redux::setArgs($opt_name, $args);

            /*
			 * ---> END ARGUMENTS
			 */
        }

        /**
         * Set Section
         */
        public function setSections()
        {
            $opt_name = GF_OPTIONS_NAME;

            /**
             * Set Fonts
             * *******************************************************
             */
            $fonts = array(
                "Arial, Helvetica, sans-serif"                         => "Arial, Helvetica, sans-serif",
                "'Arial Black', Gadget, sans-serif"                    => "'Arial Black', Gadget, sans-serif",
                "'Bookman Old Style', serif"                           => "'Bookman Old Style', serif",
                "'Comic Sans MS', cursive"                             => "'Comic Sans MS', cursive",
                "Courier, monospace"                                   => "Courier, monospace",
                "Garamond, serif"                                      => "Garamond, serif",
                "Georgia, serif"                                       => "Georgia, serif",
                "Impact, Charcoal, sans-serif"                         => "Impact, Charcoal, sans-serif",
                "'Lucida Console', Monaco, monospace"                  => "'Lucida Console', Monaco, monospace",
                "'Lucida Sans Unicode', 'Lucida Grande', sans-serif"   => "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
                "'MS Sans Serif', Geneva, sans-serif"                  => "'MS Sans Serif', Geneva, sans-serif",
                "'MS Serif', 'New York', sans-serif"                   => "'MS Serif', 'New York', sans-serif",
                "'Palatino Linotype', 'Book Antiqua', Palatino, serif" => "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
                "Tahoma,Geneva, sans-serif"                            => "Tahoma, Geneva, sans-serif",
                "'Times New Roman', Times,serif"                       => "'Times New Roman', Times, serif",
                "'Trebuchet MS', Helvetica, sans-serif"                => "'Trebuchet MS', Helvetica, sans-serif",
                "Verdana, Geneva, sans-serif"                          => "Verdana, Geneva, sans-serif",
            );

            $_framework_options = get_option(GF_OPTIONS_NAME);
            if (is_array($_framework_options)) {
                for ($i = 1; $i <= 2; $i++) {
                    if (array_key_exists('custom_font_' . $i . '_name', $_framework_options)) {
                        $custom_font = $_framework_options['custom_font_' . $i . '_name'];
                    }
                    if (array_key_exists('custom_font_' . $i . '_ttf', $_framework_options)) {
                        $ttf = $_framework_options['custom_font_' . $i . '_ttf'];
                    }
                    if (array_key_exists('custom_font_' . $i . '_eot', $_framework_options)) {
                        $eot = $_framework_options['custom_font_' . $i . '_eot'];
                    }
                    if (array_key_exists('custom_font_' . $i . '_woff', $_framework_options)) {
                        $woff = $_framework_options['custom_font_' . $i . '_woff'];
                    }
                    if (isset($custom_font) && isset($ttf) && isset($eot) && isset($woff) && $custom_font != '') {
                        $fonts[$custom_font] = 'Custom - ' . $custom_font;
                    }
                }
            }

	        /**
	         * Post Type Preset
	         */
	        $post_type_preset = apply_filters('gf_post_type_preset', array());
	        $post_type_preset_list = array();
	        foreach ($post_type_preset as $key => $value) {
		        $post_type_preset_list[] = array(
			        'id' => 'divide_preset_setting_' . $key,
			        'type' => 'divide',
		        );
		        $post_type_preset_list[] = array(
			        'id' => $key . '_preset',
			        'type' => 'select',
			        'data' => 'posts',
			        'title' => $value['name'] . esc_html__(' Preset','g5plus-arvo'),
			        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
			        'default' => '',
			        'args' => array(
				        'post_type' => 'gf_preset',
				        'posts_per_page' => -1
			        )
		        );
		        $post_type_preset_list[] = array(
			        'id' => $key . '_single_preset',
			        'type' => 'select',
			        'data' => 'posts',
			        'title' => $value['name'] . esc_html__(' Single Preset', 'g5plus-arvo'),
			        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
			        'default' => '',
			        'args' => array(
				        'post_type' => 'gf_preset',
				        'posts_per_page' => -1
			        )
		        );
	        }

            /**
             * General Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('General Setting', 'g5plus-arvo'),
                'id'     => 'general',
                'desc'   => esc_html__('These are general setting!', 'g5plus-arvo'),
                'icon'   => 'el el-home',
                'fields' => array(
                    array(
                        'id'       => 'smooth_scroll',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Smooth Scroll', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Smooth Scroll', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),

                    array(
                        'id'       => 'custom_scroll',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Custom Scroll', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Custom Scroll', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),

                    array(
                        'id'       => 'custom_scroll_width',
                        'type'     => 'text',
                        'title'    => esc_html__('Custom Scroll Width', 'g5plus-arvo'),
                        'subtitle' => esc_html__('This must be numeric (no px) or empty.', 'g5plus-arvo'),
                        'validate' => 'numeric',
                        'default'  => '10',
                        'required' => array('custom_scroll', '=', 1),
                    ),

                    array(
                        'id'       => 'custom_scroll_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Custom Scroll Color', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set Custom Scroll Color', 'g5plus-arvo'),
                        'default'  => '#19394B',
                        'validate' => 'color',
                        'required' => array('custom_scroll', '=', 1),
                    ),

                    array(
                        'id'       => 'custom_scroll_thumb_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Custom Scroll Thumb Color', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set Custom Scroll Thumb Color', 'g5plus-arvo'),
                        'default'  => '#1086df',
                        'validate' => 'color',
                        'required' => array('custom_scroll', '=', 1),
                    ),

                    array(
                        'id'       => 'back_to_top',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Back To Top', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Back to top button', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

                    array(
                        'id'       => 'enable_rtl_mode',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Enable RTL mode', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable RTL mode', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),

                    array(
                        'id'       => 'social_meta_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Enable Social Meta Tags', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable the social meta head tag output.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),

                    array(
                        'id'       => 'twitter_author_username',
                        'type'     => 'text',
                        'title'    => esc_html__('Twitter Publisher Username', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enter your twitter username here, to be used for the Twitter Card date. Ensure that you do not include the @ symbol.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => "",
                        'required' => array('enable_social_meta', '=', array('1')),
                    ),
                    array(
                        'id'       => 'googleplus_author',
                        'type'     => 'text',
                        'title'    => esc_html__('Google+ Username', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enter your Google+ username here, to be used for the authorship meta.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => "",
                        'required' => array('enable_social_meta', '=', array('1')),
                    ),


                    array(
                        'id'   => 'general_divide_2',
                        'type' => 'divide'
                    ),

                    array(
                        'id'       => 'shopping_cart_button',
                        'type'     => 'checkbox',
                        'title'    => esc_html__('Shopping Mini Cart Button', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select shopping mini cart button', 'g5plus-arvo'),
                        'options'  => array(
                            'view-cart' => esc_html__('View Cart', 'g5plus-arvo'),
                            'checkout'  => esc_html__('Checkout', 'g5plus-arvo'),
                        ),
                        'default'  => array(
                            'view-cart' => '1',
                            'checkout'  => '1',
                        ),
                    ),

                    array(
                        'id'       => 'search_popup_type',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Search Popup Type', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select search popup type.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('standard' => esc_html__('Standard', 'g5plus-arvo'), 'ajax' => esc_html__('Ajax Search', 'g5plus-arvo')),
                        'default'  => 'standard'
                    ),
	                array(
		                'id'       => 'search_popup_post_type',
		                'type'     => 'checkbox',
		                'title'    => esc_html__('Post type for Search', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Select post type for search', 'g5plus-arvo'),
		                'options'  => gf_get_post_type_search(),
		                'default'  => array(
			                'post'      => '1',
			                'page'      => '0',
			                'product'   => '1',
		                ),
		                'required' => array('search_popup_type', '=', 'ajax'),
	                ),
                    array(
                        'id'       => 'search_popup_result_amount',
                        'type'     => 'text',
                        'title'    => esc_html__('Amount Of Search Result', 'g5plus-arvo'),
                        'subtitle' => esc_html__('This must be numeric (no px) or empty (default: 8).', 'g5plus-arvo'),
                        'desc'     => esc_html__('Set mount of Search Result', 'g5plus-arvo'),
                        'validate' => 'numeric',
                        'default'  => '',
                        'required' => array('search_popup_type', '=', 'ajax'),
                    ),

                    array(
                        'id'   => 'general_divide_3',
                        'type' => 'divide'
                    ),
                    array(
                        'id'       => 'menu_transition',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Menu transition', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select menu transition', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'none'                  => esc_html__('None', 'g5plus-arvo'),
                            'x-animate-slide-up'    => esc_html__('Slide Up', 'g5plus-arvo'),
                            'x-animate-slide-down'  => esc_html__('Slide Down', 'g5plus-arvo'),
                            'x-animate-slide-left'  => esc_html__('Slide Left', 'g5plus-arvo'),
                            'x-animate-slide-right' => esc_html__('Slide Right', 'g5plus-arvo'),
                            'x-animate-sign-flip'   => esc_html__('Sign Flip', 'g5plus-arvo'),
                        ),
                        'default'  => 'x-animate-sign-flip'
                    ),
                )
            ));

            /**
             * Maintenance Mode Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'      => esc_html__('Maintenance Mode', 'g5plus-arvo'),
                'desc'       => '',
                'subsection' => true,
                'icon'       => 'el el-wrench',
                'fields'     => array(
                    array(
                        'id'       => 'enable_maintenance',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Enable Maintenance', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable the themes maintenance mode.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('2' => 'On (Custom Page)', '1' => 'On (Standard)', '0' => 'Off',),
                        'default'  => '0'
                    ),
                    array(
                        'id'       => 'maintenance_mode_page',
                        'type'     => 'select',
                        'data'     => 'pages',
                        'required' => array('enable_maintenance', '=', '2'),
                        'title'    => esc_html__('Custom Maintenance Mode Page', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select the page that is your maintenace page, if you would like to show a custom page instead of the standard WordPress message. You should use the Holding Page template for this page.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                        'args'     => array()
                    ),
                ),
            ));

            /**
             * Performance Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'      => esc_html__('Performance', 'g5plus-arvo'),
                'desc'       => '',
                'icon'       => 'el el-fire',
                'subsection' => true,
                'fields'     => array(
                    array(
                        'id'       => 'enable_minifile_js',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Enable Mini File JS', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Mini File JS', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),
                    array(
                        'id'       => 'enable_minifile_css',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Enable Mini File CSS', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Mini File CSS', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),
                )
            ));

            /**
             * Page Transition Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'      => esc_html__('Page Transition', 'g5plus-arvo'),
                'desc'       => '',
                'icon'       => 'el el-dashboard',
                'subsection' => true,
                'fields'     => array(
                    array(
                        'id'       => 'page_transition',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Page Transition', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Page Transition', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),

                    array(
                        'id'       => 'loading_animation',
                        'type'     => 'select',
                        'title'    => esc_html__('Loading Animation', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose type of preload animation', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'none'          => esc_html__('No animation', 'g5plus-arvo'),
                            'cube'          => esc_html__('Cube', 'g5plus-arvo'),
                            'double-bounce' => esc_html__('Double bounce', 'g5plus-arvo'),
                            'wave'          => esc_html__('Wave', 'g5plus-arvo'),
                            'pulse'         => esc_html__('Pulse', 'g5plus-arvo'),
                            'chasing-dots'  => esc_html__('Chasing dots', 'g5plus-arvo'),
                            'three-bounce'  => esc_html__('Three bounce', 'g5plus-arvo'),
                            'circle'        => esc_html__('Circle', 'g5plus-arvo'),
                            'fading-circle' => esc_html__('Fading circle', 'g5plus-arvo'),
                            'folding-cube'  => esc_html__('Folding cube', 'g5plus-arvo'),
                        ),
                        'default'  => 'none'
                    ),

                    array(
                        'id'       => 'loading_logo',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Logo Loading', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload logo loading.', 'g5plus-arvo'),
                        'desc'     => '',
                        'required' => array('loading_animation', 'not_empty_and', 'none'),
                    ),

                    array(
                        'id'       => 'loading_animation_bg_color',
                        'type'     => 'color_rgba',
                        'title'    => esc_html__('Loading Background Color', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set loading background color.', 'g5plus-arvo'),
                        'default'  => array(
                            'color' => '#ffffff',
                            'alpha' => 1
                        ),
                        'output'   => array('background-color' => '.site-loading'),
                        'validate' => 'colorrgba',
                        'required' => array('loading_animation', 'not_empty_and', 'none'),
                    ),

                    array(
                        'id'       => 'spinner_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Spinner color', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Pick a spinner color', 'g5plus-arvo'),
                        'default'  => '',
                        'validate' => 'color',
                        'output'   => array('background-color' => '.sk-spinner-pulse,.sk-rotating-plane,.sk-double-bounce .sk-child,.sk-wave .sk-rect,.sk-chasing-dots .sk-child,.sk-three-bounce .sk-child,.sk-circle .sk-child:before,.sk-fading-circle .sk-circle:before,.sk-folding-cube .sk-cube:before'),
                        'required' => array('loading_animation', 'not_empty_and', 'none'),
                    ),
                )
            ));

            /**
             * Custom Favicon
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'      => esc_html__('Custom Favicon', 'g5plus-arvo'),
                'desc'       => '',
                'icon'       => 'el el-eye-open',
                'subsection' => true,
                'fields'     => array(
                    array(
                        'id'       => 'custom_favicon',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Custom favicon', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a 16px x 16px Png/Gif/ico image that will represent your website favicon', 'g5plus-arvo'),
                        'desc'     => ''
                    ),
                    array(
                        'id'       => 'custom_ios_title',
                        'type'     => 'text',
                        'title'    => esc_html__('Custom iOS Bookmark Title', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enter a custom title for your site for when it is added as an iOS bookmark.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => ''
                    ),
                    array(
                        'id'       => 'custom_ios_icon57',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Custom iOS 57x57', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a 57px x 57px Png image that will be your website bookmark on non-retina iOS devices.', 'g5plus-arvo'),
                        'desc'     => ''
                    ),
                    array(
                        'id'       => 'custom_ios_icon72',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Custom iOS 72x72', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a 72px x 72px Png image that will be your website bookmark on non-retina iOS devices.', 'g5plus-arvo'),
                        'desc'     => ''
                    ),
                    array(
                        'id'       => 'custom_ios_icon114',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Custom iOS 114x114', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a 114px x 114px Png image that will be your website bookmark on retina iOS devices.', 'g5plus-arvo'),
                        'desc'     => ''
                    ),
                    array(
                        'id'       => 'custom_ios_icon144',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Custom iOS 144x144', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a 144px x 144px Png image that will be your website bookmark on retina iOS devices.', 'g5plus-arvo'),
                        'desc'     => ''
                    ),
                )
            ));

            /**
             * 404 Setting Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'      => esc_html__('404 Setting', 'g5plus-arvo'),
                'desc'       => '',
                'subsection' => true,
                'icon'       => 'el el-error',
                'fields'     => array(
                    array(
                        'id'      => 'background_image_404',
                        'type'    => 'media',
                        'url'     => true,
                        'title'   => esc_html__('Background image', 'g5plus-arvo'),
                        'desc'    => '',
                        'default' => array(
                            'url' => GF_PLUGIN_URL . 'assets/images/theme-options/bg-404.jpg'
                        )
                    ),
                )
            ));

            /**
             * Layout Section
             * *******************************************************
             */
	        $custom_page_layout = gf_get_custom_page_layout();
	        $custom_page_layout_list = array();
	        foreach ($custom_page_layout as $key => $value) {
		        $custom_page_layout_list[] = $this->get_section_start($key. '_layout',$value['title']);
		        $custom_page_layout_list[] = array(
			        'id' => 'custom_'.$key.'_layout_enable',
			        'type' => 'button_set',
			        'title' => esc_html__('Custom Layout', 'g5plus-arvo'),
			        'subtitle' => sprintf(esc_html__('Turn on this option if you want to enable custom layout on %', 'g5plus-arvo'),$value['title']),
			        'desc' => '',
			        'options' => gf_get_toggle(),
			        'default' => 0
		        );
		        $custom_page_layout_list[] = $this->get_page_layout($key.'_layout',array('custom_'.$key.'_layout_enable', '=', 1));
		        $custom_page_layout_list[] = $this->get_sidebar_layout($key.'_sidebar_layout', array('custom_'.$key.'_layout_enable', '=', 1));
		        $custom_page_layout_list[] = $this->get_sidebar($key.'_sidebar',array(
			        array('custom_'.$key.'_layout_enable', '=', 1),
			        array($key.'_sidebar_layout', '=', array('left','right'))
		        ));
		        $custom_page_layout_list[] = $this->get_sidebar_width($key.'_sidebar_width',array(
			        array('custom_'.$key.'_layout_enable', '=', 1),
			        array($key.'_sidebar_layout', '=', array('left','right'))
		        ));
		        $custom_page_layout_list[] = $this->get_sidebar_mobile_enable($key.'_sidebar_mobile_enable',array(
			        array('custom_'.$key.'_layout_enable', '=', 1),
			        array($key.'_sidebar_layout', '=', array('left','right'))
		        ));
		        $custom_page_layout_list[] = $this->get_sidebar_mobile_canvas( $key.'_sidebar_mobile_canvas', array(
			        array( 'custom_'.$key.'_layout_enable', '=', 1 ),
			        array( $key.'_sidebar_mobile_enable', '=', 1 )
		        ));
		        $custom_page_layout_list[] = $this->get_content_padding($key.'_content_padding',array('custom_'.$key.'_layout_enable', '=', 1));
		        $custom_page_layout_list[] = $this->get_content_padding_mobile($key.'_content_padding_mobile',array('custom_'.$key.'_layout_enable', '=', 1));
	        }
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Layout Options', 'g5plus-arvo'),
                'icon'   => 'el el-website',
                'fields' => array_merge(
	                array(
		                // General
		                array(
			                'id'       => 'layout_style',
			                'type'     => 'image_select',
			                'title'    => esc_html__('Layout Style', 'g5plus-arvo'),
			                'subtitle' => esc_html__('Select the layout style', 'g5plus-arvo'),
			                'desc'     => '',
			                'options'  => gf_get_layout_style(),
			                'default'  => 'wide'
		                ),
		                array(
			                'id'       => 'body_background_mode',
			                'type'     => 'button_set',
			                'title'    => esc_html__('Body Background Mode', 'g5plus-arvo'),
			                'subtitle' => esc_html__('Chose Background Mode', 'g5plus-arvo'),
			                'desc'     => '',
			                'options'  => array(
				                'background' => esc_html__('Background', 'g5plus-arvo'),
				                'pattern'    => esc_html__('Pattern', 'g5plus-arvo')
			                ),
			                'default'  => 'background',
			                'required' => array('layout_style', '=', 'boxed'),
		                ),
		                array(
			                'id'       => 'body_background',
			                'type'     => 'background',
			                'output'   => array('body'),
			                'title'    => esc_html__('Body Background', 'g5plus-arvo'),
			                'subtitle' => esc_html__('Body background (Apply for Boxed layout style).', 'g5plus-arvo'),
			                'default'  => array(
				                'background-color'      => '',
				                'background-repeat'     => 'no-repeat',
				                'background-position'   => 'center center',
				                'background-attachment' => 'fixed',
				                'background-size'       => 'cover'
			                ),
			                'required' => array(
				                array('layout_style', '=', 'boxed'),
				                array('body_background_mode', '=', array('background'))
			                ),
		                ),
		                array(
			                'id'       => 'body_background_pattern',
			                'type'     => 'image_select',
			                'title'    => esc_html__('Background Pattern', 'g5plus-arvo'),
			                'subtitle' => esc_html__('Body background pattern(Apply for Boxed layout style)', 'g5plus-arvo'),
			                'desc'     => '',
			                'height'   => '40px',
			                'options'  => array(
				                'pattern-1.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-1.png'),
				                'pattern-2.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-2.png'),
				                'pattern-3.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-3.png'),
				                'pattern-4.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-4.png'),
				                'pattern-5.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-5.png'),
				                'pattern-6.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-6.png'),
				                'pattern-7.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-7.png'),
				                'pattern-8.png' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/pattern-8.png'),
			                ),
			                'default'  => 'pattern-1.png',
			                'required' => array(
				                array('layout_style', '=', 'boxed'),
				                array('body_background_mode', '=', array('pattern'))
			                ),
		                ),
		                $this->get_page_layout('layout'),
		                $this->get_sidebar_layout('sidebar_layout'),
		                $this->get_sidebar('sidebar', array('sidebar_layout', '=', array('left', 'right'))),
		                $this->get_sidebar_width('sidebar_width', array('sidebar_layout', '=', array('left', 'right'))),
		                $this->get_sidebar_mobile_enable('sidebar_mobile_enable', array('sidebar_layout', '=', array('left', 'right'))),
		                $this->get_sidebar_mobile_canvas( 'sidebar_mobile_canvas', array('sidebar_mobile_enable','=',1)),
		                $this->get_content_padding('content_padding'),
		                $this->get_content_padding_mobile('content_padding_mobile'),
	                ),
	                $custom_page_layout_list
                )
            ));

            /**
             * Page Title Section
             * *******************************************************
             */
	        $custom_page_title = gf_get_custom_page_title();
	        $custom_page_title_list = array();
	        foreach ($custom_page_title as $key => $value) {
		        $custom_page_title_list[] = $this->get_section_start($key.'_title',$value['title']);
		        $custom_page_title_list[] = array(
			        'id' => 'custom_'.$key.'_title_enable',
			        'type' => 'button_set',
			        'title' => esc_html__('Custom Page Title', 'g5plus-arvo'),
			        'subtitle' => sprintf(esc_html__('Turn on this option if you want to enable custom title on %s', 'g5plus-arvo'),$value['title']) ,
			        'desc' => '',
			        'options' => gf_get_toggle(),
			        'default' => 0
		        );
		        $custom_page_title_list[] = $this->get_page_title_enable($key.'_title_enable',array('custom_'.$key.'_title_enable', '=', 1));
		        $custom_page_title_list[] = $this->get_page_title_layout($key.'_title_layout',array(array('custom_'.$key.'_title_enable', '=', 1),array($key.'_title_enable', '=', 1)) );
		        $custom_page_title_list[] = $this->get_page_sub_title($key.'_sub_title',array(array('custom_'.$key.'_title_enable', '=', 1),array($key.'_title_enable', '=', 1)));
		        $custom_page_title_list[] = $this->get_page_title_padding($key.'_title_padding',array(array('custom_'.$key.'_title_enable', '=', 1),array($key.'_title_enable', '=', 1)));
		        $custom_page_title_list[] = $this->get_page_title_background_image($key.'_title_bg_image',array(array('custom_'.$key.'_title_enable', '=', 1),array($key.'_title_enable', '=', 1)));
                $custom_page_title_list[] = $this->get_page_title_overlay_scheme($key.'_title_overlay_scheme',array(array('custom_'.$key.'_title_enable', '=', 1), array($key.'_title_enable', '=', 1), array($key.'_title_bg_image', '!=', '')));
		        $custom_page_title_list[] = $this->get_page_title_parallax($key.'_title_parallax',array(array('custom_'.$key.'_title_enable', '=', 1),array($key.'_title_enable', '=', 1),array($key.'_title_bg_image', '!=', '')));
		        $custom_page_title_list[] = $this->get_breadcrumb_enable($key.'_breadcrumbs_enable',array(array('custom_'.$key.'_title_enable', '=', 1),array($key.'_title_enable', '=', 1)));
	        }

            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Page Title', 'g5plus-arvo'),
                'icon'   => 'el el-asterisk',
                'fields' => array_merge(
	                array(
	                $this->get_page_title_enable('page_title_enable'),
	                $this->get_page_title_layout('page_title_layout', array('page_title_enable', '=', 1)),
	                $this->get_page_title_padding('page_title_padding', array('page_title_enable', '=', 1)),
	                $this->get_page_title_background_image('page_title_bg_image', array('page_title_enable', '=', 1),array(
		                'url' => trailingslashit(get_template_directory_uri()) . 'assets/images/page-title-bg.jpg'
	                )),
                    $this->get_page_title_overlay_scheme('page_title_overlay_scheme',array(array('page_title_enable', '=', 1), array('page_title_bg_image', '!=', '')), 'page-title-overlay-dark'),
	                $this->get_page_title_parallax('page_title_parallax', array(array('page_title_enable', '=', 1), array('page_title_bg_image', '!=', ''))),
	                $this->get_breadcrumb_enable('breadcrumbs_enable', array('page_title_enable', '=', 1)),
	                ),
		            $custom_page_title_list
                )
            ));

            /**
             * Blog Setting Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Blog Setting', 'g5plus-arvo'),
                'icon'   => 'el el-blogger',
                'fields' => array(
                    array(
                        'id'     => 'blog_start',
                        'type'   => 'section',
                        'title'  => esc_html__('Blog Options', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'post_layout',
                        'type'     => 'select',
                        'title'    => esc_html__('Post Layout', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'select2' => array('allowClear' => false),
                        'options'  => gf_get_post_layout(),
                        'default'  => 'large-image'
                    ),
                    array(
                        'id'       => 'post_column',
                        'type'     => 'select',
                        'title'    => esc_html__('Post Columns', 'g5plus-arvo'),
                        'subtitle' => '',
                        'options'  => gf_get_post_columns(),
                        'desc'     => '',
                        'default'  => 3,
	                    'select2' => array('allowClear' => false),
                        'required' => array('post_layout', '=', array('grid','masonry','1-large-image-masonry')),
                    ),
                    array(
                        'id'       => 'post_paging',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Post Paging', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_paging_style(),
                        'default'  => 'navigation'
                    ),

                    array(
                        'id'     => 'single_blog_start',
                        'type'   => 'section',
                        'title'  => esc_html__('Single Blog Options', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'single_tag_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Tags', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Turn Off this option if you want to hide tags on single blog', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

                    array(
                        'id'       => 'single_share_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Share', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Turn Off this option if you want to hide share on single blog', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

	                array(
		                'id'       => 'single_author_info_enable',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Author Info', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Turn Off this option if you want to hide author info area on single blog', 'g5plus-arvo'),
		                'desc'     => '',
		                'options'  => gf_get_toggle(),
		                'default'  => 1
	                ),

                    array(
                        'id'       => 'single_navigation_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Navigation', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Turn Off this option if you want to hide navigation on single blog', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),


                    array(
                        'id'       => 'single_related_post_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Related Post', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Turn Off this option if you want to hide related post area on single blog', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),

	                array(
		                'id' => 'single_related_enable_no_image',
		                'type' => 'button_set',
		                'title' => esc_html__('Use No Image Thumbnail', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Turn On this option if you want to use no image for thumbnail', 'g5plus-arvo'),
		                'desc' => '',
		                'options' => gf_get_toggle(),
		                'default' => 0,
		                'required'  => array('single_related_post_enable', '=', 1),
	                ),

                    array(
                        'id'       => 'single_related_post_total',
                        'type'     => 'text',
                        'title'    => esc_html__('Related Post Total', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Total record of Related Post.', 'g5plus-arvo'),
                        'validate' => 'number',
                        'default'  => 6,
                        'required' => array('single_related_post_enable', '=', 1),
                    ),

                    array(
                        'id'       => 'single_related_post_column',
                        'type'     => 'select',
                        'title'    => esc_html__('Related Post Columns', 'g5plus-arvo'),
                        'default'  => 2,
                        'options'  => array(2 => '2', 3 => '3'),
                        'select2'  => array('allowClear' => false),
                        'required' => array('single_related_post_enable', '=', 1),
                    ),

                    array(
                        'id'       => 'single_related_post_condition',
                        'type'     => 'checkbox',
                        'title'    => esc_html__('Related Post Condition', 'g5plus-arvo'),
                        'options'  => array(
                            'category' => esc_html__('Same Category', 'g5plus-arvo'),
                            'tag'      => esc_html__('Same Tag', 'g5plus-arvo'),
                        ),
                        'default'  => array(
                            'category' => '1',
                            'tag'      => '1',
                        ),
                        'required' => array('single_related_post_enable', '=', 1),
                    ),
                )
            ));

            /**
             * Logo Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Logo Setting', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-leaf',
                'fields' => array(
                    array(
                        'id'     => 'section-logo-desktop',
                        'type'   => 'section',
                        'title'  => esc_html__('Logo Desktop', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'logo',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Logo', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload your logo here.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => array(
                            'url' => trailingslashit(get_template_directory_uri()) . 'assets/images/logo.png'
                        )
                    ),
                    array(
                        'id'       => 'logo_retina',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Logo Retina', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload your logo retina here.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => array(
                            'url' => ''
                        )
                    ),
                    array(
                        'id'       => 'sticky_logo',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Sticky Logo', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a sticky version of your logo here', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => array(
                            'url' => trailingslashit(get_template_directory_uri()) . 'assets/images/logo.png'
                        )
                    ),
                    array(
                        'id'       => 'sticky_logo_retina',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Sticky Logo Retina', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload a sticky version of your logo retina here', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => array(
                            'url' => ''
                        )
                    ),
                    array(
                        'id'      => 'logo_max_height',
                        'type'    => 'dimensions',
                        'title'   => esc_html__('Logo Max Height', 'g5plus-arvo'),
                        'desc'    => esc_html__('You can set a max height for the logo here', 'g5plus-arvo'),
                        'units'   => false,
                        'width'   => false,
                        'default' => array(
                            'height' => ''
                        )
                    ),
                    array(
                        'id'             => 'logo_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Logo Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default logo top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '',
                            'padding-bottom' => '',
                            'units'          => 'px',
                        )
                    ),

                    array(
                        'id'     => 'section-logo-mobile',
                        'type'   => 'section',
                        'title'  => esc_html__('Logo Mobile', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'mobile_logo',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Mobile Logo', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload your logo here.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => array(
                            'url' => trailingslashit(get_template_directory_uri()) . 'assets/images/logo.png'
                        )
                    ),
                    array(
                        'id'       => 'mobile_logo_retina',
                        'type'     => 'media',
                        'url'      => true,
                        'title'    => esc_html__('Mobile Logo Retina', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Upload your logo retina here.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => array(
                            'url' => ''
                        )
                    ),
                    array(
                        'id'      => 'mobile_logo_max_height',
                        'type'    => 'dimensions',
                        'title'   => esc_html__('Mobile Logo Max Height', 'g5plus-arvo'),
                        'desc'    => esc_html__('You can set a max height for the logo mobile here', 'g5plus-arvo'),
                        'units'   => false,
                        'width'   => false,
                        'default' => array(
                            'height' => ''
                        )
                    ),
                    array(
                        'id'             => 'mobile_logo_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Logo Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default logo top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '',
                            'padding-bottom' => '',
                            'units'          => 'px',
                        )
                    ),

                )
            ));

            /**
             * Top Drawer Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Top Drawer', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-photo',
                'fields' => array(
                    array(
                        'id'       => 'top_drawer_type',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Top Drawer Type', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set top drawer type.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('none' => 'Disable', 'show' => 'Always Show', 'toggle' => 'Toggle'),
                        'default'  => 'none'
                    ),
                    array(
                        'id'       => 'top_drawer_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Top Drawer Sidebar', 'g5plus-arvo'),
                        'subtitle' => "Choose the default top drawer sidebar",
                        'data'     => 'sidebars',
                        'desc'     => '',
	                    'select2'  => array('allowClear' => false),
                        'default'  => 'top_drawer_sidebar',
                        'required' => array('top_drawer_type', '=', array('show', 'toggle')),
                    ),

                    array(
                        'id'       => 'top_drawer_wrapper_layout',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Top Drawer Wrapper Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select top drawer wrapper layout', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('full' => 'Full Width', 'container' => 'Container', 'container-fluid' => 'Container Fluid'),
                        'default'  => 'container',
                        'required' => array('top_drawer_type', '=', array('show', 'toggle')),
                    ),

                    array(
                        'id'       => 'top_drawer_hide_mobile',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show/Hide Top Drawer on mobile', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => 'On', '0' => 'Off'),
                        'default'  => '1',
                        'required' => array('top_drawer_type', '=', array('show', 'toggle')),
                    ),
                    array(
                        'id'             => 'top_drawer_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Logo Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default top drawer top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '',
                            'padding-bottom' => '',
                            'units'          => 'px',
                        ),
                        'required'       => array('top_drawer_type', '=', array('show', 'toggle')),
                    ),
                )
            ));

            /**
             * Top Bar Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Top Bar', 'g5plus-arvo'),
                'desc'   => esc_html__('Setting for Top Bar', 'g5plus-arvo'),
                'icon'   => 'el el-minus',
                'fields' => array(
                    array(
                        'id'     => 'section-top-bar-desktop',
                        'type'   => 'section',
                        'title'  => esc_html__('Desktop Settings', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'top_bar_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show/Hide Top Bar', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Show Hide Top Bar.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => esc_html__('Show', 'g5plus-arvo'), '0' => esc_html__('Hide', 'g5plus-arvo')),
                        'default'  => '0',
                    ),
                    array(
                        'id'       => 'top_bar_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Top bar Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select the top bar column layout.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'top-bar-1' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-1.jpg'),
                            'top-bar-2' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-2.jpg'),
                            'top-bar-3' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-3.jpg'),
                            'top-bar-4' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-4.jpg'),
                        ),
                        'default'  => 'top-bar-1',
                        'required' => array('top_bar_enable', '=', '1'),
                    ),

                    array(
                        'id'       => 'top_bar_left_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Top Left Sidebar', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the default top left sidebar', 'g5plus-arvo'),
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'top_bar_left',
	                    'select2'  => array('allowClear' => false),
                        'required' => array('top_bar_enable', '=', '1'),
                    ),
                    array(
                        'id'       => 'top_bar_right_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Top Right Sidebar', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the default top right sidebar', 'g5plus-arvo'),
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'top_bar_right',
	                    'select2'  => array('allowClear' => false),
                        'required' => array(
	                        array('top_bar_enable', '=', '1'),
	                        array('top_bar_layout', '!=', 'top-bar-4'),
                        ),
                    ),
                    array(
                        'id'       => 'top_bar_border',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Top bar border', 'g5plus-arvo'),
                        'options'  => array(
                            'none'             => esc_html__('None', 'g5plus-arvo'),
                            'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
                            'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
                        ),
                        'default'  => 'none',
                        'required' => array('top_bar_enable', '=', '1'),
                    ),
                    array(
                        'id'             => 'top_bar_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Top Bar Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default top bar top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '',
                            'padding-bottom' => '',
                            'units'          => 'px',
                        ),
                        'required'       => array('top_bar_enable', '=', '1'),
                    ),

                    //--------------------------------------------------------------
                    array(
                        'id'     => 'section-top-bar-mobile',
                        'type'   => 'section',
                        'title'  => esc_html__('Mobile Settings', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'top_bar_mobile_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show/Hide Top Bar', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Show Hide Top Bar.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => esc_html__('Show', 'g5plus-arvo'), '0' => esc_html__('Hide', 'g5plus-arvo')),
                        'default'  => '0',
                    ),
                    array(
                        'id'       => 'top_bar_mobile_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Top bar Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select the top bar column layout.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'top-bar-1' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-1.jpg'),
                            'top-bar-2' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-2.jpg'),
                            'top-bar-3' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-3.jpg'),
                            'top-bar-4' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/top-bar-layout-4.jpg'),
                        ),
                        'default'  => 'top-bar-1',
                        'required' => array('top_bar_mobile_enable', '=', '1'),
                    ),

                    array(
                        'id'       => 'top_bar_mobile_left_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Top Left Sidebar', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the default top left sidebar', 'g5plus-arvo'),
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'top_bar_left',
	                    'select2'  => array('allowClear' => false),
                        'required' => array('top_bar_mobile_enable', '=', '1'),
                    ),
                    array(
                        'id'       => 'top_bar_mobile_right_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Top Right Sidebar', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the default top right sidebar', 'g5plus-arvo'),
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'top_bar_right',
	                    'select2'  => array('allowClear' => false),
                        'required' => array(
	                        array('top_bar_mobile_enable', '=', '1'),
	                        array('top_bar_mobile_layout', '!=', 'top-bar-4'),
                        ),
                    ),
                    array(
                        'id'       => 'top_bar_mobile_border',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Top bar mobile border', 'g5plus-arvo'),
                        'options'  => array(
                            'none'             => esc_html__('None', 'g5plus-arvo'),
                            'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
                            'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
                        ),
                        'default'  => 'container-border',
                        'required' => array('top_bar_mobile_enable', '=', '1'),
                    ),
                    array(
                        'id'             => 'top_bar_mobile_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Top Bar Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default top bar top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '',
                            'padding-bottom' => '',
                            'units'          => 'px',
                        ),
                        'required'       => array('top_bar_mobile_enable', '=', '1'),
                    ),
                )
            ));

            /**
             * Header Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Header', 'g5plus-arvo'),
                'desc'   => esc_html__('Setting for Theme Header', 'g5plus-arvo'),
                'icon'   => 'el el-credit-card',
                'fields' => array(
                    array(
                        'id'       => 'header_responsive_breakpoint',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Header responsive breakpoint', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set header responsive breakpoint', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
	                        '1199' => esc_html__('Large Devices: < 1200px', 'g5plus-arvo'),
                            '991'  => esc_html__('Medium Devices: < 992px', 'g5plus-arvo'),
                            '767'  => esc_html__('Tablet Portrait: < 768px', 'g5plus-arvo'),
                        ),
                        'default'  => '991'
                    ),
                    array(
                        'id'     => 'section-header-desktop',
                        'type'   => 'section',
                        'title'  => esc_html__('Desktop Settings', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'header_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Header Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select a header layout option from the examples.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'header-1'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-1.png'),
                            'header-2'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-2.png'),
                            'header-3'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-3.png'),
                            'header-4'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-4.png'),
	                        'header-5'  => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-5.png'),
                        ),
                        'default'  => 'header-1'
                    ),
                    array(
                        'id'      => 'header_container_layout',
                        'type'    => 'button_set',
                        'title'   => esc_html__('Header container layout', 'g5plus-arvo'),
                        'options' => array(
                            'container'      => esc_html__('Container', 'g5plus-arvo'),
                            'container-fluid' => esc_html__('Container Fluid', 'g5plus-arvo'),
                        ),
                        'default' => 'container',
	                    'required' => array('header_layout', 'equals', array('header-1','header-2','header-5')),
                    ),
                    array(
                        'id'       => 'header_float',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Header Float', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable/Disable Header Float.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => '0',
	                    'required' => array('header_layout', 'not', 'header-4'),
                    ),
                    array(
                        'id'       => 'header_sticky',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show/Hide Header Sticky', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Show Hide header Sticky.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => '1',
	                    'required' => array('header_layout', 'not', 'header-4'),
                    ),
	                array(
		                'id'       => 'header_sticky_change_style',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Header Sticky change style', 'g5plus-arvo'),
		                'subtitle' => esc_html__('On/Of change style when header sticky.', 'g5plus-arvo'),
		                'desc'     => '',
		                'options'  => gf_get_toggle(),
		                'default'  => '1',
		                'required' => array(
			                array('header_float', '=', '1'),
			                array('header_sticky', '=', '1'),
			                array('header_layout', 'not', 'header-4')
		                ),
	                ),
                    array(
                        'id'       => 'header_border_bottom',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Header border bottom', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set header border bottom', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'none'             => esc_html__('None', 'g5plus-arvo'),
                            'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
	                        'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
                        ),
                        'default'  => 'none',
	                    'required' => array('header_layout', 'equals', array('header-1','header-2','header-5')),
                    ),
                    array(
                        'id'       => 'header_above_border_bottom',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Header above border bottom', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set header above border bottom', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'none'             => esc_html__('None', 'g5plus-arvo'),
                            'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
	                        'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
                        ),
                        'default'  => 'none',
                        'required' => array('header_layout', '=', array('header-3')),
                    ),
                    array(
                        'id'             => 'header_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Header Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default header top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '',
                            'padding-bottom' => '',
                            'units'          => 'px',
                        ),
	                    'required' => array('header_layout', 'not', 'header-4'),
                    ),


	                array(
		                'id'       => 'navigation_float',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Navigation Float', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Enable/Disable Navigation Float.', 'g5plus-arvo'),
		                'desc'     => '',
		                'options'  => gf_get_toggle(),
		                'default'  => '1',
		                'required' => array('header_layout', '=', 'header-3'),
	                ),

                    array(
                        'id'      => 'navigation_height',
                        'type'    => 'dimensions',
                        'title'   => esc_html__('Navigation height', 'g5plus-arvo'),
                        'desc'    => esc_html__('Set header navigation height (px). Do not include unit. Empty to default', 'g5plus-arvo'),
                        'units'   => false,
                        'width'   => false,
                        'default' => array(
                            'height' => ''
                        ),
                    ),
                    array(
                        'id'      => 'navigation_spacing',
                        'type'    => 'slider',
                        'title'   => esc_html__('Navigation Spacing (px)', 'g5plus-arvo'),
                        'default' => '30',
                        'min'     => 0,
                        'step'    => 1,
                        'max'     => 100,
	                    'required' => array('header_layout', 'not', 'header-4'),
                    ),

                    //---------------------------------------------------------------
                    array(
                        'id'     => 'section-header-mobile',
                        'type'   => 'section',
                        'title'  => esc_html__('Mobile Settings', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'mobile_header_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Header Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select header mobile layout', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'header-mobile-1' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-1.png'),
                            'header-mobile-2' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-2.png'),
                            'header-mobile-3' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-3.png'),
                            'header-mobile-4' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/header-mobile-layout-4.png'),
                        ),
                        'default'  => 'header-mobile-1'
                    ),
                    array(
                        'id'       => 'mobile_header_menu_drop',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Menu Drop Type', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set menu drop type for mobile header', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'menu-drop-fly'      => esc_html__('Fly Menu', 'g5plus-arvo'),
                            'menu-drop-dropdown' => esc_html__('Dropdown Menu', 'g5plus-arvo'),
                        ),
                        'default'  => 'menu-drop-fly'
                    ),
                    array(
                        'id'       => 'mobile_header_stick',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Stick Mobile Header', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable Stick Mobile Header.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => esc_html__('On', 'g5plus-arvo'), '0' => esc_html__('Off', 'g5plus-arvo')),
                        'default'  => '0'
                    ),
                    array(
                        'id'       => 'mobile_header_search_box',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Search Box', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable Search Box.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => esc_html__('Show', 'g5plus-arvo'), '0' => esc_html__('Hide', 'g5plus-arvo')),
                        'default'  => '1'
                    ),
                    array(
                        'id'       => 'mobile_header_shopping_cart',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Shopping Cart', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable Shopping Cart', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => esc_html__('Show', 'g5plus-arvo'), '0' => esc_html__('Hide', 'g5plus-arvo')),
                        'default'  => '1'
                    ),
                    array(
                        'id'      => 'mobile_header_border_bottom',
                        'type'    => 'button_set',
                        'title'   => esc_html__('Mobile header border bottom', 'g5plus-arvo'),
                        'options' => array(
                            'none'             => esc_html__('None', 'g5plus-arvo'),
                            'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
                            'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
                        ),
                        'default' => 'none',
                    ),
                )
            ));

            /**
             * Header Customize Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Header Customize', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-credit-card',
                'fields' => array(
                    array(
                        'id'     => 'section-header-customize-nav',
                        'type'   => 'section',
                        'title'  => esc_html__('Header Customize Navigation', 'g5plus-arvo'),
                        'indent' => true,
                    ),
                    array(
                        'id'      => 'header_customize_nav',
                        'type'    => 'sorter',
                        'title'   => 'Header customize navigation',
                        'desc'    => 'Organize how you want the layout to appear on the header navigation',
                        'options' => array(
                            'enabled'  => array(
	                            'shopping-cart' => esc_html__('Shopping Cart', 'g5plus-arvo'),
	                            'search'        => esc_html__('Search', 'g5plus-arvo'),
                            ),
                            'disabled' => array(
	                            'canvas-menu' => esc_html__('Canvas Menu','g5plus-arvo'),
                                'custom-text'   => esc_html__('Custom Text', 'g5plus-arvo'),
                            )
                        ),

                    ),
                    array(
                        'id'      => 'header_customize_nav_search',
                        'type'    => 'button_set',
                        'title'   => esc_html__('Search Type', 'g5plus-arvo'),
                        'default' => 'button',
                        'options' => gf_get_search_type(),
                    ),
                    array(
                        'id'       => 'header_customize_nav_text',
                        'type'     => 'ace_editor',
                        'mode'     => 'html',
                        'theme'    => 'monokai',
                        'title'    => esc_html__('Custom Text Content', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Add Content for Custom Text', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                        'options'  => array('minLines' => 5, 'maxLines' => 60),
                    ),
                    array(
                        'id'      => 'header_customize_nav_spacing',
                        'type'    => 'slider',
                        'title'   => esc_html__('Navigation Item Spacing (px)', 'g5plus-arvo'),
                        'default' => '20',
                        'min'     => 0,
                        'step'    => 1,
                        'max'     => 100,
                    ),
	                array(
		                'id'       => 'header_customize_nav_css_class',
		                'type'     => 'text',
		                'title'    => esc_html__('Custom CSS Class', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Add custom css class', 'g5plus-arvo'),
		                'default'  => '',
	                ),
                )
            ));

            /**
             * Footer Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Footer', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-website',
                'fields' => array(
                    array(
                        'id'       => 'footer_parallax',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Footer Parallax', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable Footer Parallax', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array('1' => 'Enable', '0' => 'Disable'),
                        'default'  => '0'
                    ),
	                array(
		                'id'       => 'footer_bg_image',
		                'type'     => 'media',
		                'url'      => true,
		                'title'    => esc_html__('Background image', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Upload footer background image here', 'g5plus-arvo'),
		                'desc'     => '',
		                'default'  => '',
	                ),

	                array(
		                'id'       => 'footer_bg_image_apply_for',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Footer Image apply for', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Select region apply for footer image', 'g5plus-arvo'),
		                'desc'     => '',
		                'options'  => array(
			                'footer.main-footer-wrapper'            => esc_html__('Footer Wrapper', 'g5plus-arvo'),
			                'footer .main-footer' => esc_html__('Main Footer', 'g5plus-arvo'),
		                ),
		                'default'  => 'footer.main-footer-wrapper',
		                'required' => array('footer_bg_image', '!=', ''),
	                ),
	                array(
		                'id'       => 'set_footer_custom',
		                'type'     => 'select',
		                'title'    => esc_html__('Custom Footer', 'g5plus-arvo'),
		                'desc'     => esc_html__('Select one to apply to the page footer', 'g5plus-arvo'),
		                'data'     => 'posts',
		                'default' => '',
		                'args' => array(
			                'post_type' => 'gf_footer',
			                'posts_per_page' => -1
		                ),
	                ),


	                //--------------------------------------------------------------------------------
                    array(
                        'id'     => 'section-footer-main-settings',
                        'type'   => 'section',
                        'title'  => esc_html__('Main Footer', 'g5plus-arvo'),
                        'indent' => true,
                        'required' => array('set_footer_custom', '=', ''),
                    ),

	                array(
		                'id'       => 'footer_show_hide',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Show/Hide Footer', 'g5plus-arvo'),
		                'options'  => gf_get_toggle(),
		                'default'  => '1'
	                ),

	                array(
		                'id'       => 'footer_container_layout',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Footer Container Layout', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Select Footer Container Layout', 'g5plus-arvo'),
		                'desc'     => '',
		                'options'  => gf_get_page_layout(),
		                'default'  => 'container',
		                'required' => array('footer_show_hide', '=', '1'),
	                ),
                    array(
                        'id'       => 'footer_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select the footer column layout.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'footer-1' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-1.jpg'),
                            'footer-2' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-2.jpg'),
                            'footer-3' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-3.jpg'),
                            'footer-4' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-4.jpg'),
                            'footer-5' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-5.jpg'),
                            'footer-6' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-6.jpg'),
                            'footer-7' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-7.jpg'),
                            'footer-8' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-8.jpg'),
                            'footer-9' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/footer-layout-9.jpg'),
                        ),
                        'default'  => 'footer-1',
	                    'required' => array('footer_show_hide', '=', '1'),
                    ),

                    array(
                        'id'       => 'footer_sidebar_1',
                        'type'     => 'select',
                        'title'    => esc_html__('Sidebar 1', 'g5plus-arvo'),
                        'subtitle' => "Choose the default footer sidebar 1",
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'footer-1',
	                    'select2'  => array('allowClear' => false),
	                    'required' => array('footer_show_hide', '=', '1'),
                    ),

                    array(
                        'id'       => 'footer_sidebar_2',
                        'type'     => 'select',
                        'title'    => esc_html__('Sidebar 2', 'g5plus-arvo'),
                        'subtitle' => "Choose the default footer sidebar 2",
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'footer-2',
	                    'select2'  => array('allowClear' => false),
                        'required' => array(
	                        array('footer_layout', '=', array('footer-1', 'footer-2', 'footer-3', 'footer-4', 'footer-5', 'footer-6', 'footer-7', 'footer-8')),
	                        array('footer_show_hide', '=', '1'),
                        )
                    ),

                    array(
                        'id'       => 'footer_sidebar_3',
                        'type'     => 'select',
                        'title'    => esc_html__('Sidebar 3', 'g5plus-arvo'),
                        'subtitle' => "Choose the default footer sidebar 3",
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'footer-3',
	                    'select2'  => array('allowClear' => false),
                        'required' => array(
	                        array('footer_layout', '=', array('footer-1', 'footer-2', 'footer-3', 'footer-5', 'footer-8')),
	                        array('footer_show_hide', '=', '1'),
                        )
                    ),

                    array(
                        'id'       => 'footer_sidebar_4',
                        'type'     => 'select',
                        'title'    => esc_html__('Sidebar 4', 'g5plus-arvo'),
                        'subtitle' => "Choose the default footer sidebar 4",
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'footer-4',
	                    'select2'  => array('allowClear' => false),
                        'required' => array(
	                        array('footer_layout', '=', array('footer-1')),
	                        array('footer_show_hide', '=', '1'),
                        )
                    ),

                    array(
                        'id'       => 'collapse_footer',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Collapse footer on mobile device', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Enable collapse footer', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => '0',
	                    'required' => array('footer_show_hide', '=', '1'),
                    ),
                    array(
                        'id'             => 'footer_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Footer Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px)', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default footer top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '90px',
                            'padding-bottom' => '90px',
                            'units'          => 'px',
                        ),
	                    'required' => array('footer_show_hide', '=', '1'),
                    ),

                    //--------------------------------------------------------------------------------
                    array(
                        'id'     => 'section-footer-bottom-settings',
                        'type'   => 'section',
                        'title'  => esc_html__('Bottom Bar Settings', 'g5plus-arvo'),
                        'indent' => true,
                        'required' => array('set_footer_custom', '=', ''),
                    ),
	                array(
		                'id'       => 'bottom_bar_visible',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Show/Hide Bottom Bar', 'g5plus-arvo'),
		                'options'  => gf_get_toggle(),
		                'default'  => '1'
	                ),
	                array(
		                'id'       => 'bottom_bar_container_layout',
		                'type'     => 'button_set',
		                'title'    => esc_html__('Bottom bar Container Layout', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Select Bottom bar Container Layout', 'g5plus-arvo'),
		                'desc'     => '',
		                'options'  => gf_get_page_layout(),
		                'default'  => 'container',
		                'required' => array('bottom_bar_visible', '=', '1'),
	                ),
                    array(
                        'id'       => 'bottom_bar_layout',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Bottom bar Layout', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Select the bottom bar column layout.', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'bottom-bar-1' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/bottom-bar-layout-1.jpg'),
                            'bottom-bar-2' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/bottom-bar-layout-2.jpg'),
                            'bottom-bar-3' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/bottom-bar-layout-3.jpg'),
                            'bottom-bar-4' => array('title' => '', 'img' => GF_PLUGIN_URL . 'assets/images/theme-options/bottom-bar-layout-4.jpg'),
                        ),
                        'default'  => 'bottom-bar-1',
	                    'required' => array('bottom_bar_visible', '=', '1'),
                    ),

                    array(
                        'id'       => 'bottom_bar_left_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Bottom Left Sidebar', 'g5plus-arvo'),
                        'subtitle' => "Choose the default bottom left sidebar",
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'bottom_bar_left',
	                    'select2'  => array('allowClear' => false),
	                    'required' => array('bottom_bar_visible', '=', '1'),
                    ),
                    array(
                        'id'       => 'bottom_bar_right_sidebar',
                        'type'     => 'select',
                        'title'    => esc_html__('Bottom Right Sidebar', 'g5plus-arvo'),
                        'subtitle' => "Choose the default bottom right sidebar",
                        'data'     => 'sidebars',
                        'desc'     => '',
                        'default'  => 'bottom_bar_right',
	                    'select2'  => array('allowClear' => false),
                        'required' => array(
	                        array('bottom_bar_layout', '!=', 'bottom-bar-4'),
	                        array('bottom_bar_visible', '=', '1')
                        ),
                    ),
                    array(
                        'id'             => 'bottom_bar_padding',
                        'type'           => 'spacing',
                        'mode'           => 'padding',
                        'units'          => 'px',
                        'units_extended' => 'false',
                        'title'          => esc_html__('Bottom Bar Top/Bottom Padding', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('This must be numeric (no px). Leave balnk for default.', 'g5plus-arvo'),
                        'desc'           => esc_html__('If you would like to override the default bottom bar top/bottom padding, then you can do so here.', 'g5plus-arvo'),
                        'left'           => false,
                        'right'          => false,
                        'default'        => array(
                            'padding-top'    => '23px',
                            'padding-bottom' => '23px',
                            'units'          => 'px',
                        ),
                    ),
                    array(
                        'id'      => 'bottom_bar_border_top',
                        'type'    => 'button_set',
                        'title'   => esc_html__('Bottom bar border top', 'g5plus-arvo'),
                        'options' => array(
                            'none'             => esc_html__('None', 'g5plus-arvo'),
                            'full-border'      => esc_html__('Full Border', 'g5plus-arvo'),
                            'container-border' => esc_html__('Container Border', 'g5plus-arvo'),
                        ),
                        'default' => 'none',
	                    'required' => array('bottom_bar_visible', '=', '1'),
                    ),
                )
            ));

            /**
             * Theme Color Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Theme Colors', 'g5plus-arvo'),
                'desc'   => esc_html__('If you change value in this section, you must "Save & Generate CSS"', 'g5plus-arvo'),
                'icon'   => 'el el-magic',
                'fields' => array(
                    array(
                        'id'     => 'section-theme-color-general',
                        'type'   => 'section',
                        'title'  => esc_html__('General', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'accent_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Accent Color', 'g5plus-arvo'),
                        'default'  => '#3AB8BD',
                        'validate' => 'color',
                        'colors'   => array('#3AB8BD', '#BDDCDA', '#FEC735', '#EA7B45', '#EA4336', '#4285F4', '#2F659A', '#C60325', '#35BFEE', '#FFC40E', '#6EA9AD')
                    ),
                    array(
                        'id'       => 'foreground_accent_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Foregound Accent Color', 'g5plus-arvo'),
                        'default'  => '#fff',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Text Color', 'g5plus-arvo'),
                        'default'  => '#858585',
                        'validate' => 'color',
                    ),
	                array(
		                'id'       => 'text_color_bold',
		                'type'     => 'color',
		                'title'    => esc_html__('Text Color Bolder', 'g5plus-arvo'),
		                'default'  => '#333',
		                'validate' => 'color',
	                ),
                    array(
                        'id'       => 'border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Border Color', 'g5plus-arvo'),
                        'default'  => '#eee',
                        'validate' => 'color',
                    ),

                    //--------------------------------------------------------------------
                    array(
                        'id'     => 'section-theme-color-top-drawer',
                        'type'   => 'section',
                        'title'  => esc_html__('Top Drawer', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'top_drawer_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Top drawer background color', 'g5plus-arvo'),
                        'default'  => '#2f2f2f',
                        'validate' => 'color',
                    ),

                    array(
                        'id'       => 'top_drawer_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Top drawer text color', 'g5plus-arvo'),
                        'default'  => '#c5c5c5',
                        'validate' => 'color',
                    ),

	                //--------------------------------------------------------------------
	                array(
		                'id'     => 'section-theme-color-top-bar',
		                'type'   => 'section',
		                'title'  => esc_html__('Top Bar', 'g5plus-arvo'),
		                'indent' => true
	                ),
	                array(
		                'id'       => 'top_bar_bg_color',
		                'type'     => 'color',
		                'title'    => esc_html__('Top bar background color', 'g5plus-arvo'),
		                'default'  => '#222',
		                'validate' => 'color',
	                ),
	                array(
		                'id'       => 'top_bar_overlay',
		                'type'     => 'slider',
		                'title'    => esc_html__('Top bar overlay', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Set the opacity level of the overlay.', 'g5plus-arvo'),
		                'default'  => '0',
		                'min'      => 0,
		                'step'     => 1,
		                'max'      => 100,
		                'required' => array(
			                array('header_float', '=', 1),
		                ),
	                ),
	                array(
		                'id'       => 'top_bar_text_color',
		                'type'     => 'color',
		                'title'    => esc_html__('Top bar text color', 'g5plus-arvo'),
		                'default'  => '#fff',
		                'validate' => 'color',
	                ),
	                array(
		                'id'       => 'top_bar_border_color',
		                'type'     => 'color',
		                'title'    => esc_html__('Top bar border color', 'g5plus-arvo'),
		                'default'  => '#333',
		                'validate' => 'color',
	                ),

                    //--------------------------------------------------------------------
                    array(
                        'id'     => 'section-theme-color-header-color',
                        'type'   => 'section',
                        'title'  => esc_html__('Header', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'header_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header background color', 'g5plus-arvo'),
                        'default'  => '#fff',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'header_overlay',
                        'type'     => 'slider',
                        'title'    => esc_html__('Header Overlay', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set the opacity level of the overlay', 'g5plus-arvo'),
                        'default'  => '0',
                        'min'      => 0,
                        'step'     => 1,
                        'max'      => 100,
                        'required' => array(
                            array('header_float', '=', 1),
                        ),
                    ),
                    array(
                        'id'       => 'header_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header text color', 'g5plus-arvo'),
                        'default'  => '#212121',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'header_border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header border color', 'g5plus-arvo'),
                        'default'  => '#eee',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'header_above_border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header above border color', 'g5plus-arvo'),
                        'default'  => '#eee',
                        'validate' => 'color',
	                    'required' => array(
		                    array('header_layout', '=', 'header-3'),
	                    ),
                    ),



                    //--------------------------------------------------------------------
                    array(
                        'id'     => 'section-theme-color-navigation-color',
                        'type'   => 'section',
                        'title'  => esc_html__('Navigation', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'navigation_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Navigation background color', 'g5plus-arvo'),
                        'default'  => '#fff',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'navigation_overlay',
                        'type'     => 'slider',
                        'title'    => esc_html__('Navigation overlay', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Set the opacity level of the overlay.', 'g5plus-arvo'),
                        'default'  => '0',
                        'min'      => 0,
                        'step'     => 1,
                        'max'      => 100,
                        'required' => array(
                            array('header_float', '=', 1),
                        )
                    ),
                    array(
                        'id'       => 'navigation_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Navigation text color', 'g5plus-arvo'),
                        'default'  => '#858585',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'navigation_text_color_hover',
                        'type'     => 'color',
                        'title'    => esc_html__('Navigation text hover color', 'g5plus-arvo'),
                        'default'  => '#3AB8BD',
                        'validate' => 'color',
                    ),
	                array(
		                'id'       => 'navigation_customize_text_color',
		                'type'     => 'color',
		                'title'    => esc_html__('Navigation Customize text color', 'g5plus-arvo'),
		                'default'  => '#BBB',
		                'validate' => 'color',
	                ),

                    //--------------------------------------------------------------------
                    array(
                        'id'     => 'section-theme-color-header-mobile',
                        'type'   => 'section',
                        'title'  => esc_html__('Header Mobile Color', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'top_bar_mobile_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Top bar background color', 'g5plus-arvo'),
                        'default'  => '#fff',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'top_bar_mobile_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Top bar text color', 'g5plus-arvo'),
                        'default'  => '#444',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'top_bar_mobile_border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Top bar border bottom color', 'g5plus-arvo'),
                        'default'  => '#eee',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'header_mobile_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header background color', 'g5plus-arvo'),
                        'default'  => '#fff',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'header_mobile_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header text color', 'g5plus-arvo'),
                        'default'  => '#444',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'header_mobile_border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Header border bottom color', 'g5plus-arvo'),
                        'default'  => '#eee',
                        'validate' => 'color',
                    ),

                    //--------------------------------------------------------------------
                    array(
                        'id'     => 'section-theme-color-footer-color',
                        'type'   => 'section',
                        'title'  => esc_html__('Footer', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'footer_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Footer background color', 'g5plus-arvo'),
                        'default'  => '#222',
                        'validate' => 'color',
                    ),
	                array(
		                'id'       => 'footer_bg_overlay',
		                'type'     => 'slider',
		                'title'    => esc_html__('Footer background overlay', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Set the opacity level of the overlay.', 'g5plus-arvo'),
		                'default'  => '100',
		                'min'      => 0,
		                'step'     => 1,
		                'max'      => 100,
	                ),
                    array(
                        'id'       => 'footer_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Footer text color', 'g5plus-arvo'),
                        'default'  => '#858585',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'footer_widget_title_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Footer widget title color', 'g5plus-arvo'),
                        'default'  => '#fff',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'footer_border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Footer border color', 'g5plus-arvo'),
                        'default'  => '#373737',
                        'validate' => 'color',
                    ),

                    array(
                        'id'     => 'section-theme-color-bottom-bar-color',
                        'type'   => 'section',
                        'title'  => esc_html__('Bottom Bar', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'       => 'bottom_bar_bg_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Bottom bar background color', 'g5plus-arvo'),
                        'default'  => '#333',
                        'validate' => 'color',
                    ),
	                array(
		                'id'       => 'bottom_bar_overlay',
		                'type'     => 'slider',
		                'title'    => esc_html__('Bottom bar overlay', 'g5plus-arvo'),
		                'subtitle' => esc_html__('Set the opacity level of the overlay.', 'g5plus-arvo'),
		                'default'  => '100',
		                'min'      => 0,
		                'step'     => 1,
		                'max'      => 100,
	                ),
                    array(
                        'id'       => 'bottom_bar_text_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Bottom bar text color', 'g5plus-arvo'),
                        'default'  => '#777',
                        'validate' => 'color',
                    ),
                    array(
                        'id'       => 'bottom_bar_border_color',
                        'type'     => 'color',
                        'title'    => esc_html__('Bottom bar border color', 'g5plus-arvo'),
                        'default'  => '#454545',
                        'validate' => 'color',
                    ),
                )
            ));

            /**
             * Custom font Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Custom font', 'g5plus-arvo'),
                'desc'   => wp_kses_post(__('<span style="color:red"><strong>After upload font file, please click  "Save changes" and refresh page before go to Font Options</strong></span>', 'g5plus-arvo')),
                'icon'   => 'el el-text-width',
                'fields' => array(
                    array(
                        'id'     => 'section_custom_font_1',
                        'type'   => 'section',
                        'title'  => esc_html__('Custom Font 1', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'      => 'custom_font_1_name',
                        'type'    => 'text',
                        'title'   => esc_html__('Custom font Name 1', 'g5plus-arvo'),
                        'desc'    => '',
                        'default' => ''
                    ),
                    array(
                        'id'             => 'custom_font_1_eot',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 1 (.eot)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .eot here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('eot')
                    ),
                    array(
                        'id'             => 'custom_font_1_ttf',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 1 (.ttf)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .ttf here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('ttf')
                    ),
                    array(
                        'id'             => 'custom_font_1_woff',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 1 (.woff)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .woff here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('woff')
                    ),
                    array(
                        'id'             => 'custom_font_1_svg',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 1 (.svg)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .svg here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('svg')
                    ),
                    array(
                        'id'     => 'section_custom_font_2',
                        'type'   => 'section',
                        'title'  => esc_html__('Custom Font 2', 'g5plus-arvo'),
                        'indent' => true
                    ),
                    array(
                        'id'      => 'custom_font_2_name',
                        'type'    => 'text',
                        'title'   => esc_html__('Custom font Name 2', 'g5plus-arvo'),
                        'desc'    => '',
                        'default' => ''
                    ),
                    array(
                        'id'             => 'custom_font_2_eot',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 2 (.eot)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .eot here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('eot')
                    ),
                    array(
                        'id'             => 'custom_font_2_ttf',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 2 (.ttf)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .ttf here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('ttf')
                    ),
                    array(
                        'id'             => 'custom_font_2_woff',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 2 (.woff)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .woff here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('woff')
                    ),
                    array(
                        'id'             => 'custom_font_2_svg',
                        'type'           => 'upload',
                        'url'            => true,
                        'title'          => esc_html__('Custom font 2 (.svg)', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Upload your font .svg here.', 'g5plus-arvo'),
                        'desc'           => '',
                        'library_filter' => array('svg')
                    ),
                )
            ));

            /**
             * Font Options Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'icon'   => 'el el-font',
                'title'  => esc_html__('Font Options', 'g5plus-arvo'),
                'desc'   => esc_html__('If you change value in this section, you must "Save & Generate CSS"', 'g5plus-arvo'),
                'fields' => array(
                    array(
                        'id'             => 'body_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('Body Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the body font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'text-align'     => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'line-height'    => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('body'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('body'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '15px',
                            'font-family' => 'Lato',
                            'font-weight' => '400',
                            'google'      => true
                        ),
                    ),
                    array(
                        'id'          => 'secondary_font',
                        'type'        => 'typography',
                        'title'       => esc_html__('Secondary Font', 'g5plus-arvo'),
                        'subtitle'    => esc_html__('Specify the Secondary font properties.', 'g5plus-arvo'),
                        'google'      => true,
                        'fonts'       => $fonts,
                        'line-height' => false,
                        'all_styles'  => true, // Enable all Google Font style/weight variations to be added to the page
                        'color'       => false,
                        'text-align'  => false,
                        'font-style'  => false,
                        'subsets'     => false,
                        'font-size'   => false,
                        'font-weight' => false,
                        'output'      => array(''), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'    => array(''), // An array of CSS selectors to apply this font style to dynamically
                        'units'       => 'px', // Defaults to px
                        'default'     => array(
                            'font-family' => 'Montserrat',
                        ),
                    ),

                    array(
                        'id'             => 'h1_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('H1 Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the H1 font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'text-align'     => false,
                        'line-height'    => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('h1'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('h1'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '48px',
                            'font-family' => 'Montserrat',
                            'font-weight' => '700',
                        ),
                    ),
                    array(
                        'id'             => 'h2_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('H2 Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the H2 font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'line-height'    => false,
                        'text-align'     => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('h2'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('h2'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '36px',
                            'font-family' => 'Montserrat',
                            'font-weight' => '700',
                        ),
                    ),
                    array(
                        'id'             => 'h3_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('H3 Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the H3 font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'text-align'     => false,
                        'line-height'    => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('h3'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('h3'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '24px',
                            'font-family' => 'Montserrat',
                            'font-weight' => '700',
                        ),
                    ),
                    array(
                        'id'             => 'h4_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('H4 Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the H4 font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'text-align'     => false,
                        'line-height'    => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('h4'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('h4'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '20px',
                            'font-family' => 'Montserrat',
                            'font-weight' => '700',
                        ),
                    ),
                    array(
                        'id'             => 'h5_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('H5 Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the H5 font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'line-height'    => false,
                        'text-align'     => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('h5'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('h5'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '18px',
                            'font-family' => 'Montserrat',
                            'font-weight' => '700',
                        ),
                    ),
                    array(
                        'id'             => 'h6_font',
                        'type'           => 'typography',
                        'title'          => esc_html__('H6 Font', 'g5plus-arvo'),
                        'subtitle'       => esc_html__('Specify the H6 font properties.', 'g5plus-arvo'),
                        'google'         => true,
                        'fonts'          => $fonts,
                        'line-height'    => false,
                        'text-align'     => false,
                        'color'          => false,
                        'letter-spacing' => false,
                        'all_styles'     => true, // Enable all Google Font style/weight variations to be added to the page
                        'output'         => array('h6'), // An array of CSS selectors to apply this font style to dynamically
                        'compiler'       => array('h6'), // An array of CSS selectors to apply this font style to dynamically
                        'units'          => 'px', // Defaults to px
                        'default'        => array(
                            'font-size'   => '15px',
                            'font-family' => 'Montserrat',
                            'font-weight' => '700',
                        ),
                    ),
                ),
            ));

            /**
             * Social Profiles Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Social Profiles', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-path',
                'fields' => array_merge(gf_get_social_profiles(), array(
                    array(
                        'id'   => 'social-profile-divide-0',
                        'type' => 'divide'
                    ),
                    array(
                        'title'    => esc_html__('Social Share', 'g5plus-arvo'),
                        'id'       => 'social_sharing',
                        'type'     => 'checkbox',
                        'subtitle' => esc_html__('Show the social sharing in single blog and single product', 'g5plus-arvo'),

                        //Must provide key => value pairs for multi checkbox options
                        'options'  => array(
                            'facebook'  => 'Facebook',
                            'twitter'   => 'Twitter',
                            'google'    => 'Google',
                            'linkedin'  => 'Linkedin',
                            'tumblr'    => 'Tumblr',
                            'pinterest' => 'Pinterest'
                        ),

                        //See how default has changed? you also don't need to specify opts that are 0.
                        'default'  => array(
                            'facebook'  => '1',
                            'twitter'   => '1',
                            'google'    => '1',
                            'linkedin'  => '1',
                            'tumblr'    => '1',
                            'pinterest' => '1'
                        )
                    )
                ))
            ));

            /**
             * Woocommerce Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Woocommerce', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-shopping-cart',
                'fields' => array(

                    $this->get_section_start('woocommerce_general', esc_html__('General', 'g5plus-arvo')),
                    array(
                        'id'       => 'product_featured_label_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show Featured Label', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

                    array(
                        'id'       => 'product_featured_label_text',
                        'type'     => 'text',
                        'title'    => esc_html__('Featured Label Text', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'default'  => 'Hot',
                        'required' => array('product_featured_label_enable', '=', 1),
                    ),

                    array(
                        'id'       => 'product_sale_label_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show Sale Label', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

                    array(
                        'id'       => 'product_sale_flash_mode',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Sale Flash Mode', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => array(
                            'text'    => esc_html__('Text', 'g5plus-arvo'),
                            'percent' => esc_html__('Percent', 'g5plus-arvo')
                        ),
                        'default'  => 'text',
                        'required' => array('product_sale_label_enable', '=', 1),
                    ),

                    array(
                        'id'       => 'product_sale_label_text',
                        'type'     => 'text',
                        'title'    => esc_html__('Sale Label Text', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'default'  => 'Sale',
                        'required' => array(
                            array('product_featured_label_enable', '=', 1),
                            array('product_sale_flash_mode', '=', 'text'),
                        )
                    ),


                    array(
                        'id'       => 'product_sale_count_down_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show Sale Count Down', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 0
                    ),


                    array(
                        'id'       => 'product_add_to_cart_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show Add To Cart Button', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),
                    $this->get_section_start('archive_product', esc_html__('Shop and Category Page', 'g5plus-arvo')),

                    array(
                        'id'       => 'product_display_columns',
                        'type'     => 'select',
                        'title'    => esc_html__('Product Display Columns', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the number of columns to display on shop/category pages.', 'g5plus-arvo'),
                        'options'  => array(
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5'
                        ),
                        'desc'     => '',
                        'default'  => '3',
                        'select2'  => array('allowClear' => false),
                    ),

                    array(
                        'id'          => 'product_per_page',
                        'type'        => 'text',
                        'title'       => esc_html__('Products Per Page', 'g5plus-arvo'),
                        'compiler'    => true,
                        'validate'    => 'numeric',
                        'description' => '',
                        'default'     => '9'
                    ),

                    array(
                        'id'       => 'product_image_hover_effect',
                        'type'     => 'select',
                        'title'    => esc_html__('Product Image Hover Effect', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_product_image_hover_effect(),
                        'default'  => 'change-image'
                    ),

                    array(
                        'id'       => 'product_rating_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show Rating', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

                    array(
                        'id'       => 'product_quick_view_enable',
                        'type'     => 'button_set',
                        'title'    => esc_html__('Show Quick View', 'g5plus-arvo'),
                        'subtitle' => '',
                        'desc'     => '',
                        'options'  => gf_get_toggle(),
                        'default'  => 1
                    ),

                    $this->get_section_start('single_product', esc_html__('Single Product', 'g5plus-arvo')),
                    array(
                        'id'       => 'related_product_count',
                        'type'     => 'text',
                        'title'    => esc_html__('Related Product Total Record', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Total Record Of Related Product.', 'g5plus-arvo'),
                        'validate' => 'number',
                        'default'  => '6',
                    ),

                    array(
                        'id'       => 'related_product_display_columns',
                        'type'     => 'select',
                        'title'    => esc_html__('Related Product Display Columns', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the number of columns to display on related product.', 'g5plus-arvo'),
                        'options'  => array(
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                        ),
                        'desc'     => '',
                        'default'  => '3'
                    ),

                    array(
                        'id'      => 'related_product_condition',
                        'type'    => 'checkbox',
                        'title'   => esc_html__('Related Product Condition', 'g5plus-arvo'),
                        'options' => array(
                            'category' => esc_html__('Same Category', 'g5plus-arvo'),
                            'tag'      => esc_html__('Same Tag', 'g5plus-arvo'),
                        ),
                        'default' => array(
                            'category' => '1',
                            'tag'      => '1',
                        ),
                    ),

                    array(
                        'id'       => 'up_sells_product_display_columns',
                        'type'     => 'select',
                        'title'    => esc_html__('Up Sells Product Display Columns', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the number of columns to display on up sells product.', 'g5plus-arvo'),
                        'options'  => array(
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                        ),
                        'desc'     => '',
                        'default'  => '4'
                    ),
                    $this->get_section_start('cart_pages', esc_html__('Cart Page', 'g5plus-arvo')),
                    array(
                        'id'       => 'cross_sells_product_count',
                        'type'     => 'text',
                        'title'    => esc_html__('Cross Sells Product Total Record', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Total Record Of Cross Sells Product.', 'g5plus-arvo'),
                        'validate' => 'number',
                        'default'  => '6',
                    ),

                    array(
                        'id'       => 'cross_sells_product_display_columns',
                        'type'     => 'select',
                        'title'    => esc_html__('Cross Sells Product Display Columns', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Choose the number of columns to display on cross sells product.', 'g5plus-arvo'),
                        'options'  => array(
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                        ),
                        'desc'     => '',
                        'default'  => '4'
                    ),

                )
            ));

            /**
             * Custom Post Type Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Custom Post Type', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-screenshot',
                'fields' => array(
                    array(
                        'id'       => 'cpt_disable',
                        'type'     => 'checkbox',
                        'title'    => esc_html__('Disable Custom Post Types', 'g5plus-arvo'),
                        'subtitle' => esc_html__('You can disable the custom post types used within the theme here, by checking the corresponding box. NOTE: If you do not want to disable any, then make sure none of the boxes are checked.', 'g5plus-arvo'),
                        'options'  => array(
                            'portfolio' => 'Portfolio'
                        ),
                        'default'  => array(
                            'portfolio' => '0'
                        )
                    ),
                )
            ));

            /**
             * Portfolio Post Type
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__( 'Portfolio', 'g5plus-arvo' ),
                'desc'   => '',
                'icon'   => 'el el-th-large',
                'subsection' => true,
                'fields' => array(
                    $this->get_section_start('archive_portfolio',esc_html__('Archive Page','g5plus-arvo')),
                    array(
                        'id' => 'custom_portfolio_layout_style',
                        'type'     => 'select',
                        'title' => esc_html__('Layout Style', 'g5plus-arvo'),
                        'default'  => 'portfolio-grid',
                        'options'  => array(
                            'portfolio-grid'    => esc_html__('Grid', 'g5plus-arvo'),
                            'portfolio-masonry-normal' => esc_html__('Masonry Normal', 'g5plus-arvo'),
                            'portfolio-masonry-metro-style1' => esc_html__('Masonry Metro Style 1', 'g5plus-arvo'),
                            'portfolio-masonry-metro-style2' => esc_html__('Masonry Metro Style 2', 'g5plus-arvo'),
                            'portfolio-masonry-metro-style3' => esc_html__('Masonry Metro Style 3', 'g5plus-arvo'),
                            'portfolio-masonry-metro-style4' => esc_html__('Masonry Metro Style 4', 'g5plus-arvo')
                        )
                    ),
                    array(
                        'type'     => 'select',
                        'title'    => esc_html__('Hover Effect', 'g5plus-arvo'),
                        'id'       => 'custom_portfolio_hover_effect',
                        'default'  => 'default-effect',
                        'options'  => array(
                            'default-effect' => esc_html__('Default', 'g5plus-arvo'),
                            'layla-effect'   => esc_html__('Layla', 'g5plus-arvo'),
                            'bubba-effect'   => esc_html__('Bubba', 'g5plus-arvo'),
                            'jazz-effect'    => esc_html__('Jazz', 'g5plus-arvo'),
                        )
                    ),
                    array(
                        'id'      => 'custom_portfolio_filter',
                        'type'    => 'button_set',
                        'title' => esc_html__('Show Category Filter', 'g5plus-arvo'),
                        'default' => 1,
                        'options' => gf_get_toggle(),
                        'required' => array('custom_portfolio_layout_style', '!=', 'portfolio-masonry-metro-style4')
                    ),
                    array(
                        'type'       => 'button_set',
                        'title'    => esc_html__('Active Category Color', 'g5plus-arvo'),
                        'id' => 'custom_portfolio_active_cate_color',
                        'default' => 'active-cate-accent',
                        'options' => array(
                            'active-cate-accent'=> esc_html__( 'Primary', 'g5plus-arvo' ),
                            'active-cate-dark'=> esc_html__( 'Dark', 'g5plus-arvo'  )
                        ),
                        'required' => array('custom_portfolio_filter', '=', 1)
                    ),
                    array(
                        'id' => 'custom_portfolio_paging',
                        'type' => 'button_set',
                        'title' => esc_html__('Page Paging', 'g5plus-arvo'),
                        'default' => '',
                        'options'  => array(
                            '' => esc_html__('Show All', 'g5plus-arvo'),
                            'load-more' => esc_html__('Load More', 'g5plus-arvo')
                        ),
                        'required' => array('custom_portfolio_layout_style', '!=', 'portfolio-masonry-metro-style4')
                    ),
                    array(
                        'id' => 'custom_portfolio_item_amount',
                        'type' => 'text',
                        'title' => esc_html__('Items Amount', 'g5plus-arvo'),
                        'default' => 8,
                        'subtitle' => esc_html__('Enter Items Amount or Items Per Page If Load More', 'g5plus-arvo'),
                    ),
                    array(
                        'id' => 'custom_portfolio_column',
                        'type' => 'select',
                        'select2' => array('allowClear' => false),
                        'title' => esc_html__('Columns', 'g5plus-arvo'),
                        'default' => '4',
                        'options'  => array( '2' => '2' , '3' => '3','4' => '4', '5'=> '5' ),
                        'required' =>array('custom_portfolio_layout_style', '=', array('portfolio-grid', 'portfolio-masonry-normal'))
                    ),
                    array(
                        'type'    => 'select',
                        'title'   => esc_html__('Image Size', 'g5plus-arvo'),
                        'id'      => 'custom_portfolio_image_size',
                        'options' => array(
                            'portfolio-size-xs' => '370x370',
                            'portfolio-size-sm' => '480x480',
                            'portfolio-size-md' => '585x585',
                            'portfolio-size-lg' => '640x427',
                            'portfolio-size-xl' => '640x480',
                            'portfolio-size-xxl' => '640x640'
                        ),
                        'default' => 'portfolio-size-xxl',
                        'required' => array('custom_portfolio_layout_style', '=', array('portfolio-grid'))
                    ),
                    array(
                        'type' => 'select',
                        'title' => esc_html__('Columns Gap', 'g5plus-arvo'),
                        'id' => 'custom_portfolio_columns_gap',
                        'options' => array(
                            'col-gap-30' => '30px',
                            'col-gap-10' => '10px',
                            'col-gap-0' => '0px'
                        ),
                        'default' => 'col-gap-0',
                        'required' => array('custom_portfolio_layout_style', '=', array('portfolio-masonry-normal', 'portfolio-grid'))
                    ),
                    $this->get_section_start('single_portfolio', esc_html__('Single Page', 'g5plus-arvo')),
                    array(
                        'id'       => 'portfolio-single-style',
                        'type'     => 'image_select',
                        'title'    => esc_html__('Single Portfolio Layout', 'g5plus-arvo'),
                        'desc'     => '',
                        'options'  => array(
                            'single-image' => array(
                                'title' => esc_html__('Single Image', 'g5plus-arvo'),
                                'img'   => GF_PLUGIN_URL . 'assets/images/theme-options/portfolio-single-image.jpg'
                            ),
                            'horizontal-slider' => array(
                                'title' => esc_html__('Horizontal Slider', 'g5plus-arvo'),
                                'img'   => GF_PLUGIN_URL . 'assets/images/theme-options/portfolio-horizontal-slider.jpg'
                            ),
                            'video-layout' => array(
                                'title' => esc_html__('Video Layout', 'g5plus-arvo'),
                                'img'   => GF_PLUGIN_URL . 'assets/images/theme-options/portfolio-video-layout.jpg'
                            ),
                            'two-columns' => array(
                                'title' => esc_html__('Two Columns', 'g5plus-arvo'),
                                'img'   => GF_PLUGIN_URL . 'assets/images/theme-options/portfolio-two-columns.jpg'
                            ),
                        ),
                        'default'  => 'single-image'
                    ),
                    array(
                        'id'      => 'show_portfolio_related',
                        'type'    => 'button_set',
                        'title'   => esc_html__('Show Portfolio Related?', 'g5plus-arvo'),
                        'default' => 1,
                        'options' => gf_get_toggle(),
                        'desc'    => '',
                    ),
                    array(
                        'id'       => 'portfolio_related_column',
                        'type'     => 'select',
                        'select2' => array('allowClear' => false),
                        'title'    => esc_html__('Portfolio Related Columns', 'g5plus-arvo'),
                        'default'  => 4,
                        'options'  => array(
                            '2' => '2',
                            '3' => '3',
                            '4' => '4'
                        ),
                        'required' => array('show_portfolio_related', '=', 1)
                    ),
                    array(
                        'type'    => 'select',
                        'title'   => esc_html__('Portfolio Related Image Size', 'g5plus-arvo'),
                        'id'      => 'custom_portfolio_related_image_size',
                        'options' => array(
                            'portfolio-size-xs' => '370x370',
                            'portfolio-size-sm' => '480x480',
                            'portfolio-size-md' => '585x585',
                            'portfolio-size-lg' => '640x427',
                            'portfolio-size-xl' => '640x480',
                            'portfolio-size-xxl' => '640x640'
                        ),
                        'default' => 'portfolio-size-xxl'
                    ),
                    array(
                        'type' => 'select',
                        'title' => esc_html__('Portfolio Related Columns Gap', 'g5plus-arvo'),
                        'id' => 'custom_portfolio_related_columns_gap',
                        'options' => array(
                            'col-gap-30' => '30px',
                            'col-gap-10' => '10px',
                            'col-gap-0' => '0px'
                        ),
                        'default' => 'col-gap-0'
                    ),
                )
            ));

            /**
             * Resources Options Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Resources Options', 'g5plus-arvo'),
                'desc'   => '',
                'icon'   => 'el el-th-large',
                'fields' => array(
                    array(
                        'id'       => 'cdn_bootstrap_js',
                        'type'     => 'text',
                        'title'    => esc_html__('CDN Bootstrap Script', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Url CDN Bootstrap Script', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                    ),

                    array(
                        'id'       => 'cdn_bootstrap_css',
                        'type'     => 'text',
                        'title'    => esc_html__('CDN Bootstrap Stylesheet', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Url CDN Bootstrap Stylesheet', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                    ),

                    array(
                        'id'       => 'cdn_font_awesome',
                        'type'     => 'text',
                        'title'    => esc_html__('CDN Font Awesome', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Url CDN Font Awesome', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                    ),

                )
            ));

            /**
             * Custom CSS & Script Section
             * *******************************************************
             */
            Redux::setSection($opt_name, array(
                'title'  => esc_html__('Custom CSS & Script', 'g5plus-arvo'),
                'desc'   => esc_html__('If you change Custom CSS, you must "Save & Generate CSS"', 'g5plus-arvo'),
                'icon'   => 'el el-edit',
                'fields' => array(
                    array(
                        'id'       => 'custom_css',
                        'type'     => 'ace_editor',
                        'mode'     => 'css',
                        'theme'    => 'monokai',
                        'title'    => esc_html__('Custom CSS', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Add some CSS to your theme by adding it to this textarea. Please do not include any style tags.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                        'options'  => array('minLines' => 20, 'maxLines' => 60)
                    ),
                    array(
                        'id'       => 'custom_js',
                        'type'     => 'ace_editor',
                        'mode'     => 'javascript',
                        'theme'    => 'chrome',
                        'title'    => esc_html__('Custom JS', 'g5plus-arvo'),
                        'subtitle' => esc_html__('Add some custom JavaScript to your theme by adding it to this textarea. Please do not include any script tags.', 'g5plus-arvo'),
                        'desc'     => '',
                        'default'  => '',
                        'options'  => array('minLines' => 20, 'maxLines' => 60)
                    ),

                )
            ));

	        /**
	         * Preset Settings Section
	         * *******************************************************
	         */
	        Redux::setSection( $opt_name, array(
		        'title'  => esc_html__( 'Preset Settings', 'g5plus-arvo' ),
		        'desc'   => esc_html__( 'Choose Preset Setting apply for page type', 'g5plus-arvo' ),
		        'icon'   => 'el el-edit',
		        'fields' => array_merge(
			        array(
				        array(
					        'id' => 'blog_preset',
					        'type' => 'select',
					        'data' => 'posts',
					        'title' => esc_html__('Blog Preset', 'g5plus-arvo'),
					        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
					        'default' => '',
					        'args' => array(
						        'post_type' => 'gf_preset',
						        'posts_per_page' => -1
					        )
				        ),
				        array(
					        'id' => 'blog_single_preset',
					        'type' => 'select',
					        'data' => 'posts',
					        'title' => esc_html__('Blog Single Preset', 'g5plus-arvo'),
					        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
					        'default' => '',
					        'args' => array(
						        'post_type' => 'gf_preset',
						        'posts_per_page' => -1

					        )
				        ),
				        array(
					        'id' => 'divide_preset_setting_1',
					        'type' => 'divide',
				        ),
				        array(
					        'id' => 'product_preset',
					        'type' => 'select',
					        'data' => 'posts',
					        'title' => esc_html__('Product Preset', 'g5plus-arvo'),
					        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
					        'default' => '',
					        'args' => array(
						        'post_type' => 'gf_preset',
						        'posts_per_page' => -1
					        )
				        ),
				        array(
					        'id' => 'product_single_preset',
					        'type' => 'select',
					        'data' => 'posts',
					        'title' => esc_html__('Product Single Preset', 'g5plus-arvo'),
					        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
					        'default' => '',
					        'args' => array(
						        'post_type' => 'gf_preset',
						        'posts_per_page' => -1
					        )
				        ),
			        ),
					$post_type_preset_list,
			        array(
				        array(
					        'id' => 'divide_preset_setting_2',
					        'type' => 'divide',
				        ),
				        array(
					        'id' => 'page_404_preset',
					        'type' => 'select',
					        'data' => 'posts',
					        'title' => esc_html__('404 Page Preset', 'g5plus-arvo'),
					        'placeholder' => esc_html__('Select Preset...', 'g5plus-arvo'),
					        'default' => '',
					        'args' => array(
						        'post_type' => 'gf_preset',
						        'posts_per_page' => -1
					        )
				        ),
			        )
		        )
	        ));
        }
    }

    new GF_Options_Config();
}