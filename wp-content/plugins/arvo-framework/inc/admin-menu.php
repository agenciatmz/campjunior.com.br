<?php
if (!class_exists('GF_Admin_Menu')) {
    class GF_Admin_Menu
    {
        public function __construct()
        {
            add_action('admin_menu', array($this, 'theme_admin_menu'));
        }

        //==============================================================================
        // Framework Menu
        //==============================================================================
        public function theme_admin_menu()
        {
            /**
             * Add Root Menu (bellow Dashboard)
             */
            add_menu_page(
                esc_html__('G5Plus Dashboard', 'g5plus-arvo'),
                esc_html__('Welcome', 'g5plus-arvo'),
                'manage_options',
                'g5plus_welcome',
                array($this, 'menu_welcome_page_callback'),
                'dashicons-layout',
                2
            );

            add_submenu_page(
                'g5plus_welcome',
                esc_html__('Theme Options', 'g5plus-arvo'),
                esc_html__('Theme Options', 'g5plus-arvo'),
                'manage_options',
                'admin.php?page=_options'
            );

            add_submenu_page(
                'g5plus_welcome',
                esc_html__('Preset', 'g5plus-arvo'),
                esc_html__('Preset', 'g5plus-arvo'),
                'manage_options',
                'edit.php?post_type=gf_preset'
            );

            add_submenu_page(
                'g5plus_welcome',
                esc_html__('Custom Footer', 'g5plus-arvo'),
                esc_html__('Custom Footer', 'g5plus-arvo'),
                'manage_options',
                'edit.php?post_type=gf_footer'
            );
        }

        public function menu_welcome_page_callback()
        {
            gf_get_template('inc/templates/welcome');
        }
    }

    new GF_Admin_Menu();
}