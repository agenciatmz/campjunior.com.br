/**
 * Created by Administrator on 5/29/2015.
 */
(function($){
	"use strict";
	var ReduxAPP = {
		initialize: function() {
			$('#gf_choose_preset').on('click',function() {
				var presetId = $('#gf_select_preset').val();
				if (presetId != '') {
					window.location.href = window.location.href.replace(window.location.search, '') + '?page=_options&preset_id=' + presetId;
				}
				else {
					window.location.href = window.location.href.replace(window.location.search, '') + '?page=_options';
				}
			});
		}
	};
	$(document).ready(function(){
		ReduxAPP.initialize();
	});
})(jQuery);