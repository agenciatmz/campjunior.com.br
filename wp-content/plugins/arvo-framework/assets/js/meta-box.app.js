/**
 * Created by Administrator on 5/29/2015.
 */
(function($){
    "use strict";
    var MetaBoxAPP = {
        initialize: function() {
	        var metaPreset = [
		        'layout_meta_box',
		        'title_meta_box',
		        'logo_meta_box',
		        'top_drawer_meta_box',
		        'top_bar_meta_box',
		        'header_meta_box',
		        'header_customize_meta_box',
		        'menu_meta_box',
		        'footer_meta_box',
		        'color_meta_box'
	        ];
	        MetaBoxAPP.meta_box_tab(metaPreset,'Preset Setting');
	        var metaPage = [
		        'page_custom_meta_box_general',
		        'page_custom_meta_box_layout',
		        'page_custom_meta_box_title'
	        ];
	        MetaBoxAPP.meta_box_tab(metaPage,'Pages Setting');
            MetaBoxAPP.requiredField();
        },
        meta_box_tab: function(metaboxes,title) {
	        var meta_boxes_id = '';
	        for (var i = 0, n = metaboxes.length; i < n; i++) {
		        if (i == 0) {
			        meta_boxes_id += '#' + (typeof (meta_box_prefix) == "undefined" ? '' : meta_box_prefix) + metaboxes[i];
		        }
		        else {
			        meta_boxes_id += ',#' + (typeof (meta_box_prefix) == "undefined" ? '' : meta_box_prefix) + metaboxes[i];
		        }
	        }
	        var tabBoxes = $(meta_boxes_id);

	        if (tabBoxes.length == 0) {
		        return;
	        }


	        $('#normal-sortables').append('<div class="g5plus-meta-tabs-wrap postbox"><button type="button" class="handlediv button-link" aria-expanded="true"><span class="toggle-indicator" aria-hidden="true"></span></button><h2 class=""><span>' + title + '</span></h2><div id="g5plus-tabbed-meta-boxes"></div></div>');

            $(tabBoxes).appendTo('#g5plus-tabbed-meta-boxes');
            $(tabBoxes).hide().removeClass('hide-if-no-js');

            for (var a = 0, b = tabBoxes.length; a < b; a++ ) {
                var newClass = 'editor-tab' + a;
                $(tabBoxes[a]).addClass(newClass);
            }

            var menu_html = '<ul id="g5plus-meta-box-tabs" class="clearfix">\n';
            var total_hidden = 0;
            for (var i = 0, n = tabBoxes.length; i < n; i++ ) {
                var target_id = $(tabBoxes[i]).attr('id');
                var tab_name = $(tabBoxes[i]).find('.hndle > span').text();
                var tab_class = "";

                if ($(tabBoxes[i]).hasClass('hide-if-js')) {
                    total_hidden++;
                }

                menu_html = menu_html + '\n<li id="li-'+ target_id +'" class="'+tab_class+'"><a href="#" rel="editor-tab' + i + '">' + tab_name + '</a></li>';
            }
            menu_html = menu_html + '\n</ul>';

            $('#g5plus-tabbed-meta-boxes').before(menu_html);
            $('#g5plus-meta-box-tabs a:first').addClass('active');

            $('.editor-tab0').addClass('active').show();

            $('.g5plus-meta-tabs-wrap').on('click', '.handlediv', function() {
                var metaBoxWrap = $(this).parent();
                if (metaBoxWrap.hasClass('closed')) {
                    metaBoxWrap.removeClass('closed');
                } else {
                    metaBoxWrap.addClass('closed');
                }
            });

            $('#g5plus-meta-box-tabs li').on('click', 'a', function() {
                $(tabBoxes).removeClass('active').hide();
                $('#g5plus-meta-box-tabs a').removeClass('active');

                var target = $(this).attr('rel');

                $(this).addClass('active');
                $('.' + target).addClass('active').show();

                return false;
            });
        },
	    get_input_val: function($input) {
		    var val = $input.val();
		    if ($input.is(':checkbox')) {
			    if ($input.prop('checked')) {
				    val = $input.val();
			    }
			    else {
				    val = '0';
			    }
		    }
		    return val;
	    },
	    requiredGetApplyFiled: function(required) {
		    var arrInput = [];
		    var index = 0;
		    if (typeof (required[0]) == 'object') {
			    for (var i = 0; i < required.length; i++) {
				    var arrFieldChild = required[i];
				    if (typeof (arrFieldChild[0]) == 'object') {
					    for (var j = 0; j < arrFieldChild.length; j++) {
						    arrInput[index++] = arrFieldChild[j][0];
						    if (arrFieldChild[j][1].substr(0, 1) == '&') {
							    arrInput[index++] = arrFieldChild[j][2];
						    }
					    }
				    }
				    else {
					    arrInput[index++] = arrFieldChild[0];
					    if (arrFieldChild[1].substr(0, 1) == '&') {
						    arrInput[index++] = arrFieldChild[2];
					    }
				    }
			    }
		    }
		    else {
			    arrInput[index++] = required[0];
			    if (required[1].substr(0, 1) == '&') {
				    arrInput[index++] = required[2];
			    }
		    }
		    return arrInput;
	    },
	    requiredToggle: function($this, required, toggle) {
		    var isInputOk = true;
		    if (typeof (required[0]) == 'object') {
			    for (var i = 0; i < required.length; i++) {
				    var required_AND = required[i];
				    if (typeof (required_AND[0]) == 'object') {
					    var isOR = false;

					    for (var j = 0; j < required_AND.length; j++) {
						    var required_OR = required_AND[j];
						    isOR = MetaBoxAPP.requiredFieldProcess(required_OR);
						    if (isOR) {
							    break;
						    }
					    }
					    isInputOk = isOR;
				    }
				    else {
					    isInputOk = MetaBoxAPP.requiredFieldProcess(required_AND);
				    }
				    if (!isInputOk) {
					    break;
				    }
			    }
		    }
		    else {
			    isInputOk = MetaBoxAPP.requiredFieldProcess(required);
		    }
		    if (isInputOk) {
			    if (toggle) {
				    $this.slideDown();
			    }
			    else {
				    $this.show();
			    }

		    }
		    else {
			    if (toggle) {
				    $this.slideUp();
			    }
			    else {
				    $this.hide();
			    }
		    }
	    },
	    requiredField: function() {
		    var arrInput = [];

		    $('[data-required]').each(function () {
			    var $this = $(this);
			    var required = JSON.parse($this.attr('data-required'));
			    if (typeof (required[0]) == 'object') {
				    for (var i = 0; i < required.length; i++) {
					    var arrFieldChild = required[i];
					    if (typeof (arrFieldChild[0]) == 'object') {
						    for (var j = 0; j < arrFieldChild.length; j++) {
							    arrInput[arrFieldChild[j][0]] = true;
							    if (arrFieldChild[j][1].substr(0, 1) == '&') {
								    arrInput[arrFieldChild[j][2]] = true;
							    }
						    }
					    }
					    else {
						    arrInput[arrFieldChild[0]] = true;
						    if (arrFieldChild[1].substr(0, 1) == '&') {
							    arrInput[arrFieldChild[2]] = true;
						    }
					    }
				    }
			    }
			    else {
				    arrInput[required[0]] = true;
				    if (required[1].substr(0, 1) == '&') {
					    arrInput[required[2]] = true;
				    }
			    }

				MetaBoxAPP.requiredToggle($this, required, false);
		    });

		    for (var input in arrInput) {
			    $('#' + input).change(function() {
					var $this = $(this);

				    $('[data-required]').each(function () {
					    var $thisRequired = $(this);
					    var required = JSON.parse($thisRequired.attr('data-required'));
					    var reqInputs = MetaBoxAPP.requiredGetApplyFiled(required);
					    var thisInput = $this.attr('id');
					    if (reqInputs.indexOf(thisInput) == -1) {
						    return;
					    }
					    MetaBoxAPP.requiredToggle($thisRequired, required, true);
				    });
			    });
		    }

	    },
	    requiredFieldProcess: function(required) {
		    var data_ref = required[0];
		    var data_op  = required[1];
		    var data_val = required[2];
		    var val = MetaBoxAPP.get_input_val($('#' + data_ref));
		    if (typeof (data_val) == "object") {
			    if (((data_op == '=') && (data_val.indexOf(val) != -1)) || ((data_op == '<>') && (data_val.indexOf(val) == -1))) {
				    return true;
			    }
			    return false;
		    }
		    if (data_op.substr(0, 1) == '&') {
			    data_val = MetaBoxAPP.get_input_val($('#' + data_val));
			    data_op = data_op.substr(1);
		    }

		    if (((data_op == '=') && (data_val == val)) || ((data_op == '<>') && (data_val != val))) {
			    return true;
		    }
		    return false;
	    }
    };
    $(document).ready(function(){
        MetaBoxAPP.initialize();
    });
})(jQuery);