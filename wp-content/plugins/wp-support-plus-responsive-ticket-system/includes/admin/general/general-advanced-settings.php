<?php
if ( ! defined( 'ABSPATH' ) )
{
	exit; // Exit if accessed directly
}
global $wpsupportplus;
?>

<div id="tab_container">

<form method="post" action="">
	
	<input type="hidden" name="action" value="update"/>
	<input type="hidden" name="update_setting" value="general_settings_advanced"/>
  <?php wp_nonce_field('wpbdp_tab_general_section_general'); ?>
	
  
	      <table class="form-table">
      
					<tr>
					<th scope="row"><?php _e('Sign In','wp-support-plus-responsive-ticket-system'); ?></th>
					<td>
					<select name="general_advanced_settings[signin]">
						 
						 <option <?php echo $wpsupportplus->functions->toggle_button_signin() ? 'selected="selected"' : '' ?> value="1"><?php _e('Enable','wp-support-plus-responsive-ticket-system')?></option>
						 <option <?php echo $wpsupportplus->functions->toggle_button_signin()? '' : 'selected="selected"' ?> value="0"><?php _e('Disable','wp-support-plus-responsive-ticket-system')?></option>
	
				  </select>
			   	</td>
					</tr>

					<tr>
					<th scope="row"><?php _e('Sign Out','wp-support-plus-responsive-ticket-system'); ?></th>
					<td>
					<select name="general_advanced_settings[signout]">
						 
						 <option <?php echo $wpsupportplus->functions->toggle_button_signout() ? 'selected="selected"' : '' ?> value="1"><?php _e('Enable','wp-support-plus-responsive-ticket-system')?></option>
						 <option <?php echo $wpsupportplus->functions->toggle_button_signout()? '' : 'selected="selected"' ?> value="0"><?php _e('Disable','wp-support-plus-responsive-ticket-system')?></option>
					
					</select>
					</td>
					</tr>

				<tr>
				<th scope="row"><?php _e('Sign Up','wp-support-plus-responsive-ticket-system'); ?></th>
				<td>
				<select name="general_advanced_settings[signup]">
					 
					 <option <?php echo $wpsupportplus->functions->toggle_button_signup() ? 'selected="selected"' : '' ?> value="1"><?php _e('Enable','wp-support-plus-responsive-ticket-system')?></option>
					 <option <?php echo $wpsupportplus->functions->toggle_button_signup()? '' : 'selected="selected"' ?> value="0"><?php _e('Disable','wp-support-plus-responsive-ticket-system')?></option>

			  </select>
		   	</td>
				</tr>
       </table>

      <?php do_action('wpsp_after_general_settings')?>
					<p class="submit">
						<input id="submit" class="button button-primary" name="submit" value="<?php _e('Save Changes', 'wp-support-plus-responsive-ticket-system'); ?>" type="submit">											
	        </p>

</form>
</div>
