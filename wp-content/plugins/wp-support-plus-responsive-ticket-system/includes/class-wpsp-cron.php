<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WPSP_Cron' ) ) :
    
    /**
     * Cron class for WPSP.
     * @class WPSP_Cron
     */
    class WPSP_Cron {
        
        /**
         * Constructor
         */
        public function __construct(){
            
        }
        
    }
    
endif;