<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Check nonce
 */
if( !isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce']) ){
    die(__('Cheating huh?', 'wp-support-plus-responsive-ticket-system'));
}

global $wpsupportplus, $wpdb;

$agent_id   = intval(sanitize_text_field($_POST['wpsp_agent']['agent_id']));
$role       = intval(sanitize_text_field($_POST['wpsp_agent']['role']));

$values = apply_filters('wpsp_set_add_user', array(
    'user_id'       => $agent_id,
    'role'          => $role
));

$wpdb->insert( $wpdb->prefix.'wpsp_users', $values );

if($wpdb->insert_id){
    
    $user = get_userdata($agent_id);
    
    $user->remove_cap('wpsp_agent');
    $user->remove_cap('wpsp_supervisor');
    $user->remove_cap('wpsp_administrator');
    
    switch ($role){
        case 1: $user->add_cap('wpsp_agent');
            break;
        case 2: $user->add_cap('wpsp_supervisor');
            break;
        case 3: $user->add_cap('wpsp_administrator');
            break;
    }
}

do_action('wpsp_after_add_new_agent', $wpdb->insert_id);
