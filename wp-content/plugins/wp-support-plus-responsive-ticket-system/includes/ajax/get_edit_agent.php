<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$agent_id = isset($_POST['load_id']) ? sanitize_text_field($_POST['load_id']) : 0;
$nonce = isset($_POST['nonce']) ? sanitize_text_field($_POST['nonce']) : 0;

/**
 * Check nonce
 */
if( !wp_verify_nonce($nonce, $agent_id) ){
    die(__('Cheating huh?', 'wp-support-plus-responsive-ticket-system'));
}

global $wpsupportplus, $wpdb;

$role = $wpdb->get_var("select role from {$wpdb->prefix}wpsp_users where user_id=".$agent_id);

?>

<form action="" onsubmit="return validate_wpsp_admin_popup_form(this);">
    
    <div id="wpsp_popup_form_error" class="error notice" style="display: none;">
        <p></p>
    </div>
    
    <div class="wpsp_popup_form_element">
        <strong><?php _e('Role','wp-support-plus-responsive-ticket-system');?></strong>:<br>
        <select name="role">
            <option <?php echo $role && $role==1 ? 'selected="selected"' : ''?> value="1"><?php _e('Agent','wp-support-plus-responsive-ticket-system')?></option>
            <option <?php echo $role && $role==2 ? 'selected="selected"' : ''?> value="2"><?php _e('Supervisor','wp-support-plus-responsive-ticket-system')?></option>
            <option <?php echo $role && $role==3 ? 'selected="selected"' : ''?> value="3"><?php _e('Administrator','wp-support-plus-responsive-ticket-system')?></option>
        </select>
    </div>
    
    <input type="hidden" name="action" value="wpsp_set_edit_agent" />
    <input type="hidden" name="load_id" value="<?php echo $agent_id?>" />
    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce($agent_id)?>" />
    
    <hr class="wpsp_admin_popup_footer_saperator">
    <button class="button button-primary" type="submit"><?php _e('Submit','wp-support-plus-responsive-ticket-system')?></button>
    <button class="button" type="button" onclick="wpsp_close_admin_popup();"><?php _e('Cancel','wp-support-plus-responsive-ticket-system')?></button>
    
</form>

<script>
jQuery(document).ready(function(){
    wpspjq('.wpsp_color_picker').wpColorPicker();
});
    
function validate_wpsp_admin_popup_form(obj){
    
    var error_flag = false;
    
    if(!error_flag){
        wpsp_admin_submit_popup(obj);
    } else {
        wpspjq('#wpsp_popup_form_error').show();
    }
    
    return false;
}
</script>