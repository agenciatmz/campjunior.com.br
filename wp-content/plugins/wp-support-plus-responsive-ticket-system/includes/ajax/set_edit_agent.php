<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$agent_id    = isset($_POST['load_id']) ? intval(sanitize_text_field($_POST['load_id'])) : 0;
$role        = isset($_POST['role']) ? intval(sanitize_text_field($_POST['role'])) : 0;
$nonce          = isset($_POST['nonce']) ? sanitize_text_field($_POST['nonce']) : 0;

/**
 * Check nonce
 */
if( !wp_verify_nonce($nonce, $agent_id) ){
    die(__('Cheating huh?', 'wp-support-plus-responsive-ticket-system'));
}

global $wpsupportplus, $wpdb;

$agent = get_userdata($agent_id);
if( $agent->has_cap('wpsp_supervisor') && $role!=2 ){
    $categories = $wpdb->get_results("select id, supervisor from {$wpdb->prefix}wpsp_catagories");
    foreach ($categories as $category){
        $existing_supervisors = $wpdb->get_var("select supervisor from {$wpdb->prefix}wpsp_catagories where id=".$category->id);
        $existing_supervisors = $existing_supervisors ? unserialize($existing_supervisors) : array();
        if(in_array($agent->ID, $existing_supervisors)){
            $agent->remove_cap('wpsp_cat_'.$category->id);
            unset($existing_supervisors[array_search($agent->ID, $existing_supervisors)]);
            $values = array(
                'supervisor'    => serialize($existing_supervisors)
            );
            $wpdb->update( $wpdb->prefix.'wpsp_catagories', $values, array('id'=>$category->id) );
        }
    }
}


$values = apply_filters('wpsp_set_edit_agent', array(
    'role' => $role
));

$wpdb->update( $wpdb->prefix.'wpsp_users', $values, array('user_id'=>$agent_id) );
    
$agent->remove_cap('wpsp_agent');
$agent->remove_cap('wpsp_supervisor');
$agent->remove_cap('wpsp_administrator');

switch ($role){
    case 1: $agent->add_cap('wpsp_agent');
        break;
    case 2: $agent->add_cap('wpsp_supervisor');
        break;
    case 3: $agent->add_cap('wpsp_administrator');
        break;
}

do_action('wpsp_after_edit_agent', $agent_id);
