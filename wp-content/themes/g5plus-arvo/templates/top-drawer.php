<?php
$top_drawer_type = g5plus_get_option('top_drawer_type', 'none');
if ((!$top_drawer_type) || ($top_drawer_type === 'none')) {
    return;
}
global $wp_registered_sidebars;

$top_drawer_sidebar = g5plus_get_option('top_drawer_sidebar', '');
$top_drawer_class = array(
    'top-drawer-wrapper',
    'top-drawer-type-' . $top_drawer_type
);

$top_drawer_hide_mobile = g5plus_get_option('top_drawer_hide_mobile', '1');
$top_drawer_wrapper_layout = g5plus_get_option('top_drawer_wrapper_layout', 'container');
if ($top_drawer_hide_mobile) {
    $top_drawer_class[] = 'top-drawer-mobile-invisible';
}

$top_drawer_container_class = array('top-drawer-container');
if ($top_drawer_wrapper_layout && ($top_drawer_wrapper_layout !== 'full')) {
    $top_drawer_container_class[] = esc_attr($top_drawer_wrapper_layout);
}
?>
<div class="<?php echo join(' ', $top_drawer_class); ?>">
    <div class="<?php echo join(' ', $top_drawer_container_class); ?>">
        <div class="top-drawer-inner">
	        <?php if (is_active_sidebar($top_drawer_sidebar)): ?>
		        <?php dynamic_sidebar($top_drawer_sidebar); ?>
	        <?php else: ?>
		        <div class="no-widget-content"> <?php printf(wp_kses_post(__('Please insert widget into sidebar <b>%s</b> in Appearance > <a class="text-color-accent" title="manage widgets" href="%s">Widgets</a> ','g5plus-arvo')),$wp_registered_sidebars[$top_drawer_sidebar]['name'],admin_url( 'widgets.php' )); ?></div>
	        <?php endif; ?>
        </div>
    </div>
    <?php if ($top_drawer_type === 'toggle'): ?>
        <span class="top-drawer-toggle"><i class="fa fa-plus"></i></span>
    <?php endif;?>
</div>
