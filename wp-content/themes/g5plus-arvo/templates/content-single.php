<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
$size = 'large-image';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-single clearfix'); ?>>
	<?php g5plus_get_post_thumbnail( $size, 0, true ); ?>
	<div class="entry-content-wrap">
		<h3 class="entry-post-title"><?php the_title(); ?></h3>
		<?php g5plus_get_template('archive/post-meta',array('is_full' => 1)); ?>
		<div class="entry-content clearfix">
			<?php
			the_content();
			wp_link_pages(array(
				'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__('Pages:','g5plus-arvo') . '</span>',
				'after' => '</div>',
				'link_before' => '<span class="page-link">',
				'link_after' => '</span>',
			)); ?>
		</div>
	</div>
</article>
<?php
/**
 * @hooked - g5plus_post_tag - 5
 * @hooked - g5plus_post_author_info - 10
 * @hooked - g5plus_post_nav - 15
 * @hooked - g5plus_post_comment - 20
 * @hooked - g5plus_post_related - 30
 *
 **/
do_action('g5plus_after_single_post');
?>

