<?php
/**
 * @var $customize_location
 */
if (!function_exists('g5plus_add_canvas_menu_region')) {
	function g5plus_add_canvas_menu_region() {
		global $wp_registered_sidebars; ?>
		<nav class="canvas-menu-wrapper">
			<a href="#" class="canvas-menu-close"><i class="pe-7s-close"></i></a>
			<div class="canvas-menu-inner sidebar">
				<?php if (is_active_sidebar('canvas-menu')): ?>
					<?php dynamic_sidebar('canvas-menu'); ?>
				<?php else: ?>
					<div class="no-widget-content"> <?php printf(wp_kses_post(__('Please insert widget into sidebar <b>%s</b> in Appearance > <a class="text-color-accent" title="manage widgets" href="%s">Widgets</a> ','g5plus-arvo')),$wp_registered_sidebars['canvas-menu']['name'],admin_url( 'widgets.php' )); ?></div>
				<?php endif; ?>
			</div>
		</nav>
		<?php }
	add_action('g5plus_after_page_wrapper','g5plus_add_canvas_menu_region');
}
?>
<div class="header-customize-item items-canvas-menu">
	<a class="canvas-menu-toggle" href="#"><i class="fa fa-align-justify"></i></a>
</div>

