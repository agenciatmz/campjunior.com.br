<?php
$header_class = array('header-wrapper', 'clearfix', 'header-nav-wrapper', 'header-nav-hidden');
$header_container_layout = g5plus_get_option('header_container_layout','container');
$header_border_bottom = g5plus_get_option('header_border_bottom','none');
$header_sticky = g5plus_get_option('header_sticky', 0);

if ($header_border_bottom != 'none') {
	$header_class[] = $header_border_bottom;
}

$sticky_wrapper = array();
if ($header_sticky) {
	$sticky_wrapper[] = 'sticky-wrapper';
	$header_class[] = 'sticky-region';
}

/**
 * Get page custom menu
 */
$page_menu = g5plus_get_option('page_menu', '');
?>
<div class="<?php echo join(' ',$sticky_wrapper); ?>">
	<div class="<?php echo join(' ', $header_class); ?>">
		<div class="<?php echo esc_attr($header_container_layout); ?>">
			<div class="header-above-inner container-inner clearfix">
				<?php get_template_part('templates/header/logo'); ?>
				<?php if (has_nav_menu('primary') || $page_menu): ?>
					<nav class="primary-menu">
						<?php
						$arg_menu = array(
							'menu_id' => 'main-menu',
							'container' => '',
							'theme_location' => 'primary',
							'menu_class' => 'main-menu'
						);
						wp_nav_menu( $arg_menu );
						?>
						<?php  g5plus_get_template('header/header-customize', array('customize_location' => 'nav'));?>
					</nav>
				<?php else: ?>
					<nav class="primary-menu">
						<div class="no-menu"><?php printf(wp_kses_post(__('Please assign a menu to the <b>Primary Menu</b> in Appearance > <a title="Menus" class="text-color-accent" href="%s">Menus</a>','g5plus-arvo')),admin_url( 'nav-menus.php' )); ?></div>
					</nav>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>