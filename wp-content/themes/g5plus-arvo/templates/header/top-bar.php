<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/17/2016
 * Time: 11:06 AM
 */

$top_bar_enable = g5plus_get_option('top_bar_enable', 0);

if(!$top_bar_enable){
    return;
}

global $wp_registered_sidebars;

$left_sidebar = g5plus_get_option('top_bar_left_sidebar', 'top_bar_left');
$right_sidebar = g5plus_get_option('top_bar_right_sidebar', 'top_bar_right');
$top_bar_border = g5plus_get_option('top_bar_border', 'none');
$top_bar_layout = g5plus_get_option('top_bar_layout','top-bar-1');

$topBar_Class = array('top-bar-wrapper', 'bar-wrapper');
if ($top_bar_border != 'none') {
    $topBar_Class[] = esc_attr($top_bar_border);
}

$topBar_layout_matrix = array(
    'top-bar-1' => array('col-md-6', 'col-md-6'),
    'top-bar-2' => array('col-md-8', 'col-md-4'),
    'top-bar-3' => array('col-md-4', 'col-md-8'),
    'top-bar-4' => array('col-md-12', '')
);
$col_left_sidebar = $topBar_layout_matrix[$top_bar_layout][0];
$col_right_sidebar = $topBar_layout_matrix[$top_bar_layout][1];

if ($top_bar_layout === 'top-bar-4') {
	$topBar_Class[] = 'text-center';
	$right_sidebar = '';
}

?>
<div class="<?php echo join(' ', $topBar_Class); ?>">
    <div class="container">
        <div class="top-bar-inner">
            <div class="row">
	            <div class="top-bar-left bar-left <?php echo esc_attr($col_left_sidebar); ?>">
		            <?php if (is_active_sidebar($left_sidebar)): ?>
			            <?php dynamic_sidebar( $left_sidebar );?>
					<?php else: ?>
			            <div class="no-widget-content"> <?php printf(wp_kses_post(__('Please insert widget into sidebar <b>%s</b> in Appearance > <a class="text-color-accent" title="manage widgets" href="%s">Widgets</a> ','g5plus-arvo')),$wp_registered_sidebars[$left_sidebar]['name'],admin_url( 'widgets.php' )); ?></div>
		            <?php endif;?>
	            </div>
				<?php if (!empty($right_sidebar)): ?>
					<div class="top-bar-right bar-right <?php echo esc_attr($col_right_sidebar); ?>">
						<?php if (is_active_sidebar($right_sidebar)): ?>
							<?php dynamic_sidebar( $right_sidebar );?>
						<?php else: ?>
							<div class="no-widget-content"> <?php printf(wp_kses_post(__('Please insert widget into sidebar <b>%s</b> in Appearance > <a class="text-color-accent" title="manage widgets" href="%s">Widgets</a> ','g5plus-arvo')),$wp_registered_sidebars[$right_sidebar]['name'],admin_url( 'widgets.php' )); ?></div>
						<?php endif;?>
					</div>
	            <?php endif; ?>
            </div>
        </div>
    </div>
</div>