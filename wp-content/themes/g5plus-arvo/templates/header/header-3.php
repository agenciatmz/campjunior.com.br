<?php
$header_class = array('header-wrapper', 'clearfix');
$header_sticky = g5plus_get_option('header_sticky', 0);
$navigation_float = g5plus_get_option('navigation_float', 1);
$sticky_wrapper = array();
$sticky_region_class = array('header-row','header-nav-wrapper');
if ($header_sticky) {
	$sticky_wrapper[] = 'sticky-wrapper';
	$sticky_region_class[] = 'sticky-region';
}
if ($navigation_float) {
	$sticky_region_class[] = 'header-nav-float';
}

/**
 * Get page custom menu
 */
$page_menu = g5plus_get_option('page_menu', '');
?>
<div class="<?php echo join(' ', $header_class); ?>">
	<div class="<?php echo esc_attr('header-row header-above-wrapper'); ?>">
		<div class="container">
			<div class="header-above-inner clearfix text-center">
				<?php get_template_part('templates/header/logo'); ?>
			</div>
		</div>
	</div>
	<div class="<?php echo join(' ', $sticky_wrapper); ?>">
		<div class="<?php echo join(' ', $sticky_region_class); ?>">
			<div class="container">
				<div class="container-inner">
					<?php if (has_nav_menu('primary') || $page_menu): ?>
						<nav class="primary-menu header-row">
							<?php
							$arg_menu = array(
								'menu_id' => 'main-menu',
								'container' => '',
								'theme_location' => 'primary',
								'menu_class' => 'main-menu'
							);
							wp_nav_menu( $arg_menu );
							g5plus_get_template('header/header-customize', array('customize_location' => 'nav'));
							?>
						</nav>
					<?php else: ?>
						<nav class="primary-menu">
							<div class="no-menu"><?php printf(wp_kses_post(__('Please assign a menu to the <b>Primary Menu</b> in Appearance > <a title="Menus" class="text-color-accent" href="%s">Menus</a>','g5plus-arvo')),admin_url( 'nav-menus.php' )); ?></div>
						</nav>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>