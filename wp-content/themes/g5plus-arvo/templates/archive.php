<?php
/**
 * The template for displaying archive
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */

global $wp_query;
$post_layouts = &g5plus_get_post_layout_settings();
$blog_wrap_classes = array('blog-wrap clearfix');
$blog_wrap_classes[] = 'blog-' . $post_layouts['layout'];
if (in_array($post_layouts['layout'],array('grid','masonry','1-large-image-masonry')) && !$post_layouts['slider']) {
	$page_layouts = &g5plus_get_page_layout_settings();
	$blog_wrap_classes[] = 'row';
	$blog_wrap_classes[] = 'columns-'.$post_layouts['columns'];
	if ($page_layouts['has_sidebar']) {
		$blog_wrap_classes[] = 'columns-md-2';
	} else {
		$blog_wrap_classes[] = 'columns-md-'.$post_layouts['columns'];
	}
	$blog_wrap_classes[] = 'columns-sm-2';
}
$paged   = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
$posts_per_page  = get_query_var( 'posts_per_page' ) ? intval( get_query_var( 'posts_per_page' ) ) : 1;

?>
    <div class="<?php echo esc_attr(join(' ',$blog_wrap_classes));?>">
        <?php if (have_posts()) : ?>
            <?php
            // Start the Loop.
            while (have_posts()) : the_post();

	            $index = $wp_query->current_post;
	            if ($post_layouts['paging'] !== 'navigation') {
		            $index = $wp_query->current_post + (($paged - 1) * $posts_per_page);
	            }
                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
	            if ($post_layouts['layout'] == '1-large-image-masonry') {
		            if(($wp_query->current_post == 0) && (($paged == 1) || ($post_layouts['paging'] == 'navigation'))) {
			            get_template_part('templates/archive/content-large-image-2');
		            } else {
			            g5plus_get_template('archive/content-masonry-2',array('index' => ($index - 1), 'columns' => $post_layouts['columns']));
		            }
	            } else {
		            get_template_part('templates/archive/content', $post_layouts['layout']);
	            }
                // End the loop.
            endwhile;

        // If no content, include the "No posts found" template.
        else :
            get_template_part('templates/archive/content', 'none');
            ?>

        <?php endif;
        ?>
    </div>
<?php if ($wp_query->max_num_pages > 1) {
    get_template_part('templates/paging/'.$post_layouts['paging']);
}