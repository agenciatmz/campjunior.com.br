<?php
/**
 * The template for displaying page title
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */

$page_title = '';
$page_sub_title = '';
$page_title_enable = 1;
$page_breadcrumbs_enable = 1;
$page_title_bg_image = '';
$page_title_layout = g5plus_get_option('page_title_layout', 'centered');
$page_title_padding = g5plus_get_option('page_title_padding', array('padding-top' => '145px', 'padding-bottom' => '110px'));
$page_title_overlay_scheme = g5plus_get_option('page_title_overlay_scheme', 'page-title-overlay-dark');
$page_title_parallax = g5plus_get_option('page_title_parallax', 0);

if (is_home()) {
    if (empty($page_title)) {
        $page_title = esc_html__("Blog", 'g5plus-arvo');
    }
} elseif (!is_singular() && !is_front_page()) {
    if (!have_posts()) {
        $page_title = esc_html__('Nothing Found', 'g5plus-arvo');
    } elseif (is_tag() || is_tax('product_tag')) {
        $page_title = single_tag_title(esc_html__("Tags: ", 'g5plus-arvo'), false);
    } elseif (is_category() || is_tax()) {
        $page_title = single_cat_title('', false);
    } elseif (is_author()) {
        $page_title = sprintf(esc_html__('Author: %s', 'g5plus-arvo'), get_the_author());
    } elseif (is_day()) {
        $page_title = sprintf(esc_html__('Daily Archives: %s', 'g5plus-arvo'), get_the_date());
    } elseif (is_month()) {
        $page_title = sprintf(esc_html__('Monthly Archives: %s', 'g5plus-arvo'), get_the_date(_x('F Y', 'monthly archives date format', 'g5plus-arvo')));
    } elseif (is_year()) {
        $page_title = sprintf(esc_html__('Yearly Archives: %s', 'g5plus-arvo'), get_the_date(_x('Y', 'yearly archives date format', 'g5plus-arvo')));
    } elseif (is_search()) {
        $page_title = esc_html__('Search Results', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-aside')) {
        $page_title = esc_html__('Asides', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-gallery')) {
        $page_title = esc_html__('Galleries', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-image')) {
        $page_title = esc_html__('Images', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-video')) {
        $page_title = esc_html__('Videos', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-quote')) {
        $page_title = esc_html__('Quotes', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-link')) {
        $page_title = esc_html__('Links', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-status')) {
        $page_title = esc_html__('Statuses', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-audio')) {
        $page_title = esc_html__('Audios', 'g5plus-arvo');
    } elseif (is_tax('post_format', 'post-format-chat')) {
        $page_title = esc_html__('Chats', 'g5plus-arvo');
    }
}

$page_title_enable = g5plus_get_option('page_title_enable', 1);
$page_title = g5plus_get_option('page_title', $page_title);
$page_sub_title = g5plus_get_option('page_sub_title', '');
$page_breadcrumbs_enable = g5plus_get_option('breadcrumbs_enable', 1);
$page_title_bg_image = g5plus_get_option('page_title_bg_image', array('url' => G5PLUS_THEME_URL . 'assets/images/page-title-bg.jpg'));
$page_title_bg_image = isset($page_title_bg_image['url']) ? $page_title_bg_image['url'] : '';
if (is_singular()) {
    if (!$page_title) {
	    $page_title = get_the_title(get_the_ID());
    }

	$custom_page_title_visible = g5plus_get_rwmb_meta('custom_page_title_visible');
	if (($custom_page_title_visible !== '-1') && ($custom_page_title_visible !== '')) {
		$page_title_enable = $custom_page_title_visible;
	}

	$custom_breadcrumbs_visible = g5plus_get_rwmb_meta('custom_breadcrumbs_visible');
	if (($custom_breadcrumbs_visible !== '-1') && ($custom_breadcrumbs_visible !== '')) {
		$page_breadcrumbs_enable = $custom_breadcrumbs_visible;
	}

	$is_custom_page_title = g5plus_get_rwmb_meta('is_custom_page_title');
	if ($is_custom_page_title) {
		$page_title = g5plus_get_rwmb_meta('custom_page_title');
		$page_sub_title = g5plus_get_rwmb_meta('custom_page_sub_title');
	}

	$is_custom_page_title_bg = g5plus_get_rwmb_meta('is_custom_page_title_bg');
	if ($is_custom_page_title_bg) {
		$page_title_bg_image = g5plus_get_rwmb_meta_image('custom_page_title_bg_image');
	}
} elseif(is_category() || is_tax()) {
	$cat = get_queried_object();
	if ($cat && property_exists($cat, 'term_id')) {
		$custom_page_title_enable = g5plus_get_tax_meta($cat->term_id, 'page_title_enable');
		if ($custom_page_title_enable != '' && $custom_page_title_enable != -1) {
			$page_title_enable = $custom_page_title_enable;
		}

		$bg_image = g5plus_get_tax_meta($cat->term_id, 'page_title_bg_image');
		if (isset($bg_image['url'])) {
			$page_title_bg_image = $bg_image['url'];
		}

		$term_description = strip_tags(term_description());
		if ($term_description) {
			$page_sub_title = $term_description;
		}
	}
}

if (!$page_title_enable) return;

$page_title = apply_filters('g5plus_page_title', $page_title);
$page_sub_title = apply_filters('g5plus_sub_page_title', $page_sub_title);

$page_title_class = array('page-title');
$page_title_class[] = 'page-title-layout-' . $page_title_layout;

// region Custom Styles

$custom_styles = array();
$page_title_bg_styles = array();
$page_title_bg_class = array();
if (isset($page_title_padding['padding-top']) && !empty($page_title_padding['padding-top']) && ($page_title_padding['padding-top'] != 'px')) {
	$custom_styles[] = "padding-top:" . $page_title_padding['padding-top'];
} else {

}
if (isset($page_title_padding['padding-bottom']) && !empty($page_title_padding['padding-bottom']) && ($page_title_padding['padding-bottom'] != 'px')) {
    $custom_styles[] = "padding-bottom:" . $page_title_padding['padding-bottom'];
}

if (!empty($page_title_bg_image)) {
	$page_title_bg_styles[] = 'style="background-image: url(' . $page_title_bg_image . ')"';
	$page_title_bg_class[] = 'page-title-background';
	$page_title_class[] = 'page-title-background';

    if ($page_title_parallax) {
	    $page_title_bg_class[] = 'page-title-parallax';
    }
	$page_title_bg_class[] = $page_title_overlay_scheme;
}

$custom_style = '';
if ($custom_styles) {
	$custom_style = 'style="' . join(';', $custom_styles) . '"';
}
if (!empty($page_title_bg_image) && $page_title_parallax) {
	$page_title_bg_styles[]= 'data-stellar-background-ratio="0.5"';
}

// endregion
?>
<section data-layout="<?php echo esc_attr($page_title_layout); ?>" class="<?php echo join(' ', $page_title_class); ?>" <?php echo wp_kses_post($custom_style); ?>>
	<div class="<?php echo join(' ', $page_title_bg_class);?>" <?php echo join(' ', $page_title_bg_styles); ?>></div>
    <div class="container">
        <div class="page-title-inner">
	        <?php if (!empty($page_sub_title)): ?>
	            <h1 class="has-subtitle">
	                <span><?php echo esc_html($page_title) ?></span>
	            </h1>
	            <p><?php echo esc_html($page_sub_title) ?></p>
	        <?php else: ?>
		        <h1>
			        <span><?php echo esc_html($page_title) ?></span>
		        </h1>
            <?php endif; ?>
            <?php if ($page_breadcrumbs_enable) {
                get_template_part('templates/breadcrumb');
            } ?>
        </div>
    </div>
</section>
