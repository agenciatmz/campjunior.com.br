<?php
/**
 * The template for displaying navigation on single post
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */

?>
<nav class="post-navigation">
	<?php
	previous_post_link('<div class="nav-previous">%link</div>', '<i class="pe-7s-angle-left"></i> ' . esc_html__('Previous','g5plus-arvo'));
	next_post_link('<div class="nav-next">%link</div>', esc_html__('Next','g5plus-arvo') . ' <i class="pe-7s-angle-right"></i>');
	?>
	<!-- .nav-links -->
</nav><!-- .navigation -->
