<?php
/**
 * The template for displaying author info
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
$profiles = g5plus_get_social_profiles();
?>
<div class="author-info clearfix">
    <div class="author-info-inner">
        <div class="author-avatar">
            <?php
            /**
             * Filter the Arvo author bio avatar size.
             *
             * @since Arvo 1.0
             *
             * @param int $size The avatar height and width size in pixels.
             */
            $author_bio_avatar_size = apply_filters( 'g5plus_author_bio_avatar_size', 100 );
            echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
            ?>
        </div><!-- .author-avatar -->
        <div class="author-description">
            <h2 class="author-title"><?php the_author_posts_link(); ?></h2>
            <p class="author-bio">
                <?php the_author_meta( 'description' ); ?>
            </p><!-- .author-bio -->
	        <div class="social-share classic">
		        <?php foreach ($profiles as $key => $field):?>
			        <?php $social_url = get_the_author_meta($field['id']);  ?>
			        <?php if (!empty($social_url)) : ?>
				        <a target="_blank" title="<?php echo esc_attr($field['title']); ?>" href="<?php echo esc_url($social_url) ?>"><i class="<?php echo esc_attr($field['icon'])?>"></i></a>
			        <?php endif; ?>
		        <?php endforeach; ?>
	        </div>
        </div><!-- .author-description -->
    </div>
</div><!-- .author-info -->
