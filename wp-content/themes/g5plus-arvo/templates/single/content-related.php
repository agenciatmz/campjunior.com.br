<?php
/**
 * The template for displaying content related
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
$size = 'small-image';
$excerpt =  get_the_excerpt();
$noImage = g5plus_get_option('single_related_enable_no_image',0);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-grid clearfix'); ?>>
	<?php g5plus_get_post_thumbnail($size,$noImage); ?>
	<div class="entry-content-wrap">
		<h3 class="entry-post-title"><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php get_template_part('templates/archive/post-meta'); ?>
		<?php if ($excerpt !== ''): ?>
			<div class="entry-excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php endif; ?>
		<a href="<?php the_permalink(); ?>" class="btn btn-link btn-black"><?php esc_html_e('Read More', 'g5plus-arvo'); ?></a>
	</div>
</article>
