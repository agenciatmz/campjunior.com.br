<?php
/**
 * The template for displaying post related
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
global $post;

$single_related_post_enable = g5plus_get_option('single_related_post_enable', 0);
if (!$single_related_post_enable || !isset($post->ID)) return;

$single_related_post_condition = g5plus_get_option('single_related_post_condition', array());
$related_by_category = isset($single_related_post_condition['category']) && $single_related_post_condition['category'] == 1 ? true : false;
$related_by_tag = isset($single_related_post_condition['tag']) && $single_related_post_condition['tag'] == 1 ? true : false;
$single_related_post_total = g5plus_get_option('single_related_post_total', 6);
$single_related_post_column = g5plus_get_option('single_related_post_column', 2);

$tag_slugs = wp_get_post_tags($post->ID, array('fields' => 'slugs'));
$cat_ids = wp_get_post_terms($post->ID, 'category', array('fields' => 'ids'));

if (count($tag_slugs) == 0 && count($cat_ids) == 0) return;

$args = array(
    'numberposts'         => $single_related_post_total,
    'ignore_sticky_posts' => 1,
    'post__not_in'        => array($post->ID),
    'tax_query'           => array(
        'relation' => 'AND',
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => array('post-format-quote', 'post-format-link'),
            'operator' => 'NOT IN'
        )
    )
);
if ($related_by_category && isset($cat_ids) && count($cat_ids) > 0) {
    $args['category__in'] = $cat_ids;
}
if ($related_by_tag && isset($tag_slugs) && count($tag_slugs) > 0) {
    $args['tax_query'][] = array(
        'taxonomy' => 'post_tag',
        'field'    => 'slug',
        'terms'    => $tag_slugs,
        'operator' => 'IN'
    );
}
$args = apply_filters('g5plus_related_post_args', $args);
$posts = get_posts($args);

if (sizeof($posts) == 0) return;

$data_plugin_options = '"margin": 30,"autoHeight" : true, "loop": false, "responsiveClass": true, "dots" : false, "nav" : true, "autoplay": false, "autoplayHoverPause": true';
switch ($single_related_post_column) {
    case '2':
        $data_plugin_options .= ',"responsive" : {"0" : {"items" : 1, "margin": 0}, "600": {"items" : 2, "margin": 30}, "992": {"items" : 2, "margin": 30}}';
        break;
    case '3' :
        $data_plugin_options .= ',"responsive" : {"0" : {"items" : 1, "margin": 0}, "600": {"items" : 2, "margin": 30}, "992": {"items" : 3, "margin": 30}}';
        break;
}

?>
<div class="post-related-wrap clearfix">
    <h4 class="block-title"><?php esc_html_e('Related Posts', 'g5plus-arvo') ?></h4>
    <div class="owl-carousel owl-nav-top-68" data-plugin-options='{<?php echo esc_attr($data_plugin_options); ?>}'>
        <?php foreach ($posts as $item): setup_postdata($GLOBALS['post'] = &$item); ?>
            <?php get_template_part('templates/single/content-related'); ?>
        <?php endforeach; ?>
    </div>
</div>
<?php wp_reset_postdata(); ?>
