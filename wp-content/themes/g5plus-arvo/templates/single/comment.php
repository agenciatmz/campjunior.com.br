<?php
/**
 * The template for displaying comment item
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	<div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
		<?php echo get_avatar($comment, $args['avatar_size']); ?>
		<div class="comment-text entry-content">
			<div class="author-name"><?php printf('%s', get_comment_author_link()) ?></div>
			<div class="comment-meta-date"><?php echo (get_comment_date(get_option('date_format')) . ' ' . esc_html__('at','g5plus-arvo') . ' ' . get_comment_date('H:i a')) ; ?></div>
			<div class="comment-meta">
				<?php edit_comment_link(esc_html__('Edit','g5plus-arvo')); ?>
				<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>
			<div class="text">
				<?php comment_text() ?>
				<?php if ($comment->comment_approved == '0') : ?>
					<em><?php esc_html_e('Your comment is awaiting moderation.','g5plus-arvo');?></em>
				<?php endif; ?>
			</div>
		</div>
	</div>

