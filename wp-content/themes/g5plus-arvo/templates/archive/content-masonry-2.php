<?php
/**
 * The template for displaying content masonry
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 * @var $index
 * @var $columns
 */
$excerpt =  get_the_excerpt();
$matrix = array(
	3 => array('370x543','370x370','370x243','370x370','370x243','370x243','370x370','370x243'),
	2 => array('370x543','370x243','370x370','370x243')
);
$index = $index % sizeof($matrix[$columns]);
$size = 'size-'.$matrix[$columns][$index];
$hasThumb = false;
$galleryId = rand();
$show_content = true;

$image_size = g5plus_get_image_size($size);
$width = $image_size['width'];
$height = $image_size['height'];

$preview_image_full_src = '';
$preview_image_thumb_src = '';
$preview_embed_src = '';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-masonry gf-item-wrap clearfix'); ?>>
	<div class="entry-thumb-wrap">
	<?php if (has_post_format('gallery')) : ?>
		<?php $g5plus_gallery = get_post_meta(get_the_ID(), G5Plus_Post_Formats_UI::$format_gallery_images, true); ?>
		<?php if ($g5plus_gallery) : ?>
			<?php
				$hasThumb = true;
				$gallery_index = 0;
			?>
			<div class="owl-carousel owl-dot-absolute-bottom owl-nav-center-inner owl-dark" data-plugin-options='{"items": 1, "loop" : false, "dots" : false, "nav" : false, "autoplay": true, "autoplayHoverPause": true}'>
				<?php foreach ($g5plus_gallery as $image_id): ?>
					<?php
						$image_src = matthewruddy_image_resize_id($image_id,$width,$height);
						$image_full_src = $image_src;
						$image_full_arr = wp_get_attachment_image_src($image_id,'full');
						if ($image_full_arr) {
							$image_full_src = $image_full_arr[0];
						}
						$image_thumb_arr = wp_get_attachment_image_src($image_id);
						$image_thumb_src = '';
						if (sizeof($image_thumb_arr) > 0) {
							$image_thumb_src = $image_thumb_arr[0];
						}
						if ($gallery_index == 0) {
							$preview_image_full_src = $image_full_src;
							$preview_image_thumb_src = $image_thumb_src;
						}
						$image_title 	= esc_attr( get_the_title( $image_id ) );
						$gallery_index++;
					?>
					<div>
					<img alt="<?php echo esc_attr($image_title);?>" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height); ?>" src="<?php echo esc_url($image_src); ?>" />
					<?php if ($gallery_index > 0): ?>
						<a title="<?php echo esc_attr($image_title);?>" data-thumb-src="<?php echo esc_url($image_thumb_src); ?>" data-gallery-id="<?php echo esc_attr($galleryId); ?>"
						   data-rel="lightGallery" href="<?php echo esc_url($image_full_src); ?>" ></a>
					<?php endif; ?>
					</div>
				<?php endforeach;; ?>
			</div>

		<?php endif; ?>
	<?php elseif (has_post_format('video')) : ?>
		<?php $g5plus_video = get_post_meta(get_the_ID(), G5Plus_Post_Formats_UI::$format_video_embed, true); ?>
		<?php if (!empty($g5plus_video)): ?>
			<?php $hasThumb = true; ?>
			<?php if (wp_oembed_get($g5plus_video)) : ?>
				<?php $preview_embed_src = $g5plus_video; ?>
				<?php if (has_post_thumbnail()) : ?>
					<?php $image_src = matthewruddy_image_resize_id(get_post_thumbnail_id(),$width,$height); ?>
					<img width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height); ?>" src="<?php echo esc_url($image_src); ?>"
					     alt="<?php the_title(); ?>" class="img-responsive">
				<?php else: ?>
					<?php $show_content = false; ?>
					<div class="embed-responsive embed-responsive-16by9 embed-responsive-<?php echo esc_attr($size); ?>">
						<?php echo wp_oembed_get($g5plus_video, array('wmode' => 'transparent')); ?>
					</div>
				<?php endif; ?>
			<?php else : ?>
				<div class="embed-responsive embed-responsive-16by9 embed-responsive-<?php echo esc_attr($size); ?>">
					<?php echo wp_kses_post($g5plus_video);
					$show_content = false;?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php elseif (has_post_format('audio')): ?>
		<?php $g5plus_audio = get_post_meta(get_the_ID(), G5Plus_Post_Formats_UI::$format_audio_embed, true); ?>
		<?php if (!empty($g5plus_audio)): ?>
			<?php $hasThumb = true;
			$show_content = false;	?>
			<div class="embed-responsive embed-responsive-16by9 embed-responsive-<?php echo esc_attr($size); ?>">
				<?php if (wp_oembed_get($g5plus_audio)) : ?>
					<?php echo wp_oembed_get($g5plus_audio); ?>
				<?php else : ?>
					<?php echo wp_kses_post($g5plus_audio); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php if (!$hasThumb): ?>
		<?php if (has_post_thumbnail()): ?>
			<?php
			$image_id =  get_post_thumbnail_id();
			$image_src = matthewruddy_image_resize_id($image_id,$width,$height);
			$image_full_src = $image_src;
			$image_full_arr = wp_get_attachment_image_src($image_id,'full');
			if ($image_full_arr) {
				$image_full_src = $image_full_arr[0];
			}
			$image_thumb_arr = wp_get_attachment_image_src($image_id);
			$image_thumb_src = '';
			if (sizeof($image_thumb_arr) > 0) {
				$image_thumb_src = $image_thumb_arr[0];
			}

			$preview_image_full_src = $image_full_src;
			$preview_image_thumb_src = $image_thumb_src;
			?>
			<img width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height); ?>" src="<?php echo esc_url($image_src); ?>"
			     alt="<?php the_title(); ?>" class="img-responsive">
		<?php else: ?>
			<div class="bg-black embed-responsive embed-responsive-<?php echo esc_attr($size); ?>"></div>
		<?php endif; ?>
	<?php endif; ?>
	<?php if ($show_content): ?>
		<div class="entry-content-wrap">
			<div class="block-center">
				<div class="block-center-inner">
					<h3 class="entry-post-title"><a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<div class="entry-meta-date">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"> <?php echo  get_the_date(get_option('date_format'));?> </a>
					</div>
					<div class="entry-actions">
						<?php if (!empty($preview_embed_src)) : ?>
						<a href="javascript:;" class="view-video zoomGallery" data-src="<?php echo esc_url($preview_embed_src); ?>"><i class="fa fa-search"></i></a>
						<?php elseif (!empty($preview_image_thumb_src) && !empty($preview_image_full_src)): ?>
							<a data-thumb-src="<?php echo esc_url($preview_image_thumb_src); ?>" data-gallery-id="<?php echo esc_attr($galleryId); ?>"
							   data-rel="lightGallery" href="<?php echo esc_url($preview_image_full_src); ?>"><i class="fa fa-search"></i></a>
						<?php endif; ?>
						<a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><i class="fa fa-share"></i></a>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	</div>
</article>
