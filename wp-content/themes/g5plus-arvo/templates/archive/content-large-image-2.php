<?php
/**
 * The template for displaying content large image
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
$size = 'large-image';
$excerpt =  get_the_excerpt();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-large-image-2 clearfix'); ?>>
	<div class="entry-content-top text-center">
		<?php if (has_category()): ?>
			<div class="entry-meta-category">
				<?php echo get_the_category_list(' / '); ?>
			</div>
		<?php endif; ?>
		<h3 class="entry-post-title"><a title="<?php the_title(); ?>"
		                                href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<div class="entry-meta-date">
			<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"> <?php echo  get_the_date(get_option('date_format'));?> </a>
		</div>
	</div>
	<?php g5plus_get_post_thumbnail($size); ?>
	<div class="entry-content-wrap">
		<?php if ($excerpt !== ''): ?>
			<div class="entry-excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php endif; ?>
		<div class="text-center">
			<a href="<?php the_permalink(); ?>" class="btn btn-outline btn-md btn-round"><?php esc_html_e('Read More', 'g5plus-arvo'); ?></a>
		</div>
	</div>
</article>
