<?php
/**
 * The template for displaying post meta
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 * @var $is_full
 */
?>
<div class="entry-post-meta">
	<div class="entry-meta-author">
		<i class="fa fa-user"></i> <?php printf('<a href="%1$s">%2$s</a>',esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),esc_html( get_the_author() )); ?>
	</div>
	<div class="entry-meta-date">
		<i class="fa fa-clock-o"></i> <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"> <?php echo  get_the_date(get_option('date_format'));?> </a>
	</div>

	<?php if (isset($is_full) && $is_full): ?>

		<?php if (has_category()): ?>
			<div class="entry-meta-category">
				<i class="fa fa-folder-open-o"></i> <?php echo get_the_category_list(', '); ?>
			</div>
		<?php endif; ?>

		<?php if ( comments_open() || get_comments_number() ) : ?>
			<div class="entry-meta-comment">
				<?php comments_popup_link(wp_kses_post(__('<i class="fa fa-comments"></i> 0','g5plus-arvo')) ,wp_kses_post(__('<i class="fa fa-comments"></i> 1','g5plus-arvo')),wp_kses_post(__('<i class="fa fa-comments"></i> %','g5plus-arvo'))); ?>
			</div>
		<?php endif; ?>

	<?php endif; ?>
	<?php edit_post_link(esc_html__( 'Edit', 'g5plus-arvo' ), '<div class="edit-link"><i class="fa fa-pencil"></i> ', '</div>' ); ?>
</div>
