<?php
/**
 * The template for displaying content large image
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
$size = 'large-image';
$excerpt =  get_the_excerpt();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post-large-image clearfix'); ?>>
	<?php g5plus_get_post_thumbnail($size); ?>
	<div class="entry-content-wrap">
		<h3 class="entry-post-title"><a title="<?php the_title(); ?>"
		                                href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php g5plus_get_template('archive/post-meta',array('is_full' => 1)); ?>
		<?php if ($excerpt !== ''): ?>
			<div class="entry-excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php endif; ?>
		<div class="entry-content-footer clearfix">
			<a href="<?php the_permalink(); ?>" class="btn btn-outline btn-gray btn-md btn-round"><?php esc_html_e('Read More', 'g5plus-arvo'); ?></a>
			<?php g5plus_the_social_share('circle'); ?>
		</div>
	</div>
</article>
