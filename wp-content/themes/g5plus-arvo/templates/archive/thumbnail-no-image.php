<?php
/**
 * The template for displaying post thumbnail no image
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
?>
<div class="entry-thumbnail <?php echo esc_attr($size); ?>">
    <div class="no-image block-center">
        <div class="block-center-inner"><?php esc_html_e('No Image', 'g5plus-arvo'); ?></div>
    </div>
</div>