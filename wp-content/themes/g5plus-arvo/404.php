<?php
get_header();
$background_image_404 = g5plus_get_option('background_image_404','');
$custom_style = '';
if (isset($background_image_404['url']) && $background_image_404['url']) {
    $custom_style = 'style="background-image: url(' . esc_url($background_image_404['url']) . ')"';
}
?>
    <div class="page404">
        <div class="bg-404" <?php echo wp_kses_post($custom_style); ?>></div>
        <div class="container">
            <div class="content-wrap">
                <h2 class="title"><?php esc_html_e('404','g5plus-arvo'); ?></h2>
				<h4 class="subtitle"><?php esc_html_e('PAGE NOT FOUND','g5plus-arvo'); ?></h4>
				<p class="description"><?php esc_html_e("It seems we couldn't find the page you are looking for. Please check to make sure you've typed the URL correctly.",'g5plus-arvo'); ?></p>
				<a class="btn btn-sm btn-round btn-outline" title="<?php esc_attr_e('BACK TO HOMEPAGE','g5plus-arvo'); ?>" href="<?php echo esc_url(home_url('/')) ?>"><?php esc_html_e('BACK TO HOMEPAGE','g5plus-arvo'); ?></a>
            </div>
        </div>
    </div>
<?php
get_footer();