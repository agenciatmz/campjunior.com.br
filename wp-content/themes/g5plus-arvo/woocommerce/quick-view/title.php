<?php
/**
 * The template for displaying Product Quick Views Title
 *
 * @package WordPress
 * @subpackage Arvo
 * @since arvo 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<h3 itemprop="name" class="product_title entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
