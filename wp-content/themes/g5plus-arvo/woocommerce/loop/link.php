<?php
/**
 * Template display product link
 *
 * @package WordPress
 * @subpackage Arvo
 * @since arvo 1.0
 */
?>
<a class="product-link" href="<?php the_permalink(); ?>"></a>