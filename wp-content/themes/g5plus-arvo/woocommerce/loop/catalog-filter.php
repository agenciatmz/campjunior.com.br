<?php
/**
 * The template for displaying Catalog Filter
 *
 * @package WordPress
 * @subpackage Arvo
 * @since arvo 1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if (!woocommerce_products_will_display()) return;
?>
<div class="catalog-filter clearfix">
	<?php
		woocommerce_result_count();
		woocommerce_catalog_ordering();
	?>
</div>
