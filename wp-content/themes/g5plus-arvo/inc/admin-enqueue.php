<?php
if (!function_exists('g5plus_admin_enqueue_scripts')) {

	function g5plus_admin_enqueue_scripts() {
		$min_suffix = !(defined('G5PLUS_SCRIPT_DEBUG') && G5PLUS_SCRIPT_DEBUG) ? '.min' : '';

		/**
		 * font-awesome
		 */
		$url_font_awesome = G5PLUS_THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome.min.css';
		$cdn_font_awesome = g5plus_get_option('cdn_font_awesome', '');
		if ($cdn_font_awesome) {
			$url_font_awesome = $cdn_font_awesome;
		}

		wp_enqueue_style('fontawesome', $url_font_awesome, array());
		wp_enqueue_style('fontawesome-animation', G5PLUS_THEME_URL . 'assets/plugins/fonts-awesome/css/font-awesome-animation.min.css', array());

		/**
		 * font theme icon
		 */
		wp_enqueue_style('arvo-icon', G5PLUS_THEME_URL . 'assets/plugins/arvo-icon/css/arvo-icon'.$min_suffix.'.css', array());

		wp_enqueue_style('pe-icon-7-stroke', G5PLUS_THEME_URL . 'assets/plugins/pe-icon-7-stroke/css/pe-icon-7-stroke'.$min_suffix.'.css', array());
		wp_enqueue_style('pe-icon-7-stroke-helper', G5PLUS_THEME_URL . 'assets/plugins/pe-icon-7-stroke/css/helper'.$min_suffix.'.css', array());
	}
	add_action( 'admin_enqueue_scripts', 'g5plus_admin_enqueue_scripts' );
}