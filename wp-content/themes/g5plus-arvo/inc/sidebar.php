<?php
/**
 * Register Sidebar
 * *******************************************************
 */
if (!function_exists('g5plus_register_sidebar')) {
    function g5plus_register_sidebar()
    {
        $sidebars = array(
            array(
                'id'          => 'main-sidebar',
                'name'        => esc_html__('Main Sidebar', 'g5plus-arvo'),
                'description' => esc_html__('Add widgets here to appear in your sidebar', 'g5plus-arvo'),
            ),
	        array(
		        'name'          => esc_html__("Top Drawer",'g5plus-arvo'),
		        'id'            => 'top_drawer_sidebar',
		        'description'   => esc_html__("Top Drawer",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Top Bar Left",'g5plus-arvo'),
		        'id'            => 'top_bar_left',
		        'description'   => esc_html__("Top Bar Left",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Top Bar Right",'g5plus-arvo'),
		        'id'            => 'top_bar_right',
		        'description'   => esc_html__("Top Bar Right",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Footer 1",'g5plus-arvo'),
		        'id'            => 'footer-1',
		        'description'   => esc_html__("Footer 1",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Footer 2",'g5plus-arvo'),
		        'id'            => 'footer-2',
		        'description'   => esc_html__("Footer 2",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Footer 3",'g5plus-arvo'),
		        'id'            => 'footer-3',
		        'description'   => esc_html__("Footer 3",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Footer 4",'g5plus-arvo'),
		        'id'            => 'footer-4',
		        'description'   => esc_html__("Footer 4",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Bottom Bar Left",'g5plus-arvo'),
		        'id'            => 'bottom_bar_left',
		        'description'   => esc_html__("Bottom Bar Left",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Bottom Bar Right",'g5plus-arvo'),
		        'id'            => 'bottom_bar_right',
		        'description'   => esc_html__("Bottom Bar Right",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Woocommerce",'g5plus-arvo'),
		        'id'            => 'woocommerce',
		        'description'   => esc_html__("Woocommerce",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Single Product",'g5plus-arvo'),
		        'id'            => 'single-product',
		        'description'   => esc_html__("Single Product",'g5plus-arvo'),
	        ),
	        array(
		        'name'          => esc_html__("Canvas Menu",'g5plus-arvo'),
		        'id'            => 'canvas-menu',
		        'description'   => esc_html__("Canvas Menu Widget Area",'g5plus-arvo'),
	        ),
        );
        foreach ($sidebars as $sidebar) {
            register_sidebar(array(
                'name'          => $sidebar['name'],
                'id'            => $sidebar['id'],
                'description'   => $sidebar['description'],
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h4 class="widget-title"><span>',
                'after_title'   => '</span></h4>',
            ));
        }
    }

    add_action('widgets_init', 'g5plus_register_sidebar');
}

/**
 * Add filter for dynamic sidebar
 * *******************************************************
 */
if (!function_exists('g5plus_redux_custom_widget_area_filter')) {
	function g5plus_redux_custom_widget_area_filter($arg) {
		return array(
			'before_title'  => '<h4 class="widget-title"><span>',
			'after_title'   => '</span></h4>',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>'
		);
	}
	add_filter('redux_custom_widget_args','g5plus_redux_custom_widget_area_filter');
}
