<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 6/17/2016
 * Time: 10:32 AM
 */

/**
 * Body Class
 * *******************************************************
 */
if (!function_exists('g5plus_body_class_name')) {
    function g5plus_body_class_name($classes)
    {
        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
        if ($is_lynx) $classes[] = 'lynx';
        elseif ($is_gecko) $classes[] = 'gecko';
        elseif ($is_opera) $classes[] = 'opera';
        elseif ($is_NS4) $classes[] = 'ns4';
        elseif ($is_safari) $classes[] = 'safari';
        elseif ($is_chrome) $classes[] = 'chrome';
        elseif ($is_IE) $classes[] = 'ie';
        else $classes[] = 'unknown';
        if ($is_iphone) $classes[] = 'iphone';

        $action = isset($_GET['action']) ? $_GET['action'] : '';
        $page_transition = g5plus_get_option('page_transition', '0');
        if (($page_transition === '1') && ($action != 'yith-woocompare-view-table')) {
            $classes[] = 'page-transitions';
        }

        if (is_singular()) {
            $page_class_extra =  g5plus_get_rwmb_meta('custom_page_css_class');
            if (!empty($page_class_extra)) {
                $classes[] = $page_class_extra;
            }
        }

        if ($action === 'yith-woocompare-view-table') {
            $classes[] = 'woocommerce-compare-page';
        }

        $loading_animation = g5plus_get_option('loading_animation', '');
        if (!empty($loading_animation) && ($loading_animation != 'none')) {
            $classes[] = 'page-loading';
        }

        $layout_style = g5plus_get_option('layout_style', 'wide');

        if ($layout_style === 'boxed') {
            $classes[] = 'boxed';
        }

        if (g5plus_get_option('header_float', '0')) {
            $classes[] = 'header-is-float';

	        if (g5plus_get_option('header_sticky_change_style', '1')) {
		        $classes[] = 'header-sticky-fix-style';
	        }
        }

	    $header_layout = g5plus_get_option('header_layout', 'header-1');
	    if ($header_layout == 'header-4') {
		    $classes[] = 'header-is-left';
	    }

        $enable_rtl_mode = $enable_rtl_mode = g5plus_get_option('enable_rtl_mode', '0');
        if ($enable_rtl_mode === '1' || isset($_GET['RTL']) || is_rtl()) {
            $classes[] = 'rtl';
        }


	    $page_layouts = &g5plus_get_page_layout_settings();
	    if ($page_layouts['has_sidebar']) {
		    $classes[] = 'has-sidebar';
	    }
        return $classes;
    }

    add_filter('body_class', 'g5plus_body_class_name');
}


/**
 * Filter Comment Fields
 */
if (!function_exists('g5plus_comment_form_fields')) {
    function g5plus_comment_form_fields() {
        $req      = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' );
        $commenter = wp_get_current_commenter();
        $html_req = ( $req ? " required='required'" : '' );
        $html5    = 'html5' === current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

        return  array(
            'author' => '<p class="comment-form-field comment-form-author">'.
                '<input placeholder="'. sprintf(esc_attr__('Your Name %s','g5plus-arvo'),$req ? '*' : '') .'" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . $html_req . ' /></p>',
            'email'  => '<p class="comment-form-field comment-form-email">' .
                '<input placeholder="'. sprintf(esc_attr__('Email Address %s','g5plus-arvo'),$req ? '*' : '') .'" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p>'
        );
    }
    add_filter('comment_form_default_fields','g5plus_comment_form_fields');
}

/**
 * Filter Comment Form Args Default
 */
if (!function_exists('g5plus_comment_form_defaults')) {
    function g5plus_comment_form_defaults($defaults) {
        $args = array(
            'comment_field'        => '<p class="comment-form-field comment-form-comment"><textarea placeholder="'. esc_attr__('Your Comment *','g5plus-arvo') .'" id="comment" name="comment" cols="45" rows="8"  aria-required="true" required="required"></textarea></p>',
            'title_reply_before' => '<h4 id="reply-title" class="block-title-classic mg-bottom-40">',
            'title_reply_after'  => '</h4>',
            'label_submit'         => esc_html__('Send Message','g5plus-arvo'),
            'class_submit' => 'btn btn-outline btn-gray btn-md btn-round'
        );
        $defaults = array_merge( $defaults, $args );
        return $defaults;
    }
    add_filter('comment_form_defaults','g5plus_comment_form_defaults');
}

/**
 * Filter Layout Wrap Class
 */
if (!function_exists('g5plus_layout_wrap_class')) {
    function g5plus_layout_wrap_class($layout_wrap_class){
        global $post;
        $post_type = get_post_type($post);
        $wrap_class = array();
        // custom layout wrap class page
        if (is_page()) {
            $wrap_class[] = 'page-wrap';
        }else{
            // custom layout wrap class blog
            if ((is_home() || is_category() || is_tag() || is_search() || is_archive()) && ($post_type == 'post')) {
                $wrap_class[] = 'archive-wrap';
            }


            // custom layout wrap class single blog
            if (is_singular('post')) {
                $wrap_class[] = 'single-blog-wrap';
            }

            // custom layout wrap class archive product
            if (is_post_type_archive( 'product' ) || is_tax('product_cat') || is_tax('product_tag') || (is_search() && ($post_type == 'product'))) {
                $wrap_class[] = 'archive-product-wrap';
            }

            // custom layout wrap class single product
            if (is_singular('product')) {
                $wrap_class[] = 'single-product-wrap';
            }
        }

        return array_merge($layout_wrap_class,$wrap_class);

    }
    add_filter('g5plus_filter_layout_wrap_class','g5plus_layout_wrap_class');
}

/**
 * Filter Layout Inner Class
 */
if (!function_exists('g5plus_layout_inner_class')){
    function g5plus_layout_inner_class($layout_inner_class){
        global $post;
        $post_type = get_post_type($post);
        $inner_class = array();

        // custom layout inner class page
        if (is_page()) {
            $inner_class[] = 'page-inner';
        }else{
            // custom layout inner class blog
            if ((is_home() || is_category() || is_tag() || is_search() || is_archive()) && ($post_type === 'post')) {
                $inner_class[] = 'archive-inner';
            }

            // custom layout inner class single blog
            if (is_singular('post')) {
                $inner_class[] = 'single-blog-inner';
            }

            // custom layout inner class archive product
            if (is_post_type_archive( 'product' ) || is_tax('product_cat') || is_tax('product_tag') || (is_search() && ($post_type === 'product'))) {
                $inner_class[] = 'archive-product-inner';
            }

            // custom layout inner class single product
            if (is_singular('product')) {
                $inner_class[] = 'single-product-inner';
            }
        }

        return array_merge($layout_inner_class,$inner_class);
    }
    add_filter('g5plus_filter_layout_inner_class','g5plus_layout_inner_class');
}

/**
 * Add search form before Mobile Menu
 * *******************************************************
 */
if (!function_exists('g5plus_search_form_before_menu_mobile')) {
	function g5plus_search_form_before_menu_mobile($params) {
		ob_start();
		if (g5plus_get_option('mobile_header_menu_drop', 'menu-drop-fly') === 'menu-drop-fly') {
			get_search_form();
		}
		$params .= ob_get_clean();
		return $params;
	}
	add_filter('g5plus_before_menu_mobile_filter','g5plus_search_form_before_menu_mobile', 10);
}