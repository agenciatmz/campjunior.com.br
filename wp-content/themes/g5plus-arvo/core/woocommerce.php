<?php
/**
 * woocommerce class
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */
if ( class_exists( 'WooCommerce' ) && !class_exists('G5Plus_Woocommerce')){
	class G5Plus_Woocommerce{
		function __construct() {
			$this->define_filter();
			$this->remove_hook();
			$this->define_hook();
		}

		function define_filter() {
			// custom page title
			add_filter('g5plus_page_title',array($this,'page_title'));

			// custom page subtitle
			add_filter('g5plus_sub_page_title',array($this,'page_sub_title'));

			// remove shop page title
			add_filter('woocommerce_show_page_title','__return_false');

			// shop per page
			add_filter('loop_shop_per_page', array($this,'loop_shop_per_page'));

			// filter pagination
			add_filter('woocommerce_pagination_args',array($this,'pagination_args'));

			add_filter('woocommerce_review_gravatar_size', array($this,'review_gravatar_size'));

			// related columns and total
			add_filter('woocommerce_output_related_products_args',array($this,'output_related_products_args'));

			// up-sell columns and total
			add_filter('woocommerce_upsell_display_args',array($this,'upsell_display_args'));

			add_filter('woocommerce_cross_sells_total',array($this,'cross_sells_total'));

		}

		function remove_hook() {
			// remove woocommerce sidebar
			remove_action('woocommerce_sidebar','woocommerce_get_sidebar',10);

			// remove Breadcrumb
			remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);

			// remove archive description
			remove_action('woocommerce_archive_description','woocommerce_taxonomy_archive_description',10);
			remove_action('woocommerce_archive_description','woocommerce_product_archive_description',10);

			// remove result count and catalog ordering
			remove_action('woocommerce_before_shop_loop','woocommerce_result_count',20);
			remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering',30);

			// remove product link close
			remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_product_link_close',5);
			remove_action('woocommerce_before_shop_loop_item','woocommerce_template_loop_product_link_open',10);

			// remove product thumb
			remove_action('woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10);

			// remove product title
			remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);

			// remove add to cart
			remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',10);

			// remove compare button
			global $yith_woocompare;
			if ( isset($yith_woocompare) && isset($yith_woocompare->obj)) {
				remove_action( 'woocommerce_after_shop_loop_item', array($yith_woocompare->obj,'add_compare_link'), 20 );
				remove_action( 'woocommerce_single_product_summary', array( $yith_woocompare->obj, 'add_compare_link' ), 35 );
			}

			// remove process checkout button in cart total
			remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );


		}

		function define_hook(){

			// add catalog filter
			add_action('woocommerce_before_shop_loop',array($this,'catalog_filter'),10);

			// settings archive product
			add_action('woocommerce_before_shop_loop',array($this,'archive_product_settings'),50);

			// product sale count down
			add_action('woocommerce_before_shop_loop_item_title',array($this,'g5plus_woocommerce_template_loop_sale_count_down'),10);
			add_action('g5plus_after_single_product_image_main',array($this,'g5plus_woocommerce_template_loop_sale_count_down'),10);

			// product images thumb
			add_action('woocommerce_before_shop_loop_item_title',array($this,'g5plus_woocommerce_template_loop_product_thumbnail'),20);

			// product link
			add_action('woocommerce_before_shop_loop_item_title',array($this,'g5plus_woocomerce_template_loop_link'),30);

			// product title
			add_action('woocommerce_shop_loop_item_title',array($this,'g5plus_woocommerce_template_loop_product_title'),10);

			// product actions
			add_action('g5plus_woocommerce_product_actions',array($this,'g5plus_woocomerce_template_loop_compare'),5);
			add_action('g5plus_woocommerce_product_actions',array($this,'g5plus_woocomerce_template_loop_wishlist'),10);
			add_action('g5plus_woocommerce_product_actions',array($this,'g5plus_woocomerce_template_loop_quick_view'),10);


			add_action('woocommerce_before_shop_loop_item_title',array($this,'g5plus_woocommerce_template_loop_add_to_cart'),40);

			// single product wishlist and compare
			add_action('woocommerce_single_product_summary',array($this,'g5plus_woocommerce_template_single_function'),35);

			// single product share
			add_action('woocommerce_share', array($this,'g5plus_woocommerce_share'),10);

			// check-out button in cart page
			add_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display',20 );

			// add shipping_calculator form
			add_action('woocommerce_before_cart_totals','woocommerce_shipping_calculator',5);

			// quick-views ajax
			add_action( 'wp_ajax_nopriv_product_quick_view', array($this,'popup_product_quick_view'));
			add_action( 'wp_ajax_product_quick_view', array($this,'popup_product_quick_view') );
			add_action( 'wp_footer', array( $this, 'quick_view' ) );

			// quick view
			add_action('woocommerce_before_quick_view_product_summary','woocommerce_show_product_sale_flash',10);
			add_action('woocommerce_before_quick_view_product_summary',array($this,'g5plus_show_product_quick_view_images'),20);

			add_action('woocommerce_quick_view_product_summary',array($this,'g5plus_template_quick_view_product_title'),5);
			add_action('woocommerce_quick_view_product_summary',array($this,'g5plus_template_quick_view_rating'),10);
			add_action('woocommerce_quick_view_product_summary','woocommerce_template_single_price',10);
			add_action('woocommerce_quick_view_product_summary','woocommerce_template_single_excerpt',20);
			add_action('woocommerce_quick_view_product_summary','woocommerce_template_single_add_to_cart',30);
			add_action('woocommerce_quick_view_product_summary',array($this,'g5plus_woocommerce_template_single_function'),35);
			add_action('woocommerce_quick_view_product_summary','woocommerce_template_single_meta',40);
			add_action('woocommerce_quick_view_product_summary','woocommerce_template_single_sharing',50);

			// woocompare style
			add_action('wp_print_footer_scripts',array($this,'woocompare_custom_style'));


		}


		function page_title($page_title){
			if (is_post_type_archive('product')) {
				$shop_page_id = wc_get_page_id( 'shop' );
				if ($shop_page_id) {
					if (!$page_title) {
						$page_title   = get_the_title( $shop_page_id );
					}
					$is_custom_page_title = g5plus_get_rwmb_meta('is_custom_page_title',array(),$shop_page_id);
					if ($is_custom_page_title) {
						$page_title = g5plus_get_rwmb_meta('custom_page_title',array(),$shop_page_id);
					}
				}
			}
			return $page_title;
		}

		function page_sub_title($page_sub_title){
			if ( is_post_type_archive( 'product' ) ) {
				$shop_page_id   = wc_get_page_id( 'shop' );
				if ( $shop_page_id ) {
					$is_custom_page_title = g5plus_get_rwmb_meta('is_custom_page_title',array(),$shop_page_id);
					if ($is_custom_page_title) {
						return g5plus_get_rwmb_meta('custom_page_sub_title',array(),$shop_page_id);
					}
				}
			}
			return $page_sub_title;
		}

		function catalog_filter() {
			wc_get_template( 'loop/catalog-filter.php' );
		}

		function loop_shop_per_page() {
			$product_per_page = g5plus_get_option('product_per_page',9);
			if (empty($product_per_page)) {
				$product_per_page = 9;
			}
			$page_size = isset($_GET['page_size']) ? wc_clean($_GET['page_size']) : $product_per_page;
			return $page_size;
		}

		function archive_product_settings(){
			$g5plus_woocommerce_loop = &G5Plus_Woocommerce::get_woocommerce_loop();

			// setting columns
			$g5plus_woocommerce_loop['columns'] = g5plus_get_option('product_display_columns','3');
			$product_display_columns = isset($_GET['columns']) ? $_GET['columns'] : '';
			if (in_array($product_display_columns, array('2','3','4','5'))) {
				$g5plus_woocommerce_loop['columns'] = $product_display_columns;
			}
		}

		function g5plus_woocommerce_template_loop_sale_count_down(){
			wc_get_template('loop/sale-count-down.php');
		}

		function g5plus_woocommerce_template_loop_product_thumbnail() {
			wc_get_template('loop/product-thumb.php');
		}

		function g5plus_woocomerce_template_loop_link() {
			wc_get_template( 'loop/link.php' );
		}

		function g5plus_woocommerce_template_loop_product_title() {
			wc_get_template( 'loop/title.php' );
		}

		function g5plus_woocomerce_template_loop_quick_view() {
			wc_get_template( 'loop/quick-view.php' );
		}

		function g5plus_woocommerce_template_loop_add_to_cart(){
			$product_add_to_cart_enable = g5plus_get_option('product_add_to_cart_enable');
			if ($product_add_to_cart_enable) {
				woocommerce_template_loop_add_to_cart();
			}
		}

		function g5plus_woocomerce_template_loop_compare() {
			if (in_array('yith-woocommerce-compare/init.php', apply_filters('active_plugins', get_option('active_plugins'))) && get_option('yith_woocompare_compare_button_in_products_list') == 'yes') {
				echo do_shortcode('[yith_compare_button container="false" type="link"]');
			}
		}

		function g5plus_woocomerce_template_loop_wishlist() {
			if (in_array('yith-woocommerce-wishlist/init.php', apply_filters('active_plugins', get_option('active_plugins'))) && (get_option( 'yith_wcwl_enabled' ) == 'yes')) {
				echo do_shortcode('[yith_wcwl_add_to_wishlist]');
			}
		}

		function pagination_args($woocommerce_pagination_args) {
			$args = array(
				'type' => '',
				'prev_text' => wp_kses_post(__('<i class="fa fa-caret-left"></i> Prev','g5plus-arvo')) ,
				'next_text' => wp_kses_post(__('Next <i class="fa fa-caret-right"></i>','g5plus-arvo')),
			);
			$woocommerce_pagination_args = array_merge($woocommerce_pagination_args,$args);
			return $woocommerce_pagination_args;
		}

		function g5plus_woocommerce_template_single_function(){
			wc_get_template( 'single-product/product-function.php' );
		}

		function output_related_products_args($args){
			$default = array(
				'posts_per_page' 	=> g5plus_get_option('related_product_count',6),
				'columns' 			=> g5plus_get_option('related_product_display_columns',3)
			);
			$args = array_merge($args,$default);
			return $args;
		}

		function upsell_display_args($args){
			$default = array(
				'columns' 			=> g5plus_get_option('up_sells_product_display_columns',4)
			);
			$args = array_merge($args,$default);
			return $args;
		}

		function review_gravatar_size(){
			return 100;
		}

		function popup_product_quick_view(){
			$product_id = $_REQUEST['id'];
			global $post, $product;
			$post = get_post($product_id);
			setup_postdata($post);
			$product = wc_get_product( $product_id );
			wc_get_template_part('content-product-quick-view');
			wp_reset_postdata();
			die();

		}

		function g5plus_show_product_quick_view_images(){
			wc_get_template('quick-view/product-image.php');
		}

		function g5plus_template_quick_view_product_title(){
			wc_get_template('quick-view/title.php');
		}

		function g5plus_template_quick_view_rating(){
			wc_get_template('quick-view/rating.php');
		}

		function quick_view(){
			$product_quick_view = g5plus_get_option('product_quick_view_enable',1);
			if ($product_quick_view) {
				wp_enqueue_script( 'wc-add-to-cart-variation' );
			}
		}

		function woocompare_custom_style() {
			$action = isset($_GET['action']) ? $_GET['action'] : '';
			if ($action == 'yith-woocompare-view-table') {
				$custom_css = '
			.woocommerce-compare-page h1 {
			font-size: 24px;
			background-color: #f4f4f4;
			color: #444;
			}';
				echo '<style id="g5plus_woocompare_custom_style" type="text/css"></style>';
				echo sprintf('<style type="text/css">%s %s</style>',"\n",$custom_css);
			}
		}

		function g5plus_woocommerce_share(){
			g5plus_the_social_share('square');
		}

		// GET Woocommerce loop
		public static function &get_woocommerce_loop() {
			if (isset($GLOBALS['g5plus_woocommerce_loop']) && is_array($GLOBALS['g5plus_woocommerce_loop'])) {
				return $GLOBALS['g5plus_woocommerce_loop'];
			}
			$GLOBALS['g5plus_woocommerce_loop'] = array(
				'layout' => '',
				'columns' => '',
				'columns_md' => '',
				'columns_sm' => '',
				'columns_xs' => '',
				'columns_mb' => '',
				'rows' => '',
				'dots' => 'false',
				'dot_style' => '',
				'nav' => 'true',
				'nav_position' => 'top',
				'nav_style' => '',
				'owl_scheme' => ''
			);
			return $GLOBALS['g5plus_woocommerce_loop'];
		}

		public static function reset_loop() {
			unset($GLOBALS['g5plus_woocommerce_loop']);
		}

		function cross_sells_total($limit){
			$limit = g5plus_get_option('cross_sells_product_count','6');
			return $limit;
		}

	}
	new G5Plus_Woocommerce();
}