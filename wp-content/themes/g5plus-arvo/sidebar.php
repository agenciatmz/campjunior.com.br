<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Arvo
 * @since Arvo 1.0
 */

$page_layouts = &g5plus_get_page_layout_settings();
if (!$page_layouts['has_sidebar']) return;
global $wp_registered_sidebars;
$sidebar_class = array('primary-sidebar', 'sidebar');
if ($page_layouts['sidebar_layout'] != 'none') {
	$sidebar_col = 3;
	if ($page_layouts['sidebar_width'] == 'large') {
		$sidebar_col = 4;
	}
	$sidebar_class[] = 'col-md-'.$sidebar_col;
	if ($page_layouts['sidebar_layout'] == 'left') {
		$sidebar_class[] = 'col-md-pull-'.(12-$sidebar_col);
	}

	if ($page_layouts['sidebar_mobile_enable'] == 0) {
		$sidebar_class[] = 'hidden-sm hidden-xs';
	} else if ($page_layouts['sidebar_mobile_canvas']) {
		$sidebar_class[] = 'sidebar-mobile-canvas';
	}
}
?>
<?php if ($page_layouts['sidebar_mobile_enable'] && $page_layouts['sidebar_mobile_canvas']) : ?>
	<div class="sidebar-mobile-canvas-icon" title="<?php esc_attr_e('Click to show Canvas Sidebar', 'g5plus-arvo' ) ?>">
		<i class="fa fa-sliders"></i>
	</div>
<?php endif; ?>
<div class="<?php echo esc_attr(join(' ', $sidebar_class)); ?>">
	<?php if (is_active_sidebar($page_layouts['sidebar'])): ?>
		<?php dynamic_sidebar($page_layouts['sidebar']); ?>
	<?php else: ?>
		<div class="no-widget-content"> <?php printf(wp_kses_post(__('Please insert widget into sidebar <b>%s</b> in Appearance > <a class="text-color-accent" title="manage widgets" href="%s">Widgets</a> ','g5plus-arvo')),$wp_registered_sidebars[$page_layouts['sidebar']]['name'],admin_url( 'widgets.php' )); ?></div>
	<?php endif; ?>

</div>
